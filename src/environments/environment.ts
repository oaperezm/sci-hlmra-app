// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  nameApp: 'Recursos Académicos - LOCAL',
  apiUrl: 'http://localhost:60817',
  firebase:  {
    apiKey: 'AIzaSyC6pRUOdHuELne0QrpqIDqetPeLobTc4OM',
    authDomain: 'sali-2.firebaseapp.com',
    databaseURL: 'https://sali-2.firebaseio.com',
    projectId: 'sali-2',
    storageBucket: 'sali-2.appspot.com',
    messagingSenderId: '753653057085'
  }
  // firebase:  {
  //   apiKey: "AIzaSyC6pRUOdHuELne0QrpqIDqetPeLobTc4OM",
  //   authDomain: "sali-2.firebaseapp.com",
  //   databaseURL: "https://sali-2.firebaseio.com",
  //   projectId: "sali-2",
  //   storageBucket: "sali-2.appspot.com",
  //   messagingSenderId: "753653057085",
  //   appId: "1:753653057085:web:047ac4c176966d71"
  // }
};
