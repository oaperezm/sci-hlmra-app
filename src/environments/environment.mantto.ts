/**
 * Configuración para el pre-productivo de Recursos Academicos
 */
export const environment = {
  production: true,
  nameApp: 'Recursos Académicos - Mantenimiento',
  // URL produccion
  // apiUrl: 'https://ra-prod.azurewebsites.net',

  // URL desarrollo
  apiUrl: 'https://ra-prod2.azurewebsites.net',


  // URL pruebas
  // apiUrl: 'https://ra-back-pre-prod.azurewebsites.net',
  firebase:  {
    apiKey: 'AIzaSyC6pRUOdHuELne0QrpqIDqetPeLobTc4OM',
    authDomain: 'sali-2.firebaseapp.com',
    databaseURL: 'https://sali-2.firebaseio.com',
    projectId: 'sali-2',
    storageBucket: 'sali-2.appspot.com',
    messagingSenderId: '753653057085'
  }
};
//https://ra-prod2.azurewebsites.net
//http://localhost:60817/