$(function() {

  // ==============================================================
  // This is for the top header part and sidebar part
  // ==============================================================
  $(document).ready(setHeight);
  $(window).resize(setHeight);

    window.dataLayer = window.dataLayer || [];
    // window.dataLayer.push({
    //   'userId' : '1234567',
    //   'cctv' : 'masclicks574'
    // })
  // Botón Toggle
  $(document).on('click','.comment-avatar', function( ) {
    $('.usr-nav').slideToggle();
  });

});


function addDataLayer(id, ccts){

    window.dataLayer.push({
      'userId' : id,
      'cctv' : ccts
    });
}

function setHeight() {
  var vh = $(window).height();
  var ch = vh - 100;
  //$("#nav-structure, #main-container, .col-lef").attr("min-height", ch +"px");
  $("#nav-structure, #main-container, .col-lef").attr('style','min-height:' + ch+ 'px;');
}

// Eliminar el background
function removeBackground() {
  var body = document.getElementsByTagName('body')[0];
  body.classList.remove('bg-login');
  body.classList.remove('bg-landing');
}

// Asignar el tamaño d ela pantalla como el máximo del selector
function setFullHeightByElement( selector ) {
  var vh = $(window).height();
  var vw= $(window).width();
  var ch = vh - 135;

  if(vw > 900) {
    $(selector).attr('style','max-height:' + ch+ 'px; min-height:' + ch+ 'px;');
  }
}

// Asignar el tamaño d ela pantalla como el máximo del selector
function setFullHeightWithOffset( selector, offset ) {
  var vh = $(window).height();
  var vw= $(window).width();
  var ch = vh - 135 - offset;

  if(vw > 900) {
    $(selector).attr('style','max-height:' + ch+ 'px; min-height:' + ch+ 'px;');
  }
}

// Asignar al selector la medida mínima de la pantalla
function setMinHeightByElement( selector ) {
  var vh = $(window).height();
  var vw= $(window).width();
  var ch = vh - 100;

  if(vw > 900) {
    $(selector).attr('style','min-height:' + ch+ 'px;');
  }
}

function scrollingToElement( contentId ) {

  if( $(contentId) ){
    $('html, body').animate({
      scrollTop: $(contentId).offset().top
    }, 800, function(){
      // Add hash (#) to URL when done scrolling (default click behavior)
      // window.location.hash = hash;
    });
  }
}

$(document).ready( function(){
  // Scroll Down Effect
  jQuery(window).scroll( function() {
    var scroll = jQuery(window).scrollTop();

    if (scroll >= 10) {
      jQuery('.site-header').addClass('header-solid');
    } else {
      jQuery('.site-header').removeClass('header-solid');
    }
  });
});
