import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-dialog-response-question',
  templateUrl: './dialog-response-question.component.html',
  styleUrls: ['./dialog-response-question.component.scss']
})
export class DialogResponseQuestionComponent implements OnInit {

  question: any;
  constructor(  public dialogRef: MatDialogRef<DialogResponseQuestionComponent>,
                @Inject(MAT_DIALOG_DATA) public data) {
                  this.question = data;
                 }

  ngOnInit() {
  }

  onCloseConfirm() {
    this.dialogRef.close();
  }
}
