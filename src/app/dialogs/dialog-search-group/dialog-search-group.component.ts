import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';


import { MatDialogRef } from '@angular/material';
import { MAT_DIALOG_DATA } from '@angular/material';

// Servicios
import { CityService, StudenService } from '../../service/service.index';
// Modelos
import { StudentGroup } from '../../models/courses/studentGroup.model';
import { Group } from 'src/app/models/courses/group.model';
import swal from 'sweetalert2';

import { Appsettings } from '../../configuration/appsettings';


@Component({
  selector: 'app-dialog-search-group',
  templateUrl: './dialog-search-group.component.html',
  styleUrls: ['./dialog-search-group.component.scss']
})
export class DialogSearchGroupComponent implements OnInit {
  showContacts: boolean;
  groupsRes: StudentGroup[] = [];
  groups: StudentGroup[] = [];
  textSearch: any;
  pass: string;
  frmInvitation: FormGroup;
  showSend: boolean;
  ids: any[] = [];
  working: boolean;
  constructor(private dialogRef: MatDialogRef<DialogSearchGroupComponent>,
    @Inject(MAT_DIALOG_DATA) public data,
      public _srvCity: CityService,
      public _srvStudent: StudenService) {

    this.showContacts = false;
    this.showSend = false;
    this.frmInvitation = new FormGroup({
      pass: new FormControl(''),

    });
   }

  ngOnInit() {
    this.working = true;
    this._srvCity.getAllGroups().subscribe( res => {

      if ( res.success) {
        this.groups = [];
        this.groupsRes = [];
        for (let g of res.data){
          let group = new StudentGroup();
          group.id = g.id;
          group.description = g.description;
          group.courseId = g.courseId;
          group.code = g.code;
          group.pass = '';
          this.groupsRes.push(group);
        }
      }
      this.working = false;
    });
  }

   // Cierre del modal
   onCloseConfirm( event: boolean) {
    this.dialogRef.close( event );
  }

  onKeyUpPass( event ) {
    this.showSend = ( this.pass.length > 0 ) ? true : false;
  }
  onKeyUp( event ) {

    let contacts;
    if ( this.textSearch.length > 0 ) {
      let number = parseInt(this.textSearch);
      contacts = this.groupsRes.filter( u =>  u.id === number);
    this.groups = contacts;
    this.showContacts = (this.groups.length > 0) ? true : false;
    } else {
     this.groups = [];
     this.showContacts =  false;
    }
  }

  onSendMessage(item) {
    this.working = true;
    if (item.pass == item.code){
       this._srvStudent.addInvitation(this.ids , item.id).subscribe(res => {
          if( res.success){
            this.confirmInvitation(res.data);
          }
          else {
            this.working = false;
            swal(Appsettings.APP_NAME, res.message, 'error');
          }
        });
    } else {
      this.working = false;
      swal(Appsettings.APP_NAME, 'La contraseña ingresada no pertenece al grupo seleccionado', 'error');
    }


  }

  confirmInvitation( code){

    this._srvStudent.confirmInvitation(code).subscribe(res => {
      if ( res.success) {
        this.working = false;
        swal(Appsettings.APP_NAME, 'Te has registrado correctamente.', 'success');
        this.onCloseConfirm(true);
      }else {
        swal(Appsettings.APP_NAME, 'Error al registrarte intenta de nuevo.', 'error');
      }

      this.working = false;
    });
  }

}
