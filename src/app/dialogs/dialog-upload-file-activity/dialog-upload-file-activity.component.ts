import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-dialog-upload-file-activity',
  templateUrl: './dialog-upload-file-activity.component.html',
  styleUrls: ['./dialog-upload-file-activity.component.scss']
})
export class DialogUploadFileActivityComponent implements OnInit {

  activityId: number;
  delivered: boolean;
  qualified: boolean;

  constructor(private dialogRef: MatDialogRef<DialogUploadFileActivityComponent>,
    @Inject(MAT_DIALOG_DATA) public data) {
      this.activityId = data.taskId;
      this.delivered = data.delivered;
      this.qualified = data.qualified;
     }

  ngOnInit() {
  }

  onCloseConfirm( event ) {
    this.dialogRef.close( event );
  }
}
