import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { MAT_DIALOG_DATA } from '@angular/material';

// SERVICIOS
import { TeacherExamService } from '../../service/service.index';

@Component({
  selector: 'app-dialog-preview-exam',
  templateUrl: './dialog-preview-exam.component.html',
  styleUrls: ['./dialog-preview-exam.component.scss']
})
export class DialogPreviewExamComponent implements OnInit {

  working: boolean;
  examName: string;
  questions: any[] = [];
  showIsCorrect: boolean;
  constructor(  private _srvTeacher: TeacherExamService,
                public dialogRef: MatDialogRef<DialogPreviewExamComponent>,
                @Inject(MAT_DIALOG_DATA) public data) {

    this.working = true;
    this.showIsCorrect = false;
    this.examName = this.data.description;
    this.getAllQuestions();
  }

  ngOnInit() {
  }

  getAllQuestions() {
    this._srvTeacher.getById( this.data.id).subscribe( result => {
      if ( result.success ) {
        const examQuestions = result.data.teacherExamQuestion;
        this.questions = [];
        for ( const q of examQuestions) {
          // tslint:disable-next-line:max-line-length
          this.questions.push( {
            'answers': q.question.answers,
            'content': q.question.content,
            'id': q.question.id,
            'questionTypeId': q.question.questionTypeId,
            'explanation': q.question.explanation,
            'urlImage': q.question.urlImage,
            'imageBase64': q.question.imageBase64,
            'imageHeight': q.question.imageHeight,
            'imageWidth': q.question.imageWidth } );
        }
      }
      this.showIsCorrect = false;
      this.working = false;
    });
  }
  onCloseConfirm() {
    this.dialogRef.close( null );
  }

  showCorrect( event ){
    this.showIsCorrect = event.checked;
  }

}
