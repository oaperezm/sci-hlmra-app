import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-dialog-studen-homework',
  templateUrl: './dialog-studen-homework.component.html',
  styleUrls: ['./dialog-studen-homework.component.scss']
})
export class DialogStudenHomeworkComponent implements OnInit {

  studensHomework: any[] = [];
  homeworkName: string;
  homeworkDescription: string;

  // options2 = [
  //   {"id": 1, "name": "a"},
  //   {"id": 2, "name": "b"},
  //   {"id": 3, "name": "c"},
  //   {"id": 4, "name": "d"}
  // ]

  // selected2 = this.options2[2].id;

  constructor(  private dialogRef: MatDialogRef<DialogStudenHomeworkComponent>,
                @Inject(MAT_DIALOG_DATA) public data) {

    this.studensHomework = data.files;
    this.homeworkName = data.homeworkName;
    this.homeworkDescription = data.homeworkDescription;
  }

  ngOnInit() {
  }

  onCloseConfirm() {
    this.dialogRef.close( true );
  }
}
