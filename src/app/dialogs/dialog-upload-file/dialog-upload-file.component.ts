import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-dialog-upload-file',
  templateUrl: './dialog-upload-file.component.html',
  styleUrls: ['./dialog-upload-file.component.scss']
})
export class DialogUploadFileComponent implements OnInit {

  taskId: number;
  qualified: boolean;
  delivered: boolean;
  constructor(  private dialogRef: MatDialogRef<DialogUploadFileComponent>,
                @Inject(MAT_DIALOG_DATA) public data) {
    this.taskId = data.taskId;
    this.delivered = data.delivered;
    this.qualified = data.qualified;
                 }

  ngOnInit() {
  }

  onCloseConfirm( event ) {
    this.dialogRef.close( event );
  }
}
