import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-dialog-student-activity',
  templateUrl: './dialog-student-activity.component.html',
  styleUrls: ['./dialog-student-activity.component.scss']
})
export class DialogStudentActivityComponent implements OnInit {

  studensHomework: any[] = [];
  homeworkName: string;
  homeworkDescription: string;

  constructor(  private dialogRef: MatDialogRef<DialogStudentActivityComponent>,
                @Inject(MAT_DIALOG_DATA) public data) {
    this.studensHomework = data.files;
    this.homeworkName = data.homeworkName;
    this.homeworkDescription = data.homeworkDescription;
  }

  ngOnInit() {
  }

  onCloseConfirm() {
    this.dialogRef.close( true );
  }

}
