import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { SchedulingPlartial } from '../../models/courses/schedulingPartial.model';
import { FinalScoreService } from '../../service/publishing/final-score.service';

import swal from 'sweetalert2';
import { Appsettings } from '../../configuration/appsettings';

@Component({
  selector: 'app-dialog-final-score',
  templateUrl: './dialog-final-score.component.html',
  styleUrls: ['./dialog-final-score.component.scss']
})
export class DialogFinalScoreComponent implements OnInit {

  //Form
  frmFinalScore: FormGroup;
  partials: any[] = [];
  studentInfo: any;
  showForm: boolean;
  working: boolean = false;
  partialDialog: any [] = [];
  studentScoreStatus: boolean;
  statusScoreNumber: number;
  ynDisableSubmit: boolean;

  constructor(private dialogRef: MatDialogRef<DialogFinalScoreComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private _srvFinalScore: FinalScoreService) {
    this.studentInfo = data;
    this.partials = data.data.partials;

    let partials = data.data.partials.filter(item => item.description === data.description)[0];

    this.frmFinalScore = new FormGroup({
      'id': new FormControl(partials.id),
      'studentName': new FormControl({ value: data.studentName, disabled: true }),
      'schedulingPartial': new FormControl(partials.description),
      'schedulingPartialId': new FormControl(partials.schedulingPartialId),
      'points': new FormControl(data.data.points),
      'userId': new FormControl(data.data.userId),
      'studentId': new FormControl(data.data.studentId),
      'studentGroupId': new FormControl(partials.studentGroupId),
      'comment': new FormControl(partials.comment, [Validators.required]),
      'status': new FormControl(partials.status),
      'exam': new FormControl({ value: '', disabled: true }),
      'homework': new FormControl({ value: '', disabled: true }),
      'activity': new FormControl({ value: '', disabled: true }),
      'assistance': new FormControl({ value: '', disabled: true }),
      'finalScore': new FormControl({ value: '', disabled: true })
    });
    this.GetPartialDescription(this.studentInfo.description);
    this.showForm = false;

  }
  ngOnInit() {
  }

  // Registrar una nueva video conferencia
  onSubmit() {
    const StudentScore = new SchedulingPlartial();
    StudentScore.id = this.frmFinalScore.controls['id'].value;
    StudentScore.schedulingPartialId = this.frmFinalScore.controls['schedulingPartialId'].value;
    StudentScore.points = this.frmFinalScore.controls['points'].value;
    StudentScore.comment = this.frmFinalScore.controls['comment'].value;
    StudentScore.studentId = this.frmFinalScore.controls['studentId'].value;
    StudentScore.userId = this.frmFinalScore.controls['userId'].value;
    StudentScore.studentGroupId = this.frmFinalScore.controls['studentGroupId'].value;
    StudentScore.status = this.statusScoreNumber;
    StudentScore.exam = this.frmFinalScore.controls['exam'].value;
    StudentScore.homework = this.frmFinalScore.controls['homework'].value;
    StudentScore.activity = this.frmFinalScore.controls['activity'].value;
    StudentScore.assistance = this.frmFinalScore.controls['assistance'].value;
    StudentScore.score = this.frmFinalScore.controls['finalScore'].value;

    const ConstStudentScore = {
      id: StudentScore.id,
      SchedulingPartialId: StudentScore.schedulingPartialId,
      Points: StudentScore.points,
      Comment: StudentScore.comment,
      StudentGroupId: StudentScore.studentGroupId,
      StudentId: StudentScore.studentId,
      UserId: StudentScore.userId,
      status: StudentScore.status,
      Exam : StudentScore.exam,
      Homework: StudentScore.homework,
      Activity: StudentScore.activity,
      Assistance: StudentScore.assistance,
      Score: StudentScore.score
    };


    if (ConstStudentScore.id > 0) {
      this.updateData(ConstStudentScore);
    } else {
      this.saveData(ConstStudentScore);
    }

  }

  updateData(ConstStudentScore) {
    this.working = true;
    this._srvFinalScore.update(ConstStudentScore).subscribe(res => {
      if (res.success) {
        this.onCloseConfirm(true);
        this.resetForm();
        swal(Appsettings.APP_NAME, res.message, 'success');

      } else {
        swal(Appsettings.APP_NAME, res.message, 'error');
      }
      this.working = false;
    }, err => {
      this.working = false;
    });

  }
  saveData(ConstStudentScore) {
    this._srvFinalScore.save(ConstStudentScore).subscribe(res => {
      if (res.success) {
        this.onCloseConfirm(true);
        this.resetForm();
        swal(Appsettings.APP_NAME, res.message, 'success');
      } else {
        swal(Appsettings.APP_NAME, res.message, 'error');
      }
      this.working = false;
    }, err => {
      this.working = false;
    });

  }


  // Inicializar los datos del formulario
  resetForm() {
    this.showForm = false;
    this.frmFinalScore.reset();
  }

  onChangePartials(description) {

    if (description === '') {
      return;
    }

    let partials = this.partials.filter(item => item.description === description)[0];

    this.frmFinalScore.controls['comment'].setValue(partials.comment);
    this.frmFinalScore.controls['finalScore'].setValue(partials.score);
    this.frmFinalScore.controls['exam'].setValue(partials.exam);
    this.frmFinalScore.controls['homework'].setValue(partials.homework);
    this.frmFinalScore.controls['activity'].setValue(partials.activity);
    this.frmFinalScore.controls['assistance'].setValue(partials.assistance);

    if (partials.status === 0) {
      this.frmFinalScore.controls['points'].enable();
      this.frmFinalScore.controls['comment'].enable();
      this.frmFinalScore.controls['status'].enable();
      this.studentScoreStatus = false;
      this.ynDisableSubmit = false;
    } else if (partials.status === 1) {
      this.frmFinalScore.controls['points'].disable();
      this.frmFinalScore.controls['comment'].disable();
      this.frmFinalScore.controls['status'].disable();
      this.studentScoreStatus = true;
      this.ynDisableSubmit = true;
    }

  }

  GetPartialDescription(description) {
    if (description === '') {
      return;
    }
    let partialsDescription = this.partials.filter(item => item.description === description)[0];

    if (partialsDescription.status === 0) {
      this.frmFinalScore.controls['points'].enable();
      this.frmFinalScore.controls['comment'].enable();
      this.frmFinalScore.controls['status'].enable();
      this.ynDisableSubmit = false;
      this.studentScoreStatus = false;
    } else if (partialsDescription.status === 1) {
      this.frmFinalScore.controls['points'].disable();
      this.frmFinalScore.controls['comment'].disable();
      this.frmFinalScore.controls['status'].disable();
      this.ynDisableSubmit = true;
      this.studentScoreStatus = true;
    }

    this.frmFinalScore.controls['studentName'].setValue(this.studentInfo.data.studentName);
    this.frmFinalScore.controls['comment'].setValue(partialsDescription.comment);
    this.frmFinalScore.controls['finalScore'].setValue(partialsDescription.score);
    this.frmFinalScore.controls['exam'].setValue(partialsDescription.exam);
    this.frmFinalScore.controls['homework'].setValue(partialsDescription.homework);
    this.frmFinalScore.controls['activity'].setValue(partialsDescription.activity);
    this.frmFinalScore.controls['assistance'].setValue(partialsDescription.assistance);
    this.frmFinalScore.controls['points'].setValue(partialsDescription.points);
    this.frmFinalScore.controls['schedulingPartial'].setValue(partialsDescription.description);
    this.frmFinalScore.controls['schedulingPartialId'].setValue(partialsDescription.schedulingPartialId);

  }


  onCloseConfirm(data) {
    this.dialogRef.close(data);
  }

  onChangeStudentScoreStatus(event) {

    if (this.studentScoreStatus === false) {
      swal({
        title: '¿Estás seguro de cerrar el parcial?',
        text: 'Ya no podrá actualizar la calificación y se cerrará el parcial',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: '¡Sí, cerrar parcial!',
        cancelButtonText: 'Cancelar'
      }).then((result) => {
        if (result.value) {
          this.working = true;
          this.studentScoreStatus = true;
          this.statusScoreNumber = 1;
        } else {
          this.working = true;
          this.studentScoreStatus = false;
          this.statusScoreNumber = 0;
        }
        this.working = false;
      });
    }
  }


}

