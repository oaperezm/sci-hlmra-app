import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material';
import { MAT_DIALOG_DATA } from '@angular/material';

import 'rxjs/add/operator/debounceTime';

// Servicios
import { GroupService, StudenService } from '../../service/service.index';
import { FinalScoreService } from '../../service/publishing/final-score.service';

// Modelos
import { Studen } from '../../models/courses/student.model';

import swal from 'sweetalert2';

import { Appsettings } from '../../configuration/appsettings';
@Component({
  selector: 'app-dialog-final-score-download',
  templateUrl: './dialog-final-score-download.component.html',
  styleUrls: ['./dialog-final-score-download.component.scss']
})
export class DialogDownloadFinalScoreComponent implements OnInit {

  frmdownload: FormGroup;
  working: boolean;
  workingProgess: boolean;
  students: Studen[] = [];
  stateCtrl = new FormControl();
  filteredStates: any[] = [];
  ids: any[] = [];
  studenGroupId: number;
  courseId: number;
  studentsSelect: Studen[] = [];
  textSearch: string;
  user: FormGroup;

  turn:String;
  schoolCycle: string;

  constructor(  public dialogRef: MatDialogRef<DialogDownloadFinalScoreComponent>,
      private _srvGroup: GroupService,
      private _srvStuden: StudenService,
      private _srvFinalScore: FinalScoreService,
      @Inject(MAT_DIALOG_DATA) public data) {
        this.studenGroupId = data.studenGroupId;
        const roleId = Appsettings.ROL_ID;
        this.working = false;
        this.stateCtrl.valueChanges.debounceTime(400).subscribe( item => {
        this.textSearch = item;

        if ( item.length > 0 ) {

        this.working = true;
        this.filteredStates = [];
        }
        });
        this.frmdownload = new FormGroup({
          'turn': new FormControl( '', [Validators.required]),
          'schoolCycle': new FormControl( '', [Validators.required]),
        });
      }

  ngOnInit() {
    this.frmdownload.controls['turn'].setValue(this.turn);

    this.frmdownload.controls['schoolCycle'].setValue(this.schoolCycle);
  }

  onSubmit() {
    this._srvFinalScore.download(this.studenGroupId,this.frmdownload.controls['turn'].value,this.frmdownload.controls['schoolCycle'].value).subscribe(blob => {
      if (blob.toString() =="OK")
      {
        swal('¡Éxito!', 'El archivo se creo correctamente', 'success' );
        this.onCloseConfirm();
      }
      else
      {
        swal('¡Error!', 'Algo salió mal en la generación del archivo', 'error' );
      }
 });
  }
  onCloseConfirm() {
    this.dialogRef.close( null );
  }
}
