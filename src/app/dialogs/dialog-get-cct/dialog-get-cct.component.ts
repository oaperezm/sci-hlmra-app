import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { MAT_DIALOG_DATA } from '@angular/material';

import { Appsettings } from '../../configuration/appsettings';

import { FormGroup, FormControl, Validators } from '@angular/forms';

import { UserService } from '../../service/service.index';


import swal from 'sweetalert2';

@Component({
  selector: 'app-dialog-get-cct',
  templateUrl: './dialog-get-cct.component.html',
  styleUrls: ['./dialog-get-cct.component.scss']
})
export class DialogGetCctComponent implements OnInit {
  working: boolean = false;
  frmCct: FormGroup;

  id: number;
  fullName: string;
  birthDay: Date;
  gender: string;
  contry: number;
  stateId: number;
  email: string;
  key: string;
  institution: string;


  constructor(public _srvUser: UserService,
    private dialogRef: MatDialogRef<DialogGetCctComponent>) {
    this.frmCct = new FormGroup({
      'cct' : new FormControl( '', [Validators.required])
    });
   }

  ngOnInit() {
    this._srvUser.getProfileData().subscribe( res => {
      if ( res.success ) {
        let data = res.data;
        this.id = data.id;
        this.fullName = data.fullName;
        this.birthDay = data.birthday;
        this.gender = data.gender;
        this.contry = data.state.city.id;
        this.stateId = data.state.id;
        this.email = data.email;
        this.institution = data.institucion;
      }
    });
  }

  onSaveCct() {



     const USER = {
        id: this.id,
        fullName: this.fullName,
        birthDay: this.birthDay,
        gender: this.gender,
        contry: this.contry,
        stateId: this.stateId,
        email: this.email,
        key: this.frmCct.controls['cct'].value,
        institution: (this.institution) ? this.institution : 'N/A',
      };

      this._srvUser.update(USER).subscribe(
        res => {
          if (res.success) {
            swal(Appsettings.APP_NAME, res.message, 'success');
          } else {
            swal(Appsettings.APP_NAME, res.message, 'error');
          }
          this.working = false;
        },
        err => {
          swal('Problemas al realizar el registro', 'SAIL', 'error');
          this.working = false;
        }
      );

    this.onCloseConfirm( true );
  }



  onCloseConfirm( event ) {
    this.dialogRef.close( event );
  }
}
