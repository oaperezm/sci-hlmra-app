import { Component, OnInit, Inject, ɵConsole } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { UserUtilities } from '../../shared/user.utilities';

// Servicios
import { PlanningService } from '../../service/service.index';

// Modeloa
import { SchedulingPlartial } from '../../models/courses/schedulingPartial.model';

import swal from 'sweetalert2';

import { Appsettings } from '../../configuration/appsettings';


@Component({
  selector: 'app-dialog-add-partial',
  templateUrl: './dialog-add-partial.component.html',
  styleUrls: ['./dialog-add-partial.component.scss']
})
export class DialogAddPartialComponent implements OnInit {

  courseId: number;
  groupId: number;
  planningId: number;

  frmPartial: FormGroup;

  working: boolean;

  courseDateInit: Date;
  courseDateEnd: Date;

  partialDateInit: Date;
  partialDateEnd: Date;

  valid: boolean;
  showDateEnd: boolean;

  schedulingPartials: SchedulingPlartial [] = [];

  newPartial: SchedulingPlartial;

  showMsgInit: boolean;
  showMsgEnd: boolean;

  countInit: number = 0;
  countEnd: number = 0;

  dateInit: Date;
  dateEnd: Date;
  constructor(  private dialogRef: MatDialogRef<DialogAddPartialComponent>,
                @Inject(MAT_DIALOG_DATA) public data,
                private _srvPlanning: PlanningService) {

    this.dateInit =  data.dateInit;
    this.dateEnd = data.dateEnd;

    this.working = false;
    this.valid = false;

    this.schedulingPartials = data.partials;

    this.newPartial = data.newPartial;

    this.planningId = data.planningId;
    this.courseDateInit =  new Date( data.courseDateInit );
    this.courseDateEnd = new Date( data.courseDateEnd );

    this.frmPartial = new FormGroup({
          'id' : new FormControl( this.newPartial.id),
          'description': new FormControl( this.newPartial.description, [Validators.required]),
          'startDate': new FormControl( this.newPartial.startDate, [Validators.required]),
          'endDate': new FormControl( this.newPartial.endDate, [Validators.required]),
          'coursePlanningId': new FormControl( this.newPartial.coursePlanningId )
        });



    }

  ngOnInit() {
    if( this.newPartial.id > 0) {

      this.showDateEnd = true;

      this.frmPartial.controls['startDate'].setValue(new Date( this.newPartial.startDate ));

      this.frmPartial.controls['endDate'].setValue(new Date( this.newPartial.endDate));



    }

  }

  onCloseConfirm( data ) {
    this.dialogRef.close( data );
  }

  onSavePartial() {
    this.working = true;

    if ( this.countInit > 0 && this.countEnd > 0 ) {
      swal( Appsettings.APP_NAME, 'Los datos tiene errores favor de verificar', 'error');
    }

    let partial = new SchedulingPlartial();
    partial.id = this.frmPartial.controls['id'].value;
    partial.description = this.frmPartial.controls['description'].value;
    partial.startDate = this.frmPartial.controls['startDate'].value;
    partial.endDate = this.frmPartial.controls['endDate'].value;
    partial.coursePlanningId = this.planningId;

    ( partial.id > 0) ? this.updatePartial( partial ) : this.savePartial( partial);
  }

  savePartial( partial: SchedulingPlartial): void {
    this._srvPlanning.savePartial(partial ).subscribe( result => {
      if ( result.success ) {
        this.onCloseConfirm( true );
        swal( Appsettings.APP_NAME, result.message, 'success');
      } else {
        swal( Appsettings.APP_NAME, result.message, 'error');
      }
      this.working = false;
    });
  }

  updatePartial( partial: SchedulingPlartial): void{
    this._srvPlanning.updatePartial(partial ).subscribe( result => {
      if ( result.success ) {
        this.onCloseConfirm( true );
        swal( Appsettings.APP_NAME, result.message, 'success');
      } else {
        swal( Appsettings.APP_NAME, result.message, 'error');
      }
      this.working = false;
    });
  }
  onChangeInit( event) {
    this.showMsgInit = false;
    let date = new Date( event);
    let dateInit = new Date ( this.courseDateInit.getFullYear(), this.courseDateInit.getMonth(), this.courseDateInit.getDate(), 0, 0, 0);
    let dateEnd =  new Date ( this.courseDateEnd.getFullYear(), this.courseDateEnd.getMonth(), this.courseDateEnd.getDate(), 0, 0, 0);
    if(  date < dateInit || date > dateEnd ) {
      swal( Appsettings.APP_NAME, 'La fecha de inicio del parcial no puede ser menor o mayor a las fechas del curso', 'error');
      this.frmPartial.controls['startDate'].setValue('');
      this.showDateEnd = false;
    } else {
      this.showDateEnd = true;
      this.partialDateInit = date;
      this.validateDatePartialsInit();
    }
  }

  onChangeEnd( event ) {
    this.showMsgEnd = false;
    let date = new Date( event);
    let dateInit = new Date ( this.courseDateInit.getFullYear(), this.courseDateInit.getMonth(), this.courseDateInit.getDate(), 0, 0, 0);

    let dateEnd = new Date ( this.courseDateEnd.getFullYear(), this.courseDateEnd.getMonth(), this.courseDateEnd.getDate(), 0, 0, 0);

    if( date < dateInit || date > dateEnd) {
      swal( Appsettings.APP_NAME, 'La fecha final del parcial no puede ser mayor a la del fin del curso', 'error');
      this.frmPartial.controls['endDate'].setValue('');
    } else {
      if ( date <= this.partialDateInit) {
        swal( Appsettings.APP_NAME, 'La fecha final no puede ser menor a la fecha de inicio', 'error');
        this.frmPartial.controls['endDate'].setValue('');
      } else {
        this.partialDateEnd = date;
        this.validateDatePartialsEnd();
      }
    }
  }


  validateDatePartialsInit() {
    let errorsInit = 0;
    let partials = this.schedulingPartials.filter( x => x.description !== 'Final');
    if ( this.newPartial.id > 0) {
       partials = this.schedulingPartials.filter( x => x.id !== this.newPartial.id);
    }
    for ( let p of partials) {
      let startDate = new Date(p.startDate);
      let pInit = new Date(  startDate.getFullYear(), startDate.getMonth(), startDate.getDate(), 0, 0, 0 );

      let endDate = new Date(p.endDate);
      let pEnd = new Date( endDate.getFullYear(), endDate.getMonth(), endDate.getDate(), 0, 0, 0  );

      // tslint:disable-next-line:curly
      if ( this.partialDateInit >= pInit && this.partialDateInit <= pEnd )
        errorsInit++;
        this.countInit = errorsInit;

    }
    // tslint:disable-next-line:curly
    if( errorsInit )
      this.frmPartial.controls['startDate'].setValue('');

    this.showMsgInit = ( errorsInit > 0 ) ? true : false;
  }

  validateDatePartialsEnd() {
    let errorsEnd = 0;

    let partials = this.schedulingPartials.filter( x => x.description !== 'Final');
    if ( this.newPartial.id > 0) {
       partials = this.schedulingPartials.filter( x => x.id !== this.newPartial.id);
    }


    for ( let p of partials) {


      let startDate = new Date(p.startDate);
      let pInit = new Date(  startDate.getFullYear(), startDate.getMonth(), startDate.getDate(), 0, 0, 0 );

      let endDate = new Date(p.endDate);
      let pEnd = new Date( endDate.getFullYear(), endDate.getMonth(), endDate.getDate(), 0, 0, 0  );


      // tslint:disable-next-line:curly
      if ( this.partialDateEnd >= pInit && this.partialDateEnd <= pEnd )
        errorsEnd++;
        this.countEnd = errorsEnd;
    }

    // tslint:disable-next-line:curly
    if ( errorsEnd > 0 )
      this.frmPartial.controls['endDate'].setValue('');

    this.showMsgEnd = ( errorsEnd > 0 ) ? true : false;
  }
}
