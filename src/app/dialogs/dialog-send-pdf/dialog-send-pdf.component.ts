//Todo Armand
import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material';
import { MAT_DIALOG_DATA } from '@angular/material';

import 'rxjs/add/operator/debounceTime';
import { MatPaginator, MatTableDataSource } from '@angular/material';

// Servicios
import { GroupService, StudenService } from '../../service/service.index';
import { FinalScoreService } from '../../service/publishing/final-score.service';

// Modelos
import { Studen } from '../../models/courses/student.model';

import swal from 'sweetalert2';

import { Appsettings } from '../../configuration/appsettings';
@Component({
  selector: 'app-dialog-send-pdf',
  templateUrl: './dialog-send-pdf.component.html',
  styleUrls: ['./dialog-send-pdf.component.scss']
})
export class DialogSendPdfComponent implements OnInit {

  working: boolean;
  workingProgess: boolean;
  students: Studen[] = [];
  stateCtrl = new FormControl();
  filteredStates: any[] = [];
  ids: any[] = [];
  groupId: number;
  courseId: number;
  studentsSelect: Studen[] = [];
  textSearch: string;
  user: FormGroup;

  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(  public dialogRef: MatDialogRef<DialogSendPdfComponent>,
      private _srvGroup: GroupService,
      private _srvStuden: StudenService,
      private _srvFinalScore: FinalScoreService,
      @Inject(MAT_DIALOG_DATA) public data) {
        this.groupId = data.groupId;
        this.courseId = data.courseId;
        const roleId = Appsettings.ROL_ID;
        this.working = false;

        this.stateCtrl.valueChanges.debounceTime(400).subscribe( item => {
        this.textSearch = item;

        if ( item.length > 0 ) {

        this.working = true;
        this.filteredStates = [];
        this._srvStuden.getStudenByEmail(item, roleId, this.courseId).subscribe( result => {
        if ( result.success ) {
        for ( let s of result.data) {

          let seleted = this.studentsSelect.filter( x => x.id === s.id)[0];

          if ( seleted === undefined ) {
            let student = new Studen();
            student.id = s.id;
            student.fullName = s.fullName;
            student.email = s.email;

            this.filteredStates.push( student );
          }
        }
        }
        this.working = false;
        }, err => {
        this.working = false;
        });
        }
        });
        }

  ngOnInit() {
    this.user = new FormGroup({
      turno: new FormControl('', [Validators.required]),
      cicloEscolar: new FormControl('', [Validators.required])
    });
  }

  onSubmit() {
    //Mandar a metodo de envío
    this._srvFinalScore.SendMail(this.groupId, this.user.get('turno').value, this.user.get('cicloEscolar').value).subscribe( result => {
      if(result.success){
        swal('¡Éxito!', 'Los correos han sido envíados', 'success' );
        this.onCloseConfirm();
      } else{
        swal('¡Error!', 'Algo salió mal al enviar los correos', 'error' );
      }
    });
  }
  onCloseConfirm() {
    this.dialogRef.close( null );
  }
}
