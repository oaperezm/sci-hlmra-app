
import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { MAT_DIALOG_DATA } from '@angular/material';

// SERVICIOS
import { TeacherExamService } from '../../service/service.index';
import { TeacherExam } from '../../models/courses/teacher-exam.model';
import { FormGroup, FormControl, Validators, ValidatorFn, ValidationErrors } from '@angular/forms';
import { isNumeric } from 'rxjs/internal-compatibility';
import { Weighting } from '../../models/courses/weighting.model';
import { Appsettings } from '../../configuration/appsettings';
import swal from 'sweetalert2';


@Component({
    selector: 'app-dialog-exam-weighting',
    templateUrl: './dialog-exam-weighting.component.html',
    styleUrls: ['./dialog-exam-weighting.component.scss']
})

export class DialogExamWeightingComponent implements OnInit {
    working: boolean;
    examName: string;
    automaticValue: string;
    questions: any[] = [];
    weighting: string;
    currentMaximumExamScore: number;
    teacherExam: TeacherExam;
    frmExamWeighting: FormGroup;

    constructor(private _srvTeacher: TeacherExamService,
        public dialogRef: MatDialogRef<DialogExamWeightingComponent>,
        @Inject(MAT_DIALOG_DATA) public data) {
        this.working = true;
        this.frmExamWeighting = new FormGroup({
            weighting: new FormControl('', [Validators.required]),
            automaticValue: new FormControl('', [Validators.required]),
            maximumExamScore: new FormControl('', [Validators.required, Validators.maxLength(4), Validators.min(1), Validators.max(1000), Validators.pattern('[0-9]*')]),
        },
            { validators: [this.examWeightingValidator] });

        this.frmExamWeighting.controls['weighting'].setValue(data.weighting.unit);
        this.frmExamWeighting.controls['automaticValue'].setValue(data.isAutomaticValue ? '1' : '2');
        this.frmExamWeighting.controls['maximumExamScore'].setValue(data.maximumExamScore);
        this.teacherExam = new TeacherExam();
        this.teacherExam.description = data.description;
        this.teacherExam.weighting = data.weighting;
        this.teacherExam.isAutomaticValue = data.isAutomaticValue;
        this.teacherExam.maximumExamScore = data.maximumExamScore;
        this.currentMaximumExamScore = data.maximumExamScore;

        this.getAllQuestions();
    }

    examWeightingValidator: ValidatorFn = (control: FormGroup): ValidationErrors | null => {
        if (this.questions.length > 0
            && control.get('maximumExamScore').value == this.currentMaximumExamScore) {

            const maximumExamScore = control.get('maximumExamScore').value;
            let sumQuestionValues = 0;
            for (const q of this.questions) {
                sumQuestionValues += Math.round(q.value * 100);
                if (q.value == 0)
                    return { 'examWeightingErrorValueQuestion': true }
            }
            if (sumQuestionValues != maximumExamScore * 100)
                return { 'examWeightingErrorSum': true }
        }
        return null;
    };


    ngOnInit() {
    }

    getAllQuestions() {
        this._srvTeacher.getById(this.data.id).subscribe(result => {
            if (result.success) {
                const examQuestions = result.data.teacherExamQuestion;
                this.questions = [];
                for (const q of examQuestions) {
                    this.questions.push({
                        index: this.questions.length + 1,
                        answers: q.question.answers,
                        content: q.question.content,
                        id: q.question.id,
                        questionTypeId: q.question.questionTypeId,
                        explanation: q.question.explanation,
                        urlImage: q.question.urlImage,
                        imageBase64: q.question.imageBase64,
                        imageHeight: q.question.imageHeight,
                        imageWidth: q.question.imageWidth,
                        value: q.value,
                        userEdited: q.userEdited,
                    });
                };
            }
            this.frmExamWeighting.updateValueAndValidity();
            this.working = false;
        });
    }

    onCloseConfirm() {
        this.dialogRef.close(null);
    }

    automaticCalc(event) {
        let frm = this.frmExamWeighting.controls;
        if (frm['automaticValue'].valid) {
            if (frm['automaticValue'].value == '1' && frm['automaticValue'].valid)
                for (const q of this.questions)
                    q.userEdited = false;
            this.calcRest({ value: '' });
        }
    }

    calcRest(item) {
        let frm = this.frmExamWeighting.controls;

        if (frm['maximumExamScore'].valid) {
            let total = 0;
            let totalEdited = 0;
            let maximumExamScore = parseFloat(this.frmExamWeighting.controls['maximumExamScore'].value);
            if (this.currentMaximumExamScore == 0)
                this.currentMaximumExamScore = maximumExamScore
            let factor = maximumExamScore / this.currentMaximumExamScore;
            this.currentMaximumExamScore = maximumExamScore
            if (!isNumeric(item.value)) {
                item.userEdited = false;
            }
            else {
                item.value = parseFloat(item.value);
                if (item.value <= 0 || item.value > maximumExamScore)
                    item.userEdited = false;
                else
                    item.userEdited = true;
            }

            for (const q of this.questions) {
                if (q.userEdited) {
                    q.value = Math.round(q.value * factor * 100) / 100;
                    total += q.value;
                    totalEdited++;
                }
            }
            for (const q of this.questions) {
                if (!q.userEdited) {
                    if (maximumExamScore > total) {
                        q.value = Math.round((maximumExamScore - total) / (this.questions.length - totalEdited) * 100) / 100;
                        total += q.value;
                        totalEdited++;
                    }
                    else
                        q.value = 0;

                }
            }
            this.frmExamWeighting.updateValueAndValidity();
        }
    }

    saveWeighting() {
        let te = new TeacherExam();
        te.id = this.data.id;
        te.weighting = new Weighting();
        te.weighting.unit = this.frmExamWeighting.controls['weighting'].value;
        te.maximumExamScore = this.frmExamWeighting.controls['maximumExamScore'].value;
        te.isAutomaticValue = this.frmExamWeighting.controls['automaticValue'].value == '1';
        if (this.frmExamWeighting.valid) {
            this.working = true;
            this._srvTeacher.saveExamWeighting(te, this.questions).subscribe(result => {
                if (result.success) {
                    this.data.weighting = te.weighting;
                    this.data.isAutomaticValue = te.isAutomaticValue;
                    this.data.maximumExamScore = te.maximumExamScore;
                    this.data.weighting.description = te.weighting.unit == '%' ? 'Porcentual' : 'Puntuación';
                }; 
                swal(Appsettings.APP_NAME, result.message, 'success');
                this.onCloseConfirm();
                this.working = false;
            });
        }
    }
}
