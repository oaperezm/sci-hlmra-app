import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { MAT_DIALOG_DATA } from '@angular/material';
@Component({
  selector: 'app-dialog-get-file-activity',
  templateUrl: './dialog-get-file-activity.component.html',
  styleUrls: ['./dialog-get-file-activity.component.scss']
})
export class DialogGetFileActivityComponent implements OnInit {

  activity: any[] = [];

  constructor(  private dialogRef: MatDialogRef<DialogGetFileActivityComponent>,
    @Inject(MAT_DIALOG_DATA) public data ) {
      this.activity = data.task;

     }

  ngOnInit() {
  }

  onCloseConfirm( event ) {
    this.dialogRef.close( event );
  }

}
