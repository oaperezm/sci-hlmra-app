
import { Component, OnInit, Inject } from '@angular/core';
import { Router } from '@angular/router';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Appsettings } from '../../configuration/appsettings';

import { StorageService, UserService } from '../../service/service.index';

import swal from 'sweetalert2';

@Component({
  selector: 'app-dialog-details-notification',
  templateUrl: './dialog-details-notification.component.html',
  styleUrls: ['./dialog-details-notification.component.scss']
})
export class DialogDetailsNotification implements OnInit {

  working: boolean;
  isChangeData: boolean;
  tittle: string= '';
  body: string= '';  

  constructor(  public dialogRef: MatDialogRef<DialogDetailsNotification>,
                @Inject(MAT_DIALOG_DATA) public data,
                private _srvUser: UserService,
                private _srvStorage: StorageService) {
    this.working = false;
    this.tittle = data.tittle;
    this.body = data.body;

  
                 }

  ngOnInit() {
  }

  // Evento para cerrar la ventana modal
  onCloseConfirm() {
    this.dialogRef.close(false);
  }

  closeModalConfirm( event ) {
    this.dialogRef.close(event[0]);
  }


}
