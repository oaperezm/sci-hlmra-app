import { Routes, RouterModule } from '@angular/router';
import { HomeComponent} from './routes/home/home.component';
import { LoginComponent } from './routes/anonymous/login/login.component';

const APP_ROUTES: Routes = [
  { path: '', component: LoginComponent},
  { path: 'inicio/:section', component: LoginComponent },
  { path: '**', component: LoginComponent}
];

export const APP_ROUTING = RouterModule.forRoot(APP_ROUTES, {
  useHash: true,
  scrollPositionRestoration: 'enabled',
  onSameUrlNavigation: 'reload'
});
