import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { Appsettings } from './configuration/appsettings';

import { LOCALE_ID, NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { HttpModule, XHRBackend, RequestOptions } from '@angular/http';

import { registerLocaleData } from '@angular/common';

import { AppComponent } from './app.component';
import { PersistenceModule } from 'angular-persistence';
import { CoursesModule } from './routes/course/courses.module';
import { TeacherModule } from './routes/teacher/teacher.module';
import { RaModule } from './routes/ra/ra.module';

import { AngularFireModule } from '@angular/fire';
import { AngularFireMessagingModule } from '@angular/fire/messaging';

import localEsMX from '@angular/common/locales/es-MX';
import localEn from '@angular/common/locales/en';

registerLocaleData(localEsMX, 'es-Mx');
registerLocaleData(localEn, 'en');
// Rutas
import { APP_ROUTING } from './app.routes';

// Modules
import { PagesExternalModule } from './routes/anonymous/pagesexternal.module';
import { ServiceModule } from './service/service.module';
import { AnonymousModule } from './components/layouts/anonymous/anonymous.module';
import { ResourceModule } from './routes/resource/resource.module';
import { UserModule } from './routes/user/user.module';
import { CoreModule } from './core/core.module';
import { AdminModule } from './routes/administrator/admin.module';

import { OwlModule } from 'ngx-owl-carousel';
import { EmbedVideoService } from 'ngx-embed-video';
import { LandingPageModule } from './routes/landing-page/landingpage.module';
import { HotkeyModule } from 'angular2-hotkeys';
import { JoyrideModule } from 'ngx-joyride';
@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    OwlModule,
    AngularFireModule.initializeApp(Appsettings.FIREBASE_CONFIG),
    AngularFireMessagingModule,
    APP_ROUTING,
    PagesExternalModule,
    PersistenceModule,
    ServiceModule,
    HttpModule,
    BrowserAnimationsModule,
    AnonymousModule,
    ResourceModule,
    UserModule,
    CoreModule,
    CoursesModule,
    TeacherModule,
    AdminModule,
    RaModule,
    LandingPageModule,
    JoyrideModule.forRoot(),
    HotkeyModule.forRoot()
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ],
  providers: [
    { provide: LOCALE_ID, useValue: 'es-Mx' },
    EmbedVideoService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
