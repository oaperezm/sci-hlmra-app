import { Injectable } from '@angular/core';

import { HttpService } from '../security/http.service';
import { Headers, URLSearchParams } from '@angular/http';
import { Appsettings } from '../../configuration/appsettings';

// Modelo
import { ActivitySchedule } from '../../models/courses/activitySchedule.model';

@Injectable({
  providedIn: 'root'
})
export class ActivityService {

  constructor(  private httpService: HttpService ) { }


    // Obtiene las tareas programadas de un grupo
    getAll( id: number) {
     const API_URL = `${Appsettings.API_ENDPOINT_FULL}/activities/group?studentGroupId=${id}`;
     return this.httpService.get( API_URL )
                             .map( res => res.json());
   }

    /**
     * Obtiene las tareas programadas de un grupo
     * Falta servico
    **/
    getAllByStudent( id: number) {
    const API_URL = `${Appsettings.API_ENDPOINT_FULL}/activities/student?studentGroupId=${id}`;
    return this.httpService.get( API_URL )
                            .map( res => res.json());
  }

   // Elimina una programacion
   deletContent( id: number) {
     const apiUrl = `${Appsettings.API_ENDPOINT_FULL}/activities?id=${ id }`;

     return this.httpService.delete(apiUrl)
                            .map( res => res.json());
   }

   // Guadarda una programacion de tarea
   save( params: ActivitySchedule ) {
     const API_URL = `${Appsettings.API_ENDPOINT_FULL}/activities`;
     const HEADERS = new Headers();

     HEADERS.append('Content-Type', 'application/json');

     return this.httpService.post(API_URL, JSON.stringify( params ), { headers: HEADERS } )
                            .map( res => res.json());
   }

   // Actualiza una programacion de tarea
   update( params: ActivitySchedule) {

     const API_URL = `${Appsettings.API_ENDPOINT_FULL}/activities`;
     const HEADERS = new Headers();


     HEADERS.append('Content-Type', 'application/json');

    return this.httpService.put(API_URL, JSON.stringify( params ), { headers: HEADERS } )
                            .map( res => res.json());
   }

   // Obtiene los archivos de las tareas de un alumno
   getFilesStudent( homeWorkId: number ) {
     const API_URL = `${Appsettings.API_ENDPOINT_FULL}/activities/${homeWorkId}/answers`;
     return this.httpService.get( API_URL )
                             .map( res => res.json());
   }

   // Elimina un archivo de tarea del alumno
   deleteFilesHomerwork( homeWorkId: number, homeWorkFileId: number ) {
     const apiUrl = `${Appsettings.API_ENDPOINT_FULL}/activities/${homeWorkId}/deleteAnswer?answerId=${ homeWorkFileId}`;

     return this.httpService.delete(apiUrl)
                            .map( res => res.json());
   }

   // Obtiene todas las tareas entregadas por los alumnos de un grupo
   getAllActivities( groupId: number, homeworkId: number) {
     const API_URL = `${Appsettings.API_ENDPOINT_FULL}/activities/score?id=${ homeworkId }&studentGroupId=${ groupId}`;
     return this.httpService.get( API_URL )
                             .map( res => res.json());
   }

   // Guarda las califcaciones de un grupo
   saveQualifyGroup( params) {
     const API_URL = `${Appsettings.API_ENDPOINT_FULL}/activities/score`;
     const HEADERS = new Headers();

     HEADERS.append('Content-Type', 'application/json');

     return this.httpService.post(API_URL, JSON.stringify( params ), { headers: HEADERS } )
                            .map( res => res.json());
   }

   // Actualiza el estatus de la tarea
   updateStatusHomwork( params ) {
     // /homeworks/close
     const API_URL = `${Appsettings.API_ENDPOINT_FULL}/activities/close`;
     const HEADERS = new Headers();


     HEADERS.append('Content-Type', 'application/json');

    return this.httpService.put(API_URL, JSON.stringify( params ), { headers: HEADERS } )
                            .map( res => res.json());
   }
}
