import { Injectable } from '@angular/core';

import { HttpService } from '../security/http.service';
import { Headers, URLSearchParams } from '@angular/http';
import { Appsettings } from '../../configuration/appsettings';

// Modelos
import { SchedulingPlartial } from '../../models/courses/schedulingPartial.model';
@Injectable({
  providedIn: 'root'
})
export class PlanningService {

  constructor(  private _httpService: HttpService  ) { }

  // Obtiene las tareas programadas de un grupo
  getAll( id: number) {
    // http://{{server}}/api/homeworks/student?studentGroupId=122
    const API_URL = `${Appsettings.API_ENDPOINT_FULL}/homeworks/group?studentGroupId=${id}`;
    return this._httpService.get( API_URL )
                           .map( res => res.json());
  }

  update( planning ) {
    let API_URL = `${Appsettings.API_ENDPOINT_FULL}/courseplanning/`;
    const HEADERS = new Headers();
    HEADERS.append('Content-Type', 'application/json');

   return this._httpService.put(  API_URL, JSON.stringify( planning ),
                                { headers: HEADERS } )
                           .map( res => res.json());
  }


  // Guarda la planeacion de parcial
  savePartial( partial: SchedulingPlartial) {
    let API_URL = `${Appsettings.API_ENDPOINT_FULL}/schedulingpartial/`;
    const HEADERS = new Headers();
    HEADERS.append('Content-Type', 'application/json');


   return this._httpService.post(API_URL, JSON.stringify( partial ), { headers: HEADERS } )
                           .map( res => res.json());
  }

  // Elimina un parcial
  deletePartial( partialId: number, coursePlanningId: number){
    const API_URL = `${Appsettings.API_ENDPOINT_FULL}/schedulingpartial?id=${partialId}&coursePlanningId=${coursePlanningId}`;
    return this._httpService.delete(API_URL )
                            .map( res => res.json());
  }

  // Edita un parcial
  updatePartial( partial ) {
    let API_URL = `${Appsettings.API_ENDPOINT_FULL}/schedulingpartial/`;
    const HEADERS = new Headers();
    HEADERS.append('Content-Type', 'application/json');

   return this._httpService.put(  API_URL, JSON.stringify( partial ),
                                { headers: HEADERS } )
                           .map( res => res.json());
  }

  // Obtiene la planeacion del curso para el alumno
  getPlanningCourse( groupId: number){
    const API_URL = `${Appsettings.API_ENDPOINT_FULL}/courseplanning/Student?StudentGroupId=${groupId}`;
    return this._httpService.get( API_URL )
                           .map( res => res.json());
  }
}
