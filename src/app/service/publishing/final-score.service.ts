import { Injectable } from '@angular/core';


import { HttpService } from '../security/http.service';
import { Headers, URLSearchParams } from '@angular/http';
import { Appsettings } from '../../configuration/appsettings';

// Modelos
import { SchedulingPlartial } from '../../models/courses/schedulingPartial.model';

@Injectable({
  providedIn: 'root'
})
export class FinalScoreService {

  constructor( private httpService: HttpService ) { }

  // Get all final scores by id
  getAll( id: number) {
   const API_URL = `${Appsettings.API_ENDPOINT_FULL}/scores/group?studentGroupId=${id}`;
   return this.httpService.get( API_URL )
                           .map( res => res.json());
 }

  // Obtiene las tareas programadas de un grupo
  getAllByStudent( id: number) {
  const API_URL = `${Appsettings.API_ENDPOINT_FULL}/scores/student?studentGroupId=${id}`;
  return this.httpService.get( API_URL )
                          .map( res => res.json());
}

 // Elimina una programacion
 deletContent( id: number) {
   const apiUrl = `${Appsettings.API_ENDPOINT_FULL}/scores?id=${ id }`;

   return this.httpService.delete(apiUrl)
                          .map( res => res.json());
 }

 // Guadarda una programacion de tarea
 save( params: SchedulingPlartial ) {
   const API_URL = `${Appsettings.API_ENDPOINT_FULL}/scores`;
   const HEADERS = new Headers();

   HEADERS.append('Content-Type', 'application/json');

   return this.httpService.post(API_URL, JSON.stringify( params ), { headers: HEADERS } )
                          .map( res => res.json());
 }

 // Actualiza una programacion de tarea
 update( params: SchedulingPlartial) {

   const API_URL = `${Appsettings.API_ENDPOINT_FULL}/scores/`;
   const HEADERS = new Headers();


   HEADERS.append('Content-Type', 'application/json');

  return this.httpService.put(API_URL, JSON.stringify( params ), { headers: HEADERS } )
                          .map( res => res.json());
 }

 //Ricardo - Agregado - Inicio
   //Descarga el excel con las calificaciones
 download(id: number,turn:string, schoolCycle:string)
 {
   let head = new Headers();
   head.append('Content-Type', 'application/json');
   head.append('responseType', 'blob');
   head.append('Content-Disposition','texto');
   const API_URL = `${Appsettings.API_ENDPOINT_FULL}/scores/Excel?studentGroupId=${id}&turn=${turn}&schoolCycle=${schoolCycle}`;

   return this.httpService.post( API_URL, {headers: head} )
   .map(response => {
     
     if (response.status == 400) {
       return "FAILURE";
     } else if (response.status == 200) {
       let content= (<any>response)._body;
       let contentdetails = content.split("|",2);
       if (contentdetails[0]!="")
       {
        let data = 'data:application/octet-stream;base64,' +  contentdetails[1];
        let btnDownload = document.createElement('a');
        btnDownload.download = contentdetails[0]+".xlsx";
        btnDownload.href = data;
        document.body.appendChild( btnDownload);
        btnDownload.click();
        return "OK";
      }
      else  return "FAILURE";
     }
 });
 }

 //Ricardo - Agregado - Fin

 //Armand - Agregado - Inicio
///Conecta con la API, crea PDF y envía a estudiantes.
SendMail(id:number, turno:string, cicloEscolar:string){
  let API_URL = `${Appsettings.API_ENDPOINT_FULL}/scores/EmailPdf?studentGroupId=${id}&turno=${turno}&cicloEscolar=${cicloEscolar}`;
  let headers = new Headers();
  headers.set('Content-Type', 'application/json');
  return this.httpService.get( API_URL ).map( res => res.json());
}
//Armand - Agregado - Fin

}
