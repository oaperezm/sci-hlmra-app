import { Injectable } from '@angular/core';
import { Http, Headers, URLSearchParams } from '@angular/http';
import { HttpService } from '../security/http.service';
import { Appsettings} from '../../configuration/appsettings';

import { Course } from '../../models/catalogs/course.model';
import { NewCourse } from '../../models/catalogs/newcourse.model'; // Ricardo - Agregado 
@Injectable({
  providedIn: 'root'
})
export class CourseService {
  newcourse: NewCourse;// Ricardo - Agregado 
  constructor(  private _httpService: HttpService ) { }

   // Obtener todos los cursos
   getAll() {
    const API_URL = `${Appsettings.API_ENDPOINT_FULL}/courses/teacher`;

    return this._httpService
        .get(API_URL)
        .map(res => res.json());
  }

  // Ricardo - Cambiado - Inicio
  // Agregar un nuevo curso
  save( course: Course, grade:number ) {
    let API_URL = `${Appsettings.API_ENDPOINT_FULL}/courses/`;
    const HEADERS = new Headers();
    HEADERS.append('Content-Type', 'application/json');
    const newcourse = new NewCourse(
      new Array(),
      course.id,
      course.name,
      course.description,
      course.gol,
      course.startDate,
      course.endDate,
      course.nodeId,
      grade,
      course.subject,
      course.monday,
      course.tuesday,
      course.wednesday,
      course.thursday,
      course.friday,
      course.saturday,
      course.sunday,
      course.institution
    );
   
    
   return this._httpService.post( API_URL, JSON.stringify( newcourse ),
                                 { headers: HEADERS } )
                           .map( res => res.json());
  }
  // Ricardo - Cambiado - Fin
  // Actualizar los datos de un curso
  update( course: Course ) {
    let API_URL = `${Appsettings.API_ENDPOINT_FULL}/courses/`;
    const HEADERS = new Headers();
    HEADERS.append('Content-Type', 'application/json');

   return this._httpService.put(  API_URL, JSON.stringify( course ),
                                { headers: HEADERS } )
                           .map( res => res.json());
  }

  // Eliminar los datos de un curso
  delete( id: number ) {

    const API_URL = `${Appsettings.API_ENDPOINT_FULL}/courses?id=${id}`;
    return this._httpService.delete(API_URL )
                            .map( res => res.json());

  }

  // Obtener la relación de los estudiantes con el profesor
  getRelatedStudents() {

    const API_URL = `${Appsettings.API_ENDPOINT_FULL}/relationship?id=0`;

    return this._httpService
        .get(API_URL)
        .map(res => res.json());
  }

  // Obtener la relación de los estudiantes con el profesor
  getRelatedStudentsPagination(pageSize: number, currentPage: number, filter: string, courseId: string, groupId: string ) {

    // tslint:disable-next-line:max-line-length
    const API_URL = `${Appsettings.API_ENDPOINT_FULL}/relationship/all?id=0&currentPage=${currentPage}&pageSize=${pageSize}&filter=${filter}&courseId=${courseId}&groupId=${groupId}`;

    return this._httpService
        .get(API_URL)
        .map(res => res.json());
  }

   // Obtener la relación de los estudiantes con el profesor
   getResourcesPagination( pageSize: number, currentPage: number, nodeId: number ) {

    const API_URL = `${Appsettings.API_ENDPOINT_FULL}/resources/all?id=0&currentPage=${currentPage}&pageSize=${pageSize}&type=${nodeId}`;

    return this._httpService
        .get(API_URL)
        .map(res => res.json());
  }
   // Obtener la planeacion de un curso
   getPlanning( courseId: number ) {

    const API_URL = `${Appsettings.API_ENDPOINT_FULL}/courseplanning/Getgroup?id=${courseId}`;

    return this._httpService
        .get(API_URL)
        .map(res => res.json());
  }

  // Obtener la calificacion final de un curso por alumno
     // Obtener la planeacion de un curso
     getFinalScore( groupId: number ) {

      const API_URL = `${Appsettings.API_ENDPOINT_FULL}/scores/student?studentGroupId=${groupId}`;

      return this._httpService
          .get(API_URL)
          .map(res => res.json());
    }
}
