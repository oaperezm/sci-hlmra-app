import { Injectable } from '@angular/core';
import { Appsettings } from '../../configuration/appsettings';
import { Coupon } from 'src/app/models/coupon/coupon';
import { HttpClient } from '@angular/common/http';

// Modelo

@Injectable({
  providedIn: 'root'
})
export class CouponService {


  constructor(private http: HttpClient) { }


  // OBTENER CODIGO CONSECUTIVO PARA CANJEAR CUPON
  getCodeCoupon () {

    const API_URL = `${Appsettings.API_ENDPOINT_FULL}/codeCoupon`;
    return this.http.get( API_URL );

  }


  getAllCoupon() {
    const API_URL = `${Appsettings.API_ENDPOINT_FULL}/coupon`;
    return this.http.get( API_URL );
  }


  // CANJEAR CUPON
  exchange( coupon: Coupon ) {

    const API_URL = `${Appsettings.API_ENDPOINT_FULL}/coupons`;
    const HEADERS = new Headers();
    HEADERS.append('Content-Type', 'application/json');

    return this.http.post(API_URL, JSON.stringify( coupon ) );

  }
   
  

}
