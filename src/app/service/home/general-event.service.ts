import { Injectable } from '@angular/core';

import { HttpService } from '../security/http.service';
import { Headers, URLSearchParams } from '@angular/http';
import { Appsettings } from '../../configuration/appsettings';

import { GeneralEvent } from '../../models/home/general-event.model';

@Injectable({
  providedIn: 'root'
})
export class GeneralEventService {

  constructor(  private httpService: HttpService ) { }

   // Obtiene todas las noticias paginadas
   getAll( page: number, size: number, filter: string ) {

    // tslint:disable-next-line:max-line-length
    const API_URL = `${Appsettings.API_ENDPOINT_FULL}/general-events/all?currentPage=${page}&pageSize=${size}&filter=${filter}&systemId=${ Appsettings.SYSTEM_ID }`;
    return this.httpService.get( API_URL )
                           .map( res => res.json());
  }

  getById( id: number ) {
    const API_URL = `${Appsettings.API_ENDPOINT_FULL}/general-events/${id}/detail`;
    return this.httpService.get( API_URL )
                           .map( res => res.json());
  }

  // Registrar un nuevo recurso
  save ( event: GeneralEvent ) {

    const API_URL = `${Appsettings.API_ENDPOINT_FULL}/general-events/`;
    const HEADERS = new Headers();

    HEADERS.append('Content-Type', 'application/json');

    return this.httpService.post(API_URL, JSON.stringify( event ), { headers: HEADERS } )
                           .map( res => res.json());
  }

  // Actualizar un nuevo recurso
  update (  event: GeneralEvent ) {

    const API_URL = `${Appsettings.API_ENDPOINT_FULL}/general-events/`;
    const HEADERS = new Headers();

    HEADERS.append('Content-Type', 'application/json');

    return this.httpService.put(API_URL, JSON.stringify( event ), { headers: HEADERS } )
                           .map( res => res.json());
  }

  // Eliminar una noticia existente
  delete( id: number ) {
    let apiUrl = `${Appsettings.API_ENDPOINT_FULL}/general-events?id=${id}`;
    return this.httpService.delete(apiUrl)
                           .map( res => res.json());
  }

}
