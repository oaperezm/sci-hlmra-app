import { NgModule} from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpModule, XHRBackend, RequestOptions} from '@angular/http';

import {
  HttpService,
  UserService,
  StorageService,
  SessionService,
  StudenService,
  AuthGuardService,
  NavService,
  StructureService,
  CatalogService,
  MessageService,
  MenuService,
  TipsHomeService,
  NewsHomeService,
  QuestionsService,
  CourseService,
  GroupService,
  AttendanceService,
  ForumService,
  PagerService,
  QuestionBankService,
  PdfService,
  NodeService,
  TeacherResourceService,
  TeacherExamService,
  ExamScheduleService,
  InterestSiteService,
  SessionValidateService,
  CityService,
  ModuleService,
  ContentTeacherService,
  HomeworksService,
  ActivityService,
  PlanningService,
  AnnouncementService,
  EventService
} from './service.index';


@NgModule({
  imports: [
    CommonModule
  ],
  providers: [
    HttpService,
    UserService,
    StorageService,
    SessionService,
    StudenService,
    AuthGuardService,
    NavService,
    StructureService,
    CatalogService,
    MessageService,
    MenuService,
    TipsHomeService,
    NewsHomeService,
    QuestionsService,
    CourseService,
    AttendanceService,
    ForumService,
    PagerService,
    GroupService,
    QuestionBankService,
    PdfService,
    NodeService,
    TeacherResourceService,
    TeacherExamService,
    ExamScheduleService,
    InterestSiteService,
    SessionValidateService,
    CityService,
    ModuleService,
    ContentTeacherService,
    HomeworksService,
    ActivityService,
    PlanningService,
    AnnouncementService,
    EventService,
    {
      provide: HttpService,
      useFactory: (
        backend: XHRBackend,
        options: RequestOptions,
        storageService: StorageService,
        session: SessionService ) => { return new HttpService(backend, options, storageService, session);
      },
      deps: [XHRBackend, RequestOptions, StorageService, SessionService]
    }
  ],
  declarations: []
})
export class ServiceModule { }
