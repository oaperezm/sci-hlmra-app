import { Injectable } from '@angular/core';
import { HttpService } from '../security/http.service';
import { StorageService } from '../shared/storage.service';
import { Http, Headers, URLSearchParams } from '@angular/http';
import { Appsettings } from '../../configuration/appsettings';
import { FileContent } from '../../models/catalogs/file.model';


@Injectable({
  providedIn: 'root'
})
export class StructureService {

  constructor(  public storageService: StorageService,
                private http: Http,
                private httpService: HttpService  ) { }

  // Obtiene la estructura
  getStructureByParent( id: number) {

    const API_URL = `${Appsettings.API_ENDPOINT_FULL}/nodes/${id}/structure`;


    return this.httpService
        .get(API_URL)
        .map(res => res.json());
  }

  // Obtiene el contenido por nodo
  getContentbyNodeId( nodeId: number, fileTypeId: number, currentPage: number, blockId: number, formativeId: number) {
    // tslint:disable-next-line:max-line-length
    const API_URL = `${Appsettings.API_ENDPOINT_FULL}/nodes/${nodeId}/all-contents?nodeId=${nodeId}&fileTypeId=${fileTypeId}&currentPage=${currentPage}&sizePage=5&purposeId=0&blockId=${blockId}&formativeFieldId=${formativeId}`;


    return this.httpService
        .get(API_URL)
        .map(res => res.json());
  }

  // Obtiene el contenido por medio de una cadena de id´s de tipo de contenido
  getContentbyTypeId( nodeId: number, fileTypeId: string, currentPage: number, blockId: number, formativeId: number, filter: string, areaPersonalSocialDevelopmentId: number, areasCurriculumAutonomyId: number, axisId: number, keylearningId: number, learningexpectedId: number, purposeId: number, educationLevel: number) {
    // tslint:disable-next-line:max-line-length
    const API_URL = `${Appsettings.API_ENDPOINT_FULL}/nodes/${nodeId}/all-contents?nodeId=${nodeId}&fileTypeId=${fileTypeId}&currentPage=${currentPage}&sizePage=10&blockId=${blockId}&formativeFieldId=${formativeId}&filter=${filter}&areaPersonalSocialDevelopmentId=${areaPersonalSocialDevelopmentId}&areasCurriculumAutonomyId=${areasCurriculumAutonomyId}&axisId=${axisId}&keylearningId=${keylearningId}&learningexpectedId=${learningexpectedId}&purposeId=${purposeId}&educationLevel=${educationLevel}`;
    return this.httpService
        .get(API_URL)
        .map(res => res.json());
  }

  // Guarda una visita a un contenido
  addVisitContent( contentId: number ) {
    const API_URL = `${Appsettings.API_ENDPOINT_FULL}/contents/${ contentId }/visits`;
    const HEADERS = new Headers();
    HEADERS.append('Content-Type', 'application/json');

    const params = {
      'id': contentId,

    };

   return this.httpService.post(API_URL, JSON.stringify( params), { headers: HEADERS } )
                           .map( res => res.json());
  }

// Descripción: Obtiene los contenidos para gestor documenta
// Merhod: GET
// Parámetros:
// Id: Identificador del nodo
// currentPage: Página actual
// sizePage: Tamaño de la página
// searchId: Typo de búsqueda
// - 1 -> Reciente
// - 2 -> Visitas
// - 3 -> Recomendados

  // Obtiene el contenido para gestor academico
  getContenByNodeIdAcademic( nodeId: number, searchId: number, currentPage: number) {
    // tslint:disable-next-line:max-line-length
    const API_URL = `${Appsettings.API_ENDPOINT_FULL}/nodes/${nodeId}/academic-manager?currentPage=${currentPage}&sizePage=10&searchId=${searchId}`;


    return this.httpService
        .get(API_URL)
        .map(res => res.json());
  }


}
