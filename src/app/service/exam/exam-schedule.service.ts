import { Injectable } from '@angular/core';
import { HttpService } from '../security/http.service';
import { Headers, URLSearchParams } from '@angular/http';
import { Appsettings } from '../../configuration/appsettings';

// MODELS
import { ExamSchedule } from '../../models/courses/exam-schedule.model';

@Injectable({
  providedIn: 'root'
})
export class ExamScheduleService {

  constructor(private httpService: HttpService) { }

  // Obtener todas programaciones de exámenes por grupo
  getAllByGroup(groupId: number) {

    const API_URL = `${Appsettings.API_ENDPOINT_FULL}/examchedules/group`;
    const params = new URLSearchParams();

    params.append('groupId', groupId.toString());

    return this.httpService.get(API_URL, { params })
      .map(res => res.json());
  }

  // Obtener todas las programaciones del exámen por grupo para el estudiante
  getAllExamStuden(groupId: number) {

    const API_URL = `${Appsettings.API_ENDPOINT_FULL}/examchedules/student?groupId=${groupId}`;
    const params = new URLSearchParams();


    return this.httpService.get(API_URL, { params })
      .map(res => res.json());
  }

  // Registrar una nueva programación de exámenes
  save(examnSchedule: ExamSchedule) {

    const API_URL = `${Appsettings.API_ENDPOINT_FULL}/examchedules`;
    const HEADERS = new Headers();
    HEADERS.append('Content-Type', 'application/json');

    const params = this.getParameters(examnSchedule);
    params.id = 0;
    return this.httpService.post(API_URL, JSON.stringify(params), { headers: HEADERS })
      .map(res => res.json());

  }

  // Actualizar los datos de una programación de exámenes existente
  update(examnSchedule: ExamSchedule) {

    const API_URL = `${Appsettings.API_ENDPOINT_FULL}/examchedules`;
    const HEADERS = new Headers();
    HEADERS.append('Content-Type', 'application/json');    
    const params = this.getParameters(examnSchedule);

    return this.httpService.put(API_URL, JSON.stringify(params), { headers: HEADERS })
      .map(res => res.json());

  }
  private getUTCDate(date: Date) {
    if (date != null)
      return new Date(Date.UTC(
        date.getFullYear(),
        date.getMonth(),
        date.getDate(),
        date.getHours(),
        date.getMinutes(),
        date.getSeconds(),
      ));
    return null;
  }
  private getParameters(examnSchedule: ExamSchedule) {
    return {
      id: examnSchedule.id,
      description: examnSchedule.description,
      beginApplicationDate: this.getUTCDate(examnSchedule.beginApplicationDate),
      endApplicationDate: this.getUTCDate(examnSchedule.endApplicationDate),
      beginApplicationTime: this.getUTCDate(examnSchedule.beginApplicationTime),
      endApplicationTime: this.getUTCDate(examnSchedule.endApplicationTime),
      schedulingPartialId: examnSchedule.schedulingPartialId,
      minutesExam: examnSchedule.minutesExam = ! null ? examnSchedule.minutesExam : null,
      studentGroupId: examnSchedule.studentGroupId,
      teacherExamId: examnSchedule.teacherExamId
    }
  }
  // Actualizar los datos de una programación de exámenes existente
  updateStatus(id: number, examScheduleTypeId: number) {

    const API_URL = `${Appsettings.API_ENDPOINT_FULL}/examchedules/updateStatus`;
    const HEADERS = new Headers();
    HEADERS.append('Content-Type', 'application/json');

    const params = { id: id, examScheduleTypeId: examScheduleTypeId };

    return this.httpService.put(API_URL, JSON.stringify(params), { headers: HEADERS })
      .map(res => res.json());

  }

  remove(id: number) {

    const API_URL = `${Appsettings.API_ENDPOINT_FULL}/examchedules?id=${id.toString()}`;
    return this.httpService.delete(API_URL)
      .map(res => res.json());
  }

  getAnswersStuden(examId: number, studentId: number) {
    const API_URL = `${Appsettings.API_ENDPOINT_FULL}/tests/${examId}/student?studentId=${studentId}`;
    const params = new URLSearchParams();


    return this.httpService.get(API_URL, { params })
      .map(res => res.json());
  }

  
   // Obtener todas programaciones de exámenes por grupo
   getSchedulingpartial(groupId: number) {

    const API_URL = `${Appsettings.API_ENDPOINT_FULL}/schedulingpartial/GetByStudentGroup`;
    const params = new URLSearchParams();

    params.append('Id', groupId.toString());

    return this.httpService.get(API_URL, { params })
      .map(res => res.json());
  }

}
