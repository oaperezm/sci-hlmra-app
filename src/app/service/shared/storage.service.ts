import { Injectable, Inject } from '@angular/core';
import { PersistenceService, StorageType} from 'angular-persistence';
import { Router } from '@angular/router';
import { DOCUMENT } from '@angular/common';

@Injectable({
  providedIn: 'root'
})
export class StorageService {

  constructor(  private persistenceService: PersistenceService,
                private router: Router,
                @Inject(DOCUMENT) private document: any) { }

  get token (): string {
    return this.persistenceService.get( 'token', StorageType.LOCAL);
  }

  set token (token: string) {
    this.persistenceService.remove('token');
    this.persistenceService.set('token', token, { type: StorageType.LOCAL, timeout: 3600000 });
  }

  get refreshToken (): string {
    return this.persistenceService.get( 'refreshToken', StorageType.LOCAL);
  }

  set refreshToken (refreshToken: string) {
    this.persistenceService.remove('refreshToken');
    this.persistenceService.set('refreshToken', refreshToken, { type: StorageType.LOCAL, timeout: 18000000 });
  }

  get name (): string {
    return this.persistenceService.get( 'name', StorageType.LOCAL);
  }

  set name (name: string) {
    this.persistenceService.remove('name');
    this.persistenceService.set('name', name, { type: StorageType.LOCAL });
  }

  get userName (): string {
    return this.persistenceService.get( 'username', StorageType.LOCAL );
  }

  set userName( username: string ) {
    this.persistenceService.remove('username');
    this.persistenceService.set( 'username', username,  { type: StorageType.LOCAL } );
  }

  get email (): string {
    return this.persistenceService.get( 'email', StorageType.LOCAL );
  }

  set email (email: string) {
    this.persistenceService.remove('email');
    this.persistenceService.set( 'email', email, { type: StorageType.LOCAL} );
  }

  get rol(): string {
    return this.persistenceService.get( 'rol', StorageType.LOCAL );
  }

  set rol (rolId: string) {
    this.persistenceService.remove('rol');
    this.persistenceService.set( 'rol', rolId, { type: StorageType.LOCAL } );
  }

  get changePassword (): boolean {
    return this.persistenceService.get('changePassword', StorageType.LOCAL);
  }

  set changePassword (changePassword: boolean) {
    this.persistenceService.remove('changePassword');
    this.persistenceService.set( 'changePassword', changePassword, { type: StorageType.LOCAL} );
  }

  get permissions (): string {
    return this.persistenceService.get('permissions', StorageType.LOCAL);
  }

  set permissions ( permissions: string ) {
    this.persistenceService.remove('permissions');
    this.persistenceService.set( 'permissions', permissions, { type: StorageType.LOCAL} );
  }

  get nodes (): string {
    return this.persistenceService.get('nodes', StorageType.LOCAL);
  }

  set nodes ( nodes: string ) {
    this.persistenceService.remove('nodes');
    this.persistenceService.set( 'nodes', nodes, { type: StorageType.LOCAL} );
  }

  get profileImage (): string {
    return this.persistenceService.get('profileImage', StorageType.LOCAL);
  }

  set profileImage ( profileImage: string ) {
    this.persistenceService.remove('profileImage');
    this.persistenceService.set( 'profileImage', profileImage, { type: StorageType.LOCAL} );
  }

  get notifications (): string {
    return this.persistenceService.get('notifications', StorageType.LOCAL);
  }

  set notifications ( notifications: string ) {
    this.persistenceService.remove('notifications');
    this.persistenceService.set( 'notifications', notifications, { type: StorageType.LOCAL} );
  }

  get tokenMessage (): string {
    return this.persistenceService.get('tokenMessage', StorageType.LOCAL);
  }

  set tokenMessage ( tokenMessage: string ) {
    this.persistenceService.remove('tokenMessage');
    this.persistenceService.set( 'tokenMessage', tokenMessage, { type: StorageType.LOCAL} );
  }

  loggedIn (): boolean {
    if (this.token === undefined || this.token === '') {
      return false;
    } else {
      return true;
    }
  }

  resetLocalData(): void {
    this.persistenceService.removeAll(StorageType.LOCAL);
  }

  logoutRedirect(): void {
    this.resetLocalData();

    setTimeout(() => {
      this.document.location.href = 'https://hlmra.com';
      // this.document.location.href = 'https://www.recursosacademicos.com/'; //Produccion, Ricardo - Agregado
      //this.document.location.href = 'https://ra-front-prod2.azurewebsites.net/#/';//Desarrollo, Ricardo - Cambio
    }, 4000);
  }

  get breadRemenber (): string {
    return this.persistenceService.get( 'persistenceBread', StorageType.LOCAL );
  }

  set breadRemenber (bread: string) {
    this.persistenceService.set( 'persistenceBread', bread, { type: StorageType.LOCAL } );
  }

  get title (): string {
    return this.persistenceService.get( 'title', StorageType.LOCAL);
  }

  set title (title: string) {
    this.persistenceService.remove('title');
    this.persistenceService.set('title', title, { type: StorageType.LOCAL });
  }

  get showBackBotton (): boolean {
    return this.persistenceService.get( 'showBackBotton', StorageType.LOCAL);
  }

  set showBackBotton (showBackBotton: boolean) {
    this.persistenceService.remove('showBackBotton');
    this.persistenceService.set('showBackBotton', showBackBotton, { type: StorageType.LOCAL });
  }

  get content (): string {
    return this.persistenceService.get('content', StorageType.LOCAL);
  }

  set content ( content: string ) {
    this.persistenceService.remove('content');
    this.persistenceService.set( 'content', content, { type: StorageType.LOCAL} );
  }

  get nodeId (): number {
    return this.persistenceService.get('nodeId', StorageType.LOCAL);
  }

  set nodeId ( nodeId: number ) {
    this.persistenceService.remove('nodeId');
    this.persistenceService.set( 'nodeId', nodeId, { type: StorageType.LOCAL} );
  }
  // modal de bienvenida
  get showModal(): number {
    return this.persistenceService.get('number', StorageType.LOCAL);
  }

  set showModal ( number: number ) {
    this.persistenceService.remove('number');
    this.persistenceService.set( 'number', number, { type: StorageType.LOCAL} );
  }

  // Tour de menú principal
  get showTour(): number {
    return this.persistenceService.get('number', StorageType.LOCAL);
  }

  set showTour ( id: number ) {
    this.persistenceService.remove('id');
    this.persistenceService.set( 'id', id, { type: StorageType.LOCAL} );
  }

  // Tour de material didactico
  get showTourMaterial(): number {
    return this.persistenceService.get('showTourMaterial', StorageType.LOCAL);
  }

  set showTourMaterial( idMat: number ) {
    this.persistenceService.remove('showTourMaterial');
    this.persistenceService.set( 'showTourMaterial', idMat, { type: StorageType.LOCAL} );
  }

   // Tour de libros
   get showTourBooks(): number {
    return this.persistenceService.get('showTourBooks', StorageType.LOCAL);
  }

  set showTourBooks( idBook: number ) {
    this.persistenceService.remove('showTourBooks');
    this.persistenceService.set( 'showTourBooks', idBook, { type: StorageType.LOCAL} );
  }

  // Tour de generador de examenes
  get showTourGexan(): number {
    return this.persistenceService.get('showTourGexan', StorageType.LOCAL);
  }

  set showTourGexan( idBook: number ) {
    this.persistenceService.remove('showTourGexan');
    this.persistenceService.set( 'showTourGexan', idBook, { type: StorageType.LOCAL} );
  }

  // Tour de generador de examenes nuevo
  get showTourGexanNuevo(): number {
    return this.persistenceService.get('showTourGexanNuevo', StorageType.LOCAL);
  }

  set showTourGexanNuevo( idBook: number ) {
    this.persistenceService.remove('showTourGexanNuevo');
    this.persistenceService.set( 'showTourGexanNuevo', idBook, { type: StorageType.LOCAL} );
  }

  // Tour de generador de examenes nuevo
  get showTourQbank(): number {
    return this.persistenceService.get('showTourQbank', StorageType.LOCAL);
  }

  set showTourQbank( idBook: number ) {
    this.persistenceService.remove('showTourQbank');
    this.persistenceService.set( 'showTourQbank', idBook, { type: StorageType.LOCAL} );
  }
  // Tour de generador de examenes nuevo
  get showTourQbankAdd(): number {
    return this.persistenceService.get('showTourQbankAdd', StorageType.LOCAL);
  }

  set showTourQbankAdd( idBook: number ) {
    this.persistenceService.remove('showTourQbankAdd');
    this.persistenceService.set( 'showTourQbankAdd', idBook, { type: StorageType.LOCAL} );
  }

   // Tour de generador de examenes nuevo
   get showTourListExam(): number {
    return this.persistenceService.get('showTourListExam', StorageType.LOCAL);
  }

  set showTourListExam( idBook: number ) {
    this.persistenceService.remove('showTourListExam');
    this.persistenceService.set( 'showTourListExam', idBook, { type: StorageType.LOCAL} );
  }
  // Tour de generador de examenes nuevo
  get showTourAnoummce(): number {
    return this.persistenceService.get('showTourAnoummce', StorageType.LOCAL);
  }

  set showTourAnoummce( idBook: number ) {
    this.persistenceService.remove('showTourAnoummce');
    this.persistenceService.set( 'showTourAnoummce', idBook, { type: StorageType.LOCAL} );
  }
  // Tour de generador de examenes nuevo
  get showTourGenAnoummce(): number {
    return this.persistenceService.get('showTourGenAnoummce', StorageType.LOCAL);
  }

  set showTourGenAnoummce( idBook: number ) {
    this.persistenceService.remove('showTourGenAnoummce');
    this.persistenceService.set( 'showTourGenAnoummce', idBook, { type: StorageType.LOCAL} );
  }

  // Tour de generador de examenes nuevo
  get showTourCourse(): number {
    return this.persistenceService.get('showTourCourse', StorageType.LOCAL);
  }

  set showTourCourse( idBook: number ) {
    this.persistenceService.remove('showTourCourse');
    this.persistenceService.set( 'showTourCourse', idBook, { type: StorageType.LOCAL} );
  }

  // Tour de generador de examenes nuevo
  get showTourRepo(): number {
    return this.persistenceService.get('showTourRepo', StorageType.LOCAL);
  }

  set showTourRepo( idBook: number ) {
    this.persistenceService.remove('showTourRepo');
    this.persistenceService.set( 'showTourRepo', idBook, { type: StorageType.LOCAL} );
  }

  // Tour de generador de examenes nuevo
  get showTourForum(): number {
    return this.persistenceService.get('showTourForum', StorageType.LOCAL);
  }

  set showTourForum( idBook: number ) {
    this.persistenceService.remove('showTourForum');
    this.persistenceService.set( 'showTourForum', idBook, { type: StorageType.LOCAL} );
  }
  // Tour de generador de examenes nuevo
  get showTourAddSala(): number {
    return this.persistenceService.get('showTourAddSala', StorageType.LOCAL);
  }

  set showTourAddSala( idBook: number ) {
    this.persistenceService.remove('showTourAddSala');
    this.persistenceService.set( 'showTourAddSala', idBook, { type: StorageType.LOCAL} );
  }

}
