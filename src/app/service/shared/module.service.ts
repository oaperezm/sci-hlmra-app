import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ModuleService {

  constructor() { }

  getModule( rol: string) {
    const modules =
    {
      "Preescolar": [
        {
          "icon": "fas fa-book",
          "title": "Guías del maestro y recursos digitales",
          "url": "/resource/content/0"
        },
        {
          "title": "Documentos de gestión académica",
        },
        {
          "title": "Catálogo de servicios pedagógicos",
        },
        {
          "title": "Sitios de interés recomendados",
        },
        {
          "title": "Aplicaciones recomendadas",
        },
        {
          "title": "Libros de texto en PDF",
        },
		    {
          "title": "Libros de texto descargables",
        }
      ],
      "Primaria": [
        {
          "title": "Guías del maestro y recursos digitales",
        },
        {
          "title": "Documentos de gestión académica",
        },
        {
          "title": "Catálogo de servicios pedagógicos",
        },
        {
          "title": "Sitios de interés recomendados",
        },
        {
          "title": "Aplicaciones recomendadas",
        },
        {
          "title": "Generador de exámenes",
        },
        {
          "title": "Libros de texto en PDF",
        },
		    {
          "title": "Libros de texto descargables",
        }
      ],
      "Secundaria":  [
        {
          "title": "Guías del maestro y recursos digitales",
        },
        {
          "title": "Documentos de gestión académica",
        },
        {
          "title": "Generador de exámenes",
        },
        {
          "title": "Generador de evaluaciones del Plan Lector",
        },
        // {
        //   "title": "Serie de Inglés - Help!",
        // },
        {
          "icon": "fa fa-user-plus",
          "title": "Catálogo de servicios pedagógicos",
          "url": "/resource/services/3",
        },
        {
          "title": "Sitios de interés recomendados",
        },
        {
          "title": "Aplicaciones recomendadas",
        },
        {
          "title": "Libros de texto en PDF",
        },
		    {
          "title": "Libros de texto descargables",
        }
      ]
    };

    const module = modules[rol];

    return module;
  }

}
