import { Injectable, Inject } from '@angular/core';
import { DOCUMENT } from '@angular/platform-browser';
import { StorageService } from '../service.index';
import { Appsettings } from 'src/app/configuration/appsettings';

@Injectable({
  providedIn: 'root'
})
export class SettingsService {

  themeName: string;
  themeUrl: string;

  constructor( @Inject(DOCUMENT) private _document) { }

  aplicarTema( nodes ) {

    let themeSelected: string;
    const levelAccess = nodes.filter( x => x.parentId ===  124 )
                              .map( x =>  x.id );
      // themeSelected = 'default';
      themeSelected = 'secundaria';
      if( levelAccess.indexOf(144) > -1 ) {
        themeSelected = 'preescolar';
      }

      if( levelAccess.indexOf(143) > -1 ) {
        themeSelected = 'primaria';
      }

      if( levelAccess.indexOf(145) > -1 ) {
        themeSelected = 'secundaria';
      }


      //console.log(themeSelected);

      if ( themeSelected !== '' ) {
        const url = `./assets/css/branding/${ themeSelected }.css`;
        this._document.getElementById('branding').setAttribute('href', url );

        this.themeName = themeSelected;
        this.themeUrl = url;
      }
  }

  applyDefaultTheme() {
    const url = `./assets/css/branding/default.css`;
    this._document.getElementById('branding').setAttribute('href', url );
  }
}
