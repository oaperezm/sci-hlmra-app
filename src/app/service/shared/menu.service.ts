import { Injectable } from '@angular/core';
import { StorageService } from './storage.service';
@Injectable({
  providedIn: 'root'
})
export class MenuService {

  menu: any = [];
  constructor(  private _srvStorage: StorageService) {
    this.getMenu();
  }

  getMenu() {
    this.menu = [];
    if ( this._srvStorage.permissions.length ) {
      const menuJSON = JSON.parse( this._srvStorage.permissions);
      if ( menuJSON ) {
        this.menu = menuJSON.filter( x => x.showInMenu !== false );
      }
    }
    return this.menu;
  }
}
