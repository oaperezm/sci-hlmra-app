import { Injectable } from '@angular/core';
import { HttpService } from '../security/http.service';
import { StorageService } from '../shared/storage.service';
import { Http, Headers, URLSearchParams } from '@angular/http';
import { Appsettings } from '../../configuration/appsettings';
import { FileContent } from '../../models/catalogs/file.model';

import { HttpClient } from '@angular/common/http';

// Model
import { Event } from '../../models/content/event.model';
@Injectable({
  providedIn: 'root'
})
export class EventService {

   // URL del blog que vamos a trabajar con su REST API
   public URL = 'https://hlmra.com/';
   public API = `${this.URL}wp-json/wl/v1/acceso`;

  constructor(  public storageService: StorageService,
                private httpService: HttpService,
                private http: HttpClient) { }


  // Guarda un registro de evento de clic
  addEventClic( data: Event) {
    const API_URL = `${Appsettings.API_ENDPOINT_FULL}/EventReport`;
    const HEADERS = new Headers();
    HEADERS.append('Content-Type', 'application/json');

    // const params = {
    //   'id': contentId,

    // };

   return this.httpService.post(API_URL, JSON.stringify( data ), { headers: HEADERS } )
                           .map( res => res.json());
  }


  getUserWp() {
    // return this.httpService.get(API_URL, JSON.stringify( data ), { headers: HEADERS } )
    //                        .map( res => res.json());

    // return  this.httpService.get(API_URL);

    return this.http.get(`${this.API}`);

  }
}
