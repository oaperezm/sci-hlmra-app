import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class QuestionsService {

  constructor() { }

  getQuestions() {
    const questions =   [
        {
            'question': '¿Qué es el Portal de Portal Recursos Académicos de Hachette Livre México?',
            'response': 'Es un sitio en la WEB que ofrece una serie de recursos desarrollados por un equipo de pedagogos y expertos en diversas áreas del conocimiento que buscan mejorar de manera lúdica, interactiva y tecnológica el proceso de enseñanza-aprendizaje dentro del aula.'
        },
        {
            'question': '¿Qué debo hacer para suscribirme al Portal Recursos Académicos de Hachette Livre México?',
            'response': 'El Portal Recursos Académicos de Hachette Livre México está dirigido a usuarios de los materiales tanto de Ediciones Larousse como de Grupo Editorial Patria en cualquiera de los niveles de educación básica.'
        },
        {
            'question': '¿Qué beneficios me aporta el Portal Recursos Académicos de Hachette Livre México?',
            'response': 'Estimular la función de todos los sentidos en los alumnos dentro del aula, de esta manera integrar los diferentes estilos de aprendizaje (de acuerdo con la neurolingüística VAK) y brindar mayor accesibilidad en el desarrollo de las competencias.'
        },
        {
            'question': 'En mi Colegio no se han entregado los folios de acceso al Portal Recursos Académicos de Hachette Livre México, ¿a quién me tengo que dirigir? o ¿de qué manera puedo recibirlos?',
            'response': 'Es importante informar al representante de zona, pues él es el contacto directo con la editorial y será quien le indique qué realizar. Se recomienda checar las bandejas de entrada como de correos no deseados de la persona contacto constantemente.'
        },
        {
            'question': 'El Portal Recursos Académicos de Hachette Livre México no reconoce mi usuario y/o contraseña, ¿qué debo hacer?',
            'response': 'Se recomienda ponerse en contacto por medio del siguiente correo electrónico y tener la información de registro a la mano; con ello se le proporcionará la ayuda necesaria: vacosta@larousse.com.mx.'
        },
        {
            'question': '¿Qué tipo de usuarios admite el Portal Recursos Académicos de Hachette Livre México?',
            'response': 'El Portal Recursos Académicos de Hachette Livre México sólo admite a DOCENTES USUARIOS DE LOS MATERIALEES DE EDUCACIÓN BÁSICA de Ediciones Larousse y/o Grupo Editorial Patria.'
        },
        {
            'question': '¿En qué consiste la selección de los contenidos del Portal Recursos Académicos de Hachette Livre México?',
            'response': '<ul><li> Documentos de Gestión Escolar, donde encontrará formatos imprimibles de uso diario, como son el control de Asistencia, Horario y el Formato de seguimiento de Competencias Lectoras (exclusivo Primaria), así como diversos documentos, información, ligas de apoyo al docente tales como los Planes de Estudio 2011.</li><li> Recursos didácticos, los cuales están divididos por nivel Educativo teniendo los siguientes tipos de recursos:</li><li> Actividades imprimibles que indican la secuencia didáctica a emplear, desde la fase de preparación hasta sugerencias de indicadores de evaluación.</li><li> Podcast que apoyan diversos temas.</li><li> Videos de apoyo para ampliar información.</li><li> Aplicaciones interactivas para que el alumno aplique los conocimientos adquiridos de una manera más ágil y divertida.</li><li>Los recursos que ofrece el Portal de Recursos Académicos Hachette son adaptables a las necesidades escolares ya que pueden ser ubicados según las características de cada nivel.</li></ul>'
        },
        {
            'question': '¿Con qué dispositivo electrónico es recomendable trabajar el Portal Recursos Académicos de Hachette Livre México?',
            'response': 'El Portal de Recursos Académicos de Hachette podrá visualizarse y trabajarse en cualquier dispositivo electrónico, desde una computadora de escritorio, una Lap Top, una tableta, un Ipad y hasta un teléfono celular; no importa si el sistema operativo es compatible con Android o Ios.'
        },
        {
            'question': '¿El Portal Recursos Académicos de Hachette Livre México es compatible para trabajarse en el pizarrón electrónico o interactivo?',
            'response': 'Sí, es la mejor manera de apoyar el proceso desde adentro del aula, por la retroalimentación y confrontamiento de los procedimientos que genera la ejecución y resolución de las actividades.'
        },
        {
            'question': '¿Cuáles son los recursos del Portal Recursos Académicos de Hachette Livre México que sí puedo descargar?',
            'response': '<p>Los materiales descargables se encuentran en su mayoría dentro de la ventana GESTOR ACADÉMICO y son todos los formatos útiles dentro del salón de clases:</p><ul><li>Matrícula de los alumnos</li><li>Lista de Asistencias</li><li>Promedios finales</li><li>Formato de Comprensión Lectora (aula)</li><li>Registro de Comprensión Lectora (casa)</li><li>Horario</li><li>Planeador mensual</li></ul>'
        },
        {
            'question': '¿Hasta qué día estará activa mi cuenta en el Portal Recursos Académicos de Hachette Livre México?',
            'response': 'La cuenta se activa una vez que la recibe y se dará de baja un día después del término del ciclo escolar.'
        },
        {
            'question': '¿Cómo puedo suscribirme al Portal Recursos Académicos de Hachette Livre México?',
            'response': 'El primer paso es ser usuari@ de los materiales de Ediciones Larousse o Grupo Editorial Patria, una vez cumplido éste, recibirá los accesos (entregado al dueño, director, coordinador y/o contacto), cumplido lo anterior, el acceso lo podrá realizar de manera inmediata.'
        },
        {
            'question': '¿Puedo compartir mi usuario y contraseña del Portal Recursos Académicos de Hachette Livre México a otro docente?',
            'response': 'No es recomendable compartir la información, debido a que se llevará el registro de todas las actividades realizadas durante el ciclo escolar, lo cual nos permitirá evaluar el trabajo realizado e incrementar la cantidad y tipo de recursos por asignatura.'
        },
        {
            'question': '¿Qué necesito tener instalado para visualizar y trabajar con el Portal Recursos Académicos de Hachette Livre México?',
            'response': 'Al ser un sitio de internet lo único indispensable para visualizar y comenzar a trabajar con el Portal Recursos Académicos de Hachette Livre México es una buena conexión a internet.'
        },
        {
            'question': 'Uno de los recursos (aplicaciones interactivas) del Portal Recursos Académicos de Hachette Livre México no tiene solución, ¿qué puedo hacer?',
            'response': 'Sugerimos revisar el apartado de CATÁLOGOS en la parte final de RECURSOS (en cada nivel), donde se encuentra la TABLA DE RESPUESTAS DE ACTIVIDADES INTERACTIVAS.'
        },
        {
            'question': '¿Puedo controlar el acceso de los alumnos al Portal Recursos Académicos de Hachette Livre México?',
        
            'response': 'Durante esta primera etapa del Portal Recursos Académicos de Hachette Livre México es sólo para DOCENTES.'
        },
        {
            'question': '¿En qué consiste la selección de los contenidos del Portal Recursos Académicos de Hachette Livre México?',
            'response': '<p>Los contenidos del Portal Digital de Recursos Académicos de Hachette Livre México fueron seleccionados con la intención de darle a los maestros y maestras de Educación Básica apoyo en las áreas académicas más importantes, que se irán incrementando con frecuencia, así como en tareas esenciales de su labor docente de acuerdo con el grado y nivel educativo que atienden.</p><p>De esta manera, para Preescolar se incluyen materiales para reforzar la adquisición del conocimiento y la práctica del abecedario en tipo script y cursiva y actividades manuales del campo de las artes; para Primaria, ejercicios para desarrollo de habilidades lectoras y caligrafía, y para secundaria, actividades prácticas para las asignaturas de Ciencias y Tecnología y líneas del tiempo.</p><p>En cuanto al Gestor académico, se incluyen formatos para el registro de logros en lectura individuales y grupales, reportes de evaluación para Primaria y Secundaria, calendario escolar mes a mes, y otros.</p>'
        },
        {
            'question': 'No recuerdo el usuario o/y contraseña, ¿qué debo hacer?',
            'response': '<p>Lo primero que puede hacer es ver si su navegador web ha almacenado los datos de su cuenta, en caso de que no lo obtenga, le pedimos de favor contactarnos al siguiente correo electrónico: vacosta@larousse.com.mx'
        },
        {
            'question': '¿Los alumnos tienen acceso al Portal Recursos Académicos de Hachette Livre México?',
            'response': 'Aún seguimos trabajando para brindar este servicio para el ciclo escolar 2019-2020; sin embargo, si este elemento es importante puede contactarlo vía correo electrónico.'
        },
        {
            'question': 'Aún no cuento con folio para accesar al Portal Recursos Académicos de Hachette Livre México México, ¿qué debo hacer?',
            'response': '<p>Recordemos que el portal está dirigido para usuarios de Ediciones Larousse y Grupo Editorial Patria, si ya es usuario, lo primero que se debe hacer es preguntar a la directora o coordinadora si ya recibieron los folios. De no ser así, se tienen dos opciones:</p><ul><li>Revise en el correo electrónico la bandeja de correos no deseados, algunas veces si no se registra la dirección se envían a esa bandeja por seguridad. Si se encontraron aquí, imprima la carta y entregue a cada docente el folio correspondiente al nivel que imparte.</li><li>Si no se cuenta con la información, lo mejor será comunicarse con el representante de ventas directamente para solicitarlos y se entreguen de manera física en la institución.</li></ul>'
        },
        {
            'question': 'Quisiera compartir material en el Portal Recursos Académicos de Hachette Livre México, ¿qué debo hacer?',
            'response': '<p>No sólo eso, el material que se quiera compartir deberá enviarse por correo electrónico con la siguiente información:</p><ul><li>Nombre del docente que elaboro el material</li><li>Nombre del colegio en el que labora</li><li>Título del material</li><li>Grado</li><li>Campo formativo al que pertenece</li><li>Asignatura a la que pertenece</li><li>Bloque al que pertenece</li><li>Competencia a desarrollar</li><li>Aprendizaje esperado por alcanzar</li></ul><p>Es importante mencionar que ni Ediciones Larousse ni Grupo Editorial Patria, remunerarán o gratificarán de manera económica por el material que algún docente quiera compartir en el Portal de Recursos Académicos de Hachette Livre México, sin embargo se podrá visualizar el nombre del colegio, así como de quien lo elaboró. Al abrir dicho material, le haremos llegar toda la información que necesite.</p>'
        },
        {
            'question': '¿Cuántas descargas puedo hacer de los materiales descargables del Portal Recursos Académicos de Hachette Livre México?',
            'response': 'Las que sean necesarias a lo largo del ciclo escolar. Al tratarse de un PORTAL dentro de la web, usted puede descargar (los materiales que así lo permitan) tantas veces quiera y en los dispositivos que desee.'
        },
        {
            'question': '¿Necesito estar  conectado siempre a una red para trabajar con el Portal Recursos Académicos de Hachette Livre México?',
            'response': '<p>Al trabajar con archivos en una red se puede hacer que los archivos estén disponibles sin conexión de manera que pueda obtener acceso a los mismos aunque el equipo no esté conectado a la red.</p><p>Si hace que un archivo esté disponible sin conexión, el sistema operativo crea una copia del archivo en el equipo. La copia se denomina archivo sin conexión. Cuando se desconecte de la red, podrá abrir, modificar y guardar el archivo de la misma manera que si estuviera conectado. Cuando vuelva a conectarse a la red, el sistema sincronizará automáticamente el archivo sin conexión del equipo con el archivo correspondiente de la red.</p><p>Esto es especialmente útil si usa un equipo portátil para conectarse a una red del lugar de trabajo.</p>'
        },
        {
            'question': '¿Para qué se utilizan los datos de registro al Portal Recursos Académicos de Hachette Livre México?',
            'response': 'El uso de registros y de fuentes de datos administrativos es meramente para fines estadísticos; con el fin de enriquecer cada ciclo escolar los recursos con los que se cuenta, cubriendo de esta manera: intereses y necesidades de la comunidad usuarios de materiales Larousse y Grupo Editorial Patria.'
        },
        {
            'question': '¿Cómo puedo solicitar que un Representante visite mi Colegio?',
            'response': 'Si su colegio aún no ha sido visitado por algún representante de Ediciones Larousse o Grupo Editorial Patria y desea que se realice por primera vez, por favor, envíe un correo electrónico a la siguiente dirección para programar la visita de un asesor a la dirección que usted indique.'
        },
        {
            'question': '¿Cómo puedo contactarlos?',
            'response': '<p>Si tienes cualquier duda puedes contactar con nosotros a través de vacosta@larousse.com.mx  o en:</p><ul><li>Renacimiento 180</li><li>Colonia San Juan Tlihuaca</li><li>Delegación Azcapotzalco</li><li>Código Postal 02400, Ciudad de México.</li><li>Tel: (01 55) 1102 1300</li><li>(01 55) 5354 9100</li><li>Fax: (01 55) 5354 9109</li></ul>'
        },
        {
            'question': 'Quiero recibir avisos, novedades y promociones.',
            // tslint:disable-next-line:max-line-length
            'response': '<p>Ediciones Larousse y Grupo Editorial Patria cuentan, cada una, con sitio web donde podrá encontrar todas nuestras novedades. Visite estas ligas y conózcalas:</p><ul><li><a href="http://www.larousse.mx/">http://www.larousse.com.mx/</a></li><li><a href="http://www.editorialpatria.com.mx/">http://www.editorialpatria.com.mx/</a></li></ul>'
        }
  ];

  return questions;

  }
}
