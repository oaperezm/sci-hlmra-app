import { Injectable } from '@angular/core';
import { HttpService } from '../security/http.service';
import { Headers, URLSearchParams } from '@angular/http';
import { Appsettings } from '../../configuration/appsettings';
import { Observable } from 'rxjs/Observable';
import { forkJoin } from 'rxjs';  // RxJS 6 syntax
@Injectable({
  providedIn: 'root'
})
export class CatalogService {

  constructor(  private httpService: HttpService  ) { }

  // Obtener todos los contenidos registrados
  getLotationAll () {

    const API_URL = `${Appsettings.API_ENDPOINT_FULL}/locations`;
    return this.httpService.get( API_URL )
                           .map( res => res.json());
  }

  // Obtener todos los propositos registrados
  getPurposesAll () {

    const API_URL = `${Appsettings.API_ENDPOINT_FULL}/purposes`;
    return this.httpService.get( API_URL )
                           .map( res => res.json());
  }

  // Obtener todas las areas registradas
  getAreasAll () {

    const API_URL = `${Appsettings.API_ENDPOINT_FULL}/areas`;
    return this.httpService.get( API_URL )
                           .map( res => res.json());
  }

  // Obtener todos los bloques registrados
  getBlockAll () {

    const API_URL = `${Appsettings.API_ENDPOINT_FULL}/blocks`;
    return this.httpService.get( API_URL )
                           .map( res => res.json());
  }

  // Obtener todos los campos de entrenamiento
  getTrainingFieldskAll () {

    const API_URL = `${Appsettings.API_ENDPOINT_FULL}/trainingfields`;
    return this.httpService.get( API_URL )
                           .map( res => res.json());
  }

  // Obtener todos los campos de formación registrados
  getFormativeFieldskAll () {

    const API_URL = `${Appsettings.API_ENDPOINT_FULL}/formativefields`;
    return this.httpService.get( API_URL )
                           .map( res => res.json());
  }

  // Obtener todos los grados registrados
  getGradesAll () {

    const API_URL = `${Appsettings.API_ENDPOINT_FULL}/grades`;
    return this.httpService.get( API_URL )
                           .map( res => res.json());
  }

  // Obtener todos los subsistemas registrados
  getSubSystemAll () {

    const API_URL = `${Appsettings.API_ENDPOINT_FULL}/subsystems`;
    return this.httpService.get( API_URL )
                           .map( res => res.json());
  }

  // Obtener todos los roles registrados
  getRoleAll () {

    const API_URL = `${Appsettings.API_ENDPOINT_FULL}/roles`;
    return this.httpService.get( API_URL )
                           .map( res => res.json());
  }

  // Obtener todos los roles registrados
  getQuestionTypesAll () {

    const API_URL = `${Appsettings.API_ENDPOINT_FULL}/questiontypes`;
    return this.httpService.get( API_URL )
                           .map( res => res.json());
  }

  // Obtener todos los tipos de nodo
  getNodenTypesAll () {

    const API_URL = `${Appsettings.API_ENDPOINT_FULL}/nodetypes`;
    return this.httpService.get( API_URL )
                           .map( res => res.json());
  }


  // Obtener todos los tipos de nodo
  getFileTypesAll () {

    const API_URL = `${Appsettings.API_ENDPOINT_FULL}/filetypes`;
    return this.httpService.get( API_URL )
                           .map( res => res.json());
  }

   // Obtener todos los tipos de nodo
   getContentTypesAll () {

    const API_URL = `${Appsettings.API_ENDPOINT_FULL}/contenttypes`;
    return this.httpService.get( API_URL )
                           .map( res => res.json());
  }

   // Obtener todos los tipos de nodo
   getResourceTypesAll () {

    const API_URL = `${Appsettings.API_ENDPOINT_FULL}/resourcestypes`;
    return this.httpService.get( API_URL )
                           .map( res => res.json());
  }

  // Obtiene el contenido relacionado
  getResourceShare( NodeId: number) {
    const API_URL = `${Appsettings.API_ENDPOINT_FULL}/contents/${ NodeId }/relation/?currentPage=1&pageSize=10`;
    return this.httpService.get( API_URL )
                           .map( res => res.json());
  }
  // Obtiene los cursos de un grupo
  getAllCourseStudent( ) {
    const API_URL = `${Appsettings.API_ENDPOINT_FULL}/students/courses?blockId=0`;
    return this.httpService.get( API_URL )
                           .map( res => res.json());
  }

  // valida si el token se session esta activo
  validateToken() {

    const API_URL = `${Appsettings.API_ENDPOINT_FULL}/users/validateSession`;
    return this.httpService.get( API_URL );
  }


  /**************
   * Nuevos Catalogo para estrucutra
  ***************/
  // Niveles educativos
  getEducationLevelAll() {
    const API_URL = `${Appsettings.API_ENDPOINT_FULL}/EducationLevel`;
    return this.httpService.get( API_URL )
                           .map( res => res.json());
  }
  // EJE / AMBITO
  getAxisAll() {
    const API_URL = `${Appsettings.API_ENDPOINT_FULL}/Axis`;
    return this.httpService.get( API_URL )
                           .map( res => res.json());
  }
  // Aprendizaje clave
  getKeyLearningAll() {
    const API_URL = `${Appsettings.API_ENDPOINT_FULL}/Keylearning`;
    return this.httpService.get( API_URL )
                           .map( res => res.json());
  }
  // Aprendizaje esperado
  getLearningExpectedAll() {
    const API_URL = `${Appsettings.API_ENDPOINT_FULL}/Learningexpected`;
    return this.httpService.get( API_URL )
                           .map( res => res.json());
  }

  // Ámbitos de autonomía curricular
  getAreasCurriculumAutonomyAll() {
    const API_URL = `${Appsettings.API_ENDPOINT_FULL}/AreasCurriculumAutonomy`;
    return this.httpService.get( API_URL )
                           .map( res => res.json());
  }
  // Area de desarrollo personal y solcial
  getAreaPersonalSocialDevelopmentAll() {
    const API_URL = `${Appsettings.API_ENDPOINT_FULL}/AreaPersonalSocialDevelopment`;
    return this.httpService.get( API_URL )
                           .map( res => res.json());
  }

   public requestCatalogos(): Observable<any[]> {
    // let response1 = this.getPurposesAll(); // Proposito
    // let response4 = this.getLearningExpectedAll(); // Aprendizaje esperado
    let response1 = this.getAreaPersonalSocialDevelopmentAll(); // Area de desarrollo personal y solcial
    let response2 = this.getAreasCurriculumAutonomyAll(); // Ámbitos de autonomía curricular
    let response3 = this.getAxisAll(); // Eje / ambito
    let response4 = this.getKeyLearningAll(); // Aprendizaje clave
    let response5 = this.getLearningExpectedAll(); // Aprendizaje esperado
    let response6 = this.getPurposesAll(); // Proposito
    let response7 = this.getEducationLevelAll(); // Nivel educativo
    let response8 = this.getFormativeFieldskAll(); // Campo formativo
    return forkJoin([response1, response2, response3, response4, response5, response6, response7, response8]);

  }
}
