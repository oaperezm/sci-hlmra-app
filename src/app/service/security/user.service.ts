import { Injectable } from '@angular/core';
import { HttpService } from './http.service';
import { Http, Headers, URLSearchParams } from '@angular/http';
import { Appsettings } from '../../configuration/appsettings';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: Http,
    private httpService: HttpService) { }


  // Método que autentica un usuario en la aplicación
  login( userName: string, password: string ) {
    const apiUrl = `${Appsettings.API_ENDPOINT}/token`;
    const headers = new Headers();
    const body = `username=${userName}&password=${password}&grant_type=password&client_id=ngAuthApp&origen_id=${Appsettings.SYSTEM_ID}`;
    headers.append('Content-Type', 'application/x-www-form-urlencoded');

    return this.http
      .post(apiUrl, body, { headers: headers })
      .map(res => res.json());
  }

  // Método que realiza el registro de un nuevo usuario con rol de alumno o maestro
  register( user ) {

    const apiUrl = `${Appsettings.API_ENDPOINT_FULL}/users`;
    const headers = new Headers();

    headers.append('Content-Type', 'application/json');

    return this.http
      .post(apiUrl, JSON.stringify(user), { headers: headers })
      .map(res => res.json());
  }

  // Método que realiza petición al API para reiniciar la contraseña de un usuario
  resetPassword( email: string ) {

    const apiUrl = `${Appsettings.API_ENDPOINT_FULL}/users/resetpassword`;
    const headers = new Headers();

    headers.append('Content-Type', 'application/json');
    const params = {
      'email': email,
      'systemId': Appsettings.SYSTEM_ID
    };

    return this.httpService
      .post(apiUrl, JSON.stringify(params), { headers: headers })
      .map(res => res.json());
  }

  getProfile() {
    const apiUrl = `${Appsettings.API_ENDPOINT_FULL}/users/profile?origin=2`;
    return this.httpService.get(apiUrl)
      .map(res => res.json());
  }

  logOut() {
    const apiUrl = `${Appsettings.API_ENDPOINT_FULL}/users/logout`;
    return this.httpService.get(apiUrl)
      .map(res => res.json());
  }

  getNotifications( page: number, size: number ) {

    const apiUrl = `${Appsettings.API_ENDPOINT_FULL}/users/all-notifications`;
    const parameters = new URLSearchParams();

    parameters.append('page', page.toString());
    parameters.append('sizePage', size.toString());

    return this.httpService.get(apiUrl, { params: parameters })
      .map(res => res.json());

  }

  readNotification( id: number ) {

    const apiUrl = `${Appsettings.API_ENDPOINT_FULL}/users/read`;
    const headers = new Headers();

    headers.append('Content-Type', 'application/json');

    const params = { id: id };

    return this.httpService.put(apiUrl, JSON.stringify(params), { headers: headers })
      .map(res => res.json());
  }

  update( user ) {

    const apiUrl = `${Appsettings.API_ENDPOINT_FULL}/users`;
    const headers = new Headers();

    headers.append('Content-Type', 'application/json');

    return this.httpService
      .put(apiUrl, JSON.stringify(user), { headers: headers })
      .map(res => res.json());
  }

  uploadImg( user, imgProfile ) {
    const apiUrl = `${Appsettings.API_ENDPOINT_FULL}/users/uploadImgProfile`;
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');


    const params = {

      'fileContentBase64': imgProfile.content,
      'fileName': imgProfile.name
    };

    return this.httpService
      .put(apiUrl, JSON.stringify(user), { headers: headers })
      .map(res => res.json());
  }

  getProfileData() {
    const apiUrl = `${Appsettings.API_ENDPOINT_FULL}/users/profiledata`;
    return this.httpService.get(apiUrl)
      .map(res => res.json());
  }

  changePassword( Email: string, NewPassword: string, OldPassword: string ) {

    const apiUrl = `${Appsettings.API_ENDPOINT_FULL}/users/changepassword`;
    const headers = new Headers();

    headers.append('Content-Type', 'application/json');
    const params = {
      'Email': Email,
      'NewPassword': NewPassword,
      'OldPassword': OldPassword,
    };

    return this.httpService
      .put(apiUrl, JSON.stringify(params), { headers: headers })
      .map(res => res.json());
  }

  // Método que autentica un usuario en la aplicación
  sendMail( contentEmail ) {

    const apiUrl = `${Appsettings.API_ENDPOINT}/api/user/send`;
    const headers = new Headers();

    headers.append('Content-Type', 'application/json');
    return this.http.post(apiUrl, contentEmail, { headers: headers })
      .map(res => res.json());
  }

  updateDevice( token: string ) {

    const apiUrl = `${Appsettings.API_ENDPOINT_FULL}/users/registerDevice`;
    const headers = new Headers();

    headers.append('Content-Type', 'application/json');

    const device = {
      'deviceId': '',
      'token': token,
      'clientId': 1
    };

    return this.httpService.post(apiUrl, JSON.stringify(device),
      { headers: headers })
      .map(res => res.json());

  }

  sendContactMail( contentEmail ) {

    const apiUrl = `${Appsettings.API_ENDPOINT}/api/mails/send`;
    const headers = new Headers();

    headers.append('Content-Type', 'application/json');
    return this.http.post(apiUrl, contentEmail, { headers: headers })
                    .map(res => res.json());

  }

  // Validar usuario en side
  validateUserSIDE ( email: string ) {
    const apiUrl = `${Appsettings.API_ENDPOINT_FULL}/users/side?email=${ email}`;
    return this.httpService.get(apiUrl)
      .map(res => res.json());
  }
  // Validar usuario en side
  getaDataUserSIDE ( email: string ) {
    const apiUrl = `${Appsettings.API_ENDPOINT_FULL}/users/cctside?email=${ email}`;
    return this.httpService.get(apiUrl)
      .map(res => res.json());
  }

  insertEventSIDE () {
    // const apiUrl = `${Appsettings.API_ENDPOINT_FULL}/`;
    // const headers = new Headers ();

    // headers.append('Content-Type', 'application/json');
    // return this.httpService.post(apiUrl, { headers: headers })
    // .map(res => res.json());
  }

  gerUserProfileData( userId: number) {
    const apiUrl = `${Appsettings.API_ENDPOINT_FULL}/users/GetUserProfileData?userId=${ userId }`;
    return this.httpService.get(apiUrl)
      .map(res => res.json());
  }


   // Método que realiza el registro de un nuevo usuario con rol de alumno o maestro
  userActiveCount( email: string ) {

    const API_URL = `${Appsettings.API_ENDPOINT_FULL}/users/confirmSuscripcion?email=${email.toString()}`;
    const params = new URLSearchParams();
    return this.httpService.get(API_URL, { params })
      .map(res => res.json());


  }
}

