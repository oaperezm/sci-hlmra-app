import { Injectable } from '@angular/core';
import { Appsettings } from '../../configuration/appsettings';
import { HttpClient } from '@angular/common/http';

// Modelo

@Injectable({
  providedIn: 'root'
})
export class PointService {

  constructor(private http: HttpClient) { }


  // OBTENER CODIGO CONSECUTIVO PARA CANJEAR CUPON
  getAll () {

    const API_URL = `${Appsettings.API_ENDPOINT_FULL}/point`;
    return this.http.get( API_URL );

  }

  
}
