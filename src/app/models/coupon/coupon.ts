export class Coupon {
    discount_type: string;
    individual_use: boolean;
    date_expire_gmt: Date;
    code: string;
    amount: number;
}