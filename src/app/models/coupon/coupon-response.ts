export class CouponResponse {

    id: number;
    code: string;
    amount: number;
    date_created: Date;
    date_created_gmt: Date;
    date_modified: Date;
    date_modified_gmt: Date;
    discount_type: string;
    description: string;
    date_expires?: Date;
    date_expires_gmt?: Date;
    usage_count: number;
    individual_use: boolean;
    product_ids: any[];
    excluded_product_ids: any[];
    usage_limit: any;
    usage_limit_per_user: any;
    limit_usage_to_x_items: any;
    free_shipping: boolean;
    product_categories: any[];
    excluded_product_categories: any[];
    exclude_sale_items: boolean;
    minimum_amount: number;
    maximum_amount: number;
    email_restrictions: any[];
    used_by: any[];
    meta_data: any[];
    _links: Link;
}

export class Link {
    self: any[];
    colletion: any[];
}