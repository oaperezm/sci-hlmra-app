export class Node {
  active: boolean;
  description: string;
  id: number;
  nodeTypeDesc: string;
  nodeTypeId: number;
  parentId: number;
  urlImage: string;
  accessType: number;
  accessTypeDescription: string;
}
