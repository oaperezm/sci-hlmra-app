export class Content {
  active: boolean;
  area: string;
  content: any[];
  contentResourceType: number;
  contentTypeDesc: string;
  contentTypeId: number;
  fileTypeDesc: string;
  fileTypeId: number;
  formativeField: boolean;
  id: number;
  isPublic: boolean;
  name: string;
  purpose: boolean;
  thumbnails: string;
  totalContent: number;
  trainingField: string;
  urlContent: string;
  block: string;
  description: string;
  purposeId: number;

  axis: string;
  formativeFieldName: string;
  purposeName: string;
}
