export class Event {
  //  Id: number;
  // //  CCT del side
  //  cct: string;
  // // Correo del usuario
  //  email: string;
  // // Nombre del usuario
  // username: string;
  // // Nombre de la Escuela
  // institutionName: string;

  // level: string;
  // // Nombre del estado
  // state: string;
  // // Clasificación de escuela
  // clasificationSchool: string;
  // // Nombre de la zona
  // zone: string;
  // // Nombre del control
  // control: string;
  // // Nombre del Promotor
  // prometer: string;
  // // Fecha de ingreso
  // entryDate: Date;
  // // Tiempo de navegación
  // timeNavegation: string;
  // // Nombre de sección
  // sections: string;
  // // El id de la secciom
  // sectionId: number
  // // Nombre del grado
  // grade: string;
  // // Nombre del Campo formativo
  // educationField: string;
  // // Nobre de la asignature
  // subject: string;
  // //  tipo del formato
  // format: string;
  // // Nombre del recurso
  // resourcenName: string;

  // // Registros específicos en los que se realizó la acción
  // element: string;
  // // Comentario
  // comment: string;
  // // Detalle de comentario
  // commentDetail: string;

  // eventId: number;

  // fileTypeId: number;
  // educationLevelId: number;
  // contentTypeId: number;

  // userId:  number;
  // registerDate: Date;
  id: Number;
  cct: string;
  email: string;
  username: string;
  institutionName: string;
  userId: number;
  level: string;
  state: string;
  clasificationSchool: string;
  zone: string;
  control: string;
  prometer: string;
  entryDate: Date;
  timeNavegation: string;
  subject: string;
  sectionId: number;
  grade: string;
  educationField: string;
  fileTypeId: number;
  registerDate: Date;
  eventId: number;
  element: string;
  comment: string;
  commentDetail: string;
  educationLevelId: number;
  contentTypeId: number;
  ResourceName: string;
}


// Guardado
/*


public int FileTypeId { get; set; }
//id del usuario
public int? UserId { get; set; }
public int EducationLevelId { get; set; }
public int ContentTypeId { get; set; }
*/
