export class TeacherContent {
  id: number;
  name: string;
  registerDate: Date;
  description: string;
  contentUrl: string;
  selected: boolean;
  resourceTypeId: number;
}
