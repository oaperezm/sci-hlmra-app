import { Group } from './group.model';
import { Course } from '../catalogs/course.model';
export class Announcement {
  id: number;  
  title: string;
  description: string;
  registerDate: Date;  
  active: boolean;
  courses: Course[];
  index: number;
  groupId: number = 0;
  groups: Group[];
  selected: boolean;
}
