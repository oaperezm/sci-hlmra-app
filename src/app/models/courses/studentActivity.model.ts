export class StudenActivity {
  id: number;
  comment: string;
  fullName: string;
  hasActivity: boolean;
  activityId: number;
  score: number;
  files: any[];
  userId: number;
  statusFiles: boolean;
  delivered: boolean;
  addNewAnswers: boolean;
  typeScore: boolean;
}
