import {SchedulingPlartial} from './schedulingPartial.model';

export class TeacherFinalScore {
  id: number;
  studentGroupId: number;
  studentGroupDescription: string;
  status: number;
  studentId: number;
  studentName: string;
  homework: number;
  exam: number;
  assitance: number;
  activity: number;
  partial: number;
  comment: string;
  score: number;
  points: number;
  partials: SchedulingPlartial [];
  userId: number;
   //Ricardo - Agregado - Inicio
   percentageActivity :number;
   percentageAssitance :number;
   percentageExam :number;
   percentageHomework :number;
//Ricardo - Agregado - Fin

}
