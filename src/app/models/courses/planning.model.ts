export class Planning {
  id: number;
  homework: number;
  exam: number;
  assistance: number;
  activity: number;
  numberPartial: number;
  studentGroupId: number;
}
