export class TaskSchedule {
  id: number;
  name: string;
  scheduledDate: Date;
  description: string;
  studentGroupId: number;
  filesId: number[];
  score: number;
  comment: string;
  openingDate: Date;
  typeOfQualification: number;
  assignedQualification: number;
  SchedulingPartialId: number;
  qualified: boolean;
  schedulingPartialDescription: string;
  delivered: boolean;
  addNewAnswers: boolean;
  typeScore: boolean;
  isQualified: boolean;
  totalAnswers: number;

}
