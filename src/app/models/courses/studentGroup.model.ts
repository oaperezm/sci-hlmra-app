export class StudentGroup {
  id: number;
  description: string;
  courseId: number;
  code: string;
  pass: string;
}
