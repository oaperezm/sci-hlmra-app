export class QuestionBankUpload {
  content: string;
  explanation: string;
  questionTypeId: number;
  answers: any[];
  selected: boolean;
  questionType: string;
}
