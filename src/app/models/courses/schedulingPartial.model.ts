export class SchedulingPlartial {
  id: number;
  startDate: Date;
  endDate: Date;
  description: string;
  finalPartial: number;
  coursePlanningId: number;
  activity?: number;
  assistance?: number;
  comment?: string;
  exam?: number;
  homework?: number;
  partialperiod?: string;
  points?: number;
  score?: number;
  studentGroupId?: number;
  schedulingPartialId?: number;
  status?: number;
  registerDate?: Date;
  studentId?: number;
  userId: number;


constructor(_id?: number, _description?: string, _activity?: number, _assistance?: number, _comment?: string, _exam?: number,
            _homework?: number, _partialperiod?: string, _points?:number, _score?: number, _studentGroupId?: number ) {

  this.id = _id;
  this.description = _description;
  this.activity = _activity;
  this.assistance = _assistance;
  this.comment = _comment;
  this.exam = _exam;
  this.homework = _homework;
  this.partialperiod = _partialperiod;
  this.points = _points;
  this.score = _score;
  this.studentGroupId = _studentGroupId;
}

}
