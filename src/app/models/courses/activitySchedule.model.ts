export class ActivitySchedule {
  id: number;
  name: string;
  description: string;
  openingDate: Date;
  scheduledDate: Date;
  registerDate: Date;
  studentGroupId: number;
  filesId: number[];
  schedulingPartialId: number;
  score: number;
  comment: string;
  qualified: boolean;
  schedulingPartialDescription: string;
  assignedQualification: number;
  typeScore: boolean;
  typeOfQualification: boolean;
  delivered: boolean;
  addNewActivity: boolean;
  addNewAnswers: boolean;
  totalAnswers: number;
}
