export class TeacherExamType {
    id: number;
    description: string;
    active: boolean;
  }
  