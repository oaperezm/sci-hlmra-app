export class TeacherResource {
    id: number;
    name: string;
    description: string;
    teacherResourceId: number;
    url?: string;
    filename?: string;
    fileContentBase64?: string;
  }
  