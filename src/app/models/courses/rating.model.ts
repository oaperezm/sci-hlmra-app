export class Rating {
  id: number;
  examScheduleId: number;
  userId: number;
  score: number;
  correctAnswers: number;
  comments: string;
  testId: number;
  totalAnswerScore: number;
  maximumExamScore: number;
  weightingId: number;
  weightingDescription: string;
  weightingUnit: string;
}
