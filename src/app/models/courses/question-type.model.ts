export class QuestionType {
    id: number;
    description: string;
    active: boolean;
  }
  