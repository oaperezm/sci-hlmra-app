import { TeacherExamType } from './teacher-exam-type.model';
import { Weighting } from './weighting.model';

export class TeacherExam {
  id: number;
  teacherExamTypeId: number;
  description: string;
  active: boolean;
  questions?: number[];
  totalQuestions: number;
  index: number;
  teacherExamType?: TeacherExamType;
  weighting?: Weighting;
  isAutomaticValue: boolean;
  maximumExamScore: number;
}
