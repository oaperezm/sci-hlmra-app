import { ExamScheduleType } from './exam-schedule-type.model';

export class ExamSchedule {
  id: number;
  description: string;
  studentGroupId: number;
  active: boolean;
  status?: ExamScheduleType;
  teacherExamId?: number;
  typeExam: number;
  examScheduleTypeId: number;
  schedulingPartialDescription : string;
  schedulingPartialId: number;
  beginApplicationDate: Date;
  endApplicationDate: Date;
  beginApplicationTime?: Date;
  endApplicationTime?: Date;
  minutesExam?: number;
  ponderacion: string;
}
