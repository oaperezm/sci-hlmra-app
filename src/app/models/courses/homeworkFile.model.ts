export class HomeworkFile {
  id: number;
  comment: string;
  url: string;
  registerDate: Date;
}
