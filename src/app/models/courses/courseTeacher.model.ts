export class CourseTeacher {
  id: number;
  name: string;
  description: string;
  groups: any[];
}
