export class Group {
  id: number;
  description: string;
  status: number;
  courseId: number;
  code: string;
  examScheduleTotal?: number;
  selected: boolean;
  pass?: string;
  constructor ( _id: number, _description: string, _status: number, _courseId: number, _code: string, _pass?: string) {
    this.id = _id;
    this.description = _description;
    this.status = _status;
    this.courseId = _courseId;
    this.selected = false;
    this.code = _code;
    this.pass = _pass;
  }
}
