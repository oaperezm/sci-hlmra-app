export class Axis {
  id: number;
  description: string;
  active: boolean;
  educationLevelId: number;
  name: string;
}
