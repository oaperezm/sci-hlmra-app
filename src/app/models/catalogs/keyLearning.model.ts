export class KeyLearning {
  id: number;
  description: string;
  active: boolean;
  educationLevelId: number;
  name: string;
}
