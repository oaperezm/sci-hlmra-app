export class LearningExpected {
  id: number;
  description: string;
  active: boolean;
  educationLevelId: number;
  name: string;
}
