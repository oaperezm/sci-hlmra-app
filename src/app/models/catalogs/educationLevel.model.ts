export class EducationLevel {
  id: number;
  active: string;
  description: string;
}
