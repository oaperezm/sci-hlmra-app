export class Attendance {
  attendanceDate: string;
  studentGroupId: number;
  userId: number;
  attendanceClass: boolean;
}
