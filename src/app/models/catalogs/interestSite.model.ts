export class InterestSite {
  id: number;
  title: string;
  description: string;
  uri: string;
  courseId: number;
}
