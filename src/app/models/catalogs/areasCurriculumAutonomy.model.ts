export class AreasCurriculumAutonomy {
  id: number;
  description: string;
  active: boolean;
  educationLevelId: number;
  name: string;
}
