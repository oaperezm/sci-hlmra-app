export class AreaPersonalSocialDevelopment {
  description: string;
  educationLevelId: number;
  fileTypeId: number;
  id: number;
  name: string;
}
