export class TipHome {
  id: number;
  name: string;
  description: string;
  urlImage: string;
  registerDate: Date;
  published: boolean;
  index: number;
  filename?: string;
  fileContentBase64?: string;

  constructor() {
      this.id = 0;
      this.name = '';
      this.description = '';
      this.urlImage = '';
      this.published = false;
  }
}
