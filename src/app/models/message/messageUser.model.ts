export class MessageUser {
  messages: string;
  senderDate: Date;
  owner: boolean;
  receiverUserId: number;
  chatGroupId: number;
}


