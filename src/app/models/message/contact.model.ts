export class Contact {
  avatar: string;
  email: string;
  fullName: string;
  id: number;
}
