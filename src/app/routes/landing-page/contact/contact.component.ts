// MODELS
import { NewHome } from '../../../models/home/new.model';
import { TipHome } from '../../../models/home/tip.model';
import { GeneralEvent } from '../../../models/home/general-event.model';

import { FormControl, FormGroup, Validators } from '@angular/forms';
import swal from 'sweetalert2';

import { Component, OnInit, ViewEncapsulation, AfterContentInit, AfterViewChecked, ElementRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

// Servicio
import { HeaderService } from '../../../components/layouts/anonymous/header/header.service';

import { StudenService, TipsHomeService, NewsHomeService, GeneralEventService, UserService } from '../../../service/service.index';
import { Appsettings } from '../../../configuration/appsettings';


@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit {

  news: NewHome[] = [];
  tips: TipHome[] = [];
  generalEvents: GeneralEvent[] = [];

  frmContact: FormGroup;
  sendingMail: boolean;
  constructor(  public _srvHeader: HeaderService,
                private route: ActivatedRoute,
                private _router: Router,
                private _srvStudent: StudenService,
                private elementRef: ElementRef,
                private _srvUser: UserService ) {
    this.frmContact = new FormGroup( {
      'fullName': new FormControl('', [ Validators.required ]),
      'email': new FormControl('', [ Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+.[a-z]{2,3}$'), Validators.required ]),
      'subject': new FormControl(''),
      'message': new FormControl('', [ Validators.required ]),
      'school': new FormControl(''),
      'workplace': new FormControl('')
    });
    const body = document.body.style.background = 'white';

  }

  ngOnInit() {
  }

  // Enviar correo
  onSubmit() {

    const mail = {
      'fullName':  this.frmContact.controls['fullName'].value,
      'email':  this.frmContact.controls['email'].value,
      'subject':  this.frmContact.controls['subject'].value,
      'message':  this.frmContact.controls['message'].value,
      'school':  this.frmContact.controls['school'].value,
      'workplace':  this.frmContact.controls['workplace'].value,
      'systemId': Appsettings.SYSTEM_ID
    };

    this.sendingMail = true;

    this._srvUser.sendContactMail( mail ).subscribe( res => {
      if ( res.success ) {
        swal(Appsettings.APP_NAME, 'En breve un asesor se pondrá en contacto contigo.' , 'success');
      } else {
        swal(Appsettings.APP_NAME, res.message, 'error');
      }
      this.sendingMail = false;
      this.frmContact.reset();
    }, err => {
      this.sendingMail = false;
      this.frmContact.reset();
    });

  }
}

