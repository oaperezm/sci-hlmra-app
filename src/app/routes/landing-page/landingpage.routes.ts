import { Routes, RouterModule} from '@angular/router';
import { RegistryComponent } from './registry/registry.component';
// import { LandingPageModule } from './landingpage.module';
import { LandingPageComponent } from './landingpage.component';
import { LoginComponent } from '../anonymous/login/login.component';
import { ContactComponent } from './contact/contact.component';
import { PasswordRecoveryComponent } from '../anonymous/password-recovery/password-recovery.component';

const LandingPageRoutes: Routes = [
  {
    path: 'landing',
    component: LandingPageComponent,
    children: [
      // { path: 'login', component: LoginComponent },
      { path: 'registry', component: RegistryComponent },
      { path: 'contact', component: ContactComponent },
      { path: '', redirectTo: '/home/login', pathMatch: 'full'},

    ],
  },
  { path: 'login', component: LoginComponent },
  { path: 'recover-password', component: PasswordRecoveryComponent },
];

export const LANDINGPAGE_ROUTES = RouterModule.forChild( LandingPageRoutes );
