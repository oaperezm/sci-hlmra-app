import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

// SERVICES COMPONENT
import { StorageService } from '../../../service/service.index';
import { AdminHeaderService } from '../../../components/layouts/administrator/admin-header/admin-header.service';

@Component({
  selector: 'app-interesting-sites',
  templateUrl: './interesting-sites.component.html',
  styleUrls: ['./interesting-sites.component.scss']
})
export class InterestingSitesComponent implements OnInit, OnDestroy {
  nodeId: number;
  courseId: number;
  constructor(private _route: ActivatedRoute,
    private _svrStorage: StorageService,
    public _srvHeaderAdmin: AdminHeaderService ) { }

  ngOnInit() {
    this._srvHeaderAdmin.setUrlBackButton(['/ra/list-courses', 'Mis cursos']);

    if (this._svrStorage.rol === 'Profesor') {
      // this._bookNavService.showContentTab = true;
    }

    if (this._svrStorage.rol === 'Alumno') {
      // this._bookNavService.showCourseTab = true;
    }

    this._route.params.subscribe(params => {

        this.nodeId = Number(params['nodeId']) ;
        this.courseId = Number(params['id'])
        // this._bookNavService.setNode( this.nodeId );
    });
  }
  ngOnDestroy() {
    this._srvHeaderAdmin.setUrlBackButton([]);

  }
}
