import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MatDialog } from '@angular/material';

// Servicios
import { ActivityService } from '../../../service/service.index';

// Modelos
import { ActivitySchedule } from '../../../models/courses/activitySchedule.model';
import { Course } from '../../../models/catalogs/course.model';

// Servicios de componentes
import { CourseNavService  } from '../../../components/layouts/publication/course-nav/course-nav.service';

// Dialogs
import { DialogUploadFileActivityComponent } from '../../../dialogs/dialog-upload-file-activity/dialog-upload-file-activity.component';
import { DialogGetFileActivityComponent } from '../../../dialogs/dialog-get-file-activity/dialog-get-file-activity.component';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-course-activity',
  templateUrl: './course-activity.component.html',
  styleUrls: ['./course-activity.component.scss']
})
export class CourseActivityComponent implements OnInit, OnDestroy {

  courseId: number;
  nodeId: number;
  course: Course;

  activitySchedules: ActivitySchedule[] = [];
  private changeCourseEndRef: Subscription = null;

  constructor(  public _dialog: MatDialog,
                private _route: ActivatedRoute,
                private _router: Router,
                private _srvActivity: ActivityService,
                public _courseNavService: CourseNavService) { }

  ngOnInit() {
    this._route.params.subscribe(params => {
      this.courseId = Number(params['id']);
      this.nodeId = Number(params['nodeId']);

      this._courseNavService.asyncGetStudentCourses( this.nodeId, this.courseId ).then( () => {
        this.course = this._courseNavService.course;
        this.getActivityWork();
      });

      this.changeCourseEndRef = this._courseNavService.ChangeCourseEnd$.subscribe( () => {
        this.getActivityWork();
      });

    });
  }

  ngOnDestroy() {
    this.changeCourseEndRef.unsubscribe();
  }

  // Obtiene todas la programacions de tarea del grupo
  getActivityWork( ) {

    if ( this.course == null ) {
      return;
    }

    this.activitySchedules = [];
    this._courseNavService.working = true;

    this._srvActivity.getAllByStudent( this._courseNavService.course.groupId ).subscribe( result => {
      if ( result.success ) {
        for (let h of result.data ) {
          const activity = new ActivitySchedule();

          activity.id = h.id;
          activity.name = h.name;
          activity.description = h.description;
          activity.scheduledDate = h.scheduledDate;
          activity.filesId = h.files;
          activity.openingDate = h.openingDate;
          activity.studentGroupId = h.studentGroupId;
          activity.schedulingPartialId = h.schedulingPartialId;
          activity.score = h.score;
          activity.comment = h.comment;
          activity.qualified = h.qualified;
          activity.schedulingPartialDescription = h.schedulingPartialDescription;
          activity.delivered = h.delivered;
          activity.addNewAnswers = h.addNewActivity;
          activity.totalAnswers = h.totalAnswers;
          this.activitySchedules.push( activity );
        }
      }
      this._courseNavService.working = false;
    }, err => {
      this._courseNavService.working = false;
    });
  }

   // Muestra modal para cargar las tareas del alumno
   onShowUploadFile( activitie ) {
     let taskId = activitie.id;
     let delivered = activitie.delivered;
     let qualified = activitie.qualified;
    const DIALOG_REF = this._dialog.open( DialogUploadFileActivityComponent, {
      width: '800px',
      height: '600px',
      autoFocus: false,
      disableClose: true,
      data: { taskId: taskId,
              delivered: delivered,
              qualified: qualified }
    });

    DIALOG_REF.afterClosed().subscribe(response => {
      if ( response) {
        this.getActivityWork();
      }
    });
  }

   // Muestra modal con los archivos asociados a la tarea
   onShowFilesTask( task ) {
    const DIALOG_REF = this._dialog.open( DialogGetFileActivityComponent, {
      width: '800px',
      height: '600px',
      autoFocus: false,
      disableClose: true,
      data: { task: task }
    });

    DIALOG_REF.afterClosed().subscribe(response => {});
  }

}
