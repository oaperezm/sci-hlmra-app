import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MatDialog } from '@angular/material';

// Servicios de componentes
import { CourseNavService  } from '../../../components/layouts/publication/course-nav/course-nav.service';


// Servicios
import { CourseService, ExamScheduleService } from '../../../service/service.index';
import { AdminHeaderService } from '../../../components/layouts/administrator/admin-header/admin-header.service';

import { SchedulingPlartial } from '../../../models/courses/schedulingPartial.model';
@Component({
  selector: 'app-course-final-score',
  templateUrl: './course-final-score.component.html',
  styleUrls: ['./course-final-score.component.scss']
})
export class CourseFinalScoreComponent implements OnInit {

  courseId: number;
  nodeId: number;
  groupId: number;
  score: any[] = [];
  schedulingPlartial: SchedulingPlartial[] = [];
  constructor(  public _courseNavService: CourseNavService,
                private _srvCourse: CourseService,
                private _route: ActivatedRoute,
                private _srvHeaderAdmin: AdminHeaderService,
                private _srvExam: ExamScheduleService) {

    this._courseNavService.working = false;

    this._route.params.subscribe(params => {
      this._srvHeaderAdmin.setUrlBackButton(['/ra/list-courses', 'Mis cursos']);

      this.courseId = Number(params['id']);
      this.nodeId = Number(params['nodeId']);
      this.groupId = Number(params['groupId']);
      this.getPartials( this.groupId );
    });

   }

  ngOnInit() {
  }

  getPartials( groupId: number): void {
    this._srvExam.getSchedulingpartial( groupId ).subscribe( result => {
      this.schedulingPlartial = [];
      if ( result.success) {
        for ( let p of result.data){
          let partial = new SchedulingPlartial();
          partial.id = p.id;
          partial.startDate = p.startDate;
          partial.endDate = p.endDate;
          partial.description = p.description;
          partial.coursePlanningId = p.coursePlanningId;

          this.schedulingPlartial.push( partial  );
        }
        this.getFinalScore( this.groupId );

      }
    });
  }

  getFinalScore( courseId: number ) {
    this._srvCourse.getFinalScore( courseId).subscribe( result => {
      if ( result.success ) {
        for ( let d of result.data ) {
          let partial = this.schedulingPlartial.filter( x => x.id === d.schedulingPartialId )[0];
          d.schedulingPartial = partial.description;
          d.startDate = partial.startDate;
          d.endDate = partial.endDate;
          this.score.push( d );
        }
      }
    });

  }

}
