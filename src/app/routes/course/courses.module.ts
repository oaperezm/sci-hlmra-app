import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// // ROUTES
import { COURSE_ROUTES } from './courses.routes';

// LAYOUT
import { CoursesLayoutComponent } from './courses-layout.component';

// // MODULES
import { AdministratorModule } from '../../components/layouts/administrator/administrator.module';
import { CoreModule } from '../../core/core.module';
import { PublicationModule } from '../../components/layouts/publication/publication.module';
import { AdmonComponentModule } from '../../components/administrator/admon-component.module';
// // COMPONENTS

// MATERIALIZE
import {  MatIconModule, MatButtonModule, MatSidenavModule, MatSliderModule } from '@angular/material';
import { CourseContainComponent } from './course-contain/course-contain.component';
import { CourseExamsComponent } from './course-exams/course-exams.component';
import { InterestingSitesComponent } from './interesting-sites/interesting-sites.component';
import { CourseHomeworksComponent } from './course-homeworks/course-homeworks.component';
import { CourseActivityComponent } from './course-activity/course-activity.component';
import { CourseFinalScoreComponent } from './course-final-score/course-final-score.component';


@NgModule({
  imports: [
    CommonModule,
    PublicationModule,
    AdministratorModule,
    CoreModule,
    MatIconModule,
    MatButtonModule,
    MatSidenavModule,
    MatSliderModule,
    COURSE_ROUTES,
    AdmonComponentModule
  ],
  declarations: [
    CoursesLayoutComponent,

    CourseExamsComponent,

    InterestingSitesComponent,
    CourseContainComponent,
    CourseHomeworksComponent,
    CourseActivityComponent,
    CourseFinalScoreComponent,
  ]
})
export class CoursesModule { }
