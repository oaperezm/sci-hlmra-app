import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

// COMPONENT SERVICES
import { CourseNavService } from '../../../components/layouts/publication/course-nav/course-nav.service';

// MODELS
import { Course } from '../../../models/catalogs/course.model';
import { AdminNavService } from '../../../components/layouts/administrator/admin-nav/admin-nav.service';
import { AdminHeaderService } from '../../../components/layouts/administrator/admin-header/admin-header.service';
import { Planning } from '../../../models/courses/planning.model';
import { CatalogService, PlanningService } from '../../../service/service.index';
@Component({
  selector: 'app-course-contain',
  templateUrl: './course-contain.component.html',
  styleUrls: ['./course-contain.component.scss']
})
export class CourseContainComponent implements OnInit, OnDestroy {

  courseId: number;
  nodeId: number;
  course: Course;
  planinng: Planning;
  total: number;
  constructor(  private _srvCatalog: CatalogService,
                private _route: ActivatedRoute,
                public _srvAdminNav: AdminNavService,
                public _courseNavService: CourseNavService,
                private _srvHeaderAdmin: AdminHeaderService,
                private _srvPlanning: PlanningService) {
      this.planinng = new Planning();
      this.total = 0;
   }

  ngOnInit() {
    this._route.params.subscribe(params => {

      this.courseId = Number(params['id']);
      this.nodeId = Number(params['nodeId']);
      this._srvHeaderAdmin.setUrlBackButton(['/ra/list-courses', 'Mis cursos']);
      this._courseNavService.asyncGetStudentCourses( this.nodeId, this.courseId ).then( () => {
             this.course = this._courseNavService.course;
             this._srvAdminNav.setTitle(this.course.name);
             this.getPlanningCourse( this.course.groupId);
      });

    });

  }

  ngOnDestroy(): void {
    this._srvHeaderAdmin.setUrlBackButton([]);
  }

  getPlanningCourse( groupId: number): void {
    this._srvPlanning.getPlanningCourse( groupId ).subscribe( result => {
      this.planinng = result.data[0];
      let total = this.planinng.exam + this.planinng.homework + this.planinng.assistance + this.planinng.activity;
      this.total = total;
    });
  }
}


