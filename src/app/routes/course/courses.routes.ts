import { Routes, RouterModule } from '@angular/router';
import { CoursesLayoutComponent } from './courses-layout.component';
import { CourseContainComponent } from './course-contain/course-contain.component';
import { CourseExamsComponent } from './course-exams/course-exams.component';
import { InterestingSitesComponent} from './interesting-sites/interesting-sites.component';
import { CourseHomeworksComponent } from './course-homeworks/course-homeworks.component';
import { CourseActivityComponent } from './course-activity/course-activity.component';
import { CourseFinalScoreComponent } from './course-final-score/course-final-score.component';
const AppPageCourse: Routes = [
  {
    path: 'course',
    component: CoursesLayoutComponent,
    children: [
      { path: 'content/:id/:nodeId', component:  CourseContainComponent },
      { path: 'exams/:id/:nodeId', component:  CourseExamsComponent },
      { path: 'sites/:id/:nodeId', component: InterestingSitesComponent },
      { path: 'homeworks/:id/:nodeId', component: CourseHomeworksComponent},
      { path: 'activities/:id/:nodeId', component: CourseActivityComponent},
      { path: 'scores-final/:id/:nodeId/:groupId', component: CourseFinalScoreComponent}
    ]
  }
];

export const COURSE_ROUTES = RouterModule.forChild( AppPageCourse );
