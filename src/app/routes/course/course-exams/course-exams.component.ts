import { Component, OnInit, AfterViewInit, OnDestroy, Output, EventEmitter } from '@angular/core';

import { ActivatedRoute, Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';

import { ExamScheduleService } from '../../../service/service.index';

// COMPONENT SERVICES
import { CourseNavService } from '../../../components/layouts/publication/course-nav/course-nav.service';
import { AdminHeaderService } from '../../../components/layouts/administrator/admin-header/admin-header.service';
import { AdminNavService } from '../../../components/layouts/administrator/admin-nav/admin-nav.service';

// DIALOGS
import { DialogStudentAnswersComponent } from '../../../dialogs/dialog-student-answers/dialog-student-answers.component';

// MODELS
import { Course } from '../../../models/catalogs/course.model';

import { Subscription } from 'rxjs';
import { ExamSchedule } from '../../../models/courses/exam-schedule.model';
import { Appsettings } from '../../../configuration/appsettings';
import swal from 'sweetalert2';

@Component({
  selector: 'app-course-exams',
  templateUrl: './course-exams.component.html',
  styleUrls: ['./course-exams.component.scss']
})
export class CourseExamsComponent implements OnInit, AfterViewInit, OnDestroy {

  courseId: number;
  nodeId: number;
  working: boolean;
  course: Course;

  exams: any[] = [];

  private changeCourseEndRef: Subscription = null;
  constructor(public _dialog: MatDialog,
    private _srvExam: ExamScheduleService,
    private _route: ActivatedRoute,
    private _router: Router,
    public _courseNavService: CourseNavService,
    public _srvHeaderAdmin: AdminHeaderService,
    public _srvAdminNav: AdminNavService) { }

  ngOnInit() {
    this._srvHeaderAdmin.setUrlBackButton(['/ra/list-courses', 'Mis cursos']);
    this._route.params.subscribe(params => {
      this.courseId = Number(params['id']);
      this.nodeId = Number(params['nodeId']);

      this._courseNavService.asyncGetStudentCourses(this.nodeId, this.courseId).then(() => {
        this.course = this._courseNavService.course;
        this._srvAdminNav.setTitle(this.course.name);

        this.getExams();
      });

      this.changeCourseEndRef = this._courseNavService.ChangeCourseEnd$.subscribe(() => {
        this.getExams();
      });

    });
  }

  ngAfterViewInit(): void {

  }

  ngOnDestroy() {
    this._srvHeaderAdmin.setUrlBackButton([]);

    this.changeCourseEndRef.unsubscribe();
  }

  // Obtener la información de los exámenes
  getExams(): void {

    if (this.course == null) {
      return;
    }

    this._courseNavService.working = true;
    this._srvExam.getAllExamStuden(this._courseNavService.course.groupId).subscribe(res => {
      this.exams = [];
      if (res.success) {

        for (let exam of res.data) {
          let teacherExamTypes: number;
          if (exam.teacherExam.teacherExamTypes) {
            teacherExamTypes = exam.teacherExam.teacherExamTypes.id;
          } else {
            teacherExamTypes = 2;
          }

          let statusExam = false;

          if (!exam.isAnswered) {
            let result = this.validateDates(exam.applicationDate, this._courseNavService.course.endDate);

            if (!result) {
              statusExam = true;
            }
          }
          let examSchedule = new ExamSchedule();
          let newExam = {
            id: exam.id,
            schedulingPartialDescription: exam.schedulingPartialDescription,
            schedulingPartialId: exam.schedulingPartialId,            
            description: exam.description,
            name: exam.description,
            beginApplicationDate: exam.beginApplicationDate,
            endApplicationDate: exam.endApplicationDate,
            beginApplicationTime: exam.beginApplicationTime,
            endApplicationTime: exam.endApplicationTime,
            rating: 0,
            minutesExam: exam.minutesExam,
            ponderacion: exam.ponderacion,
            teacherExamId: exam.teacherExamId,
            hasScore: exam.hasScore,
            isAnswered: exam.isAnswered,
            score: exam.hasScore ? exam.score.score : null,
            statusExam: statusExam,
            comment: exam.hasScore ? exam.score.comments : null,
            teacherExamTypes: teacherExamTypes
          };

          this.exams.push(newExam);
        }
      }
      this._courseNavService.working = false;
    }, err => {
      this._courseNavService.working = false;
    });

  }

  // Validar fechas del curso
  validateDates(examApply: Date, endDate: Date): boolean {
    let newToday = new Date();
    let newExamApply = new Date(examApply);
    let courseEndDate = new Date(endDate);
    let result = false;

    let resToday = new Date(newToday.getFullYear(), newToday.getMonth(), newToday.getDate());
    let resApply = new Date(newExamApply.getFullYear(), newExamApply.getMonth(), newExamApply.getDate());
    let endCourseDate = new Date(courseEndDate.getFullYear(), courseEndDate.getMonth(), courseEndDate.getDate());

    if (resApply <= endCourseDate || resToday < resApply) {
      result = false;
    }
    if (resToday >= resApply) {
      result = true;
    }

    return result;
  }



  // Ver las respuestas del alumno
  onShowAnswers(exam): void {
    const DIALOG_REF = this._dialog.open(DialogStudentAnswersComponent, {
      width: '1000px',
      //height: '700px',
      autoFocus: false,
      disableClose: true,
      data: { teacherExamId: exam.id, nameExam: exam.name, studenId: 0 }
    });

    DIALOG_REF.afterClosed().subscribe(response => {
    });
  }

  // Presentar un examen
  onShowExam(exam) {
    if (exam.minutesExam != null) {
      swal({
        title: 'Atención',
        text: 'Sólo tendrá ' + exam.minutesExam + ' minutos para contestar el examen.',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: '¡Sí, deseo continuar!',
        cancelButtonText: 'Cancelar'
      }).then((result) => {
        if (result.value) {
          this._router.navigateByUrl(
            `/ra/apply-exam/${exam.teacherExamId}/${exam.id}/${this._courseNavService.nodeId}/${this._courseNavService.courseId}`
          );
        }
      });
    }
    else{
      this._router.navigateByUrl(
        `/ra/apply-exam/${exam.teacherExamId}/${exam.id}/${this._courseNavService.nodeId}/${this._courseNavService.courseId}`
      );
    }

  }

}
