import { Component, OnInit, AfterViewInit, ViewChild } from '@angular/core';
import { MatSidenav } from '@angular/material';

// COMPONENT SERVICES
import { AdminNavService } from '../../components/layouts/administrator/admin-nav/admin-nav.service';

declare function removeBackground(): any;
declare function setHeight(): any;

@Component({
  selector: 'app-courses-layout',
  templateUrl: './courses-layout.component.html',
  styleUrls: ['./courses-layout.component.scss']
})
export class CoursesLayoutComponent implements OnInit {

  @ViewChild('sidenav') public sidenav: MatSidenav;

  constructor( public _srvAdminNav: AdminNavService ) {
  }

  ngOnInit() {
    this._srvAdminNav.setSidenav(this.sidenav);
  }

}
