import { Component, OnInit, AfterViewInit, ViewEncapsulation, ViewChild, ElementRef } from '@angular/core';

// Componentes del menu

import { Router, ActivatedRoute } from '@angular/router';
import { MatDialog, MatSidenav } from '@angular/material';

// Modelos
import { Menu } from '../../models/menu/menu.mode';

// Service
import { NavService, StorageService, UserService } from '../../service/service.index';
import { AdminHeaderService } from '../../components/layouts/administrator/admin-header/admin-header.service';
import { AdminNavService } from '../../components/layouts/administrator/admin-nav/admin-nav.service';

// Dialogs
import { DialogGetCctComponent } from '../../dialogs/dialog-get-cct/dialog-get-cct.component';

@Component({
  selector: 'app-resoruce-layout',
  templateUrl: './resource-layout.component.html',
  styleUrls: ['./resource-layout.component.scss']
})
export class ResourceLayoutComponent implements OnInit, AfterViewInit {

  nameUser: string;
  breadcrum: any[];
  title: string;
  countCCT: number;
  rol: string;

  @ViewChild('sidenav') public sidenav: MatSidenav;
  @ViewChild('appDrawer') appDrawer: ElementRef;

  constructor(  private _dialog: MatDialog,
                private _router: Router,
                public navService: NavService,
                public _srvStorage: StorageService,
                private _srvUser: UserService,
                public _srvAdminNav: AdminNavService ) {

    this.nameUser = this._srvStorage.name;
  }

  ngOnInit() {
    this._srvStorage.title = '';
    this._srvStorage.showBackBotton = false;
    this.rol = this._srvStorage.rol;
    // validacion para saber si tiene cct
    this._srvUser.getProfileData().subscribe( res => {
      if( res.success) {

        this.countCCT = res.data.key;

        // console.log(this.rol);
        if (this.rol === 'Profesor que imparte cursos' && this.countCCT ) {
          // console.log('si tiene ccts');
        }else if(this.rol === 'Alumno'){
          // console.log('Es alumno');
        }else{
          this.validateUserCCt();
        }

      }


    });
    this._srvAdminNav.setSidenav(this.sidenav);
  }

  ngAfterViewInit(): void {
      this.navService.appDrawer = this.appDrawer;
  }

  validateUserCCt() {
    const DIALOG_REF = this._dialog.open( DialogGetCctComponent, {
      width: '550px',
      height: '350px',
      autoFocus: false,
      disableClose: true,
      data: { }
    });

    DIALOG_REF.afterClosed().subscribe(response => {});


  }

}
