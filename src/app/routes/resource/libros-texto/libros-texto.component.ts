import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { UserUtilities } from '../../../shared/user.utilities';
// COMPONENT SERVICES
import { AdminHeaderService } from '../../../components/layouts/administrator/admin-header/admin-header.service';
import { Appsettings } from '../../../configuration/appsettings';

// COMPONENT SERVICES
import { AdminNavService } from '../../../components/layouts/administrator/admin-nav/admin-nav.service';

// Servicios
import {
  StructureService,
  StorageService,
  CatalogService
} from '../../../service/service.index';

// Modelo
import { Node } from '../../../models/content/node.model';
import { Content } from '../../../models/content/content.model';
import { ContentType } from '../../../models/content/contentType.mode';
import { Pagination } from '../../../models/general/pagination.model';
import { Block } from '../../../models/catalogs/block.model';
import { Formative } from '../../../models/catalogs/formativeField.model';
import swal from 'sweetalert2';

import { AreaPersonalSocialDevelopment } from '../../../models/catalogs/areaPersonalSocialDevelopment.model';
import { AreasCurriculumAutonomy } from '../../../models/catalogs/areasCurriculumAutonomy.model';
import { Axis } from '../../../models/catalogs/axis.model';
import { KeyLearning } from '../../../models/catalogs/keyLearning.model';
import { LearningExpected } from '../../../models/catalogs/learningExpected.model';
import { Purpose } from '../../../models/catalogs/purpose.model';
import { EducationLevel } from '../../../models/catalogs/educationLevel.model';

declare function setFullHeightWithOffset( selement , offset ): any;
import { JoyrideService } from 'ngx-joyride';

@Component({
  selector: 'app-librostexto',
  templateUrl: './libros-texto.component.html',
  styleUrls: ['./libros-texto.component.scss']
})
export class LibrosTextoComponent implements OnInit, OnDestroy {
  textSearch = '';
  full: boolean;
  structureParentId: number;
  blocks: Block[] = [];
  formativeField: Formative[] = [];
  nodes: any[] = [];
  children: any[] = [];
  content: Content[] = [];
  contentType: ContentType[] = [];
  resources: Content[] = [];
  selectTypeFile: number[] = [];
  typeResource: string;
  nodeId: number;
  totalResources: number;
  selectedParent: Node;
  showResources: boolean;
  fileTypeId: number;
  fileTypesIds: string;
  working: boolean;
  sizePage: string = Appsettings.DEFAULT_SIZE_PAGE;
  page = 1;
  pagination: Pagination = new Pagination();
  // Variable para el numero de pagina
  currentPage = 1;
  blockId: number;
  formativeId: number;
  showFilters: boolean;
  showFiltersCatalog: boolean;

  typeId: number;

  areaPersonalSocialDevelopment: AreaPersonalSocialDevelopment[] = [];
  areaPersonalSocialDevelopmentRes: AreaPersonalSocialDevelopment[] = [];
  areaPersonalSocialDevelopmentId: number;

  areasCurriculumAutonomy: AreasCurriculumAutonomy[] = [];
  areasCurriculumAutonomyRes: AreasCurriculumAutonomy[] = [];
  areasCurriculumAutonomyId: number;

  axis: Axis[] = [];
  axisRes: Axis[] = [];
  axisId: number;

  keylearning: KeyLearning[] = [];
  keylearningRes: KeyLearning[] = [];
  keylearningId: number;

  learningexpected: LearningExpected[] = [];
  learningexpectedRes: LearningExpected[] = [];
  learningexpectedId: number;

  purposes: Purpose[] = [];
  purposeId: number;

  educationLevel: EducationLevel[] = [];
  educationLevelId: number;
  levelId: number;

  educationLevelSelectdId: number;
  constructor(
    public _srvHeaderAdmin: AdminHeaderService,
    private _srvStructure: StructureService,
    public _srvStorage: StorageService,
    private route: ActivatedRoute,
    private _router: Router,
    private _srvCatalogS: CatalogService,
    public _srvAdminNav: AdminNavService,
    private joyride: JoyrideService
  ) {
    this.showResources = true;
    this.selectedParent = new Node();
    this._srvStorage.showBackBotton = true;

    this.blockId = 0;
    this.formativeId = 0;
    this.areaPersonalSocialDevelopmentId = 0;
    this.areasCurriculumAutonomyId = 0;
    this.axisId = 0;
    this.keylearningId = 0;
    this.learningexpectedId = 0;
    this.purposeId = 0;
    this.educationLevelId = 0;
    this.educationLevelSelectdId = 0;

    this.totalResources = 0;
    this.route.params.subscribe(params => {
      this.typeId = Number(params.nodeId);
      this.nodeId = 124;
      this.getContentNode( this.nodeId );
      this.getBlockAll();

      const data = JSON.parse(this._srvStorage.nodes);
      for (const node of data) {
        const childrens = new Node();
        childrens.id = node.id;
        childrens.active = node.active;
        childrens.description = node.description;
        childrens.nodeTypeId = node.nodeTypeId;
        childrens.parentId = node.parentId;
        childrens.urlImage = node.urlImage;

        this.nodes.push(childrens);
      }

      const parent = this.nodes.filter(item => item.parentId === 0)[0];
      const children = new Node();
      children.id = parent.id;
      children.active = parent.active;
      children.description = parent.description;
      children.nodeTypeId = parent.nodeTypeId;
      children.parentId = parent.parentId;
      children.urlImage = parent.urlImage;
      this.children = this.nodes.filter(item => item.parentId === children.id && item.description === 'Secundaria');
      if (this.children.length > 0) {
        this.onClickSelectNode(this.children[0]);
      }
      this.levelId = this.children[0].id;
      this.selectedParent = children;
    });
  }

  ngOnInit() {
    if ( this.content.length === 0) {

    }
    this.showFilters = false;
    this.showFiltersCatalog = false;
    this._srvAdminNav.setTitle('Libros de texto');
    this._srvHeaderAdmin.setUrlBackButton(['/resource/home', 'Inicio']);
    setFullHeightWithOffset('.nav-structure', 70);

    this.getDataCatalogs();
  }

  ngOnDestroy(): void {
    this._srvHeaderAdmin.setUrlBackButton([]);
  }

  getDataCatalogs() {
    this._srvCatalogS.requestCatalogos().subscribe( data => {
      this.setAreaPersonal( data[0] );
      this.setAreasCurriculums( data[1] );
      this.setAxis( data[2] );
      this.setKeyLearning( data[3] );
      this.setLearningExpected( data[4] );
      this.setPurposes( data[5] );
      this.setEduationLevel( data[6] );
      this.getFormativeFieldAll( data[7] );

    });
  }
/*********** SETEO DE LOS CATALOGOS PARA FILTROS  ********/
  // Setea el catalogo de areas personal
  setEduationLevel(data) {

    if( data.success ) {
      this.educationLevel = [];
      let datas = data.data;
      for ( let e of datas) {
        let level = new EducationLevel();
        level.id = e.id;
        level.active = e.active;
        level.description = e.description;
        this.educationLevel.push( level );
      }
    }
  }
  // Setea el catalogo de areas personal
  setAreaPersonal(data) {

    if( data.success ) {
      this.areaPersonalSocialDevelopment = [];
      this.areaPersonalSocialDevelopmentRes = [];
      let datas = data.data;
      for ( let e of datas) {
        let area = new AreaPersonalSocialDevelopment();
        area.id = e.id;
        area.fileTypeId = e.fileTypeId;
        area.description = e.description;
        area.educationLevelId = e.educationLevelId;
        area.name = e.description;
        this.areaPersonalSocialDevelopment.push( area );
        this.areaPersonalSocialDevelopmentRes.push( area );
      }
    }
  }
  // Setea el catalogo de areas de curriculares
  setAreasCurriculums(data) {

    if( data.success ) {
      this.areasCurriculumAutonomy = [];
      this.areasCurriculumAutonomyRes = [];
      let datas = data.data;
      for ( let e of datas) {
        let area = new AreasCurriculumAutonomy();
        area.id = e.id;
        area.active = e.active;
        area.description = e.description;
        area.educationLevelId = e.educationLevelId;
        area.name = e.description;
        this.areasCurriculumAutonomy.push( area );
        this.areasCurriculumAutonomyRes.push( area );
      }
    }
  }

  // Setea el catalogo de eje / ambito
  setAxis(data) {
    if ( data.success ) {
      this.axis = [];
      this.axisRes = [];
      let datas = data.data;
      for ( let e of datas) {
        let axis = new Axis();
        axis.id = e.id;
        axis.active = e.active;
        axis.description = e.description;
        axis.educationLevelId = e.educationLevelId;
        axis.name = e.description;
        this.axis.push( axis );
        this.axisRes.push( axis );
      }
    }
  }

  // Setea el catalogo de aprendizaje clave
  setKeyLearning(data) {
    if( data.success ) {
      this.keylearning = [];
      this.keylearningRes = [];
      let datas = data.data;
      for ( let e of datas) {
        let key = new KeyLearning();
        key.id = e.id;
        key.active = e.active;
        key.description = e.description;
        key.educationLevelId = e.educationLevelId;
        key.name = e.description;
        this.keylearning.push( key );
        this.keylearningRes.push( key );
      }
    }
  }

  // Setea el catalogo de aprendizaje esperado
  setLearningExpected(data) {

    if( data.success ) {
      this.learningexpected = [];
      this.learningexpectedRes = [];
      let datas = data.data;
      for ( let e of datas) {
        let expeted = new LearningExpected();
        expeted.id = e.id;
        expeted.active = e.active;
        expeted.description = e.description;
        expeted.educationLevelId = e.educationLevelId;
        expeted.name = e.description;
        this.learningexpected.push( expeted );
        this.learningexpectedRes.push( expeted );
      }
    }
  }

  // Setea el catalogo de PROPOSITO
  setPurposes(data) {

    if( data.success ) {
      this.purposes = [];
      let datas = data.data;
      for ( let e of datas) {
        let purpose = new Purpose();
        purpose.id = e.id;
        purpose.description = e.description;
        purpose.name = e.description;
        this.purposes.push( purpose );
      }
    }
  }
/*********** FIN PARA FILTROS  ********/
  onKeyUp(event) {
    if (this.textSearch.length >= 4 || this.textSearch.length === 0) {
      this.setPage({ page: 1, size: this.sizePage });
    }

  }

  onClickSelectNode(node) {
    // this.validateLevel( node.id);

    this.showFilters = false;
    this.showFiltersCatalog = false;
    this.typeResource = '';
    this.showResources = true;
    this.nodeId = node.id;
    this.resources = [];
    this.selectTypeFile = [];

    this.getContentNode(this.nodeId);
    this.selectedParent = node;
    this.children = this.nodes.filter(item => item.parentId === node.id);

    this.getBreadcrumb(node.id);
  }

  // Regresar un nivel anteriro de la búsqueda
  onClickReturn(node: Node) {
    this.validateLevel( node.id);

    this.typeResource = '';
    this.resources = [];
    this.selectTypeFile = [];
    this.showResources = true;
    const parent = this.nodes.filter(x => x.id === node.parentId)[0];
    this.getContentNode(parent.id);
    this.selectedParent = parent;
    this.children = this.nodes.filter(x => x.parentId === parent.id  && x.description === 'Secundaria');

    this.getBreadcrumb(parent.id);
    this.showFilters = false;
    this.showFiltersCatalog = false;
  }

  // Obtiene el contenido por nodo
  getContentNode(nodeId: number) {
    this.working = true;
    this._srvStructure
      .getContentbyNodeId(nodeId, 18, 1, this.blockId, this.formativeId)
      .subscribe(result => {
        if (result.data) {
          this.contentType = [];
          this.content = [];
          const fileTypes = result.data.fileTypes;
          // Obtenemos los tipos de archivos
          for (const dataType of fileTypes) {
            const type = new ContentType();
            type.id = dataType.id;
            type.description = dataType.description;
            if (dataType.id === 18){
              this.contentType.push(type);
            }
          }
        }

        if ( this.typeId > 0 ) {
          this.onSelectTypeContent( this.typeId);
        } else {
          this.working = false;
        }
      });
  }

  setPage(paginationData: object) {
    this.working = true;
    if ( this.fileTypesIds === undefined) {
      this.fileTypesIds = '0';
    }
    // tslint:disable-next-line:max-line-length
    this._srvStructure
      .getContentbyTypeId(
        this.nodeId,
        '18',
        this.currentPage,
        this.blockId,
        this.formativeId,
        this.textSearch,
        this.areaPersonalSocialDevelopmentId,
        this.areasCurriculumAutonomyId,
        this.axisId,
        this.keylearningId,
        this.learningexpectedId,
        this.purposeId,
        this.educationLevelId
      )
      .subscribe(result => {
        this.content = [];

        if (result.success) {

          console.log(result.pagination.showNextPage);
          this.pagination.pageNumber = Number(paginationData['page']);
          this.pagination.pageSize = result.pagination.pageSize;
          this.pagination.showNextPage = result.pagination.showNextPage;
          this.pagination.showPreviousPage = result.pagination.showPreviousPage;
          this.pagination.total = result.pagination.total;
          this.pagination.totalPage = result.pagination.totalPage;

          this.sizePage = paginationData['size'];

          const files = result.data.files;

          if(files.length > 0) {

          // Obtenemos los archivos
          for (const f of files) {
            const resource = new Content();
            resource.active = f.active;
            resource.area = f.area;
            resource.content = f.content;
            resource.contentResourceType = f.contentResourceType;
            resource.contentTypeDesc = f.contentTypeDesc;
            resource.contentTypeId = f.contentTypeId;
            resource.fileTypeDesc = f.fileTypeDesc;
            resource.fileTypeId = f.fileTypeId;
            resource.formativeField = f.formativeField.description;
            resource.id = f.id;
            resource.isPublic = f.isPublic;
            resource.name = f.name;
            resource.purpose = f.purpose.description;
            resource.thumbnails = f.thumbnails;
            resource.totalContent = f.totalContent;
            resource.trainingField = f.trainingField;
            resource.urlContent = f.urlContent;
            resource.block = f.block.description;
            resource.description = f.description;
            resource.purposeId = f.purposeId;
            resource.axis = f.axis.description;
            resource.formativeFieldName = f.formativeField.description;
            resource.purposeName = f.purpose.description;

            this.content.push(resource);
          }
          this.totalResources = ( this.contentType.length > 0) ? this.content.length : 0;

            this.showResources = this.content.length > 0 ? false : true;
        } else {

          swal(Appsettings.APP_NAME, 'Para visualizar contenido seleccione un nivel.', 'warning');
        }
        } else {
          swal(Appsettings.APP_NAME, result.message, 'warning');
          this.working = false;
        }
        this.working = false;
        if (this._srvStorage.showTourBooks === 0) {
          this._srvStorage.showTourBooks = 1;
          this.joyride.startTour(
            {
              steps: ['firstStepIcon','firstStep0'],
              customTexts: {
                next: '>>',
                prev: '<<',
                done: 'Cerrar'
              }
            }
          )
        }

      });
  }

  // Busca los contenidos filtrados por tipo
  onSelectTypeContent(contentTypeId: number) {
    this.working = true;
    // if ( this.typeId === contentTypeId ) {
    //   this.typeId = 0;
    // }
    this.content = [];
    const res = this.selectTypeFile.indexOf(contentTypeId);
    if (res !== -1) {
      this.selectTypeFile.splice(res, 1);
    } else {
      this.selectTypeFile.push( Number(contentTypeId));
    }

    const idsType = this.selectTypeFile.join(',');
    console.log(idsType);
    console.log('hola');

    this.fileTypesIds = idsType;

    if (this.selectTypeFile.length > 0) {
      this.showFilters = true;
      this.showFiltersCatalog = true;
      this.setPage({ page: this.currentPage });
    } else {
      this.working = false;
      this.showFilters = false;
      this.showFiltersCatalog = false;
    }
    this.fileTypeId = contentTypeId;
  }

  // Crea el camino de navegacion
  getBreadcrumb(nodeId: number) {
    const router = UserUtilities.getSelectedNodeUrl(
      nodeId,
      this._srvStorage.nodes
    );
    const descriptions = router.map(x => x.description);
    this._srvHeaderAdmin.setBreadcrumb(descriptions);
  }

  selectShowFile(content: Content) {

    // Se agrega la función para realizar la inserción de los eventos dentro del contenido multimedia
    // this.working = true;

    // this._srvUser.getProfileData().subscribe(function (res) {
    //   this.userData.push(res);
    //   this.userEmail.push(this.userData.data.email);
    // });
    // console.log(this.userEmail);
    // console.log(this.userData);
    // this._srvUser.validateUserSIDE('dirsecundaria@activa.edu.mx').subscribe(res1 => {
    //   console.log(res1);
    //   // this._srvUser.insertEventSIDE(this.userData).subscribe(res => {
    //   // });
    //   this.working = false;
    // }, err => {
    //   this.working = false;
    // });
    // // this._srvUser.insertEventSIDE();

    this._srvStorage.content = JSON.stringify(content);
    this._router.navigateByUrl(`/resource/show-multimedia/${this.nodeId}`);
  }

  onChangePage(page: number, cantContinue: boolean) {
    if (!cantContinue) {
      this.currentPage = page;
      this.setPage({ page: page, size: this.sizePage });
    }
  }

  getCSSActive(id: number) {
    let cssActive;
    this.selectTypeFile.forEach(element => {
      if (element === id) {
        cssActive = { 'active-file': true };
      }
    });
    return cssActive;
  }

  // Obtiene los bloques
  getBlockAll() {
    this.blocks = [];
    this._srvCatalogS.getBlockAll().subscribe(result => {
      if (result.success) {
        for (const b of result.data) {
          const block = new Block();
          block.id = b.id;
          block.description = b.description;

          this.blocks.push(block);
        }
      }
    });
  }

  // Obtiene los campos formativos
  getFormativeFieldAll( data) {
    this.formativeField = [];
      if (data.success) {
        for (const f of data.data) {
          const formative = new Formative();
          formative.id = f.id;
          formative.description = f.description;
          formative.name = f.description;
          this.formativeField.push(formative);
        }
      }
  }

  onChangeSelect() {
    this.working = true;
    // tslint:disable-next-line:max-line-length
    this._srvStructure
      .getContentbyTypeId(
        this.nodeId,
        this.fileTypesIds,
        this.currentPage,
        this.blockId,
        this.formativeId,
        this.textSearch,
        this.areaPersonalSocialDevelopmentId,
        this.areasCurriculumAutonomyId,
        this.axisId,
        this.keylearningId,
        this.learningexpectedId,
        this.purposeId,
        this.educationLevelId
      )
      .subscribe(result => {
        this.content = [];

        if (result.success) {
          const files = result.data.files;
          // Obtenemos los archivos
          for (const f of files) {
            const resource = new Content();
            resource.active = f.active;
            resource.area = f.area;
            resource.content = f.content;
            resource.contentResourceType = f.contentResourceType;
            resource.contentTypeDesc = f.contentTypeDesc;
            resource.contentTypeId = f.contentTypeId;
            resource.fileTypeDesc = f.fileTypeDesc;
            resource.fileTypeId = f.fileTypeId;
            resource.formativeField = f.formativeField;
            resource.id = f.id;
            resource.isPublic = f.isPublic;
            resource.name = f.name;
            resource.purpose = f.purpose;
            resource.thumbnails = f.thumbnails;
            resource.totalContent = f.totalContent;
            resource.trainingField = f.trainingField;
            resource.urlContent = f.urlContent;
            resource.axis = f.axis.description;
            resource.formativeFieldName = f.formativeField.description;
            resource.purposeName = f.purpose.description;
            this.content.push(resource);
          }
          this.showResources = this.content.length > 0 ? false : true;
        }
        this.working = false;
      });
  }

    // Cmabio de nivel escolar
  onSelectEducationLevel( event ) {
     let id = event;

    this.axisId = 0;
    this.areaPersonalSocialDevelopmentId = 0;
    this.areasCurriculumAutonomyId = 0;
    this.keylearningId = 0;
    this.learningexpectedId = 0;



    this.showFiltersCatalog = ( id > 0 ) ? true : false;
    this.axis = this.axisRes.filter( x => x.educationLevelId === id);
    this.areaPersonalSocialDevelopment = this.areaPersonalSocialDevelopmentRes.filter( x => x.educationLevelId === id);
    this.areasCurriculumAutonomy = this.areasCurriculumAutonomyRes.filter( x => x.educationLevelId === id);
    this.keylearning = this.keylearningRes.filter( x => x.educationLevelId === id);
    this.learningexpected = this.learningexpectedRes.filter( x => x.educationLevelId === id);
  }

    /************** TOOLTIPS  *************/
  getToolTipDataEducationLevel( event ) {
    if ( event !== undefined) {
      let tool = this.educationLevel.filter( x => x.id === event )[0];
      if ( tool !== undefined) {
        return tool.description;
      } else {
        this.educationLevelId = 0;
      }

    }
  }
  getToolTipDataselectedFortiveField( event ) {
    if ( event !== undefined) {
      let tool = this.formativeField.filter( x => x.id === event )[0];
      if ( tool !== undefined) {
        return tool.description;
      } else {
        this.formativeId = 0;
      }

    }
  }
  getToolTipDataAreaPersonal( event ) {
    if ( event !== undefined) {
      let tool = this.areaPersonalSocialDevelopment.filter( x => x.id === event )[0];
      if ( tool !== undefined ) {
        return tool.description;
      } else {
        this.areaPersonalSocialDevelopmentId;
      }
    }
  }
  getToolTipDataAreaCurriculum( event ) {
    if ( event !== undefined) {
      let tool = this.areasCurriculumAutonomy.filter( x => x.id === event )[0];
      if ( tool !== undefined) {
        return tool.description;
      } else {
        this.areasCurriculumAutonomyId = 0;
      }

    }
  }
  getToolTipDataAxis( event ) {
    if ( event !== undefined) {
      let tool = this.axis.filter( x => x.id === event )[0];
      if ( tool !== undefined) {
        return tool.description;
      } else {
        this.axisId = 0;
      }
    }
  }
  getToolTipDataKeylearning( event ) {
    if ( event !== undefined) {
      let tool = this.keylearning.filter( x => x.id === event )[0];
      if ( tool !== undefined) {
        return tool.description;
      } else {
        this.keylearningId = 0;
      }

    }
  }
  getToolTipDaTALearningexpected( event ) {
    if ( event !== undefined) {
      let tool = this.learningexpected.filter( x => x.id === event )[0];
      if ( tool !== undefined) {
        return tool.description;
      } else {
        this.learningexpectedId = 0;
      }

    }
  }
  getToolTipDataPurpose( event ) {
    if ( event !== undefined) {
      let tool = this.purposes.filter( x => x.id === event )[0];
      if ( tool !== undefined) {
        return tool.description;
      } else {
        this.purposeId = 0;
      }

    }
  }

  // valida el nivel de la estrucutra con el catalogo
  validateLevel( id: number){
    // console.log( this.educationLevel );
    // 143 -> primaria
    // 144 -> preescolar
    // 145 -> secundaria
    let level = 0;
    switch (id) {
      case 143:
          let primaria = this.educationLevel.filter( x => x.id === 2)[0];
          this.educationLevelSelectdId = primaria.id;
          this.onSelectEducationLevel(primaria.id);
        break;
      case 144:
          let preescolar = this.educationLevel.filter( x => x.id === 1)[0];
          this.educationLevelSelectdId = preescolar.id;
          this.onSelectEducationLevel(preescolar.id);
        break;
      case 145:
          let secundaria = this.educationLevel.filter( x => x.id === 3)[0];
          this.educationLevelSelectdId = secundaria.id;
          this.onSelectEducationLevel(secundaria.id);
        break;

      default:
        this.onSelectEducationLevel( this.educationLevelSelectdId );
        break;
    }
  }
}
