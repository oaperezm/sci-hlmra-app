import { Component, OnInit, OnDestroy } from '@angular/core';
import {BrowserModule, DomSanitizer, SafeResourceUrl} from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';

// Servicios
import { StorageService} from '../../../service/service.index';

// COMPONENT SERVICES
import { AdminNavService } from '../../../components/layouts/administrator/admin-nav/admin-nav.service';
import { AdminHeaderService } from '../../../components/layouts/administrator/admin-header/admin-header.service';

@Component({
  selector: 'app-service',
  templateUrl: './service.component.html',
  styleUrls: ['./service.component.scss']
})
export class ServiceComponent implements OnInit, OnDestroy {

  pageurl: SafeResourceUrl;
  working: boolean;
  showPdf: boolean;
  selectId: number;

  nameFiles = {
    'sitios_de_interes': { name: 'ligas_de_interes', title: 'Sitios de interés'},
    'software_gratuito': { name: 'Ligas_software_gratuito', title: 'Software gratuito'},
    'servicios_pedagogicos': { name: 'Servicios_pedagogicos', title: 'Servicios pedagógicos'}
  };
  constructor(  private _domSanitizer: DomSanitizer,
                public _srvStorage: StorageService,
                public _srvAdminNav: AdminNavService,
                private route: ActivatedRoute,
                public _srvHeaderAdmin: AdminHeaderService) {
    this.working = false;
    this.showPdf = false;

  }

  ngOnInit() {
    this._srvHeaderAdmin.setUrlBackButton(['/resource/home', 'Inicio']);
    this.route.params.subscribe(params => {
      const id = params['id'] ;

      const nameFile =  this.nameFiles[id];
      if ( nameFile ) {
        this._srvAdminNav.setTitle( nameFile.title );
      }
      this.viewFile( nameFile.name, id);
    });
  }

  ngOnDestroy(): void {
    this._srvHeaderAdmin.setUrlBackButton([]);
  }

  viewFile(nameFile: string, id: number) {
    this.selectId = id;

    this.showPdf = ( nameFile === 'Servicios_pedagogicos') ? true : false;
    let apiUrl = `../../../../assets/docs/${nameFile}.pdf`;
    this.pageurl = this._domSanitizer.bypassSecurityTrustResourceUrl(apiUrl + '#toolbar=0&navpanes=0&scrollbar=0');
  }



}
