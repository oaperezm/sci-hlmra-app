import { Component, OnInit, OnDestroy } from '@angular/core';

// Servcios
import { StorageService, StructureService} from '../../../service/service.index';

// Modelos
import { Pagination } from '../../../models/general/pagination.model';

import { Content} from '../../../models/content/content.model';
import swal from 'sweetalert2';

import { Appsettings } from '../../../configuration/appsettings';
// COMPONENT SERVICES
import { AdminNavService } from '../../../components/layouts/administrator/admin-nav/admin-nav.service';
import { AdminHeaderService } from '../../../components/layouts/administrator/admin-header/admin-header.service';

@Component({
  selector: 'app-academic-manager',
  templateUrl: './academic-manager.component.html',
  styleUrls: ['./academic-manager.component.scss']
})
export class AcademicManagerComponent implements OnInit, OnDestroy {

  showVideo: boolean;
  showAudio: boolean;
  showFile: boolean;

  content: Content;
  working: boolean;
  showLevel: boolean;
  leves: any[] = [];
  levelId: number ;
  showTarget: boolean;
  contents: Content[] = [];
  optionId: number;
  showListContent: boolean;

  // Paginación
  pagination: Pagination = new Pagination();
  currentPage = 1;
  sizePage = 10;
  constructor(  public _srvStorage: StorageService,
                private _srvStructure: StructureService,
                public _srvAdminNav: AdminNavService,
                public _srvHeaderAdmin: AdminHeaderService) {
    this.working = false;
    this.showListContent = true;
    const data = JSON.parse(this._srvStorage.nodes);
    const parent = data.filter( item => item.parentId === 0)[0];
    this.leves = data.filter( item => item.parentId === parent.id);
    this.showLevel = ( this.leves.length > 1) ? true : false;
    this.showTarget = ( this.leves.length > 1) ? false : true;
    if ( this.leves.length > 0) {
      this.levelId = this.leves[0].id;
    }
    this.optionId = 1;
    this.getContentbyOption( this.optionId );
  }

  ngOnInit() {
    this._srvAdminNav.setTitle('Gestor académico');
    this._srvHeaderAdmin.setUrlBackButton(['/resource/home', 'Inicio']);
  }

  ngOnDestroy(): void { 
    this._srvHeaderAdmin.setUrlBackButton([]);
  }

  // Busca contenido segun  la opcion seleccionada
  getContentbyOption( optionId: number ) {
    this.optionId = optionId;
    this.working = true;
    this.showListContent = true;
    this.setContent( this.optionId, { page: 1, size: 10 } );

  }

  onChangeSelect() {
    this.showTarget = false;
  }

  selectShowMultimedia( item ) {

    // Se agrega la función para realizar la inserción de los eventos dentro del contenido multimedia
    // this.working = true;

    // this._srvUser.getProfileData().subscribe(function (res) {
    //   this.userData.push(res);
    //   this.userEmail.push(this.userData.data.email);
    // });
    // console.log(this.userEmail);
    // console.log(this.userData);
    // this._srvUser.validateUserSIDE('dirsecundaria@activa.edu.mx').subscribe(res1 => {
    //   console.log(res1);
    //   // this._srvUser.insertEventSIDE(this.userData).subscribe(res => {
    //   // });
    //   this.working = false;
    // }, err => {
    //   this.working = false;
    // });
    // // this._srvUser.insertEventSIDE();

    this.showListContent = false;
    this.content = item;
    this.validateContentType( this.content.fileTypeDesc);
  }

  backList() {
    this.working = true;
    this.showListContent = true;
    this.setContent( this.optionId, { page: 1, size: 10 } );
  }

  // Valida el tipo de archivo para mostra el visor o reproductor respectivamente
  validateContentType( typeDesc: string ) {
    switch ( typeDesc ) {
      case 'Audio':
      this.showVideo = false;
      this.showAudio = true;
      this.showFile = false;
        break;
      case 'Video':
      this.showVideo = true;
      this.showAudio = false;
      this.showFile = false;
        break;
      case 'Documento':
      case 'Imprimibles':
      this.showVideo = false;
      this.showAudio = false;
      this.showFile = true;
        break;

      default:
        break;
    }
  }

  // Obtiene los contenidos
  setContent( optionId: number, paginationData: object) {
    this._srvStructure.getContenByNodeIdAcademic(this.levelId, optionId, Number(paginationData['page'])).subscribe( result => {
      this.contents = [];
      if ( result.success ) {
        this.pagination.pageNumber = Number(paginationData['page']);
        this.pagination.pageSize = result.pagination.pageSize;
        this.pagination.showNextPage = result.pagination.showNextPage;
        this.pagination.showPreviousPage = result.pagination.showPreviousPage;
        this.pagination.total = result.pagination.total;
        this.pagination.totalPage = result.pagination.totalPage;
        this.sizePage = paginationData['size'];

        for ( let c of result.data ) {
          let content = new Content();
          content.active = c.active;
          content.area = c.area;
          content.content = c.content;
          content.contentResourceType = c.contentResourceType;
          content.contentTypeDesc = c.contentTypeDesc;
          content.contentTypeId = c.contentTypeId;
          content.fileTypeDesc = c.fileTypeDesc;
          content.fileTypeId = c.fileTypeId;
          content.formativeField = c.formativeField;
          content.id = c.id;
          content.isPublic = c.isPublic;
          content.name = c.name;
          content.purpose = c.purpose;
          content.thumbnails = c.thumbnails;
          content.totalContent = c.totalContent;
          content.trainingField = c.trainingField;
          content.urlContent =  c.urlContent;

          this.contents.push( content);
        }
      } else {
        swal( Appsettings.APP_NAME, result.message, 'error');

      }
      this.working = false;
    });
  }

  onChangePage(page: number, cantContinue: boolean) {
    if (!cantContinue) {
      this.currentPage = page;
      this.setContent(this.optionId , { page: page, size: this.sizePage });
    }
  }
}
