import { Routes, RouterModule } from '@angular/router';



// Servicios
import { AuthGuardService } from '../../service/service.index';

import { ResourceLayoutComponent } from './resource-layout.component';
import { ContentComponent } from './content/content.component';
import { HomeComponent } from './home/home.component';
import { MessagesComponent } from './messages/messages.component';
import { ShowMultimediaComponent } from '../../components/administrator/multimedia/show-multimedia/show-multimedia.component';
import { HelpPageComponent } from './help-page/help-page.component';
import { PrivacyPageComponent } from './privacy-page/privacy-page.component';
import { FrequentQuestionsComponent } from './frequent-questions/frequent-questions.component';
import { ServiceComponent } from './service/service.component';
import { AcademicManagerComponent } from './academic-manager/academic-manager.component';
import { LibrosTextoComponent } from './libros-texto/libros-texto.component';
import { LibrosTextoDigitalComponent } from './libros-texto-digital/libros-texto-digital.component';
import { EpubReaderComponent } from '../../components/administrator/multimedia/epub-reader/epub-reader.component';

const AppPageResource: Routes = [
  {
    path: 'resource',
    component: ResourceLayoutComponent,
    children: [
        { path: 'home', component: HomeComponent, canActivate: [ AuthGuardService] },
        { path: 'content/:nodeId', component: ContentComponent, canActivate: [AuthGuardService] },
        { path: 'messages', component: MessagesComponent, canActivate: [ AuthGuardService] },
        { path: 'show-multimedia/:nodeId', component: ShowMultimediaComponent, canActivate: [ AuthGuardService] },
        { path: 'privacy', component: PrivacyPageComponent, canActivate: [ AuthGuardService] },
        { path: 'help', component: HelpPageComponent, canActivate: [ AuthGuardService ] },
        { path: 'frequent-questions', component: FrequentQuestionsComponent, canActivate: [ AuthGuardService] },
        { path: 'services/:id', component: ServiceComponent, canActivate: [ AuthGuardService] },
        { path: 'academic-manager', component: AcademicManagerComponent, canActivate: [AuthGuardService] },
        { path: 'libros-texto/:nodeId', component: LibrosTextoComponent, canActivate: [AuthGuardService] },
        { path: 'libros-texto-digital/:nodeId', component: LibrosTextoDigitalComponent, canActivate: [AuthGuardService] },
        { path: 'epub-reader/:url', component: EpubReaderComponent },
        { path: '', redirectTo: '/', pathMatch: 'full'},
    ]
  }
];
export const RESOURCE_ROUTES = RouterModule.forChild( AppPageResource );
