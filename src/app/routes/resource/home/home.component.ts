import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

// Servicios
import {  StructureService,
          StorageService,
          QuestionsService,
          GeneralEventService,
          ModuleService,
          MenuService,
          SettingsService,
          EventService,
          CityService,
          StudenService,
          UserService } from '../../../service/service.index';

// COMPONENT SERVICES
import { AdminNavService } from '../../../components/layouts/administrator/admin-nav/admin-nav.service';

// Modelo
import { Node } from '../../../models/content/node.model';
import { Appsettings } from '../../../configuration/appsettings';

import swal from 'sweetalert2';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  title: string;
  structureParentId: number;
  nodes: any[] = [];
  leves: any[] = [];
  qe: any;
  showTagExamen: boolean;
  menu: any[] = [];
  modules: any[] = [];
  ids: any[] = [];
  constructor(  public _srvStorage: StorageService,
                private _srvStructure: StructureService,
                private _router: Router,
                private _srvQuestions: QuestionsService,
                public _srvAdminNav: AdminNavService,
                public _srvNavService: AdminNavService,
                private _srvModules: ModuleService,
                public _srvMenu: MenuService,
                public _srvEvente: EventService,
                public _srvCity: CityService,
                public _srvStudent: StudenService,
                public _srvUser: UserService ) {
                  this.menu = _srvMenu.getMenu();
    this.menu = this.menu.filter( item => item.url !== '/resource/home');

    // this._srvEvente.getUserWp().subscribe( result => {
    //   //console.log(result);
    // });

    // this._srvUser.getaDataUserSIDE('vacosta@larousse.com.mx').subscribe( data => {
    //   console.log(data);
    // });





    // this._srvCity.getAllGroups().subscribe( res => {


    //   //console.log(res);
    // });
  }

  ngOnInit() {
    this._srvStorage.showBackBotton = false;
    this.qe = this._srvQuestions.getQuestions();
    this.getNodes();
    this._srvAdminNav.setTitle('Inicio');
    this.showTagExamen = this._srvStorage.rol == 'Profesor que imparte cursos' ? true : false;
    // // SET CONTROLS
    if (this._srvStorage.showModal === 0) {
      this._srvStorage.showModal = 1;
      swal(Appsettings.APP_NAME, 'Bienvenido a nuestra plataforma de recursos académicos  para profesores de secundaria pública.', 'success');

    }

  }

  // confirmInvitation( code){
  //   console.log(code);

  //   this._srvStudent.confirmInvitation(code).subscribe(res => {
  //     console.log(res);

  //   })
  // }

  onClickLevel( level) {
    this._router.navigateByUrl(`/resource/content/${ level.id}`);
  }

  getNodes() {
    this.structureParentId = 124;
    // this.structureParentId = 145;
    this._srvStructure.getStructureByParent(this.structureParentId).subscribe( res => {
      // let nodes = JSON.parse(this._srvStorage.nodes);
      let nodes = res.data;
      this._srvStorage.nodes = JSON.stringify(res.data);
      const parent = nodes.filter( item => item.parentId === 0)[0];
      for ( const node of nodes ) {
        const childrens = new Node();
        childrens.id = node.id;
        childrens.active = node.active;
        childrens.description = node.description;
        childrens.nodeTypeId = node.nodeTypeId;
        childrens.parentId = node.parentId;
        childrens.urlImage = node.urlImage;

        this.nodes.push( childrens );

        this._srvStorage.nodes = JSON.stringify( this.nodes );
      }
      this.leves = this.nodes.filter( item => item.parentId === parent.id);
      if ( this.leves.length === 1) {
        this._srvStorage.nodeId = this.leves[0].id;
        const description = this.leves[0].description;
        this.setContent( this._srvModules.getModule( description.trim() ));
      } else {
        const description = this.setLeves( this.leves);
        this._srvStorage.nodeId = description.id;
        this.setContent( this._srvModules.getModule( description.name.trim() ) );
      }
    });

  }

  setLeves( leves){
    const values: any[] = [];
    const ids: number[] = [];

    for( let l of leves ) {
      const description = l.description
      switch ( description.trim() ) {
        case 'Preescolar':
          values[1] = {name: 'Preescolar', id: l.id };
          ids.push(1);
          break;
        case 'Primaria':
          values[2] = { name: 'Primaria', id: l.id };
          ids.push(2);
          break;
        case 'Secundaria':
          values[3] = { name: 'Secundaria', id: l.id };
          ids.push(3);
          break;
        default:
          break;
      }
    }
    return values[Math.max.apply(null, ids )];
  }

  setContent( content) {
    this.modules = [];
    for ( let m of content ) {
      for ( let mo of  this.menu) {

        if ( m.title === mo.title ) {
         this.modules.push( mo );
        }
      }
    }

  }
}
