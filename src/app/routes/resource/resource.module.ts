import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule} from '@angular/forms';
import { CommonModule } from '@angular/common';
import {  EmbedVideoService } from 'ngx-embed-video';
import { NgSelectModule } from '@ng-select/ng-select';


import { RESOURCE_ROUTES } from './resource.routes';

// Modulos
import { AdministratorModule } from '../../components/layouts/administrator/administrator.module';

// Componentes
import { ResourceLayoutComponent } from './resource-layout.component';
import { AdmonComponentModule } from '../../components/administrator/admon-component.module';

import {
  MatTabsModule,
  MatInputModule,
  MatDatepickerModule,
  MatRadioModule,
  MatCardModule,
  MatSelectModule,
  MatDialogModule,
  MatIconModule,
  MatButtonModule,
  MatCheckboxModule,
  MatTableModule,
  MatProgressBarModule,
  MatProgressSpinnerModule,
  MatMenuModule,
  MatPaginatorModule,
  MatNativeDateModule,
  MatSlideToggleModule,
  MatToolbarModule,
  MatBadgeModule,
  MatListModule,
  MatAutocompleteModule,
  MatChipsModule,
  MatDividerModule,
  MatExpansionModule,
  MatButtonToggleModule,
  MatTooltipModule,
  MatSidenavModule
} from '@angular/material';

import { ContentComponent } from './content/content.component';
import { HomeComponent } from './home/home.component';
import { MessagesComponent } from './messages/messages.component';
import { LibrosTextoComponent } from './libros-texto/libros-texto.component';
import { LibrosTextoDigitalComponent } from './libros-texto-digital/libros-texto-digital.component';

// Modulos
import { CoreModule } from '../../core/core.module';

// Modals
import { DialogSearchUserComponent} from '../../dialogs/dialog-search-user/dialog-search-user.component';
import { HelpPageComponent } from './help-page/help-page.component';
import { PrivacyPageComponent } from './privacy-page/privacy-page.component';
import { FrequentQuestionsComponent } from './frequent-questions/frequent-questions.component';
import { DialogResponseQuestionComponent } from '../../dialogs/dialog-response-question/dialog-response-question.component';
import { ServiceComponent } from './service/service.component';
import { AcademicManagerComponent } from './academic-manager/academic-manager.component';
import { DialogGetCctComponent } from '../../dialogs/dialog-get-cct/dialog-get-cct.component';
import { JoyrideService, JoyrideModule } from 'ngx-joyride';
@NgModule({
  declarations: [
    ResourceLayoutComponent,
    ContentComponent,
    HomeComponent,
    MessagesComponent,
    DialogSearchUserComponent,
    HelpPageComponent,
    PrivacyPageComponent,
    FrequentQuestionsComponent,
    DialogResponseQuestionComponent,
    ServiceComponent,
    AcademicManagerComponent,
    LibrosTextoComponent,
    LibrosTextoDigitalComponent,
    DialogGetCctComponent
  ], exports: [
  ],
  imports: [
    RESOURCE_ROUTES,
    MatTabsModule,
    MatInputModule,
    MatDatepickerModule,
    MatRadioModule,
    MatCardModule,
    MatSelectModule,
    MatDialogModule,
    MatIconModule,
    MatButtonModule,
    MatCheckboxModule,
    MatTableModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatMenuModule,
    MatPaginatorModule,
    MatNativeDateModule,
    MatSlideToggleModule,
    MatToolbarModule,
    MatBadgeModule,
    MatListModule,
    MatAutocompleteModule,
    MatChipsModule,
    MatDividerModule,
    MatExpansionModule,
    MatButtonToggleModule,
    MatTooltipModule,
    AdministratorModule,
    AdmonComponentModule,
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    MatSidenavModule,
    CoreModule,
    NgSelectModule,
    JoyrideModule.forChild()
  ],
  providers: [
    EmbedVideoService
  ],
  entryComponents: [
    DialogSearchUserComponent,
    DialogResponseQuestionComponent,
    DialogGetCctComponent
  ]
})
export class ResourceModule { }
