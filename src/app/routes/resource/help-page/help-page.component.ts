import { Component, OnInit } from '@angular/core';
import {BrowserModule, DomSanitizer, SafeResourceUrl} from '@angular/platform-browser';

import { Appsettings } from '../../../configuration/appsettings';

// Servicios
import { StorageService } from '../../../service/service.index';
@Component({
  selector: 'app-help-page',
  templateUrl: './help-page.component.html',
  styleUrls: ['./help-page.component.scss']
})
export class HelpPageComponent implements OnInit {

  working: boolean;
  pageurl: SafeResourceUrl;

  constructor(  public _srvStorage: StorageService,
                private _domSanitizer: DomSanitizer) {
    this.working = false;
    this.viewFile(1);
  }

  ngOnInit() {
    this._srvStorage.title = 'Pagina de ayuda';
  }

  viewFile(idFile: number) {


    let apiUrl = `${Appsettings.API_ENDPOINT_FULL}/contents/206/download`;
    this.pageurl = this._domSanitizer.bypassSecurityTrustResourceUrl(apiUrl + '#toolbar=0&navpanes=0&scrollbar=0');

  }


}
