import { Component, OnInit, OnDestroy } from '@angular/core';
import {BrowserModule, DomSanitizer, SafeResourceUrl} from '@angular/platform-browser';

import { Appsettings } from '../../../configuration/appsettings';

// COMPONENT SERVICES
import { AdminNavService } from '../../../components/layouts/administrator/admin-nav/admin-nav.service';
import { AdminHeaderService } from '../../../components/layouts/administrator/admin-header/admin-header.service';

// Servicios
import { StorageService } from '../../../service/service.index';
@Component({
  selector: 'app-privacy-page',
  templateUrl: './privacy-page.component.html',
  styleUrls: ['./privacy-page.component.scss']
})
export class PrivacyPageComponent implements OnInit, OnDestroy {

  working: boolean;
  pageurl: SafeResourceUrl;

  constructor(  public _srvStorage: StorageService,
                private _domSanitizer: DomSanitizer,
                public _srvAdminNav: AdminNavService,
                private _srvHeaderAdmin: AdminHeaderService ) {
    this.working = false;
    this.viewFile(1);
  }

  ngOnInit() {
    this._srvAdminNav.setTitle('Aviso de privacidad');
    this._srvHeaderAdmin.setUrlBackButton(['/resource/home', 'Inicio']);

  }

  ngOnDestroy(): void {
    this._srvHeaderAdmin.setUrlBackButton([]);
  }

  viewFile(idFile: number) {
    let apiUrl = `../../../../assets/docs/POLÍTICA_DE_PRIVACIDADLAROUSSEYPATRIA2019.pdf`;
    this.pageurl = this._domSanitizer.bypassSecurityTrustResourceUrl(apiUrl + '#toolbar=0&navpanes=0&scrollbar=0');
  }


}
