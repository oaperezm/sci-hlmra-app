import { Component, OnInit, OnDestroy } from '@angular/core';
import { MatDialog } from '@angular/material';
// COMPONENT SERVICES
import { AdminNavService } from '../../../components/layouts/administrator/admin-nav/admin-nav.service';


// Servicios
import { StorageService, QuestionsService } from '../../../service/service.index';

// Dialogo
import { DialogResponseQuestionComponent } from '../../../dialogs/dialog-response-question/dialog-response-question.component';

import { AdminHeaderService } from '../../../components/layouts/administrator/admin-header/admin-header.service';

@Component({
  selector: 'app-frequent-questions',
  templateUrl: './frequent-questions.component.html',
  styleUrls: ['./frequent-questions.component.scss']
})
export class FrequentQuestionsComponent implements OnInit, OnDestroy {

  questions: any;
  working: boolean;
  constructor(  public _srvStorage: StorageService,
                private _srvQuestions: QuestionsService,
                public dialog: MatDialog,
                public _srvAdminNav: AdminNavService,
                private _srvHeaderAdmin: AdminHeaderService ) {
    this.working = false;
    this.questions = this._srvQuestions.getQuestions();
                }

  ngOnInit() {
    this._srvHeaderAdmin.setUrlBackButton(['/resource/home', 'Inicio']);
    this._srvAdminNav.setTitle('Ayuda - Preguntas frecuentes');
  }

  ngOnDestroy(): void {
    this._srvHeaderAdmin.setUrlBackButton([]);
  }
  // Mostrar dialogo con la respuesta de la pregunta
  onShowResponse( question ) {
    const DIALOG_REF = this.dialog.open( DialogResponseQuestionComponent, {
      width: '750px',
      height: '500px',
      autoFocus: false,
      disableClose: true,
      data: question
    });

    DIALOG_REF.afterClosed().subscribe(response => {
    });
  }

}
