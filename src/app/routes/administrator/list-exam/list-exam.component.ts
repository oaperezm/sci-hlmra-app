import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { Appsettings } from '../../../configuration/appsettings';
import { MatDialog } from '@angular/material';

import swal from 'sweetalert2';
import * as pdfMake from 'pdfmake/build/pdfmake';
import * as pdfFonts from 'pdfmake/build/vfs_fonts';

// MODELS
import { Pagination } from '../../../models/general/pagination.model';
import { TeacherExam } from '../../../models/courses/teacher-exam.model';

// SERVICES
import { TeacherExamService, PdfService, StorageService } from '../../../service/service.index';

// CONPONENT SERVICES
import { AdminNavService } from '../../../components/layouts/administrator/admin-nav/admin-nav.service';
import { AdminHeaderService } from '../../../components/layouts/administrator/admin-header/admin-header.service';

// DIALOGOS
import { DialogPreviewExamComponent } from '../../../dialogs/dialog-preview-exam/dialog-preview-exam.component';
import { DialogExamWeightingComponent } from '../../../dialogs/dialog-exam-weighting/dialog-exam-weighting.component';
import { JoyrideService } from 'ngx-joyride';

@Component({
  selector: 'app-list-exam',
  templateUrl: './list-exam.component.html',
  styleUrls: ['./list-exam.component.scss']
})
export class ListExamComponent implements OnInit, OnDestroy {
  fotos: string[] = [];
  working = false;
  teacherExams: TeacherExam[] = [];
  typeFilter = '0';

  textSearch: string;
  sizePage: string = Appsettings.DEFAULT_SIZE_PAGE;
  page = 1;
  pagination: Pagination = new Pagination();

  constructor(private _srvTeacherExam: TeacherExamService,
    private _router: Router,
    private _srvPdf: PdfService,
    public _srvNavService: AdminNavService,
    public _srvHeaderAdmin: AdminHeaderService,
    public dialog: MatDialog,
    private joyride: JoyrideService,
    public _srvStorage: StorageService ) {
    pdfMake.vfs = pdfFonts.pdfMake.vfs;
    this.setPage({ page: 1, size: this.sizePage });
  }

  ngOnInit() {
    this._srvNavService.setTitle('Administrar exámenes');
    this._srvHeaderAdmin.setUrlBackButton(['/resource/home', 'Inicio']);

    this.preloadImages(this.fotos);
    if ( this._srvStorage.showTourGexan === 0) {
      this._srvStorage.showTourGexan = 1;
      this.joyride.startTour(
        {
          steps: ['firstStep'],
          customTexts: {
            next: '>>',
            prev: '<<',
            done: 'Cerrar'
          }
        }
      )
    }

  }

  ngOnDestroy(): void {
    this._srvHeaderAdmin.setUrlBackButton([]);
  }

  preloadImages(ar) {

    this.toDataURL('~/../../../../../assets/img/exam/Header.png', function (dataUrl) {
      ar.push(dataUrl);
    });
  }

  toDataURL(url, callback) {
    const xhr = new XMLHttpRequest();
    xhr.onload = function () {
      const reader = new FileReader();
      reader.onloadend = function () {
        callback(reader.result);
      };
      reader.readAsDataURL(xhr.response);
    };
    xhr.open('GET', url);
    xhr.responseType = 'blob';
    xhr.send();
  }

  // Realizar búsqueda por nombre
  onKeyUp(event: any): void {
    if (this.textSearch.length >= 4 || this.textSearch.length === 0) {
      this.setPage({ page: 1, size: this.sizePage });
    }
  }

  // Obtener los exámenes registrados del maestro
  setPage(paginationData: object) {

    this.working = true;
    this._srvTeacherExam.getByUserPagination(Number(paginationData['size']),
      Number(paginationData['page']),
      this.textSearch, Number(this.typeFilter)).subscribe(res => {
        this.teacherExams = [];
        let index = 1;

        if (res.success) {

          this.pagination.pageNumber = Number(paginationData['page']);
          this.pagination.pageSize = res.pagination.pageSize;
          this.pagination.showNextPage = res.pagination.showNextPage;
          this.pagination.showPreviousPage = res.pagination.showPreviousPage;
          this.pagination.total = res.pagination.total;
          this.pagination.totalPage = res.pagination.totalPage;
          this.sizePage = paginationData['size'];
          for (const t of res.data) {

            const exam = new TeacherExam();

            exam.id = t.id;
            exam.active = t.active;
            exam.description = t.description;
            exam.totalQuestions = t.totalQuestions;
            exam.teacherExamType = t.teacherExamTypes;
            exam.weighting = t.weighting;
            exam.isAutomaticValue = t.isAutomaticValue;
            exam.maximumExamScore = t.maximumExamScore;
            exam.index = this.pagination.getIndexRegister(index);
            this.teacherExams.push(exam);
            index++;
          }
        }

        this.working = false;

      }, err => {
        this.working = false;
        swal(Appsettings.APP_NAME, 'Problemas al conectarnos con el servidor', 'warning');
      });


  }

  // Agregar un nuevo exámen
  onClickAdd(): void {
    this._router.navigateByUrl('/admin/generate-exam');
  }

  onClickEdit(element) {
    this._router.navigateByUrl(`/admin/generate-exam/${element.id}`);
  }

  onClickDelete(element): void {

    swal({
      title: '¿Estás seguro?',
      text: '¡Esta operación no se podrá revertir!',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: '¡Si, eliminar ahora!',
      cancelButtonText: 'Cancelar'
    }).then((result) => {

      if (result.value) {
        this.working = true;
        this._srvTeacherExam.delete(element.id).subscribe(res => {

          if (res.success) {
            this.teacherExams = this.teacherExams.filter(x => x.id !== element.id);
            this.setPage({ page: 1, size: this.sizePage });
            swal(Appsettings.APP_NAME, res.message, 'success');
          } else {
            swal(Appsettings.APP_NAME, res.message, 'error');
          }

          this.working = false;
        }, err => {
          this.working = false;
        });
      }

    });

  }

  // Metodo genera el pdf del examen
  onClickPDF(element, typeExam: boolean) {
    this.working = true;
    const pic1 = this.fotos[0];

    this._srvTeacherExam.getById(element.id).subscribe(res => {


      if (res.data) {
        const examQuestions = res.data.teacherExamQuestion;
        const questions: any[] = [];
        for (const q of examQuestions) {
          // tslint:disable-next-line:max-line-length
          questions.push({
            'answers': q.question.answers,
            'content': q.question.content,
            'id': q.question.id,
            'questionTypeId': q.question.questionTypeId,
            'explanation': q.question.explanation,
            'urlImage': q.question.urlImage,
            'imageBase64': q.question.imageBase64,
            'imageHeight': q.question.imageHeight,
            'imageWidth': q.question.imageWidth
          });
        }

        const exam = {
          'name': res.data.description,
          'data': {
            'questions': questions,
            'name': res.data.description,
            'description': res.data.description,
          }
        };
        this._srvPdf.questionBank(exam, pic1, typeExam);
      } else {
        swal(Appsettings.APP_NAME, 'Error al obtener los datos, vuelve a intentar más tarde', 'error');
      }
      this.working = false;
    }, err => {
      this.working = false;

    });
  }

  onChangeTypeFilter() {
    this.setPage({ page: 1, size: this.sizePage });
  }

  onShowPreview(element): void {

    const DIALOG_REF = this.dialog.open(DialogPreviewExamComponent, {
      width: '850px',
      height: '700px',
      autoFocus: false,
      disableClose: true,
      data: element
    });

    DIALOG_REF.afterClosed().subscribe(response => {
    });
  }


  onClickWeighting(element): void {
    const DIALOG_REF = this.dialog.open(DialogExamWeightingComponent, {
      width: '850px',
      autoFocus: false,
      disableClose: true,
      data: element
    });

    DIALOG_REF.afterClosed().subscribe(response => {
    });
  }

}
