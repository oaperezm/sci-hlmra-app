import { Component, OnInit, OnDestroy } from '@angular/core';
import { AdminHeaderService } from '../../../../components/layouts/administrator/admin-header/admin-header.service';

@Component({
  selector: 'app-noaccess',
  templateUrl: './noaccess.component.html',
  styleUrls: ['./noaccess.component.scss']
})
export class NoaccessComponent implements OnInit, OnDestroy {

  constructor(  public _srvHeaderAdmin: AdminHeaderService ) { }

  ngOnInit() {
    this._srvHeaderAdmin.setUrlBackButton(['/resource/home', 'Inicio']);

  }

  ngOnDestroy(): void {
    this._srvHeaderAdmin.setUrlBackButton([]);
  }
}
