import { Injectable } from '@angular/core';
import { Appsettings } from 'src/app/configuration/appsettings';
import { HttpService } from '../../../service/security/http.service'
import { map, catchError } from 'rxjs/operators';
import { HttpErrorResponse } from '@angular/common/http';
import { throwError } from 'rxjs';
import { ResponseContentType } from '@angular/http';

@Injectable({
  providedIn: 'root'
})
export class CustomReportsService {

  constructor(  private httpService: HttpService  ) { }

  getUsersReport () {
    const API_URL = `${Appsettings.API_ENDPOINT_FULL}/report/custom/users`;
    return this.httpService.get(API_URL, { responseType: ResponseContentType.Blob })
      .pipe(
        map(response => {
            return new Blob([(<any>response)._body], { type: 'application/excel' })
          }),catchError(this.errorHandler)
      );
  }

  getContentSummaryReport () {
    const API_URL = `${Appsettings.API_ENDPOINT_FULL}/report/custom/contentSummary`;
    return this.httpService.get(API_URL, { responseType: ResponseContentType.Blob })
      .pipe(
        map(response => {
            return new Blob([(<any>response)._body], { type: 'application/excel' })
          }),catchError(this.errorHandler)
      );
  }

  errorHandler(error: HttpErrorResponse) {
    console.log('Ha ocurrido un error al ejecutar el endpoint: ', error);
    return throwError(error);
  }
}
