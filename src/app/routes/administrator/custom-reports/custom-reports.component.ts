import { Component, OnInit } from '@angular/core';
import { CustomReportsService } from './custom-reports.service';
import { FileSaverService } from 'ngx-filesaver';

export interface ReportElement {
  position: number;
  name: string;
  description: string;
}

const ELEMENT_DATA: ReportElement[] = [
  {position: 1, name: 'Reporte de usuarios'    , description: 'Genera el listado de usuarios actuales en el sistema'},
  {position: 2, name: 'Sumarisado de contenido', description: 'Genera una tabla con los totales por tipo de los contenidos en el sistema'},
];

@Component({
  selector: 'app-custom-reports',
  templateUrl: './custom-reports.component.html',
  styleUrls: ['./custom-reports.component.scss']
})
export class CustomReportsComponent implements OnInit {

  displayedColumns: string[] = ['position', 'name', 'description', 'action'];
  dataSource = ELEMENT_DATA;
  working = false;

  constructor(private reportService: CustomReportsService,
              private fileSaverService: FileSaverService) { }

  ngOnInit() {
  }

  download(position: number) {
    if (position === 1) {
      this.downloadUsersReport();
    }
    if (position === 2) {
      this.downloadContentSummaryRepoort();
    }
  }

  downloadUsersReport() {
    this.working = true;
    this.reportService.getUsersReport().subscribe((response: any) => {
      this.working = false;
      this.fileSaverService.save(response, 'usuarios.xls');
    }, (error) => {
      this.working = false;
      console.error(error);
    });
  }

  downloadContentSummaryRepoort() {
    this.working = true;
    this.reportService.getContentSummaryReport().subscribe((response: any) => {
      this.working = false;
      this.fileSaverService.save(response, 'contenido.xls');
    }, (error) => {
      this.working = false;
      console.error(error);
    });
  }
}
