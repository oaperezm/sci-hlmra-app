import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { TreeviewItem, TreeviewConfig } from 'ngx-treeview';
import { MatTableDataSource, MatPaginator, MatDialog } from '@angular/material';
import { ActivatedRoute } from '@angular/router';
import { Appsettings } from '../../../configuration/appsettings';

// MODELS
import { Question } from '../../../models/courses/question.model';
import { QuestionBank } from '../../../models/courses/question-bank.model';
import { Answer } from '../../../models/courses/answer.model';
import { FileContent } from '../../../models/catalogs/file.model';
import { QuestionType } from '../../../models/courses/question-type.model';

// SERVICES
import { QuestionBankService , NodeService, CatalogService, StorageService, StructureService} from '../../../service/service.index';


// DIALOG
import { DialogResourceComponent } from '../../../dialogs/dialog-resource/dialog-resource.component';

// COMPONENT MODELS
import { AdminNavService } from '../../../components/layouts/administrator/admin-nav/admin-nav.service';
import { AdminHeaderService } from '../../../components/layouts/administrator/admin-header/admin-header.service';
import { TreeUtilities } from '../../../shared/tree.utilities';

import swal from 'sweetalert2';
import { FileHolder } from 'angular2-image-upload';
import { JoyrideService } from 'ngx-joyride';

@Component({
  selector: 'app-question-bank',
  templateUrl: './question-bank.component.html',
  styleUrls: ['./question-bank.component.scss']
})
export class QuestionBankComponent implements OnInit, OnDestroy {


  showAddAnswerButton: boolean;

  questionBank: QuestionBank = new QuestionBank();
  working = false;
  frmQuestions: FormGroup;
  frmQuestionBank: FormGroup;
  answers: Answer[] = [];
  showForm: boolean;
  showTreeStructure = false;
  editMode = true;
  selectedQuestion: Question;

  structure: any[] = [];
  selectedItemPath = '';
  value;
  items: TreeviewItem[] = [];
  config = TreeviewConfig.create({
      hasFilter: true,
      hasCollapseExpand: true
  });

  questionTypes: any[] = [];

  displayedColumns: string[] = [
    'content',
    'questionType.description'];

  dataSource = new MatTableDataSource<Question>( this.questionBank.questions );
  nodos: any[] = [];

  // IMAGEN DE PREGUNTA
  imageProfile: string;
  fileContent: File;
  fileContentBase: string;
  fileName: string;
  isUpdateFile: boolean;
  nodeType: number;

  @ViewChild('formQuestion') formQuestion;
  @ViewChild('imageUpload') imageUpload;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  // CONSTURCTOR
  constructor( private _srvQuestionBank: QuestionBankService,
               private _srvStorage: StorageService,
               private _route: ActivatedRoute,
               private _srvNode: NodeService,
               private _srvCatalog: CatalogService,
               public dialog: MatDialog,
               public _srvNavService: AdminNavService,
               public _srvHeaderAdmin: AdminHeaderService,
               public _srvStructure: StructureService,
               private joyride: JoyrideService) {

                setTimeout(() => this.getNodes(), 0);

    // Formulario de pregunta
    this.frmQuestions = new FormGroup({
      id: new FormControl(0),
      question: new FormControl('', [Validators.required]),
      explanation: new FormControl(''),
      questionTypeId: new FormControl('', [Validators.required]),
    });

    // Formulario para banco de preguntas
    this.frmQuestionBank = new FormGroup({
      id: new FormControl(0),
      name: new FormControl('', [ Validators.required ]),
      description: new FormControl('', [ Validators.required ]),
      nodeId: new FormControl('', [ Validators.required ])
    });

    this.questionBank = new QuestionBank();
    this.dataSource.paginator = this.paginator;
    this.showTreeStructure = true;





  }

  ngOnInit( ) {
    const title = this.questionBank.id > 0 ? 'Editar banco de preguntas' : 'Crear banco de preguntas';
    // Recibe los parametros de la url
    this._route.params.subscribe( params => {
      this.questionBank.id = params['id'];
      this.nodeType = Number(params['type']);
      // Validamos si es edicion
      if ( this.questionBank.id > 0 ) {
         this.working = true;
         this._srvQuestionBank.getBydId( this.questionBank.id).subscribe( res => {
           if ( res.success ) {

             this.setQuestionBank( res.data );
             this.showTreeStructure = false;
             this.editMode  = false;
           }

           this.working = false;
         }, err => {
           this.working = false;
         });
     }
   });
    // SET CONTROLS
    this._srvNavService.setTitle(title);
    this._srvHeaderAdmin.setUrlBackButton(['/admin/list-question-bank', 'Banco de preguntas']);

    // Obtener todos los tipos de respuestas
    this._srvCatalog.getQuestionTypesAll().subscribe( res => {
      if ( res.success ) {
        for ( const type of res.data ) {
          this.questionTypes.push( { id: type.id, name: type.description });
        }
      }
    });
    if ( this._srvStorage.showTourQbank === 0 && this.nodeType === 1) {
      this._srvStorage.showTourQbank = 1;
      this.joyride.startTour(
        {
          steps: ['firstStep1', 'firstStep2' , 'firstStep3'],
          customTexts: {
            next: '>>',
            prev: '<<',
            done: 'Cerrar'
          }
        }
      )
    }


  }

  ngOnDestroy(): void {
    this._srvHeaderAdmin.setUrlBackButton([]);
  }

  // Evento que se ejecuta al dar clic en la opción eliminar de la tabla de preguntas
  onClickDeleteQuestion( question: Question ): void {

    swal({
      title: '¿Estás seguro?',
      text: '¡Esta operación no se podrá revertir!',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: '¡Si, eliminar ahora!',
      cancelButtonText: 'Cancelar'
    }).then((result) => {
      if (result.value) {

        this.working = true;
        this._srvQuestionBank.removeQuestion( question.id ).subscribe( res => {

          if ( res.success ) {
            this.questionBank.questions = this.questionBank.questions.filter( x => x.id !== question.id);
            this.resetQuestinTable();
            swal(Appsettings.APP_NAME, res.message, 'success');
          } else {
            swal(Appsettings.APP_NAME, res.message, 'error');
          }
          this.working = false;
        }, err => {
          this.working = false;
          swal(Appsettings.APP_NAME, 'Error de comunicación con el servidor', 'error');
        });


      }
    });

  }

  // Evento que se ejecuta al dar clic al formulario del banco de preguntas
  onSubmitBank( ): void {

    this.questionBank.description = this.frmQuestionBank.controls['description'].value;
    this.questionBank.name = this.frmQuestionBank.controls['name'].value;
    this.questionBank.nodeId = this.frmQuestionBank.controls['nodeId'].value;
    this.questionBank.id = this.frmQuestionBank.controls['id'].value;

    if ( this.questionBank.id > 0 ) {
      this.update();
    } else {
      this.save();
    }
  }

  // Método para actualizar los datos d eun banco de imagenes existente
  update( ): void {

    this.working = true;

    this._srvQuestionBank.update( this.questionBank ).subscribe( res => {
      if ( res.success ) {
        this.showTreeStructure = false;
        this.editMode = false;
        this.selectedItemPath = TreeUtilities.getFullPathNode(this.questionBank.nodeId, this.structure );
        swal(Appsettings.APP_NAME, res.message, 'success');
      } else {
        swal(Appsettings.APP_NAME, res.message, 'error');
      }
      this.working = false;
    }, err => {
      this.working = false;
    });
  }

  // Método para crear un nuevo banco de preguntas
  save( ): void {

    this.working = true;

    this._srvQuestionBank.save( this.questionBank ).subscribe( res => {
      if ( res.success ) {
        this.questionBank.id = res.data.id;
        this.showTreeStructure = false;
        this.editMode = false;
        this.selectedItemPath = TreeUtilities.getFullPathNode(this.questionBank.id, this.structure );
        if ( this._srvStorage.showTourQbankAdd === 0) {
          this._srvStorage.showTourQbankAdd = 1;
          this.joyride.startTour(
            {
              steps: ['addQuestion'],
              customTexts: {
                next: '>>',
                prev: '<<',
                done: 'Cerrar'
              }
            }
          )
        }

        // tslint:disable-next-line:max-line-length
        // swal(Appsettings.APP_NAME, `<p style="text-align: center;">${res.message}.</p> <p style="text-align: center;">Para continuar con el proceso: (1) agregue las preguntas requeridas y (2) vaya al generador de examen.</p>`, 'success');
      } else {
        swal(Appsettings.APP_NAME, res.message, 'error');
      }
      this.working = false;
    }, err => {
      this.working = false;
    });

  }

  // Evento que se ejecuta al dar clic en el boton cancelar del formulario de captura
  onClickCancelQuestion( ): void {

    swal({
      title: '¿Deseas cancelar la operación?',
      text: 'Se perderán los datos en caso de no guardarlos',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: '¡Sí!',
      cancelButtonText: 'Cancelar'
    }).then((result) => {
      if (result.value) {
        this.showForm = false;
        this.resetQuestionForm();
      }
    });
  }

  // Evento que permite mostrar el formulario de captura de la pregunta
  onClickAddQuestion( ): void {
     this.showForm = true;
  }

  // Evento que se ejecuta al cambiar el combo de tipo de pregunta
  onChangeQuestionType( event ): void {

      if (this.answers.length > 0) {
        swal({
          title: '¿Estás seguro?',
          text: 'Si realizas el cambio de tipo de pregunta se perderán las respuestas registradas',
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: '¡Si, cambiar ahora!',
          cancelButtonText: 'Cancelar'
        }).then((result) => {
          if (result.value) {
            this.answers = [];
          } else {
            this.frmQuestions.controls['questionTypeId'].setValue(this.selectedQuestion.questionTypeId);
          }
        });
      } else {
        this.answers = [];
      }

    if ( event.value === 1 || event.value === 3 || event.value === 5 ) {
      this.showAddAnswerButton = true;
    } else {
      this.showAddAnswerButton = false;
    }


    if ( this.frmQuestions.controls['questionTypeId'].value  === 4 ) {
      this.addAnswersTrueFalse();
    }
  }

  onClickEditBank( ): void {
    this.showTreeStructure = true;
    this.editMode = true;
  }

  onClicBankCancel( ): void {

    swal({
      title: '¿Estás seguro?',
      text: '¡Perderá la información actualizada!',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: '¡Sí, cancelar ahora!',
      cancelButtonText: 'Cancelar'
    }).then((result) => {
      if (result.value) {

        this.showTreeStructure = false;
        this.editMode = false;

        if (this.questionBank.id > 0 ) {
          this.frmQuestionBank.controls['description'].setValue( this.questionBank.description );
          this.frmQuestionBank.controls['name'].setValue( this.questionBank.name );
          this.frmQuestionBank.controls['nodeId'].setValue( this.questionBank.nodeId );
          this.frmQuestionBank.controls['id'].setValue( this.questionBank.id );

          this.selectedItemPath = TreeUtilities.getFullPathNode(this.questionBank.nodeId, this.structure );
        }
      }
    });
  }

  // Setear datos de un banco de preguntas
  setQuestionBank( data ): void {
    this.questionBank.active = data.active;
    this.questionBank.description = data.description;
    this.questionBank.name = data.name;
    this.questionBank.nodeId = data.nodeId;
    this.questionBank.questions = data.questions;

   /********************OBTENEMOS LOS NODOS DE LA ESTRUCTURA */
   this.structure = [];
   this.nodos = [];
   this._srvQuestionBank.getStructureWithBanks().subscribe( result => {

     // const fullNodes = JSON.parse(this._srvStorage.nodes);
     const fullNodes = result.data;
     let nodes = [];

     if ( this.questionBank.nodeId > 0 ) {
       const selectedNode = fullNodes.filter( x => x.id ===  this.questionBank.nodeId )[0];
       if ( selectedNode ) {
         if ( selectedNode.id === Appsettings.PLAN_LECTOR_NODE_ID_STRUCUTURE ||
              selectedNode.parentId === Appsettings.PLAN_LECTOR_NODE_ID_STRUCUTURE  ) {
           this.nodeType = 2;
         }
       }
     }
     if ( this.nodeType === 2) {
       nodes = [];
       nodes = fullNodes.filter( n => n.id === Appsettings.PLAN_LECTOR_NODE_ID_STRUCUTURE
                                || n.parentId === Appsettings.PLAN_LECTOR_NODE_ID_STRUCUTURE);
      nodes[0].parentId = 0;

     } else {

       nodes = [];

       nodes = fullNodes;
       // nodes[0].parentId = 0;

     }

     for (const node of nodes) {
       const existNode = this.nodos.filter( x => x.value === node.id )[0];
       if ( existNode === undefined ) {
         const checkedNode = false;
         this.structure.push(node);
         this.nodos.push({
           text: node.description,
           value: node.id,
           collapsed: false,
           checked: checkedNode,
           parentId: node.parentId
         });
       }
     }

     if (this.questionBank.nodeId > 0 ) {
      const node = this.structure.filter( x => x.id === this.questionBank.nodeId )[0];
      if ( node ) {
        this.value = {
          text: node.description,
          value: node.id,
          collapsed: false,
          checked: false,
          parentId: node.parentId
        };
      }
    }

    this.resetQuestinTable();

    this.frmQuestionBank.controls['description'].setValue( this.questionBank.description );
    this.frmQuestionBank.controls['name'].setValue( this.questionBank.name );
    this.frmQuestionBank.controls['nodeId'].setValue( this.questionBank.nodeId );
    this.frmQuestionBank.controls['id'].setValue( this.questionBank.id );

    this.selectedItemPath = TreeUtilities.getFullPathNode(this.questionBank.nodeId, this.structure );
   });


  }

  // Inicializar el formulario de preguntas
  resetQuestionForm( ): void {

    this.frmQuestions.reset();
    this.formQuestion.resetForm();
    this.answers = [];
    this.selectedQuestion = undefined;
    this.frmQuestions.controls['id'].setValue(0);
    this.onRemoved();
    this.imageUpload.deleteFile(this.imageUpload.files[0]);
  }

  resetQuestinTable( ): void {

    this.paginator.pageIndex  = 0;
    this.dataSource           = new MatTableDataSource<Question>( this.questionBank.questions );
    this.dataSource.paginator = this.paginator;
  }

  /********* QUESTION ***********/

  onClickEditQuestion( eleme: Question ): void {
    this.answers = eleme.answers.filter( x => x.id > 0);
    this.showForm = true;
    this.selectedQuestion = eleme;


    let index = 0;
    for ( const a of this.answers ) {
      a.index = index;
      index++;
    }

    this.frmQuestions.controls['id'].setValue(eleme.id);
    this.frmQuestions.controls['question'].setValue(eleme.content);
    this.frmQuestions.controls['explanation'].setValue(eleme.explanation);
    this.frmQuestions.controls['questionTypeId'].setValue(eleme.questionTypeId);
    this.fileName = this.selectedQuestion.urlImage;
    this.isUpdateFile = false;

    if ( eleme.questionTypeId === 1 || eleme.questionTypeId === 3 || eleme.questionTypeId === 5 ) {
      this.showAddAnswerButton = true;
    } else {
      this.showAddAnswerButton = false;
    }

  }

   // Evento que se ejecuta al dar clic al formulario de la preunta
   onSubmit( ): void {

    let fileUpload = new FileContent();

    if ( this.fileContent ) {

      const arrayFileBase = this.fileContentBase.split(',');

      fileUpload = new FileContent(
        arrayFileBase[1],
        this.fileContent.type,
        this.fileContent.name,
        arrayFileBase[0]
      );
    }

    const question = new Question();
    question.active = true;
    question.id = this.frmQuestions.controls['id'].value;
    question.content = this.frmQuestions.controls['question'].value;
    question.questionBankId = this.questionBank.id;
    question.questionTypeId = this.frmQuestions.controls['questionTypeId'].value;
    question.explanation = this.frmQuestions.controls['explanation'].value;
    question.fileContentBase64 = fileUpload.content;
    question.filename = this.fileName;

    if ( question.questionTypeId !== 2 ) {
        question.answers = this.answers;
    }

    if ( question.id === 0 ) {
     this.saveQuestion( question );
    } else {
      this.updateQuestion( question );
    }

  }

  // GUARDAR LOS DATOS DE UNA NUEVA PREGUNTA
  saveQuestion ( question: Question ): void {

    this.working = true;
    this._srvQuestionBank.addQuestion( question ).subscribe( res => {

      if ( res.success ) {
        let description;
        if (this.questionBank.questions === undefined ) {
          this.questionBank.questions = [];
        }
        question.id      = res.data.id;
        const questionType = new QuestionType();
        questionType.id  = res.data.questionType.id;
        questionType.description = res.data.questionType.description;

       if ( res.data.answers ) {

        question.answers = [];
          let index = 0;

          for ( const q of res.data.answers ) {
            const answer = new Answer();

            answer.index = index;
            answer.id = q.id;
            answer.description = q.description;
            answer.isCorrect = q.isCorrect;
            answer.relationDescription = q.relationDescription;

            question.answers.push(answer);
            index++;
          }
       }
        question.questionType  = questionType;

        this.questionBank.questions.push( question );
        this.resetQuestinTable();
        this.resetQuestionForm();

        swal(Appsettings.APP_NAME, 'Pregunta agregada correctamente', 'success');
      } else {
        swal(Appsettings.APP_NAME, res.message, 'error');
      }
      this.working = false;
    }, err => {
      this.working = false;
    });

  }

  // ACTUALIZAR LOS DATOS DE UNA PREGUNRA
   updateQuestion ( question: Question ): void {
    this.working = true;
    this._srvQuestionBank.updateQuestion( question ).subscribe( res => {
      if ( res.success ) {

        if (this.questionBank.questions === undefined ) {
          this.questionBank.questions = [];
        }

        const questionUpdate = this.questionBank.questions.filter(  x => x.id === question.id )[0];
        if ( questionUpdate ) {

          if ( res.data.answers ) {

            questionUpdate.answers = [];
            let index = 0;

            for ( const q of res.data.answers ) {
              const answer = new Answer();

              answer.index = index;
              answer.id = q.id;
              answer.description = q.description;
              answer.isCorrect = q.isCorrect;
              answer.relationDescription = q.relationDescription;

              questionUpdate.answers.push(answer);
              index++;
            }
         }

          const questionType = new QuestionType();
          questionType.id  = res.data.questionType.id;
          questionType.description = res.data.questionType.description;

          questionUpdate.explanation = question.explanation;
          questionUpdate.content = question.content;
          questionUpdate.questionType = questionType;
        }

        this.resetQuestinTable();
        this.resetQuestionForm();

        swal(Appsettings.APP_NAME, 'Pregunta agregada correctamente', 'success');
      } else {
        swal(Appsettings.APP_NAME, res.message, 'error');
      }
      this.working = false;
    }, err => {
      this.working = false;
    });

   }

   // Función que agrega un nuevo elemento al listado de respuestas
   addAnswers( ): void {

    const answer = new Answer();
    const lastindex = this.answers.length - 1;
    const lastAnswer = this.answers[ ( lastindex < 0 ? 0 : lastindex ) ];

    if ( lastAnswer ) {
      answer.index = lastAnswer.index + 1;
    } else {
      answer.index = 0;
    }
    answer.id = 0;
    this.answers.push(answer);
    setTimeout(function() {
      document.documentElement.scrollTop =  document.documentElement.scrollHeight + 200;
    }, 200);
   }

   // Función para agregar preguntas de verdadero y falso
   addAnswersTrueFalse( ): void {
    this.answers = [];

    for (let index = 0; index <= 1; index++) {
      const answer = new Answer();
      const lastindex = this.answers.length - 1;
      const lastAnswer = this.answers[ ( lastindex < 0 ? 0 : lastindex ) ];

      if ( lastAnswer ) {
        answer.index = lastAnswer.index + 1;
      } else {
        answer.index = 0;
      }
      answer.id = 0;

      this.answers.push(answer);
    }

    setTimeout(function() {
      document.documentElement.scrollTop =  document.documentElement.scrollHeight + 200;
    }, 200);
   }

  // Eliminar una respuesta del listado
  remove( index: number): void {

      swal({
        title: '¿Estás seguro?',
        text: '¡Esta operación no se podrá revertir!',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: '¡Si, eliminar ahora!',
        cancelButtonText: 'Cancelar'
      }).then((result) => {
        if (result.value) {

          if ( this.answers[index].id === 0) {
            this.answers = this.answers.filter( x => x.index !== index);
            swal(Appsettings.APP_NAME, 'Respuesta eliminada correctamente', 'success');
          } else {
            this.working = true;
            this._srvQuestionBank.removeAnswer( this.answers[index].id).subscribe( res => {
                if ( res.success ) {
                  this.answers = this.answers.filter( x => x.index !== index);

                  const question = this.questionBank.questions.filter(  x => x.id === this.selectedQuestion.id )[0];

                  if ( question ) {
                    question.answers = this.answers;
                  }

                  swal(Appsettings.APP_NAME, res.message, 'success');
                } else {
                  swal(Appsettings.APP_NAME, res.message, 'error');
                }
                this.working = false;
            }, err => {
              this.working = false;
              swal(Appsettings.APP_NAME, 'Problemas al intentar eliminar la respuesta' , 'error');
            });
          }
        }
      });
  }

  onRemoved() {
    this.imageProfile = '';
    this.fileContentBase = '';
    this.fileContent = null;
    this.fileName = '';
    this.isUpdateFile    = false;
  }

  onUploadFinished(event: FileHolder) {

    this.imageProfile    = event.src;
    this.fileContentBase = event.src;
    this.fileContent     = event.file;
    this.fileName        = this.fileContent.name;
    this.isUpdateFile    = true;

  }

  onUploadStateChanged(state: boolean) {
  }

  /********* ANSWERS **********/

  onClickAddImage( answer: Answer ) {

    const DIALOG_REF = this.dialog.open( DialogResourceComponent, {
      width: '400px',
      minHeight: '500px',
      autoFocus: false,
      disableClose: true,
      data: {
        id: 0,
        name: '',
        description: '',
        url: ''
      }

    });

    DIALOG_REF.afterClosed().subscribe( res => {
      if ( res ) {
        answer.description = res.url;

      }

    });
  }

  /******** STRUCTURE **********/

  // Evento que se ejecuta cuando se realiza un cambio en el elemento seleccionado
  onValueChange( value ): void {
    this.frmQuestionBank.controls['nodeId'].setValue(value.value);
  }
  // Obtiene los datos del servicio
  getNodes(): void {

    this.structure = [];
    this.nodos = [];
    this._srvStructure.getStructureByParent(145).subscribe( result => {
      // const fullNodes = JSON.parse(this._srvStorage.nodes);
      const fullNodes = result.data;
      let nodes = [];

      if ( this.questionBank.nodeId > 0 ) {
        const selectedNode = fullNodes.filter( x => x.id ===  this.questionBank.nodeId )[0];
        if ( selectedNode ) {
          if ( selectedNode.id === Appsettings.PLAN_LECTOR_NODE_ID_STRUCUTURE ||
               selectedNode.parentId === Appsettings.PLAN_LECTOR_NODE_ID_STRUCUTURE  ) {
            this.nodeType = 2;
          }
        }
      }
      if ( this.nodeType === 2) {
        nodes = fullNodes.filter( n => n.id === Appsettings.PLAN_LECTOR_NODE_ID_STRUCUTURE
                                 || n.parentId === Appsettings.PLAN_LECTOR_NODE_ID_STRUCUTURE);
       // nodes[0].parentId = 0;
       this.nodos.push({
        text: 'Plan Lector',
        value: 975,
        collapsed: false,
        checked: false,
        parentId: 0
      });
      } else {
        nodes = fullNodes;
      }

      if ( this.nodeType !== 2) {
        this.nodos.push({
          text: 'Recursos Acedemicos',
          value: 124,
          collapsed: false,
          checked: false,
          parentId: 0
        });
      }

      for (const node of nodes) {
        const existNode = this.nodos.filter( x => x.value === node.id )[0];
        if ( existNode === undefined ) {
          const checkedNode = false;
          this.structure.push(node);
          this.nodos.push({
            text: node.description,
            value: node.id,
            collapsed: false,
            checked: checkedNode,
            parentId: node.parentId
          });
        }
      }

      this.items.push(TreeUtilities.buildHierarchy(this.nodos));
    });


  }

  /************************** */

  onLoadImageAnswer(img, answer) {
    if ( img ) {
     answer.imageWidth = img.clientWidth;
     answer.imageHeight = img.clientHeight;
    }
  }

}
