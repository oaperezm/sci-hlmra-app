import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { AdminNavService } from '../../components/layouts/administrator/admin-nav/admin-nav.service';
import { MatSidenav } from '@angular/material';

@Component({
  selector: 'app-admin-layout',
  templateUrl: './admin-layout.component.html',
  styleUrls: ['./admin-layout.component.scss']
})
export class AdminLayoutComponent implements OnInit {

  opened: boolean = true;
  @ViewChild('sidenav') public sidenav: MatSidenav;

  constructor( public _srvAdminNav: AdminNavService ) {
  }

  ngOnInit() {
    this._srvAdminNav.setSidenav(this.sidenav);
  }

}
