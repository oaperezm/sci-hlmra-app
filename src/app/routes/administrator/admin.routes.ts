import { Routes, RouterModule } from '@angular/router';
import { ListQuestionBankComponent } from './list-question-bank/list-question-bank.component';
import { AdminLayoutComponent } from './admin-layout.component';
import { QuestionBankComponent } from './question-bank/question-bank.component';
import { ListExamComponent } from './list-exam/list-exam.component';
import { GenerateExamComponent } from './generate-exam/generate-exam.component';

import { AuthGuardService } from '../../service/service.index';
import { NoaccessComponent } from './secutiry/noaccess/noaccess.component';
import { UploadQuestionBankComponent } from './upload-question-bank/upload-question-bank.component';
import { CustomReportsComponent } from './custom-reports/custom-reports.component';

const AppPageAdmin: Routes = [
    {
      path: 'admin',
      component: AdminLayoutComponent,
      children: [
          { path: 'list-question-bank', component: ListQuestionBankComponent, canActivate: [ AuthGuardService ] },
          { path: 'question-bank/:id/:type', component: QuestionBankComponent,  canActivate: [ AuthGuardService ] },
          { path: 'question-bank/:id', component: QuestionBankComponent,  canActivate: [ AuthGuardService ] },
          { path: 'list-exam', component: ListExamComponent,  canActivate: [ AuthGuardService ] },
          { path: 'generate-exam', component: GenerateExamComponent,  canActivate: [ AuthGuardService ] },
          { path: 'generate-exam/:id', component: GenerateExamComponent,  canActivate: [ AuthGuardService ] },
          { path: 'upload-question-bank/:id/:type', component: UploadQuestionBankComponent },
          { path: 'custom-reports', component: CustomReportsComponent },
          { path: 'no-access', component: NoaccessComponent},
          { path: '', redirectTo: '/home/login', pathMatch: 'full'},
      ]
    }
  ];
 export const ADMIN_ROUTES = RouterModule.forChild( AppPageAdmin );
