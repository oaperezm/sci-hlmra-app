import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule} from '@angular/forms';
import { CommonModule } from '@angular/common';
import { TreeviewModule } from 'ngx-treeview';

import { ImageUploadModule } from 'angular2-image-upload';

// MODULES
import { ADMIN_ROUTES } from './admin.routes';
import { CoreModule } from '../../core/core.module';
import { AdministratorModule } from '../../components/layouts/administrator/administrator.module';
import { AdmonComponentModule } from '../../components/administrator/admon-component.module';


// COMPONENTS
import { ListQuestionBankComponent } from './list-question-bank/list-question-bank.component';
import { ListExamComponent } from './list-exam/list-exam.component';
import { GenerateExamComponent } from './generate-exam/generate-exam.component';


// LAYOUT
import { AdminLayoutComponent } from './admin-layout.component';

// DIALOGS
import { DialogExamSelectQuestionsComponent } from '../../dialogs/dialog-exam-select-questions/dialog-exam-select-questions.component';
import { DialogResourceComponent } from '../../dialogs/dialog-resource/dialog-resource.component';
import { DialogPreviewExamComponent } from '../../dialogs/dialog-preview-exam/dialog-preview-exam.component';
import { DialogExamWeightingComponent} from '../../dialogs/dialog-exam-weighting/dialog-exam-weighting.component';

import {
  MatInputModule,
  MatDatepickerModule,
  MatRadioModule,
  MatCardModule,
  MatSelectModule,
  MatDialogModule,
  MatIconModule,
  MatButtonModule,
  MatCheckboxModule,
  MatTableModule,
  MatMenuModule,
  MatPaginatorModule,
  MatNativeDateModule,
  MatSlideToggleModule,
  MatToolbarModule,
  MatDividerModule,
  MatListModule,
  MatProgressBarModule,
  MatTooltipModule,
  MatBadgeModule,
  MatAutocompleteModule,
  MatTabsModule,
  MatProgressSpinnerModule,
  MatSidenavModule,
  MatSliderModule,
} from '@angular/material';
import { QuestionBankComponent } from './question-bank/question-bank.component';
import { NoaccessComponent } from './secutiry/noaccess/noaccess.component';
import { UploadQuestionBankComponent } from './upload-question-bank/upload-question-bank.component';
import { FileUploadModule } from 'ng2-file-upload';
import { CustomReportsComponent } from './custom-reports/custom-reports.component';
import { FileSaverModule } from 'ngx-filesaver';
import { JoyrideModule } from 'ngx-joyride';

@NgModule({
  imports: [
    CommonModule,
    ADMIN_ROUTES,
    FormsModule,
    ReactiveFormsModule,
    MatInputModule,
    MatDatepickerModule,
    MatRadioModule,
    MatCardModule,
    MatSelectModule,
    MatDialogModule,
    MatIconModule,
    MatButtonModule,
    MatCheckboxModule,
    MatTableModule,
    MatMenuModule,
    MatPaginatorModule,
    MatNativeDateModule,
    MatSlideToggleModule,
    MatToolbarModule,
    MatDividerModule,
    MatListModule,
    MatProgressBarModule,
    MatTooltipModule,
    MatBadgeModule,
    MatAutocompleteModule,
    MatTabsModule,
    MatProgressSpinnerModule,
    MatSidenavModule,
    MatSliderModule,
    ImageUploadModule,
    CoreModule,
    AdministratorModule,
    AdmonComponentModule,
    FileUploadModule,
    FileSaverModule,
    JoyrideModule.forChild(),
  ],
  declarations: [
    ListQuestionBankComponent,
    AdminLayoutComponent,
    QuestionBankComponent,
    ListExamComponent,
    GenerateExamComponent,
    DialogExamSelectQuestionsComponent,
    DialogResourceComponent,
    NoaccessComponent,
    DialogPreviewExamComponent,
    DialogExamWeightingComponent,
    UploadQuestionBankComponent,
    CustomReportsComponent
  ],
  entryComponents: [
    DialogExamSelectQuestionsComponent,
    DialogResourceComponent,
    DialogPreviewExamComponent,
    DialogExamWeightingComponent
  ]
})
export class AdminModule { }
