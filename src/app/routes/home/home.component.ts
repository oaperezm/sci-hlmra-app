import { Component, OnInit, ViewEncapsulation, AfterContentInit, AfterViewChecked, ElementRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

// Servicio
import { HeaderService } from '../../components/layouts/anonymous/header/header.service';

import { StudenService, TipsHomeService, NewsHomeService, GeneralEventService, UserService } from '../../service/service.index';
import { Appsettings } from '../../configuration/appsettings';

// MODELS
import { NewHome } from '../../models/home/new.model';
import { TipHome } from '../../models/home/tip.model';
import { GeneralEvent } from '../../models/home/general-event.model';

import { FormControl, FormGroup, Validators } from '@angular/forms';
import swal from 'sweetalert2';

declare function scrollingToElement(element): any;

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class HomeComponent implements OnInit, AfterContentInit {

  news: NewHome[] = [];
  tips: TipHome[] = [];
  generalEvents: GeneralEvent[] = [];

  frmContact: FormGroup;
  sendingMail: boolean;
  constructor(  public _srvHeader: HeaderService,
                private route: ActivatedRoute,
                private _router: Router,
                private _srvStudent: StudenService,
                private elementRef: ElementRef,
                private _srvUser: UserService,
                ) {
    this.frmContact = new FormGroup( {
      'fullName': new FormControl('', [ Validators.required ]),
      'email': new FormControl('', [ Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+.[a-z]{2,3}$'), Validators.required ]),
      'subject': new FormControl(''),
      'message': new FormControl('', [ Validators.required ]),
      'school': new FormControl(''),
      'workplace': new FormControl('')
    });



  }

  ngOnInit() {

    this._srvHeader.showReturnButton( true );

    this.route.queryParams.subscribe(params => {
      if (typeof(params.codigo) !== 'undefined' && params.codigo !== '') {
        this._srvStudent.confirmInvitation(params.codigo).subscribe( res => {
          if ( res.success ) {
            this._router.navigateByUrl('/home/login');
          }
        });
      }
    });

    const s = document.createElement('script');
    s.type = 'text/javascript';
    s.src = './assets/js/home_functions.js';
    this.elementRef.nativeElement.appendChild(s);

    this.tips = this._srvHeader.tips;
    this.news = this._srvHeader.news;
    this.generalEvents = this._srvHeader.generalEvents;
  }

  ngAfterContentInit(): void {
    this.route.params.subscribe(params => {
      if (params['section']) {
        setTimeout(() => {
          scrollingToElement(`#${params['section']}`);
        }, 1000);
      }
    });
  }

  // Enviar correo
  onSubmit() {

    const mail = {
      'fullName':  this.frmContact.controls['fullName'].value,
      'email':  this.frmContact.controls['email'].value,
      'subject':  this.frmContact.controls['subject'].value,
      'message':  this.frmContact.controls['message'].value,
      'school':  this.frmContact.controls['school'].value,
      'workplace':  this.frmContact.controls['workplace'].value,
      'systemId': Appsettings.SYSTEM_ID
    };

    this.sendingMail = true;

    this._srvUser.sendContactMail( mail ).subscribe( res => {
      if ( res.success ) {
        swal(Appsettings.APP_NAME, 'En breve un asesor se pondrá en contacto contigo.' , 'success');
      } else {
        swal(Appsettings.APP_NAME, res.message, 'error');
      }
      this.sendingMail = false;
      this.frmContact.reset();
    }, err => {
      this.sendingMail = false;
      this.frmContact.reset();
    });

  }


}
