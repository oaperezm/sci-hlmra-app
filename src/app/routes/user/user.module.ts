import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule} from '@angular/forms';
import { CommonModule } from '@angular/common';

import { ImageUploadModule } from 'angular2-image-upload';

import { USER_ROUTES } from './user.routes';

import { ProfileComponent } from './profile/profile.component';

// Modales
import { DialogUserImgProfileComponent } from '../../dialogs/dialog-user-img-profile/dialog-user-img-profile.component';
import { DialogUserChangePasswordComponent } from '../../dialogs/dialog-user-change-password/dialog-user-change-password.component';
import { DialogDetailsNotification } from '../../dialogs/dialog-details-notification/dialog-details-notification.component';
// Modulos
import { CoreModule } from '../../core/core.module';
import {
  MatTabsModule,
  MatInputModule,
  MatDatepickerModule,
  MatRadioModule,
  MatCardModule,
  MatSelectModule,
  MatDialogModule,
  MatIconModule,
  MatButtonModule,
  MatCheckboxModule,
  MatTableModule,
  MatProgressBarModule,
  MatProgressSpinnerModule,
  MatMenuModule,
  MatPaginatorModule,
  MatNativeDateModule,
  MatSlideToggleModule,
  MatToolbarModule,
  MatBadgeModule,
  MatListModule,
  MatAutocompleteModule,
  MatChipsModule,
  MatDividerModule,
  MatExpansionModule,
  MatButtonToggleModule,
  MatTooltipModule,
  MatSidenavModule,
  MatStepperModule
} from '@angular/material';


import { CouponComponent } from './coupon/coupon.component';
import { PointComponent } from './point/point.component';
import { DialogCreateExchangeCodeComponent } from './dialog-create-exchange-code/dialog-create-exchange-code.component';




@NgModule({
  declarations: [
    ProfileComponent,
    DialogUserImgProfileComponent,
    DialogUserChangePasswordComponent,
    DialogDetailsNotification,
    DialogCreateExchangeCodeComponent,
    CouponComponent,
    PointComponent
  ], 
  exports: [],
  imports: [
    USER_ROUTES,
    MatTabsModule,
    MatInputModule,
    MatDatepickerModule,
    MatRadioModule,
    MatCardModule,
    MatSelectModule,
    MatDialogModule,
    MatIconModule,
    MatButtonModule,
    MatCheckboxModule,
    MatTableModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatMenuModule,
    MatPaginatorModule,
    MatNativeDateModule,
    MatSlideToggleModule,
    MatToolbarModule,
    MatBadgeModule,
    MatListModule,
    MatAutocompleteModule,
    MatChipsModule,
    MatDividerModule,
    MatExpansionModule,
    MatButtonToggleModule,
    MatTooltipModule,
    ImageUploadModule.forRoot(),
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    MatSidenavModule,
    MatStepperModule,
    CoreModule
  ],
  entryComponents: [
    DialogUserImgProfileComponent,
    DialogUserChangePasswordComponent,
    DialogDetailsNotification,
    DialogCreateExchangeCodeComponent
  ]
})
export class UserModule { }
