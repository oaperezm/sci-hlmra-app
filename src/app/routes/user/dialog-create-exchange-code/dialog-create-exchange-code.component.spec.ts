import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogCreateExchangeCodeComponent } from './dialog-create-exchange-code.component';

describe('DialogCreateExchangeCodeComponent', () => {
  let component: DialogCreateExchangeCodeComponent;
  let fixture: ComponentFixture<DialogCreateExchangeCodeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogCreateExchangeCodeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogCreateExchangeCodeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
