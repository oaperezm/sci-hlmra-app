import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar, MatStepper, MAT_DIALOG_DATA } from '@angular/material';
import { Subscription } from 'rxjs';
import { Coupon } from 'src/app/models/coupon/coupon';
import { CouponResponse } from 'src/app/models/coupon/coupon-Response';
import { CouponService } from 'src/app/service/coupon/coupon.service';
import swal  from  'sweetalert2';
import { Appsettings } from '../../../configuration/appsettings';


@Component({
  selector: 'app-dialog-create-exchange-code',
  templateUrl: './dialog-create-exchange-code.component.html',
  styleUrls: ['./dialog-create-exchange-code.component.scss']
})
export class DialogCreateExchangeCodeComponent implements OnInit {

  subscription: Subscription = new Subscription();
  formCode: FormGroup;
  formCodeGenerated: FormGroup;
  dateInital = new Date();
  cuopon: string = 'askdhahdkjA';
  loadingCopy = false;
  loading = false;
  completedStep = false;
  showErrorAmount: boolean = false;

  constructor(
    private fb: FormBuilder, 
    private snackbar: MatSnackBar, 
    private couponService: CouponService,
    @Inject(MAT_DIALOG_DATA) public amountAvailable: number) { 
    this.initForm();
    this.getCodeCoupon();
  }

  
  ngOnInit() {
  }


  initForm() {


    this.formCode = this.fb.group({
      code: ["Prueba-X", Validators.required],
      amount:[200, [Validators.required, Validators.min(50)]]
    });


  }


  get amount() { return this.formCode.controls.amount; }
  get code() { return this.formCode.controls.code; }


  getCodeCoupon() {
    this.code.setValue(this.dateInital.getTime().toString());

  }


  getCuopon(stepper: MatStepper) {

    const isValid = this.validAmount(this.amount.value);
    this.showErrorAmount = !isValid;

    if (isValid) {

      this.loading = true;
      this.completedStep = true;
      this.exchangeCoupon();
      // setTimeout(() => {
        this.loading = false;5
      //   stepper.next();
      // }, 5000);
    }

  }

  
  validAmount(value: number) {

    let isValid = false;

    if (value >= 50) {

      if ((this.amountAvailable - value) >= 0) {

        const result = value % 50;

        if ( result === 0 ) {
          isValid = true;
        } else {
          isValid = false;        
          swal( Appsettings.APP_NAME, 'El monto debe ser solo en múltiplos de $50', 'error');
        }
        
      } else {
        swal( Appsettings.APP_NAME, 'No tienes suficientes puntos', 'error');
        isValid = false;
      }
      
      
      
    } else {
      isValid = false;      
      swal( Appsettings.APP_NAME, 'El monto no puede ser menor que $50', 'error');
    }

    return isValid;


  }


  createModelCuopon() {

    const dateExpire = new Date(
      this.dateInital.getFullYear(), 
      this.dateInital.getMonth(), 
      this.dateInital.getDate()
    );

    const coupon: Coupon = {
      discount_type: 'fixed_cart',
      individual_use: true,
      date_expire_gmt: dateExpire,
      code: this.formCode.controls.code.value,
      amount: this.formCode.controls.amount.value
    };

    return coupon;
    
  }


  exchangeCoupon() {

    const coupon = this.createModelCuopon();
    console.log(coupon);
    this.subscription.add(this.couponService.exchange(coupon)
      .subscribe(response => {
        const result: CouponResponse = null;
        console.log(response);
      }, error => {
        console.log(error);
      }));
  }


  copyCoupon() {
    this.loadingCopy = true;
    const inputElement = document.createElement('input');
    inputElement.value = this.cuopon;
    inputElement.style.position = 'absolute';
    document.body.appendChild(inputElement);
    inputElement.focus();
    inputElement.select();
    document.execCommand('copy');
    debugger
    document.body.removeChild(inputElement);
    this.snackbar.open('Copiado en el portapapeles', 'Cerrar', {
      duration: 1500
    });
    
    this.loadingCopy = false;
  }


}
