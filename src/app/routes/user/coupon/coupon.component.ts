import { Component, Input, OnInit } from '@angular/core';
import { MatTableDataSource} from '@angular/material/table';


@Component({
  selector: 'app-coupon',
  templateUrl: './coupon.component.html',
  styleUrls: ['./coupon.component.scss']
})
export class CouponComponent implements OnInit {

  dateInitial = new Date();
  columnsCoupon = [ 'dateCreated', 'mount', 'validity', 'code', 'status' ];
  dataSourceCoupon = new MatTableDataSource<any>();

  
  @Input() set changeCoupons(coupons: any[]) {
    console.log(coupons);
    this.dataSourceCoupon.data = coupons;
  }
  
  constructor() { }

  ngOnInit() {
  }

}
