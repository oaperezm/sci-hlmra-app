import { Component, OnInit, ViewEncapsulation, ViewChild, OnDestroy } from '@angular/core';
import { MatDialog } from '@angular/material';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';

import { Appsettings } from '../../../configuration/appsettings';
import { ActivatedRoute, Router } from '@angular/router';

// Servicios
import { StorageService, CatalogService, UserService, CityService } from '../../../service/service.index';
import { AdminHeaderService } from '../../../components/layouts/administrator/admin-header/admin-header.service';
// Modales
import { DialogUserImgProfileComponent } from '../../../dialogs/dialog-user-img-profile/dialog-user-img-profile.component';
import { DialogUserChangePasswordComponent} from '../../../dialogs/dialog-user-change-password/dialog-user-change-password.component';
import { DialogDetailsNotification } from '../../../dialogs/dialog-details-notification/dialog-details-notification.component';
// Modelos
import { User } from '../../../models/security/user.model';
import { Role } from '../../../models/security/role.model';

import { Contry } from '../../../models/catalogs/contry.model';
import { State } from '../../../models/catalogs/state.model';
import { System } from '../../../models/catalogs/system.model';
import { Level } from '../../../models/catalogs/level.model';

import { Pagination } from '../../../models/general/pagination.model';

import swal from 'sweetalert2';
import { AdminNavService } from '../../../components/layouts/administrator/admin-nav/admin-nav.service';
import { CouponService } from 'src/app/service/coupon/coupon.service';
import { PointService } from 'src/app/service/point/point.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit, OnDestroy {

  coupons: any[] = [];
  points: any[] = [];

  working: boolean;
  userName: string;
  userRole: Role;

  roles: Role[] = [];
  contries: Contry[] = [];
  states: State[] = [];
  levels: Level[] = [];
  subsystems: System[] = [];
  institutions: string[] = [];
  semesters: number[] = [];
  profileImage: string;
  defaultImg: string;

  frmUserProfileData: FormGroup;
  public user: User;

  showKey: boolean;

  fileContent: File;
  fileContentBase: string;
  fileName: string;
  isUpdateFile: boolean;

  notifications: any[];
  selectedTabIndex: number;

  pagination: Pagination;
  pages: number[] = [];
  sizePage = 10;
  ccts: string;

  constructor(  public _srvStorage: StorageService,
    public dialog: MatDialog,
    private _srvUser: UserService,
    private _router: Router,
    private _route: ActivatedRoute,
    public _srvCatalog: CatalogService,
    private _srvHeaderAdmin: AdminHeaderService,
    public _srvAdminNav: AdminNavService,
    private couponService: CouponService,
    private pointService: PointService,
    private _srvCity: CityService ) {

    this.userName = '';
    this.userRole = new Role(1, '', '');
    this.showKey = false;
    this.working = false;
    this.initCatalogs();

    // Inicializar controles del formulario
    this.frmUserProfileData = new FormGroup({
      fullName: new FormControl( '', [Validators.required]),
      birthDay: new FormControl( '', [Validators.required]),
      gender: new FormControl( '', [Validators.required]),
      contry: new FormControl( 1, [Validators.required]),
      state: new FormControl( '', [Validators.required]),
      institution: new FormControl( ''),
      email: new FormControl('', [
        Validators.required,
        Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+.[a-z]{2,3}$')
      ]),
      keycct: new FormControl('', [Validators.required])
    });
  }

  ngOnInit() {

    this._srvHeaderAdmin.setUrlBackButton(['/resource/home', 'Inicio']);

    this.getNotifications(1, this.sizePage);

    this.getUserProfile();

    this._srvAdminNav.setTitle('Perfil de usuario');

    this.defaultImg = './../../../../../assets/img/profileimg/profile-placeholder.png';
    this._route.queryParams.subscribe(params => {
      if (params['tab']) {
        this.selectedTabIndex = Number(params['tab']);
      } else {
        this.selectedTabIndex = 0;
      }
    });
  }

  ngOnDestroy(): void {
    this._srvHeaderAdmin.setUrlBackButton([]);
  }
  // Obtener el cátalogo de ciudades
  getCities() {
    this.working = true;
    this._srvCity.getAll().subscribe(res => {
      if (res.success) {
        for (const c of res.data) {
          const city = new Contry(c.id, c.description);
          this.contries.push(city);
        }
      }
      this.working = false;
    }, err => {
      this.working = false;
    }, () => {
      if (this.contries.length > 0) {
        this.frmUserProfileData.controls['contry'].setValue(this.contries[0].id);
        this.getStates(this.contries[0].id);
      }
    });
  }

  // Obtener los estados de una ciudad
  getStates(cityId: number) {

    this.working = true;
    this._srvCity.getStates(cityId).subscribe(res => {

      if (res.success) {
        for (const s of res.data) {
          this.states.push(new State(s.id, s.name));
        }
      }

      this.working = false;
    }, err => {
      this.working = false;
    });
  }

  // Evento de cambiar país
  onChangeContry($event) {

    if ($event.value > 0) {
      this.getStates($event.value);
    } else {
      this.states = [];
    }

  }

  // Obtener datos del perfil
  getUserProfile() {
    this.working = true;

    this._srvUser.getProfileData().subscribe(
      res => {
        if (res.success) {
          const user = res.data;

          // tslint:disable-next-line:max-line-length
          this.user = new User(
            user.id,
            user.fullName,
            user.email,
            user.birthday,
            user.gender,
            user.role,
            user.key,
            user.state.id,
            user.institution,
            user.semester,
            user.levelId,
            user.subsystemId,
            user.urlImage
          );

          this.frmUserProfileData.controls['contry'].setValue(user.state.city.id);
          this.user.institution = user.institution;
          this.userName = this.user.fullName;
          this.userRole = this.user.role;

          if (this.user.urlImg == null) {
            this.profileImage = this.defaultImg;
          } else {
            this.profileImage = this.user.urlImg;
          }

          if (this.user.role.id === 1) {
            this.roles.push(new Role(1, 'Administrador', 'Administrador'));
          }

          // if (this.user.role.id === 2) {
          //   this.showKey = true;
          //   this.frmUserProfileData.controls['keycct'].setValidators([
          //     Validators.required
          //   ]);
          //   this.frmUserProfileData.controls['keycct'].updateValueAndValidity();
          // }

          const seasson = user.userCCTs;
          const cct = seasson.map(x => x.cct);

          if (cct.length > 0) {
            this.showKey = true;
            this.ccts = cct.join(', ');
          }
        }

        this.ccts = this.user.keycct;
        this.setFormData();
        this.working = false;
      },
      err => {
        this.working = false;
      }
    );
  }

  // Asignar datos del usuario al formularia
  setFormData() {
    this.frmUserProfileData.controls['fullName'].setValue(this.user.fullName);
    this.frmUserProfileData.controls['email'].setValue(this.user.email);
    this.frmUserProfileData.controls['birthDay'].setValue(this.user.birthday);
    this.frmUserProfileData.controls['gender'].setValue(this.user.gender);
    this.frmUserProfileData.controls['state'].setValue(this.user.state.id);
    this.frmUserProfileData.controls['keycct'].setValue(this.ccts);
    this.frmUserProfileData.controls['institution'].setValue(this.user.institution);

  }

  onChangeRole(event) {
    // tslint:disable-next-line:radix
    if (parseInt(event.value) === 2) {
      this.showKey = true;
      this.frmUserProfileData.controls['keycct'].setValidators([
        Validators.required
      ]);
      this.frmUserProfileData.controls['keycct'].updateValueAndValidity();
    } else {
      this.showKey = false;
      this.frmUserProfileData.controls['keycct'].setValue('');
      this.frmUserProfileData.controls['keycct'].setValidators([]);
      this.frmUserProfileData.controls['keycct'].clearValidators();
      this.frmUserProfileData.controls['keycct'].updateValueAndValidity();
    }
    this.frmUserProfileData.controls['keycct'].markAsUntouched();
  }

  // Guardar los datos del usuario
  onSubmit() {
    this.working = true;
    const USER = {
      id: this.user.id,
      fullName: this.frmUserProfileData.controls['fullName'].value,
      birthDay: this.frmUserProfileData.controls['birthDay'].value,
      gender: this.frmUserProfileData.controls['gender'].value,
      contry: this.frmUserProfileData.controls['contry'].value,
      stateId: this.frmUserProfileData.controls['state'].value,
      email: this.frmUserProfileData.controls['email'].value,
      key: this.frmUserProfileData.controls['keycct'].value,
      institution: this.frmUserProfileData.controls['institution'].value
    };

    this.working = true;
    // this._srvUser.update(USER).subscribe(
    //   res => {
    //     if (res.success) {
    //       swal(Appsettings.APP_NAME, res.message, 'success');
    //     } else {
    //       swal(Appsettings.APP_NAME, res.message, 'error');
    //     }
    //     this.working = false;
    //   },
    //   err => {
    //     swal('Problemas al realizar el registro', 'SAIL', 'error');
    //     this.working = false;
    //   }
    // );
  }

  // Mostrar dialogo para actualizar imágen de perfil
  onChangeImg() {
    const DIALOG_REF = this.dialog.open(DialogUserImgProfileComponent, {
      width: '750px',
      height: '600px',
      autoFocus: false,
      disableClose: true,
      data: this.user
    });

    DIALOG_REF.afterClosed().subscribe(response => {
      this.working = true;
      if (response.status) {
        if (response.image !== null) {
          this._srvStorage.profileImage = response.image;
          this.profileImage = response.image;
        } else {
          this._srvStorage.profileImage = 'assets/img/profileimg/profile-placeholder.png';
        }

        this.working = false;
      }
      this.working = false;
    });
  }

  // Mostrar dialogo para cambiar de contraseña
  onChangePasswrd() {
    const DIALOG_REF = this.dialog.open(DialogUserChangePasswordComponent, {
      width: '350px',
      height: '420px',
      autoFocus: false,
      disableClose: true
    });
  }

  // Inicializar los datos del catálogos
  initCatalogs() {

    // INIT DATA SELECT

    for (const role of this.roles) {
      this.roles.push(new Role(role.id, role.name, role.name));
    }

    this.getCities();

    // Obtener roles
    this._srvCatalog.getRoleAll().subscribe(res => {
      if (res.success) {
        const activeRoles = res.data.filter(x => x.active === true);
        this.roles = activeRoles;
      }
    });

  }

  // Obtener las notificaciones activas
  getNotifications(page: number, size: number) {

    this._srvUser.getNotifications(page, size).subscribe(res => {
      if (res.success) {

        this.pages = [];

        this.notifications = res.data;
        this._srvStorage.notifications = JSON.stringify(res.data);

        this.pagination = new Pagination();
        this.pagination.pageNumber = page;
        this.pagination.pageSize = res.pagination.pageSize;
        this.pagination.showNextPage = res.pagination.showNextPage;
        this.pagination.showPreviousPage = res.pagination.showPreviousPage;
        this.pagination.total = res.pagination.total;
        this.pagination.totalPage = res.pagination.totalPage;

        for (let i = 1; i <= this.pagination.totalPage; i++) {
          this.pages.push(i);
        }

        this._srvAdminNav.setNotifications();
      }
    });
  }

  // Asignar el index del tab seleccionado
  onChangeNotificationTab() {
    this.selectedTabIndex = 1;
  }

  // Cambiar de página de datos
  onClickChangePage(page: number) {
    this.getNotifications(page, this.sizePage);
  }
  onClickShow(tittle: string, body:string)
  {
    const DIALOG_REF = this.dialog.open( DialogDetailsNotification, {
      width: '500px',
      height: '600px',
      autoFocus: false,
      disableClose: true,
      data: { tittle: tittle, body:body}
      });

      DIALOG_REF.afterClosed().subscribe( response => {
      });
  }
  // Marcar una notificación como leida
  onReadMessages(id: number) {
    this._srvUser.readNotification(id).subscribe(res => {
      if (res.success) {
        this._srvStorage.notifications = JSON.stringify(this._srvAdminNav.notifications.filter(x => x.id !== id));
        this.notifications = this.notifications.filter(x => x.id !== id);
        this._srvAdminNav.setNotifications();
      }
    });
  }


  tabChange($event: any) {
    
    this.selectedTabIndex = $event;

    if ($event == 2) {
      this.loadPoints();
    } else if ($event == 3) {
      this.loadCoupons();
    }

  }



  loadPoints() {

    this.working = true;

    this.points = pointsData;
    // this.pointService.getAll()
    //   .subscribe(response => {
    //     this.points = response as any[];
    //     this.working = false;
    //   }, error => {
    //     this.points = [];
    //     this.working = false;
    // });
    
    this.working = false;
  }


  loadCoupons() {

    this.working = true;
    this.coupons = couponsData;
    // this.couponService.getAllCoupon()
    //   .subscribe(response => {
    //     this.coupons = response as any[];
    //     this.working = false;
    //   }, error => {
    //     this.coupons = [];
    //     this.working = false;
    // });
    this.working = false;


  }

}


const pointsData =  [
  {
    id: 1,
    dateCreated: new Date(),
    point: 100,
    status: 'Aplicado'
  },
  {
    id: 1,
    dateCreated: new Date(),
    point: 100,
    status: 'Aplicado'
  },
  {
    id: 1,
    dateCreated: new Date(),
    point: 100,
    status: 'Aplicado'
  },
  {
    id: 1,
    dateCreated: new Date(),
    point: 100,
    status: 'Aplicado'
  },
  {
    id: 1,
    dateCreated: new Date(),
    point: 100,
    status: 'Aplicado'
  }
];


const couponsData: any[] = [
  {
    id: 1,
    dateCreated: new Date(),
    mount: 200,
    validity: new Date(2021,3, 12),
    code: 'dada4das4d4746',
    status: 'Aplicado'
  },
  {
    id: 2,
    dateCreated: new Date(),
    mount: 200,
    validity: new Date(2021,3, 12),
    code: 'dada4das4d4746',
    status: 'Aplicado'
  },
  {
    id: 3,
    dateCreated: new Date(),
    mount: 200,
    validity: new Date(2021,3, 15),
    code: 'dada4das4d4746',
    status: 'Disponible'
  }
];
