import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatDialog } from '@angular/material';
import { DialogCreateExchangeCodeComponent } from '../dialog-create-exchange-code/dialog-create-exchange-code.component';
import { Subscription } from 'rxjs';


@Component({
  selector: 'app-point',
  templateUrl: './point.component.html',
  styleUrls: ['./point.component.scss']
})
export class PointComponent implements OnInit, OnDestroy {

  suscription: Subscription = new Subscription();
  columnsPoint = ['dateCreated', 'point'];
  dataSourcePoint = new MatTableDataSource<any>();
  amountAvailible: number = 500;

  @Input() set changePoints(points: any[]) {
    console.log(points);
    this.dataSourcePoint.data = points;
  }


  constructor(public dialog: MatDialog) { }


  ngOnInit() {
  }


  ngOnDestroy() {
    this.suscription.unsubscribe();
  }


  createExchangeCode() {

    const DIALOG_REF = this.dialog.open(DialogCreateExchangeCodeComponent, {
      // width: '750px',
      height: '500px',
      autoFocus: false,
      disableClose: true,
      data: this.amountAvailible
    });


    this.suscription.add(
      DIALOG_REF.afterClosed().subscribe(response => {

        console.log(response);
      // if (response.status) {
        
      // }back

    }));
  }


}


