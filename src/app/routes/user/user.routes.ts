import { Routes, RouterModule } from '@angular/router';



// Servicios
import { AuthGuardService } from '../../service/service.index';

import { ResourceLayoutComponent } from '../resource/resource-layout.component';
import { ProfileComponent } from './profile/profile.component';


const AppPageRUser: Routes = [
  {
    path: 'user',
    component: ResourceLayoutComponent,
    children: [
        { path: 'profile', component: ProfileComponent, canActivate: [ AuthGuardService] },
        { path: '', redirectTo: '/', pathMatch: 'full'},
    ]
  }
];
export const USER_ROUTES = RouterModule.forChild( AppPageRUser );
