import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

// Modelos
import { Question } from '../../../models/courses/question.model';

// Service
import { TeacherExamService } from '../../../service/service.index';

import swal from 'sweetalert2';
import { Appsettings } from '../../../configuration/appsettings';
import { interval } from 'rxjs';
import { map } from 'rxjs-compat/operator/map';
import { ExamSchedule } from '../../../models/courses/exam-schedule.model';
import { isNumeric } from 'rxjs/internal-compatibility';

@Component({
  selector: 'app-apply-exam',
  templateUrl: './apply-exam.component.html',
  styleUrls: ['./apply-exam.component.scss']
})
export class ApplyExamComponent implements OnInit {

  examId: number;
  questions: Question[] = [];
  working: boolean;
  question: Question;
  courseId: number;

  questionsTotal: number;

  orderResponse: any[] = [];
  answersOrder: any[] = [];
  answers: any[] = [];
  finalAnswers: any[] = [];
  relationsQuestions: any[] = [];
  relations: any[] = [];
  idd: number;
  idds: number[] = [];

  elementQuestions: any[] = [];
  questionsResonse: any[] = [];
  horasRestantes: number;
  minutosRestantes: number;
  segundosRestantes: number;
  beginDateTime: Date;
  examSchedule: ExamSchedule;

  examScheduleId: number;
  nodeId: number;
  typeQuestion: number;
  countTotalQuestiosn: number;
  isExamAvailable: boolean;
  isExamCompleted: boolean;
  countDown: any;

  examName: string;
  constructor(private _srvTeacherExam: TeacherExamService,
    private _routeActive: ActivatedRoute,
    private _router: Router) {
    this.countTotalQuestiosn = 0;
    this.typeQuestion = 0;
    this._routeActive.params.subscribe(params => {
      this.working = false;
      this.examId = params['examId'];
      this.nodeId = params['nodeId'];
      this.examScheduleId = params['examScheduleId'];
      this.courseId = params['courseId'];
      this.getExam(params['examId'], params['examScheduleId']);
    });
  }

  ngOnInit() {
  }


  // Obtiene las preguntas del examen
  getExam(examId: number, examScheduleId: number) {
    this.working = true;
    this._srvTeacherExam.getByScheduleId(examId, examScheduleId).subscribe(result => {
      this.working = false;
      this.readExamSchedule(result);
    });
  }

  startExam() {
    this.working = true;
    this._srvTeacherExam.startExamSchedule(this.examId, this.examScheduleId).subscribe(result => {
      this.working = false;
      this.readExamSchedule(result);
    });
  }

  saveAnswer(answer) {
    this._srvTeacherExam.saveStudentAnswer(this.examScheduleId, answer).subscribe(result => {
    });
  }
  readExamSchedule(result) {
    if (!result.success) {
      swal(Appsettings.APP_NAME, result.message, 'warning');
      this.working = false;
    }
    let data = result.data;
    let es = data.examSchedules[0];
    this.examSchedule = new ExamSchedule();
    this.examSchedule.id = es.id;
    this.examSchedule.description = es.description;
    this.examSchedule.beginApplicationDate = es.beginApplicationDate;
    this.examSchedule.endApplicationDate = es.endApplicationDate;
    this.examSchedule.beginApplicationTime = es.beginApplicationTime;
    this.examSchedule.endApplicationTime = es.endApplicationTime;
    this.examSchedule.minutesExam = es.minutesExam;
    this.examName = data.description;
    this.isExamAvailable = es.isExamAvailable;
    if (es.tests.length > 0) {
      this.isExamCompleted = es.tests[0].isCompleted;
    }
    let c = 0;
    let isCorrect: boolean;
    for (let questions of data.teacherExamQuestion) {
      let q = questions.question;

      let response: any[] = [];
      response = q.answers;
      if (q.questionTypeId !== 2) {
        if (q.questionTypeId === 3) {
          response = [];
          for (let p of q.answers) {
            p.indexRelation = '';
            p.textrRelation = '';
            response.push(p);

          }
        }
        let question = new Question();
        question.id = q.id;
        question.content = q.content;
        question.answers = response;
        question.questionTypeId = q.questionTypeId;
        question.index = c;
        question.urlImage = q.urlImage;
        question.isMultiple = q.isMultiple;
        question.imageHeight = q.imageHeight;
        question.imageWidth = q.imageWidth;
        this.questions.push(question);
        c++;
      }
    }
    this.questionsTotal = this.questions.length;
    if (this.questionsTotal > 0) {
      this.fnShowCountDown(es.tests.length > 0 ? es.tests[0].startDate : new Date());
      this.getQuestion(0);
    }
  }

  getCondition() {
    let es = this.examSchedule;
    if (es != null) {
      return (es.beginApplicationTime != null && es.minutesExam != null ?
        "El examen sólo podrá ser contestado en la fecha, horario y duración establecido."
        : es.beginApplicationTime == null && es.minutesExam != null ?
          "El examen sólo podrá ser contestado en la fecha y duración establecida."
          : es.beginApplicationTime != null && es.minutesExam == null ?
            "El examen sólo podrá ser contestado en la fecha y horario establecido."
            : "El examen sólo podrá ser contestado en el periodo de aplicación.");
    }
  }

  back() {
    this._router.navigateByUrl(`/course/exams/${this.courseId}/${this.nodeId}`);
  }

  fnShowCountDown(startDate) {
    if (!this.isExamCompleted) {
      if (this.examSchedule.minutesExam == null) {
        if (this.isExamAvailable)
          this.beginDateTime = startDate;
      }
      else {
        this.beginDateTime = new Date(startDate);
        this.countDown = setInterval(() => {
          let sec = Math.round(((new Date()).getTime() - this.beginDateTime.getTime()) / 1000);
          sec = (this.examSchedule.minutesExam * 60) - sec;
          if (sec >= 0) {
            this.segundosRestantes = Math.trunc(sec % 60);
            this.minutosRestantes = Math.trunc(sec / 60) % 60;
            this.horasRestantes = Math.trunc(sec / 60 / 60);
          }
          else {
            this.working = true;
            clearInterval(this.countDown);

            let data = {
              'clientId': 1,
              'examScheduleId': this.examScheduleId,
              'answers': this.finalAnswers
            };
            this._srvTeacherExam.saveExamStuden(data).subscribe(result => {
              this.working = false;
              if (result.success) {
                swal({
                  title: Appsettings.APP_NAME,
                  text: 'Su examen concluyó, el tiempo se ha agotado',
                  type: 'success',
                  showCancelButton: false,
                  confirmButtonColor: '#3085d6',
                  cancelButtonColor: '#d33',
                  confirmButtonText: 'Aceptar'
                }).then((res) => {                  
                    this.back();                  
                });
              } else {
                // message
                swal(Appsettings.APP_NAME, result.message, 'warning');
              }
            });
          }
        }, 200);
      }
    }
  }
  // Obtiene una pregunta individual
  getQuestion(question: number) {
    if (this.question) {
      this.validQuestionRelations(this.question);
    }

    this.question = this.questions[question];
    this.typeQuestion = this.question.questionTypeId;
    // validamos en tipo de pregunta
    if (this.question.questionTypeId === 3) {
      // revisamo si la pregunta ya tienes respuestas
      let totalRelation = this.relationsQuestions[this.question.id];
      // Si no tiene agregamos la pregunta al banco
      if (!totalRelation) {
        this.relationsQuestions[this.question.id] = [];
      } else {
        this.relations = this.relationsQuestions[this.question.id];
      }
      this.orderAnswers(this.question.answers, this.question.id);
    }
    if (this.question.questionTypeId === 1 || this.question.questionTypeId === 4 || this.question.questionTypeId === 5) {
      let id = this.answers.filter(item => item.QuestionId === this.question.id);
      this.idds = [];
      if (this.question.isMultiple) {
        this.idds = [];
        let c = 0;
        let aws = this.question.answers;
        if (id.length > 0) {
          for (let i of id) {
            let cc = 0;
            for (let a of aws) {
              if (i.AnswerId == a.id) {
                this.idds[cc] = i.AnswerId;
              }
              cc++;
            }
            c++;
          }
        }
      } else {
        if (id.length > 0) {
          this.idd = id[0].AnswerId;
        }
      }

    }

  }

  // Agrega la respuesta salecionda al array de respuestas
  change(event) {
    let res = this.questionsResonse.lastIndexOf(this.question.id);

    if (res !== -1) {
      this.answers.splice(res, 1);
    } else {
      this.questionsResonse.push(this.question.id);
      this.countTotalQuestiosn++;
    }
    let answer = {
      QuestionId: this.question.id,
      AnswerId: event.value,
      Explanation: ''
    }
    if (this.examSchedule.minutesExam != null)
      this.saveAnswer(answer);
    this.answers.push(answer);

  }

  // Agrega la respuesta salecionda al array de respuestas
  changeCheck(event) {
    let res = this.questionsResonse.lastIndexOf(this.question.id);
    if (res === -1) {
      this.questionsResonse.push(this.question.id);
      this.countTotalQuestiosn++;
    }
    let resQ = this.answers.lastIndexOf(event.source.value);
    let answer = {
      QuestionId: this.question.id,
      AnswerId: event.source.value,
      Explanation: event.checked ? '1' : '0',
      index: resQ
    }
    if (this.examSchedule.minutesExam != null)
      this.saveAnswer(answer);
    if (event.checked) {
      this.answers.push(answer);
    } else {
      this.answers = this.answers.filter(item => item.AnswerId !== event.source.value);
      let resR = this.answers.filter(item => item.QuestionId === this.question.id);
      if (resR.length === 0) {
        this.countTotalQuestiosn--;
        this.questionsResonse.splice(res, 1);
      }
    }


  }

  orderAnswers(answers, questionId: number) {
    // variables    
    this.elementQuestions = [];
    let relationDescription: any[] = [];
    // Obtenemos los complementos para la relacion
    for (let a of answers) {
      relationDescription.push(a.relationDescription);
    }
    let result = this.orderResponse[questionId];
    // Ordenamos de forma aleatorea los complementos
    if (result !== undefined) {
      this.answersOrder = this.orderResponse[questionId];
    } else {
      this.orderResponse[questionId] = this.randArray(relationDescription);
      this.answersOrder = this.orderResponse[questionId];
    }

    let c = 0;
    // Creamos el arreglo de respuestas
    for (let ans of answers) {
      this.elementQuestions.push(
        {
          answers: ans.description,
          relation: this.answersOrder[c]
        });
      c++;
    }



  }

  // Ordenena el array de respuesta en random
  public randArray(arra1: any[]) {
    let ctr = arra1.length, temp, index;

    while (ctr > 0) {
      index = Math.floor(Math.random() * ctr);
      ctr--;
      temp = arra1[ctr];
      arra1[ctr] = arra1[index];
      arra1[index] = temp;
    }
    return arra1;
  }

  relationAsugned(index: number, complementText: string, answersId: number, questionId: number, i: number) {        
    if (isNumeric(index) && index > 0 && index <= this.question.answers.length) {
      this.question.answers[i].indexRelation = index;
      this.question.answers[index - 1].textrRelation = complementText;
      let answer = {
        QuestionId: this.question.id,
        AnswerId: this.question.answers[index - 1].id,
        Explanation: complementText,
      }
      this.validQuestionRelations(this.question);
      if (this.examSchedule.minutesExam != null)
        this.saveAnswer(answer);
    }
    else {
      for (let a of this.question.answers)
        if (a.textrRelation == complementText) {
          a.index = index;
          a.textrRelation = '';
          let answer = {
            QuestionId: this.question.id,
            AnswerId: a.id,
            Explanation: '',
          }
          this.validQuestionRelations(this.question);
          if (this.examSchedule.minutesExam != null)
            this.saveAnswer(answer);
        }
    }    
  }

  // Guarda las respuestas del examen
  qualifyExam() {
    // obtenemos las preguntas relacionales
    this.working = true;
    let c = 0;
    for (let q of this.questions) {
      if (q.questionTypeId === 3) {
        for (let a of q.answers) {
          this.finalAnswers.push({
            'AnswerId': a.id,
            'Explanation': a.textrRelation
          });
        }
      }
      c++;
    }

    for (let i of this.answers) {
      this.finalAnswers.push({
        'AnswerId': i.AnswerId,
        'Explanation': i.Explanation
      });
    }
    let data = {
      'clientId': 1,
      'examScheduleId': this.examScheduleId,
      'answers': this.finalAnswers
    };
    this._srvTeacherExam.saveExamStuden(data).subscribe(result => {
      this.working = false;
      if (result.success) {
        swal({
          title: Appsettings.APP_NAME,
          text: 'Su examen concluyó satisfactoriamente',
          type: 'success',
          showCancelButton: false,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Aceptar'
        }).then((res) => {         
            this.back();          
        });
      } else {
        // message
        swal(Appsettings.APP_NAME, result.message, 'warning');
      }
    });
  }

  // Valida que la pregunta relacional tenga toda las relaciones asginadas
  validQuestionRelations(question) {
    // Validamos sea pregunta relacional        
    if (question.questionTypeId === 3) {      
      // buscamos para ver si ya fue respondida anteriormente
      let res = this.questionsResonse.lastIndexOf(question.id);
      // Contador de control para la cantidad de respuesa
      let c = 0;
      // recorremos las respuetas de la pregunta
      for (let q of question.answers) {

        // si la respusta no tiene asociado un index se incrementa el contador
        if (q.indexRelation) {
          c++;
        }
      }
      if (res === -1 && c > 0) {
        this.questionsResonse.push(question.id);

        this.countTotalQuestiosn++;

      }
      // else if ( res !== -1 && c > 0) {
      //   this.countTotalQuestiosn--;
      // } else if ( res !== -1 && c === 0 ) {
      //   this.countTotalQuestiosn--;
      //   this.countTotalQuestiosn++;
      // }
    }
  }
}
