import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule} from '@angular/forms';
import { CommonModule } from '@angular/common';
import { CoreModule } from '../../core/core.module';
import { AdministratorModule  } from '../../components/layouts/administrator/administrator.module';

import { RA_ROUTES } from './ra.routes';

// MODULES
import { AdmonComponentModule } from '../../components/administrator/admon-component.module';
import { DialogAddThreadComponent } from '../../dialogs/dialog-add-thread/dialog-add-thread.component';
import { DialogGetFileComponent } from '../../dialogs/dialog-get-file/dialog-get-file.component';
import { DialogUploadFileComponent } from '../../dialogs/dialog-upload-file/dialog-upload-file.component';
import { DialogGetFileActivityComponent } from '../../dialogs/dialog-get-file-activity/dialog-get-file-activity.component';
import { DialogUploadFileActivityComponent } from '../../dialogs/dialog-upload-file-activity/dialog-upload-file-activity.component';
import { DialogTeacherContentComponent } from '../../dialogs/dialog-teacher-content/dialog-teacher-content.component';
import { DialogSearchGroupComponent } from '../../dialogs/dialog-search-group/dialog-search-group.component';
// // COMPONENTS
import { RaLayoutComponent } from './ra-layout.component';
import { ForumComponent } from './forum/forum.component';
import { PostComponent } from './post/post.component';

// MATERIALIZE
import { MatTabsModule,
  MatInputModule,
  MatDatepickerModule,
  MatRadioModule,
  MatCardModule,
  MatSelectModule,
  MatDialogModule,
  MatIconModule,
  MatButtonModule,
  MatCheckboxModule,
  MatTableModule,
  MatMenuModule,
  MatPaginatorModule,
  MatProgressBarModule,
  MatNativeDateModule,
  MatSlideToggleModule,
  MatToolbarModule,
  MatBadgeModule,
  MatListModule,
  MatAutocompleteModule,
  MatChipsModule,
  MatDividerModule,
  MatExpansionModule,
  MatSidenavModule,
  MatTooltipModule,
  MatProgressSpinnerModule
} from '@angular/material';

import { TreeviewModule } from 'ngx-treeview';
import { MatTreeModule } from '@angular/material/tree';
import { from } from 'rxjs';

import { CourseListComponent } from './course-list/course-list.component';
import { ApplyExamComponent } from './apply-exam/apply-exam.component';
import { AnnouncementListComponent } from './announcement-list/announcement-list.component';
import { GenerateAnnouncementComponent } from './generate-announcement/generate-announcement.component';
import { ContentComponent } from './content/content.component';
import { JoyrideModule } from 'ngx-joyride';

  @NgModule({
  imports: [
    RA_ROUTES,
    CoreModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MatTabsModule,
    MatInputModule,
    MatDatepickerModule,
    MatProgressBarModule,
    MatRadioModule,
    MatCardModule,
    MatSelectModule,
    MatDialogModule,
    MatIconModule,
    MatButtonModule,
    MatCheckboxModule,
    MatTableModule,
    MatMenuModule,
    MatPaginatorModule,
    MatNativeDateModule,
    MatSlideToggleModule,
    MatToolbarModule,
    MatListModule,
    AdministratorModule,
    MatAutocompleteModule,
    MatChipsModule,
    MatDividerModule,
    MatBadgeModule,
    MatExpansionModule,
    AdmonComponentModule,
    MatSidenavModule,
    MatTooltipModule,
    TreeviewModule.forRoot(),
    MatTreeModule,
    MatProgressSpinnerModule,
    JoyrideModule.forChild(),

  ],
  declarations: [
    RaLayoutComponent,
    DialogAddThreadComponent,
    DialogGetFileComponent,
    DialogUploadFileComponent,
    ForumComponent,
    PostComponent,
    CourseListComponent,
    ApplyExamComponent,
    DialogGetFileActivityComponent,
    DialogUploadFileActivityComponent,
    AnnouncementListComponent,
    GenerateAnnouncementComponent,
    ContentComponent,
    DialogTeacherContentComponent,
    DialogSearchGroupComponent
  ],
  entryComponents: [
    DialogAddThreadComponent,
    DialogGetFileComponent,
    DialogUploadFileComponent,
    DialogGetFileActivityComponent,
    DialogUploadFileActivityComponent,
    DialogTeacherContentComponent,
    DialogSearchGroupComponent
  ]
})
export class RaModule { }
