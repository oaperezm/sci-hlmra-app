import { Component, OnInit, ViewEncapsulation, AfterViewInit, ViewChild, ElementRef } from '@angular/core';

import { NavService, StorageService, UserService } from '../../service/service.index';
import { AdminHeaderService } from '../../components/layouts/administrator/admin-header/admin-header.service';
import { MatSidenav } from '@angular/material';
import { AdminNavService } from '../../components/layouts/administrator/admin-nav/admin-nav.service';

@Component({
  selector: 'app-ra-layout',
  templateUrl: './ra-layout.component.html',
  styleUrls: ['./ra-layout.component.scss']
})
export class RaLayoutComponent implements OnInit, AfterViewInit {

  opened: boolean = true;

  @ViewChild('appDrawer') appDrawer: ElementRef;
  @ViewChild('sidenav') public sidenav: MatSidenav;

  constructor(  public navService: NavService,
                public _srvStorage: StorageService,
                public _srvAdminNav: AdminNavService,
                public _srvHeader: AdminHeaderService) {

    this._srvStorage.title = 'Administración de cursos';
    this._srvStorage.showBackBotton = false;


  }

  ngOnInit() {

    this._srvAdminNav.setSidenav(this.sidenav);

  }

  ngAfterViewInit(): void {
    this.navService.appDrawer = this.appDrawer;
}


}
