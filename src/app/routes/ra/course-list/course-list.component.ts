import { Component, OnInit } from '@angular/core';

import { ActivatedRoute } from '@angular/router';

import { CourseNavService } from '../../../components/layouts/publication/course-nav/course-nav.service';
import { StorageService } from '../../../service/service.index';
import { AdminNavService } from '../../../components/layouts/administrator/admin-nav/admin-nav.service';
import { MatDialog } from '@angular/material';

import { DialogSearchGroupComponent } from '../../../dialogs/dialog-search-group/dialog-search-group.component';

@Component({
  selector: 'app-course-list',
  templateUrl: './course-list.component.html',
  styleUrls: ['./course-list.component.scss']
})
export class CourseListComponent implements OnInit {

  nodeId: number;
  working: boolean;

  constructor(  private _route: ActivatedRoute,
                public _courseNavService: CourseNavService,
                private _svrStorage: StorageService,
                public _srvAdminNav: AdminNavService,
                public _dialog: MatDialog) {
    this.working = false;
   }

  ngOnInit() {
    this._srvAdminNav.setTitle('Mis cursos');
    if (this._svrStorage.rol === 'Profesor') {
    }

    if (this._svrStorage.rol === 'Alumno') {
    }

    this._route.params.subscribe(params => {

      this.nodeId =  Number(params['id']);
      this._courseNavService.setCourse(  );

    });

  }

  onClickSave( event ) {
    const DIALOG_REF = this._dialog.open(DialogSearchGroupComponent, {
      width: '800px',
      height: '650px',
      autoFocus: false,
      disableClose: true
    });

    DIALOG_REF.afterClosed().subscribe(response => {
      if (response) {
        this._courseNavService.setCourse(  );
      }

    });
  }

}
