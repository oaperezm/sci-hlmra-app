import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import swal from 'sweetalert2';
import { Appsettings } from '../../../configuration/appsettings';

// Modelos
import { Announcement } from '../../../models/courses/announcement.model';
import { Course } from '../../../models/catalogs/course.model';
import { Group } from '../../../models/courses/group.model'
import { Pagination } from '../../../models/general/pagination.model';


// Servicios
import { AnnouncementService, StorageService } from '../../../service/service.index';
import { CourseService } from '../../../service/service.index';


// Complemetos
import { AdminHeaderService } from '../../../components/layouts/administrator/admin-header/admin-header.service';
import { AdminNavService } from '../../../components/layouts/administrator/admin-nav/admin-nav.service';

import { JoyrideService } from 'ngx-joyride';

@Component({
  selector: 'app-announcement-list',
  templateUrl: './announcement-list.component.html',
  styleUrls: ['./announcement-list.component.scss']
})
export class AnnouncementListComponent implements OnInit , OnDestroy {

  working: boolean;
  announcements: Announcement[] = [];
  courses: Course[] = [];
  groups: Group[] = [];
  coursesList: Course[] = [];

  textSearch: string;
  sizePage: string = Appsettings.DEFAULT_SIZE_PAGE;
  page: number = 1;
  pagination: Pagination = new Pagination();
  contentSection: any[] = [];


  constructor(  private _srvAnnouncement: AnnouncementService,
                private _router: Router,
                private _srvHeader: AdminHeaderService,
                private _srvCourse: CourseService,
                public _srvAdminNav: AdminNavService,
                private joyride: JoyrideService,
                public _srvStorage: StorageService) {

    this.working = false;
    this.setPage( { page: 1, size: this.sizePage });
    // Obtener todos los cursos del profesor
     this.getCourses();

  }

  ngOnInit() {
    this._srvAdminNav.setTitle('Generador de avisos');
    this._srvHeader.setUrlBackButton(['/resource/home', 'Inicio']);
    if ( this._srvStorage.showTourAnoummce === 0) {
      this._srvStorage.showTourAnoummce = 1;
      this.joyride.startTour(
        {
          steps: ['firstStep'],
          customTexts: {
            next: '>>',
            prev: '<<',
            done: 'Cerrar'
          }
        }
      )
    }

  }

  ngOnDestroy(): void {
   this._srvHeader.setBreadcrumb([]);
  }

  getCourses(): void {
     this._srvCourse.getAll().subscribe( result => {
       if ( result.success) {
         let data = result.data;
         for ( let c of data) {

           let course = new Course( this.contentSection, c.id, c.name, c.description, c.gol, c.startDate, c.endDate, c.nodeId,
                                    c.subject, c.monday, c.tuesday, c.wednesday, c.thursday, c.friday, c.saturday, c.sunday, c.parentId);

           if ( c.groups.length > 0 ) {
             for ( let g of c.groups ) {
               let group = new Group(g.id, g.description, g.status, g.courseId, g.code);
                 course.groups.push(group);
             }
           }

           this.coursesList.push(course);
         }
       }
     });
   }

  // Obtiene todos los avisos del maestro
  setPage( paginationData: object ) {
    this.working = true;
    this._srvAnnouncement.getAll(
      Number(paginationData['size']),
                             Number(paginationData['page']) ,
                             this.textSearch
                             ).subscribe( res => {

      this.announcements = [];
      let index = 1;

      if ( res.success) {

        this.pagination.pageNumber = Number(paginationData['page']);
        this.pagination.pageSize = res.pagination.pageSize;
        this.pagination.showNextPage = res.pagination.showNextPage;
        this.pagination.showPreviousPage = res.pagination.showPreviousPage;
        this.pagination.total = res.pagination.total;
        this.pagination.totalPage = res.pagination.totalPage;

        this.sizePage = paginationData['size'];


        for ( let g of res.data) {

          let announcement = new Announcement();
          this.groups= [];
          this.courses= [];

          announcement.id = g.id;
          announcement.title = g.title;
          announcement.description = g.description;
          announcement.registerDate = g.registerDate;
          announcement.active = g.active;

          for ( let a of g.studentGroup) {
            let course = new Course( this.contentSection, a.course.id, a.course.name, a.course.description, a.course.gol, a.course.startDate, a.course.endDate, a.course.nodeId,
                                      a.course.subject, a.course.monday, a.course.tuesday, a.course.wednesday, a.course.thursday, a.course.friday, a.course.saturday, a.course.sunday, a.course.parentId);
               this.courses.push(course);
          }

          for ( let b of g.studentGroup) {
             let group = new Group(b.id, b.description, b.status, b.courseId, b.code);
                this.groups.push(group);
           }

           announcement.groups = this.groups;
           announcement.courses =this.courses;
          announcement.index = this.pagination.getIndexRegister(index);

          this.announcements.push( announcement);
          index++;
        }
      }

      this.working = false;

    }, err => {
      this.working = false;
    });
  }

  // Funcion del buscador
  onKeyUp( event: any) {
    if ( this.textSearch.length >= 4 || this.textSearch.length === 0 ) {
      this.setPage( { page: 1, size: this.sizePage });
    }
  }

  // Editar un aviso
  onEditItem( element: Announcement ) {
    localStorage.setItem('ra.coursesList', JSON.stringify(this.coursesList));
    this._router.navigateByUrl( `/ra/generate-announcement/${element.id}`);
  }

  // Eliminar un aviso
  onDeleteItem( element: Announcement ) {
    this.working = true;
    this._srvAnnouncement.deleteAnnouncement( element.id).subscribe( result => {
      if ( result. success) {
        this.setPage( { page: 1, size: this.sizePage });
        swal(Appsettings.APP_NAME, result.message , 'success');

      } else {
        swal(Appsettings.APP_NAME, result.message , 'warning');
      }

      this.working = false;
    });
  }

  // Agregar nuevo aviso
  onClickNewAnnouncement() {
    localStorage.setItem('ra.coursesList', JSON.stringify(this.coursesList));
    this._router.navigateByUrl( `ra/generate-announcement/0`);
  }


}
