import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { MatPaginator} from '@angular/material';
import { AdminHeaderService } from '../../../components/layouts/administrator/admin-header/admin-header.service';
import { Appsettings } from '../../../configuration/appsettings';

// Modelos
import { Announcement } from '../../../models/courses/announcement.model';
import { Course } from '../../../models/catalogs/course.model';
import { Group } from '../../../models/courses/group.model';
import { Pagination } from '../../../models/general/pagination.model';

// Servicios
import { AnnouncementService, StorageService } from '../../../service/service.index';

// Complementos
import swal from 'sweetalert2';

declare function setHeight(): any;
import { JoyrideService } from 'ngx-joyride';

@Component({
  selector: 'app-generate-announcement',
  templateUrl: './generate-announcement.component.html',
  styleUrls: ['./generate-announcement.component.scss']
})
export class GenerateAnnouncementComponent implements OnInit, OnDestroy {

  courses: Course[] = [];
  groups: Group[] = [];
  groupsId = [];
  announcements: Announcement[] = [];
  announcement: Announcement;

  selectedCourses: any[] = [];
  selectedGroups: any[] = [];
  announcementId: number;
  working: boolean;
  editMode: boolean = true;

  sizePage: string = Appsettings.DEFAULT_SIZE_PAGE;
  page: number = 1;
  pagination: Pagination = new Pagination();

  selectedPublications: any[] = [];
  frmAnnouncement: FormGroup;
  contentSection: any[] = [];

  courseId: string = '0';
  groupId: string = '0';
  textSearch: string = '';
  displayedColumns: string[] = ['index', 'title', 'description', 'registerDate'];
  showBotton: boolean;
  showEdit: boolean;

  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(  private _route: ActivatedRoute,
                private _srvAnnouncement: AnnouncementService,
                private _srvHeader: AdminHeaderService,
                private _router: Router,
                private joyride: JoyrideService,
                public _srvStorage: StorageService ) {
      this.showBotton = false;
      this.showEdit = false;

      this.announcement = new Announcement();

      // Creamos los elementos del formulario para nuevo aviso
      this.frmAnnouncement = new FormGroup({
       id: new FormControl(0),
        title: new FormControl('', [ Validators.required, Validators.maxLength(100) ] ),
        description: new FormControl('', [ Validators.required, Validators.maxLength(350) ] ),
        registerDate: new FormControl('', [ Validators.required]),
       courses: new FormControl(),
       groups: new FormControl('', [ Validators.required]),
        selected: new FormControl()
      });

      // Obtenemos los parametros de la ruta
      this._route.params.subscribe( params => {
        this.announcementId = params['id'];
        if ( this.announcementId > 0 ) {
          this.showBotton = true;
          this.editMode = true;
          this.showEdit = false;
          // Obtenemos los datos del aviso
          this.getDataAnnouncement();
        }
      });

    this.setPage( { page: 1, size: this.sizePage });
   }

  ngOnInit() {
    setHeight();


    const title = this.announcementId == 0 ? 'Crear nuevo aviso' : 'Editar aviso';
        this._srvHeader.setTitle(title);
        this._srvHeader.setUrlBackButton(['/ra/list-announcement', 'Mis avisos']);
        // Recuperamos todos los cursos del profesor
        this.courses = JSON.parse(localStorage.getItem('ra.coursesList'));
        if ( this._srvStorage.showTourGenAnoummce === 0) {
          this._srvStorage.showTourGenAnoummce = 1;

          this.joyride.startTour(
            {
              steps: ['firstStep', 'secondStep'],
              customTexts: {
                next: '>>',
                prev: '<<',
                done: 'Cerrar'
              }
            }
          )
        }

  }

  ngOnDestroy(): void {
    this._srvHeader.setUrlBackButton([]);
  }



  getDataAnnouncement() {

    this._srvAnnouncement.getAnnouncementById( this.announcementId).subscribe( result => {
      if ( result.success ) {
        let announcement = result.data;
        //this.frmAnnouncement.reset();
        this.editMode= true;
        this.setForm( announcement);

        this.announcement.id = announcement.id;
        this.announcement.title = announcement.title;
        this.announcement.description = announcement.description;
        this.announcement.registerDate = announcement.registerDate;
        this.announcement.groups = announcement.studentGroup;


        this.selectedCourses = [];
        this.selectedGroups = [];
      for ( let c of announcement.studentGroup) {

          this.selectedCourses.push( c.course.id );
           let groups = this.courses.filter( x => x.id === c.course.id)[0].groups;

            for ( let g of groups) {
             let group = new Group(g.id, g.description, g.status, g.courseId, g.code);

              if (!this.groups.some((item) => item.id == group.id))
                  this.groups.push(group);

              if (!this.selectedGroups.some((item) => item == c.id))
                  this.selectedGroups.push( c.id );

            }
        }

       if ( this.selectedGroups.length === this.groups.length)
       {
        this.frmAnnouncement.controls['courses'].disable();
        this.frmAnnouncement.controls['groups'].disable();
        this.frmAnnouncement.controls['selected'].setValue(true);
       }
      }
      this.working = false;
    });
  }


  // Setea los datos en el formulario para editarlos
  setForm( announcement: any ) {
    this.frmAnnouncement.controls['title'].setValue( announcement.title);
    this.frmAnnouncement.controls['description'].setValue( announcement.description);
    this.frmAnnouncement.controls['selected'].setValue( announcement.selected);
    this.frmAnnouncement.controls['registerDate'].setValue( new Date(announcement.registerDate));
   this.frmAnnouncement.controls['registerDate'].markAsTouched();
  }


  onChangeCourse( event: any): void {
    if ((event != undefined) || (event.value > 0))
    {

       this.courseId = event.value || event;
       let _event = event.value;
       this.groups = [];

       if (this.courseId.length > 0 ) {
        for ( let a  of _event)  {
           let groups = this.courses.filter( x => x.id === a)[0].groups;
            for ( let g of groups) {
             let group = new Group(g.id, g.description, g.status, g.courseId, g.code);
             this.groups.push(group);
           }
          }

        } else
       if (event != "0")
        {
          let groups = this.courses.filter( x => x.id === event)[0].groups;
            for ( let g of groups) {
             let group = new Group(g.id, g.description, g.status, g.courseId, g.code);
             this.groups.push(group);
           }
        }

         else  this.groups = [];

        this.onChangeFilter();


    }
  }

  // Realiza filtrado por curso y grupo
  onChangeFilter( ): void {
    this.setPage( { page: 1, size: this.sizePage });
  }


   // Obtiene todos los avisos del maestro
   setPage( paginationData: object ) {
    this.working = true;
    this._srvAnnouncement.getAll(
      Number(paginationData['size']),
                             Number(paginationData['page']) ,
                             this.textSearch
                             ).subscribe( res => {

      this.announcements = [];
      let index = 1;

      if ( res.success) {

        this.pagination.pageNumber = Number(paginationData['page']);
        this.pagination.pageSize = res.pagination.pageSize;
        this.pagination.showNextPage = res.pagination.showNextPage;
        this.pagination.showPreviousPage = res.pagination.showPreviousPage;
        this.pagination.total = res.pagination.total;
        this.pagination.totalPage = res.pagination.totalPage;

        this.sizePage = paginationData['size'];

        for ( let g of res.data) {

          let announcement = new Announcement();

          announcement.id = g.id;
          announcement.title = g.title;
          announcement.description = g.description;
          announcement.registerDate = g.registerDate;
          announcement.index = this.pagination.getIndexRegister(index);

          this.announcements.push( announcement);
          index++;
        }
      }

      this.working = false;

    }, err => {
      this.working = false;
    });
  }

  onChangeAll(event: any):void {
    if ((event != undefined))
    {

      if (event.checked)
      {
        this.frmAnnouncement.controls['courses'].disable();
        this.frmAnnouncement.controls['groups'].disable();


              for ( let g of this.courses) {
                for (let gg of g.groups){
                  if (!this.groupsId.some((item) => item.id == gg.id))
                    this.groupsId.push(gg.id);
                }
             }
      }
      else
      {
        this.frmAnnouncement.controls['courses'].enable();
        this.frmAnnouncement.controls['groups'].enable();
      }
    }
  }
   // Activa los controles para editar un aviso
   onClickAnnouncement(): void {
    this.editMode = true;
    this.showEdit = false;
  }

  // Guarda el nuevo aviso
  onClickAddAnnouncement(): void {

    this.working = true;
    debugger
    let announcement = new Announcement();

    announcement.id = this.announcementId;
    announcement.description = this.frmAnnouncement.controls['description'].value;
    announcement.title = this.frmAnnouncement.controls['title'].value;
    announcement.registerDate = this.frmAnnouncement.controls['registerDate'].value;



    if (this.frmAnnouncement.controls["selected"].value)
      announcement.groups = this.groupsId;
    else
    {
      announcement.courses = this.frmAnnouncement.controls['courses'].value;
      announcement.groups = this.frmAnnouncement.controls['groups'].value;
    }


    if ( this.announcementId > 0 )
      this.updateAnnouncement( announcement);
     else
      this.saveAnnouncement( announcement );

      this.editMode = false;
    this.showBotton = true;
    this.showEdit = true;

  }

  // Guarda un nuevo aviso
  saveAnnouncement( announcement: Announcement ) {
    this._srvAnnouncement.save( announcement ).subscribe( result => {
      if ( result.success) {
        this.announcementId = result.data;
        announcement.id = result.data;
        announcement.title = this.frmAnnouncement.controls['title'].value;
        announcement.description = this.frmAnnouncement.controls['description'].value;
        announcement.registerDate = this.frmAnnouncement.controls['registerDate'].value;
        swal(Appsettings.APP_NAME,  result.message, 'success');
        this._router.navigateByUrl('/ra/list-announcement');
      } else {
        swal(Appsettings.APP_NAME,  result.message, 'error');
      }

      this.working = false;
    });


  }
  // Actualiza
  updateAnnouncement( announcement: Announcement ) {
    this._srvAnnouncement.update( announcement).subscribe( result => {
      if ( result.success) {
        this.announcement.title = this.frmAnnouncement.controls['title'].value;
        this.announcement.description = this.frmAnnouncement.controls['description'].value;
        this.announcement.registerDate = this.frmAnnouncement.controls['registerDate'].value;
        swal(Appsettings.APP_NAME,  result.message, 'success');
        this._router.navigateByUrl('/ra/list-announcement');
      } else {
        swal(Appsettings.APP_NAME,  result.message, 'error');
      }
      this.working = false;

    });
  }
  // Se cancela la edicion del aviso
  onClickCancel() {
    swal({
      title: '¿Estás seguro?',
      text: 'Perderá la información actualizada',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: '¡Sí, cancelar ahora!',
      cancelButtonText: 'Cancelar'
    }).then((result) => {
      if (result.value) {
        this._router.navigateByUrl('/ra/list-announcement');
      }
    });
  }

}
