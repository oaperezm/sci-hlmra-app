import { Routes, RouterModule } from '@angular/router';
import { RaLayoutComponent } from './ra-layout.component';

// GUARD
import { AuthGuardService } from '../../service/service.index';
import { CourseComponent} from '../teacher/course/course.component';
import { ForumComponent } from './forum/forum.component';
import { PostComponent } from './post/post.component';
import { CourseListComponent } from './course-list/course-list.component';
import { ApplyExamComponent } from './apply-exam/apply-exam.component';
import { GenerateAnnouncementComponent } from './generate-announcement/generate-announcement.component';
import { AnnouncementListComponent } from './announcement-list/announcement-list.component';
import { ContentComponent } from './content/content.component';

const AppPageRa: Routes = [
  {
    path: 'ra',
    component: RaLayoutComponent,
    children: [
      { path: 'forum', component: ForumComponent, canActivate: [ AuthGuardService ]},
      { path: 'post/:id', component: PostComponent, canActivate: [ AuthGuardService ]},
      { path: 'list-courses', component: CourseListComponent },
      { path: 'apply-exam/:examId/:examScheduleId/:nodeId/:courseId', component: ApplyExamComponent},
      { path: 'generate-announcement/:id', component: GenerateAnnouncementComponent},
      { path: 'list-announcement', component: AnnouncementListComponent},
      { path: 'repository', component: ContentComponent},
      { path: '', component: CourseComponent }
    ]
  }
];
export const RA_ROUTES = RouterModule.forChild( AppPageRa );
