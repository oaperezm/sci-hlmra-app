import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

// Servicios
import { UserService, StorageService, SettingsService } from '../../../service/service.index';

// COMPONENT SERVICES
import { HeaderService } from '../../../components/layouts/anonymous/header/header.service';

import { Appsettings } from '../../../configuration/appsettings';

import swal from 'sweetalert2';

declare function addDataLayer(selement , offset): any;

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  working: boolean;
  frmLogin: FormGroup;
  urlPrivacy = Appsettings.PRIVACY_ANNOUNCEMENT;
  ccts: any[] = [];
  constructor(  private _userSrv: UserService,
                private _storageSrv: StorageService,
                private _router: Router,
                private _srvHeader: HeaderService,
                public _srveSettings: SettingsService,
                private route: ActivatedRoute ) {


    this.route.params.subscribe(params => {

      if (params.email) {
        this.working = true;
        this._userSrv.userActiveCount(params.email).subscribe( res => {
          this.working = false;
          if(res.success) {
            swal(Appsettings.APP_NAME, 'Cuenta activada correctamente', 'success');

          }else {
            swal(Appsettings.APP_NAME, 'Error al activar la cuenta, contacte al administrador.', 'warning');
          }
        })
      }

    });

    this.frmLogin = new FormGroup({
      'username': new FormControl('', [ Validators.required,
                                        Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+.[a-z]{2,3}$') ]),
      'password': new FormControl('', [ Validators.required]),
      'recaptchaReactive': new FormControl('', [Validators.required])
    });


  }

  ngOnInit() {
    this._srvHeader.showReturnButton( true );
    window.scroll(0, 0);
  }

  onSubmit() {
    this.working = true;
    const userName = this.frmLogin.controls['username'].value;
    const password = this.frmLogin.controls['password'].value;
    this._userSrv.login( userName, password ).subscribe( result => {

      if ( result.access_token ) {
        this._storageSrv.token          = result.access_token ;
        this._storageSrv.email          = result.email;
        this._storageSrv.name           = result.fullName;
        this._storageSrv.userName       = this.frmLogin.value.username;
        this._storageSrv.rol            = result.rol;
        this._storageSrv.refreshToken   = result.refresh_token;


        /*
        if ( result.rol.indexOf('Administrador') > -1 ) {
          swal( Appsettings.APP_NAME, 'El rol del usuario no está permitido iniciar sesión en este sitio', 'error');
          this._router.navigateByUrl('/login');
        }
        */

        this._userSrv.getProfile().subscribe( r => {

          if ( r.success ) {
            if ( r.data.permissions ) {
              this._storageSrv.permissions = JSON.stringify(r.data.permissions);
            }

            if ( r.data.publishingNodes ) {
              this._storageSrv.nodes = JSON.stringify(r.data.publishingNodes);
            }

            if ( r.data.notifications ) {
              this._storageSrv.notifications = JSON.stringify(r.data.notifications);
            }

            this._storageSrv.profileImage =  r.data.user.urlImage;
            this._srveSettings.aplicarTema(JSON.parse(this._storageSrv.nodes));
            this._storageSrv.showModal = 0;
            this._storageSrv.showTour = 0;
            this._storageSrv.showTourMaterial = 0;
            this._storageSrv.showTourBooks = 0;
            this._storageSrv.showTourGexan = 0;
            this._storageSrv.showTourGexanNuevo = 0;
            this._storageSrv.showTourQbank = 0;
            this._storageSrv.showTourQbankAdd = 0;
            this._storageSrv.showTourListExam = 0;
            this._storageSrv.showTourAnoummce = 0;
            this._storageSrv.showTourGenAnoummce = 0;
            this._storageSrv.showTourCourse = 0;
            this._storageSrv.showTourRepo = 0;
            this._storageSrv.showTourForum = 0;
            this._storageSrv.showTourAddSala = 0;
            this._router.navigateByUrl('/resource/home');

            /*
            if (result.rol.indexOf('Alumno') > -1 || result.rol.indexOf('Profesor') > -1 ) {
              this._router.navigateByUrl('/resource/home');
            } else {
              this._router.navigateByUrl('/login');
            }
            */
          }


          this.getDataUser();
        });

      } else {
        swal( Appsettings.APP_NAME, 'Usuario y/o contraseña no válidos', 'error');
        this.working = false;
      }
    }, err => {

      const messageError = JSON.parse(err._body);

      if ( messageError.error ) {
        swal( Appsettings.APP_NAME, messageError.error,  'error');
      } else {
        swal( Appsettings.APP_NAME, 'Problemas al autenticar el usuario',  'error');
      }
      this.working = false;
    });
  }


  getDataUser() {
    this._userSrv.getProfileData().subscribe( r => {
      let id;
      let ccts: string[] = [];
      if ( r.success) {
        let data = r.data;
        id = data.id;
        if( data.userCCTs.length > 0){
          for(let cct of data.userCCTs){
            ccts.push(cct.cct);
          }
        }


        addDataLayer(id,ccts);
      }
      this.working = false;
    })
  }
  resolved(captchaResponse: string, res) {
    console.log(`Resolved response token: ${captchaResponse}`);
  }
}
