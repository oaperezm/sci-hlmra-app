import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule} from '@angular/forms';
import { CommonModule } from '@angular/common';
import { ImageUploadModule } from 'angular2-image-upload';

import { PAGESEXTERNAL_ROUTES } from './pagesexternal.routes';
// COMPONENTS
import { LoginComponent } from './login/login.component';

import { HomeComponent } from '../home/home.component';
import { PagesexternalComponent } from './pagesexternal.compnent';

// Modulos
import { AnonymousModule } from '../../components/layouts/anonymous/anonymous.module';
import { CoreModule } from '../../core/core.module';
// ng-recaptcha
import { RecaptchaModule, RecaptchaFormsModule,  RECAPTCHA_SETTINGS, RecaptchaSettings} from 'ng-recaptcha';

// MATERIALIZE
import {
  MatAutocompleteModule,
  MatBadgeModule,
  MatBottomSheetModule,
  MatButtonModule,
  MatButtonToggleModule,
  MatCardModule,
  MatCheckboxModule,
  MatChipsModule,
  MatDatepickerModule,
  MatDialogModule,
  MatDividerModule,
  MatExpansionModule,
  MatGridListModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatMenuModule,
  MatNativeDateModule,
  MatPaginatorModule,
  MatProgressBarModule,
  MatProgressSpinnerModule,
  MatRadioModule,
  MatRippleModule,
  MatSelectModule,
  MatSidenavModule,
  MatSliderModule,
  MatSlideToggleModule,
  MatSnackBarModule,
  MatSortModule,
  MatStepperModule,
  MatTableModule,
  MatTabsModule,
  MatToolbarModule,
  MatTooltipModule,
  MatTreeModule,
  MatFormFieldModule
} from '@angular/material';

  import { RegistryComponent } from './registry/registry.component';
import { TipListComponent } from './tip-list/tip-list.component';
import { ContaintListComponent } from './containt-list/containt-list.component';
import { ContaintDetailComponent } from './containt-detail/containt-detail.component';
import { EventListComponent } from './event-list/event-list.component';
import { TipDetailComponent } from './tip-detail/tip-detail.component';
import { EventDetailComponent } from './event-detail/event-detail.component';
import { PasswordRecoveryComponent } from './password-recovery/password-recovery.component';

@NgModule({
  declarations: [
    HomeComponent,
    LoginComponent,
    PagesexternalComponent,
    RegistryComponent,
    TipListComponent,
    ContaintListComponent,
    EventListComponent,
    TipDetailComponent,
    EventDetailComponent,
    ContaintDetailComponent,
    PasswordRecoveryComponent
  ], exports: [
    HomeComponent,
    LoginComponent,
    RegistryComponent
  ],
  imports: [
    CoreModule,
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    PAGESEXTERNAL_ROUTES,
    MatAutocompleteModule,
    MatBadgeModule,
    MatBottomSheetModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatCardModule,
    MatCheckboxModule,
    MatChipsModule,
    MatStepperModule,
    MatDatepickerModule,
    MatDialogModule,
    MatDividerModule,
    MatExpansionModule,
    MatGridListModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatNativeDateModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatRippleModule,
    MatSelectModule,
    MatSidenavModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatSnackBarModule,
    MatSortModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule,
    MatTooltipModule,
    MatTreeModule,
    MatFormFieldModule,
    ImageUploadModule.forRoot(),
    AnonymousModule,
    RecaptchaModule,  //this is the recaptcha main module
    RecaptchaFormsModule, //this is the module for form incase form validation

  ], providers: [{
    provide: RECAPTCHA_SETTINGS,
    useValue: {
      siteKey: '6Lfj0cIZAAAAAOJmv6tTMl-cbXtQr5Hrm3pxTRtO',
    } as RecaptchaSettings,
  }
  ],
  entryComponents: [ ]
})

export class PagesExternalModule {}

