import { Routes, RouterModule} from '@angular/router';
import { PagesexternalComponent } from './pagesexternal.compnent';
import { LoginComponent } from './login/login.component';
import { RegistryComponent } from './registry/registry.component';
import { TipListComponent } from './tip-list/tip-list.component';
import { ContaintListComponent } from './containt-list/containt-list.component';
import { EventListComponent } from './event-list/event-list.component';
import { TipDetailComponent } from './tip-detail/tip-detail.component';
import { EventDetailComponent } from './event-detail/event-detail.component';
import { ContaintDetailComponent } from './containt-detail/containt-detail.component';
import { PasswordRecoveryComponent } from './password-recovery/password-recovery.component';

const AppPageExternalRoutes: Routes = [
  {
    path: 'home',
    component: PagesexternalComponent,
    children: [
      { path: 'registry', component: RegistryComponent },
      { path: 'list-tips', component: TipListComponent },
      { path: 'list-news', component: ContaintListComponent },
      { path: 'list-events', component: EventListComponent },
      { path: 'detail-tip/:id', component: TipDetailComponent},
      { path: 'detail-event/:id', component: EventDetailComponent},
      { path: 'detail-new/:id', component: ContaintDetailComponent},
      { path: '', redirectTo: '/home/login', pathMatch: 'full'},

    ],
  },
  { path: 'login/:email', component: LoginComponent },
  { path: 'login', component: LoginComponent },
  { path: 'recover-password', component: PasswordRecoveryComponent },
];

export const PAGESEXTERNAL_ROUTES = RouterModule.forChild( AppPageExternalRoutes );
