import { Routes, RouterModule } from '@angular/router';
import { TeacherLayoutComponent } from './teacher-layout.component';

// GUARD
import { AuthGuardService } from '../../service/service.index';
import { CourseComponent} from '../teacher/course/course.component';
import { ExamScheduleComponent } from './exam-schedule/exam-schedule.component';
import { ExamQualifyComponent } from './exam-qualify/exam-qualify.component';
import { AttendanceListComponent } from './attendance-list/attendance-list.component';
import { StudentsComponent } from './students/students.component';
import { InterestSiteComponent } from './interest-site/interest-site.component';
import { TaskScheduleComponent } from './task-schedule/task-schedule.component';
import { ActivityScheduleComponent } from './activity-schedule/activity-schedule.component';
import { ResourcePlanningComponent } from './resource-planning/resource-planning.component';
import { FinalScoreComponent } from './final-score/final-score.component';
import { HomeworkQualifyComponent } from './homework-qualify/homework-qualify.component';
import { ActivityQualifyComponent } from './activity-qualify/activity-qualify.component';

const AppPageSali: Routes = [
  {
    path: 'teacher',
    runGuardsAndResolvers: 'paramsOrQueryParamsChange',
    component: TeacherLayoutComponent,
    children: [
      { path: 'attendance/:studenGroupId/:course', component: AttendanceListComponent },
      { path: 'courses', component: CourseComponent },
      { path: 'students/:id/:course', component: StudentsComponent },
      { path: 'exam-schedule/:id/:course', component: ExamScheduleComponent},
      { path: 'qualify/:status/:groupId/:examScheduleId/:typeExam/:description/:statusId/:endDate', component: ExamQualifyComponent },
      { path: 'interest-site/:courseId', component: InterestSiteComponent},
      { path: 'task-schedule/:studenGroupId', component: TaskScheduleComponent},
      { path: 'activity-schedule/:studenGroupId', component: ActivityScheduleComponent},
      { path: 'resource-planning/:studenGroupId', component: ResourcePlanningComponent },
      { path: 'final-score/:studenGroupId', component: FinalScoreComponent },
      { path: 'homework-qualify/:groupId/:examScheduleId/:endDate', component: HomeworkQualifyComponent},
      { path: 'activity-qualify/:groupId/:examScheduleId/:endDate', component: ActivityQualifyComponent},
      { path: '', component: CourseComponent }
    ]
  }
];
export const TEACHER_ROUTES = RouterModule.forChild( AppPageSali );
