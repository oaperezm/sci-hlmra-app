import { Component, OnInit, ViewChild  } from '@angular/core';
import { Route, ActivatedRoute } from '@angular/router';
// Servicios
import { AdminHeaderService } from '../../../components/layouts/administrator/admin-header/admin-header.service';
// Modelos
import { Group } from '../../../models/courses/group.model';
// Componentes
import { InterestSitesComponent } from '../../../components/administrator/interest-sites/interest-sites.component';
import { Appsettings } from '../../../configuration/appsettings';
import { AdminNavService } from '../../../components/layouts/administrator/admin-nav/admin-nav.service';

@Component({
  selector: 'app-interest-site',
  templateUrl: './interest-site.component.html',
  styleUrls: ['./interest-site.component.scss']
})
export class InterestSiteComponent implements OnInit {

  @ViewChild( InterestSitesComponent )
  public timerComponent: InterestSitesComponent;

  group: Group;

  courseId: number;
  constructor(  private _srvHeader: AdminHeaderService,
                private _route: ActivatedRoute,
                public _srvNavService: AdminNavService) {
    this._srvHeader.setBreadcrumb([Appsettings.APP_NAME, 'Mi salón', 'Sitios de interés']);
  }

  ngOnInit() {
    this._route.params.subscribe( params => {
      this.courseId = params['courseId'];
      this.group = JSON.parse(localStorage.getItem('ra.group'));
      this._srvNavService.setTitle('Sitios de interés ' + this.group.description);
    });
  }

  // Muestra el formulario para un nuevo sitio de interes
  onClickAddNew() {
    if ( !this.timerComponent.showForm) {
      this.timerComponent.addInterestSite(  );
    }
  }

}
