import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { Route, ActivatedRoute } from '@angular/router';

// Componente
import { ActivitysScheduleComponent } from '../../../components/administrator/activitys-schedule/activitys-schedule.component';
// Servicios
import { AdminHeaderService } from '../../../components/layouts/administrator/admin-header/admin-header.service';
import { AdminNavService } from '../../../components/layouts/administrator/admin-nav/admin-nav.service';

// Modelos
import { Group} from '../../../models/courses/group.model';
import { ActivityService } from '../../../service/publishing/activity.service';

@Component({
  selector: 'app-activity-schedule',
  templateUrl: './activity-schedule.component.html',
  styleUrls: ['./activity-schedule.component.scss']
})
export class ActivityScheduleComponent implements OnInit, OnDestroy {

  @ViewChild(ActivitysScheduleComponent)
  private timerComponent: ActivitysScheduleComponent;

  group: Group;
  studenGroupId: number;
  totalActivities: number;
  constructor(  public _srvNavService: AdminNavService,
                private _srvHeader: AdminHeaderService,
                private _route: ActivatedRoute,
                private _srvActivity: ActivityService) {
    this.totalActivities = 0;
                }

  ngOnInit() {
    this._route.params.subscribe( params => {

      this.studenGroupId = params.studenGroupId;
      this.group = JSON.parse(localStorage.getItem('ra.group'));
      this.timerComponent.getActivity( this.studenGroupId);
      this._srvNavService.setTitle('Programar actividad: ' + this.group.description);

    });
  }

  // Agregar nueva actividad
  onClickAddNew() {
    if ( !this.timerComponent.showForm) {
      this.timerComponent.addTaskSchedule(  );
    }
  }

  ngOnDestroy(): void {
    this._srvHeader.setBreadcrumb([]);
  }

  totalActivity( event){
    this.totalActivities = event;
  }
}
