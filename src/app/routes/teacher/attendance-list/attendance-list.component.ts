import { Component, OnInit, ViewChild } from '@angular/core';
import { Route, ActivatedRoute } from '@angular/router';
import { Appsettings } from '../../../configuration/appsettings';
import { UserUtilities } from '../../../shared/user.utilities';

// Servicios
import { AdminHeaderService } from '../../../components/layouts/administrator/admin-header/admin-header.service';
import { AttendancesListComponent } from '../../../components/administrator/attendances-list/attendances-list.component';

// Modelos
import { Group } from '../../../models/courses/group.model';
import { Course } from '../../../models/catalogs/course.model';
import { AdminNavService } from '../../../components/layouts/administrator/admin-nav/admin-nav.service';

@Component({
  selector: 'app-attendance-list',
  templateUrl: './attendance-list.component.html',
  styleUrls: ['./attendance-list.component.scss']
})
export class AttendanceListComponent implements OnInit {

  studenGroupId: number;
  group: Group;
  course: Course;
  today: string;
  minDate: Date;
  maxDate: Date;

  @ViewChild(AttendancesListComponent)
  public timerComponent: AttendancesListComponent;

  constructor( private _route: ActivatedRoute,
               public _srvNavService: AdminNavService) {

      this.today = UserUtilities.dateFullSerialize(new Date());
      this.minDate = new Date(2000, 0, 1);
      this.maxDate = new Date(2020, 0, 1);

  }

  ngOnInit() {
    this._route.params.subscribe( params => {
      this.studenGroupId = params.studenGroupId;
      this.timerComponent.getStudentsAssistance(this.today, this.today, this.studenGroupId);
      
      this.group = JSON.parse(localStorage.getItem('ra.group'));
      this.timerComponent.group = this.group;
      this.timerComponent.startDates = undefined;
      this.timerComponent.fecha = undefined;

      this.timerComponent.students= [];
      this.timerComponent.showmsg=false;
      this.timerComponent.getStudents();
      this._srvNavService.setTitle('Lista de Asistencias ' + this.group.description);
    });
  }

}
