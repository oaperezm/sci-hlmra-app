import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { FormGroup, FormControl, Validators } from '@angular/forms';

// Servicios
import { PlanningService, CourseService, ExamScheduleService } from '../../../service/service.index';
import { AdminNavService } from '../../../components/layouts/administrator/admin-nav/admin-nav.service';

// Modelos
import { Planning } from '../../../models/courses/planning.model';
import { SchedulingPlartial } from '../../../models/courses/schedulingPartial.model';
// Modals
import { DialogAddPartialComponent } from '../../../dialogs/dialog-add-partial/dialog-add-partial.component';
import swal from 'sweetalert2';

import { Appsettings } from '../../../configuration/appsettings';
import { Group } from 'src/app/models/courses/group.model';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-resource-planning',
  templateUrl: './resource-planning.component.html',
  styleUrls: ['./resource-planning.component.scss']
})
export class ResourcePlanningComponent implements OnInit {

  working: boolean;

  frmPlanning: FormGroup;

  totalPercentage: number;

  valid: boolean;

  exam: number = 0;
  activity: number = 0;
  homework: number = 0;
  assistance: number = 0;

  total: number;

  selectedTabIndex: number;
  schedulingPlartial: SchedulingPlartial[] = [];
  group: Group;

  courseId: number;
  groupId: number;
  planningId: number;

  courseDateInit: Date;
  courseEndDate: Date;
  studentGroupId: any;

  constructor(private _dialog: MatDialog,
    private _srvCourser: CourseService,
    private _srvPlanning: PlanningService,
    private _routeActive: ActivatedRoute,
    private _srvExam: ExamScheduleService,
    public _srvNavService: AdminNavService) {
    this.working = false;

    this.frmPlanning = new FormGroup({
      id: new FormControl(0),
      homework: new FormControl(0),
      exam: new FormControl(0),
      assistance: new FormControl(0),
      activity: new FormControl(0),
      studentGroupId: new FormControl(0),
    });


    this.totalPercentage = 0;
    let course = JSON.parse(localStorage.getItem('ra.course'));
    let group = JSON.parse(localStorage.getItem('ra.group'));

    this.groupId = group.id;
    this.courseId = course.id;


    this.courseDateInit = course.startDate;
    this.courseEndDate = course.endDate;
  }


  ngOnInit() {
    this.studentGroupId = this._routeActive.params;
    this._routeActive.params.subscribe(
      params => {
        this.group = JSON.parse(localStorage.getItem('ra.group'));
        this._srvNavService.setTitle('Planear curso: ' + this.group.description);
          this.getPartials(this.studentGroupId.value.studenGroupId);
          this.getPlanning(this.studentGroupId.value.studenGroupId);
      }
  );
  }

  // Obtien los parciales de la planeacion
  getPartials(groupId: number) {
    this._srvExam.getSchedulingpartial(groupId).subscribe(resp => {
      this.schedulingPlartial = [];
      if (resp.success) {
        for (let p of resp.data) {
          let partial = new SchedulingPlartial();
          partial.id = p.id;
          partial.startDate = p.startDate;
          partial.endDate = p.endDate;
          partial.description = p.description;
          partial.coursePlanningId = p.coursePlanningId;

          this.schedulingPlartial.push(partial);
        }
      }
    });
  }

  // Obtine los valores de la planacion
  getPlanning(groupId: number) {
    this.working = true;
    this._srvCourser.getPlanning(groupId).subscribe(resp => {
      let data = resp.data[0];

      this.planningId = data.id;

      this.frmPlanning.controls['id'].setValue(data.id);
      this.frmPlanning.controls['activity'].setValue(data.activity);
      this.frmPlanning.controls['assistance'].setValue(data.assistance);
      this.frmPlanning.controls['exam'].setValue(data.exam);
      this.frmPlanning.controls['homework'].setValue(data.homework);
      this.frmPlanning.controls['studentGroupId'].setValue(data.studentGroupId);

      this.onSume();


    });

  }

  // Actualiza los valores de la planeacion
  onUpdatePlanning() {
    this.working = true;

    let planning = new Planning();

    planning.id = this.frmPlanning.controls['id'].value;
    planning.activity = this.frmPlanning.controls['activity'].value;
    planning.assistance = this.frmPlanning.controls['assistance'].value;
    planning.exam = this.frmPlanning.controls['exam'].value;
    planning.homework = this.frmPlanning.controls['homework'].value;
    planning.studentGroupId = this.frmPlanning.controls['studentGroupId'].value;
    this._srvPlanning.update(planning).subscribe(resp => {
      if (resp.success) {

        swal(Appsettings.APP_NAME, resp.message, 'success');
      } else {
        swal(Appsettings.APP_NAME, resp.message, 'error');
      }
      this.working = false;
    });

  }

  // Muestra modal para nuevo parcial
  onClickAddNew(planningId: number) {

    let newPartial = new SchedulingPlartial();

    newPartial.id = 0;
    newPartial.coursePlanningId = 0;
    newPartial.description = '';
    newPartial.endDate = null;
    newPartial.finalPartial = 0;
    newPartial.startDate = null;

    const dateInit = this.courseDateInit;
    const dateEnd = this.courseEndDate;

    const DIALOG_REF = this._dialog.open(DialogAddPartialComponent, {
      width: '500px',
      height: '400px',
      autoFocus: false,
      disableClose: true,
      data: {
        dateInit: dateInit,
        dateEnd: dateEnd,
        groupId: this.groupId,
        courseId: this.courseId,
        planningId: this.planningId,
        courseDateInit: this.courseDateInit,
        courseDateEnd: this.courseEndDate,
        partials: this.schedulingPlartial,
        newPartial: newPartial
      }
    });

    DIALOG_REF.afterClosed().subscribe(response => {
      if (response) {
        this.getPartials(this.groupId);
      }
    });
  }

  // Suma el porcentaje de todos los elementos
  onSume() {

    this.totalPercentage = this.exam + this.activity + this.assistance + this.homework;

    this.valid = (this.totalPercentage === 100) ? true : false;
    this.working = false;
  }

  // Elimina un parcial
  onDeletePartial(partial) {
    this.working = true;
    this._srvPlanning.deletePartial(partial.id, partial.coursePlanningId).subscribe(result => {

      if (result.success) {
        this.getPartials(this.groupId);
        swal(Appsettings.APP_NAME, result.message, 'success');
      } else {
        swal(Appsettings.APP_NAME, result.message, 'error');
      }
      this.working = false;
    });
  }

  // Actualiza los datos del parcial
  onUpdatePartial(partial: SchedulingPlartial): void {
    const DIALOG_REF = this._dialog.open(DialogAddPartialComponent, {
      width: '500px',
      height: '400px',
      autoFocus: false,
      disableClose: true,
      data: {
        groupId: this.groupId,
        courseId: this.courseId,
        planningId: this.planningId,
        courseDateInit: this.courseDateInit,
        courseDateEnd: this.courseEndDate,
        partials: this.schedulingPlartial,
        newPartial: partial
      }
    });

    DIALOG_REF.afterClosed().subscribe(response => {
      if (response) {
        this.getPartials(this.groupId);
      }
    });
  }
}
