import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule} from '@angular/forms';
import { CommonModule } from '@angular/common';
import { CoreModule } from '../../core/core.module';
import { AdministratorModule  } from '../../components/layouts/administrator/administrator.module';

import { TEACHER_ROUTES } from './teacher.routes';

// MODULES
import { AdmonComponentModule } from '../../components/administrator/admon-component.module';

// // COMPONENTS
import { TeacherLayoutComponent } from './teacher-layout.component';


// MATERIALIZE
import { MatTabsModule,
  MatInputModule,
  MatDatepickerModule,
  MatRadioModule,
  MatCardModule,
  MatSelectModule,
  MatDialogModule,
  MatIconModule,
  MatButtonModule,
  MatCheckboxModule,
  MatTableModule,
  MatMenuModule,
  MatPaginatorModule,
  MatProgressBarModule,
  MatNativeDateModule,
  MatSlideToggleModule,
  MatToolbarModule,
  MatBadgeModule,
  MatListModule,
  MatAutocompleteModule,
  MatChipsModule,
  MatDividerModule,
  MatExpansionModule,
  MatSidenavModule,
  MatTooltipModule,
  MatProgressSpinnerModule} from '@angular/material';

import { TreeviewModule } from 'ngx-treeview';
import { MatTreeModule } from '@angular/material/tree';
import { CdkTreeModule } from '@angular/cdk/tree';
import { CourseComponent } from './course/course.component';
import { ExamQualifyComponent } from './exam-qualify/exam-qualify.component';
import { AttendanceListComponent } from './attendance-list/attendance-list.component';
import { StudentsComponent } from './students/students.component';
import { ExamScheduleComponent } from './exam-schedule/exam-schedule.component';

// import { StudentsComponent } from './students/students.component';
// import { SectionsComponent } from './sections/sections.component';
// import { ExamScheduleComponent } from './exam-schedule/exam-schedule.component';
// import { CoursesComponent } from './courses/courses.component';
// import { DialogRelationContentComponent } from '../../dialogs/dialog-relation-content/dialog-relation-content.component';
// import { TaskScheduleComponent } from './task-schedule/task-schedule.component';
// import { EventsComponent } from './events/events.component';
// import { HomeworkQualifyComponent } from './homework-qualify/homework-qualify.component';
// import { DialogStudenHomeworkComponent } from '../../dialogs/dialog-studen-homework/dialog-studen-homework.component';
// import { InterestSiteComponent } from './interest-site/interest-site.component';

import { DialogInvitationComponent } from '../../dialogs/dialog-invitation/dialog-invitation.component';
import { DialogStudentAnswersComponent } from '../../dialogs/dialog-student-answers/dialog-student-answers.component';
import { InterestSiteComponent } from './interest-site/interest-site.component';
import { TaskScheduleComponent } from './task-schedule/task-schedule.component';
import { ActivityScheduleComponent } from './activity-schedule/activity-schedule.component';
import { ResourcePlanningComponent } from './resource-planning/resource-planning.component';
import { FinalScoreComponent } from './final-score/final-score.component';
import { DialogFinalScoreComponent } from '../../dialogs/dialog-final-score/dialog-final-score.component';
import { DialogAddPartialComponent } from '../../dialogs/dialog-add-partial/dialog-add-partial.component';
import { HomeworkQualifyComponent } from './homework-qualify/homework-qualify.component';
import { DialogStudenHomeworkComponent } from '../../dialogs/dialog-studen-homework/dialog-studen-homework.component';
import { ActivityQualifyComponent } from './activity-qualify/activity-qualify.component';
import { DialogStudentActivityComponent } from '../../dialogs/dialog-student-activity/dialog-student-activity.component';
import { DialogDownloadFinalScoreComponent } from '../../dialogs/dialog-final-score-download/dialog-final-score-download.component'; //Ricardo - Agregdo
import { DialogSendPdfComponent } from '../../dialogs/dialog-send-pdf/dialog-send-pdf.component'; //Armand - Agregdo
import { JoyrideModule } from 'ngx-joyride';

  @NgModule({
  imports: [
    TEACHER_ROUTES,
    CoreModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MatTabsModule,
    MatInputModule,
    MatDatepickerModule,
    MatProgressBarModule,
    MatRadioModule,
    MatCardModule,
    MatSelectModule,
    MatDialogModule,
    MatIconModule,
    MatButtonModule,
    MatCheckboxModule,
    MatTableModule,
    MatMenuModule,
    MatPaginatorModule,
    MatNativeDateModule,
    MatSlideToggleModule,
    MatToolbarModule,
    MatListModule,
    AdministratorModule,
    MatAutocompleteModule,
    MatChipsModule,
    MatDividerModule,
    MatBadgeModule,
    MatExpansionModule,
    AdmonComponentModule,
    MatSidenavModule,
    MatTooltipModule,
    TreeviewModule.forRoot(),
    CdkTreeModule,
    MatTreeModule,
    MatProgressSpinnerModule,
    JoyrideModule.forChild()
  ],
  declarations: [
    TeacherLayoutComponent,
    CourseComponent,
    AttendanceListComponent,
    StudentsComponent,
    ExamScheduleComponent,
    ExamQualifyComponent,
    DialogInvitationComponent,
    DialogStudentAnswersComponent,
    InterestSiteComponent,
    TaskScheduleComponent,
    ActivityScheduleComponent,
    ResourcePlanningComponent,
    FinalScoreComponent,
    DialogFinalScoreComponent,
    DialogAddPartialComponent,
    HomeworkQualifyComponent,
    DialogStudenHomeworkComponent,
    ActivityQualifyComponent,
    DialogStudentActivityComponent
    ,DialogDownloadFinalScoreComponent //Ricardo - Agregdo
    ,DialogSendPdfComponent //Armand - Agregdo
  ],
  entryComponents: [
    DialogInvitationComponent,
    DialogStudentAnswersComponent,
    DialogFinalScoreComponent,
    DialogAddPartialComponent,
    DialogStudenHomeworkComponent,
    DialogStudentActivityComponent
    ,DialogDownloadFinalScoreComponent //Ricardo - Agregdo
    ,DialogSendPdfComponent //Armand - Agregdo
  ]
})
export class TeacherModule { }
