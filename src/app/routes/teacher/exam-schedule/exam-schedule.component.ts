import { Component, OnInit, ViewChild } from '@angular/core';
import { Route, ActivatedRoute } from '@angular/router';
import { Appsettings } from '../../../configuration/appsettings';

// COMPONENT SERVICES
import { AdminNavService } from '../../../components/layouts/administrator/admin-nav/admin-nav.service';
import { ExamSchedulesComponent } from '../../../components/administrator/exam-schedules/exam-schedules.component';

// MODELS
import { Group } from '../../../models/courses/group.model';


@Component({
  selector: 'app-exam-schedule',
  templateUrl: './exam-schedule.component.html',
  styleUrls: ['./exam-schedule.component.scss']
})
export class ExamScheduleComponent implements OnInit {

  data: any;
  group: Group;

  @ViewChild(ExamSchedulesComponent)
  public timerComponent: ExamSchedulesComponent;

  constructor( public _srvNavService: AdminNavService,
               private _route: ActivatedRoute ) {
               this.data = null;
  }

  ngOnInit() {

    this._route.params.subscribe( params => {

      this.data = {
        id: params.id
      };

      this.group = JSON.parse(localStorage.getItem('ra.group'));
      this.timerComponent.studentGroupId =  this.data.id;
      this.timerComponent.groupName = this.data.description;
      this.timerComponent.getExamSchedules();
      this._srvNavService.setTitle('Programación de exámenes: ' + this.group.description);
    });
  }

  onClickAddNew() {
    if (!this.timerComponent.showForm ) {
      this.timerComponent.onClickAdd();
    }
  }

}
