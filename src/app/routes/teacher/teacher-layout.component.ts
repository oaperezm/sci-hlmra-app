import { Component, OnInit, ViewEncapsulation, AfterViewInit, ViewChild, ElementRef } from '@angular/core';

import { NavService, StorageService, UserService } from '../../service/service.index';
import { AdminHeaderService } from '../../components/layouts/administrator/admin-header/admin-header.service';
import { MatSidenav } from '@angular/material';
import { AdminNavService } from '../../components/layouts/administrator/admin-nav/admin-nav.service';

declare function setMinHeightByElement(element): any;

@Component({
  selector: 'app-teacher-layout',
  templateUrl: './teacher-layout.component.html',
  styleUrls: ['./teacher-layout.component.scss']
})
export class TeacherLayoutComponent implements OnInit, AfterViewInit {

  @ViewChild('appDrawer') appDrawer: ElementRef;
  @ViewChild('sidenav') public sidenav: MatSidenav;

  constructor(  public navService: NavService,
                public _srvStorage: StorageService,
                private _srvUser: UserService,
                public _srvHeader: AdminHeaderService,
                public _srvAdminNav: AdminNavService ) {

    this._srvAdminNav.setTitle('Administración de cursos');
    this._srvStorage.showBackBotton = false;
  }

  ngOnInit() {
    this._srvAdminNav.setSidenav(this.sidenav);
  }

  ngAfterViewInit(): void {
    this.navService.appDrawer = this.appDrawer;
    setMinHeightByElement('lst-couses');
  }

signOut() {
  this._srvUser.logOut().subscribe(
    res => {
      this._srvStorage.logoutRedirect();
      if ( this._srvStorage.tokenMessage) {
      }
    },
    error => {
      this._srvStorage.logoutRedirect();
    }
  );
}

}
