import { Component, OnInit, ViewChild, OnDestroy} from '@angular/core';
import { Route, ActivatedRoute } from '@angular/router';

// Componentes
import { TasksScheduleComponent } from '../../../components/administrator/tasks-schedule/tasks-schedule.component';

// Servicios
import { AdminHeaderService } from '../../../components/layouts/administrator/admin-header/admin-header.service';
import { AdminNavService } from '../../../components/layouts/administrator/admin-nav/admin-nav.service';

// Modelos
import { Group} from '../../../models/courses/group.model';

import { Appsettings } from '../../../configuration/appsettings';

@Component({
  selector: 'app-task-schedule',
  templateUrl: './task-schedule.component.html',
  styleUrls: ['./task-schedule.component.scss']
})
export class TaskScheduleComponent implements OnInit, OnDestroy {

  @ViewChild(TasksScheduleComponent)
  private timerComponent: TasksScheduleComponent;

  group: Group;
  studenGroupId: number;
  totalHomerwork: number;
  constructor(  public _srvNavService: AdminNavService,
                private _srvHeader: AdminHeaderService,
                private _route: ActivatedRoute) {
    this.totalHomerwork = 0;
                }

  ngOnInit() {
    this._route.params.subscribe( params => {

      this.studenGroupId = params.studenGroupId;
      this.timerComponent.getTask( this.studenGroupId);
      this.group = JSON.parse(localStorage.getItem('ra.group'));
      this._srvNavService.setTitle('Programar tarea: ' + this.group.description);
    });
  }

  ngOnDestroy(): void {
    this._srvHeader.setBreadcrumb([]);
  }

  onClickAddNew() {
    if ( !this.timerComponent.showForm) {
      this.timerComponent.addTaskSchedule(  );
    }
  }

  totalHomework( event){
    this.totalHomerwork = event;
  }

}
