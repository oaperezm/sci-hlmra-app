import { Component, OnInit, OnDestroy } from '@angular/core';

import { AdminHeaderService } from '../../../components/layouts/administrator/admin-header/admin-header.service';

@Component({
  selector: 'app-course',
  templateUrl: './course.component.html',
  styleUrls: ['./course.component.scss']
})
export class CourseComponent implements OnInit, OnDestroy {

  constructor(  private _srvHeaderAdmin: AdminHeaderService ) { }

  ngOnInit() {
    this._srvHeaderAdmin.setUrlBackButton(['/resource/home', 'Inicio']);
  }

  ngOnDestroy(): void {
    this._srvHeaderAdmin.setUrlBackButton([]);
  }
}
