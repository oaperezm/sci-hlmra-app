import { Component, OnInit, OnDestroy } from '@angular/core';
import { MatDialog } from '@angular/material';
import { Appsettings } from '../../../configuration/appsettings';

// Servicios
import { FinalScoreService } from '../../../service/publishing/final-score.service';
import { AdminNavService } from '../../../components/layouts/administrator/admin-nav/admin-nav.service';
import { CalificacionFinalPipe } from '../../../core/pipes/finalScore-text.pipe';

// Dialogos
import { DialogFinalScoreComponent } from '../../../dialogs/dialog-final-score/dialog-final-score.component';
import { TeacherFinalScore } from '../../../models/courses/teacher-final-score.model';
import { Group } from 'src/app/models/courses/group.model';
import { SchedulingPlartial } from 'src/app/models/courses/schedulingPartial.model';
import swal from 'sweetalert2';
import { Route, Router, ActivatedRoute } from '@angular/router';

import { DialogDownloadFinalScoreComponent } from '../../../dialogs/dialog-final-score-download/dialog-final-score-download.component';  //Ricardo - Agregado
import { DialogSendPdfComponent } from '../../../dialogs/dialog-send-pdf/dialog-send-pdf.component'; //Armand - Agregado

@Component({
  selector: 'app-final-score',
  templateUrl: './final-score.component.html',
  styleUrls: ['./final-score.component.scss']
})
export class FinalScoreComponent implements OnInit {

  working: boolean;
  finalScore: TeacherFinalScore[] = [];
  partials: SchedulingPlartial[] = [];
  studentScoreStatus: boolean;
  studentGroupId: any;
  group: Group;
  readOnly: boolean;

  constructor(private _dialog: MatDialog,
              public _srvNavService: AdminNavService,
              private _routeActive: ActivatedRoute,
              private _srvFinalScore: FinalScoreService) {
  }

  ngOnInit() {
    this.readOnly = true;
    this.studentGroupId = this._routeActive.params;
    this._routeActive.params.subscribe(
      params => {
        this.group = JSON.parse(localStorage.getItem('ra.group'));
          this.getAllFinalScore(this.studentGroupId.value.studenGroupId);
          this._srvNavService.setTitle('Generar calificación: ' + this.group.description);
      }
  );
  }

  // Abre modal para editar calificaciones
  onEditScore(data) {
    const DIALOG_REF = this._dialog.open(DialogFinalScoreComponent, {
      width: '500px',
      height: '9750px',
      autoFocus: false,
      disableClose: true,
      data: data
    });

    DIALOG_REF.afterClosed().subscribe(response => {
    });
  }

  onEditHeader(data) {
    const DIALOG_REF = this._dialog.open(DialogFinalScoreComponent, {
      width: '500px',
      height: '750px',
      autoFocus: false,
      disableClose: true,
      data: data
    });

    DIALOG_REF.afterClosed().subscribe(response => {
      if (response === null){
        return;
      } else {
        this.getAllFinalScore(this.group.id);
      }
    });
  }

  getAllFinalScore(studentGroupId) {
    this.finalScore = [];
    this.working = true;
    this._srvFinalScore.getAll(studentGroupId).subscribe(result => {
      if (result.success) {
        this.partials = [];
        for (let s of result.data) {
          let score = new TeacherFinalScore();
          let partials = new SchedulingPlartial();

          score.id = s.id;
          score.studentGroupDescription = s.studentGroupDescription;
          score.studentId = s.studentId;
          score.studentName = s.studenName;
          score.userId = s.userId;
          score.partials = [];

           //Ricardo - Agregado - Inicio
           score.percentageActivity = s.percentageActivity
           score.percentageAssitance =s.percentageAssitance;
           score.percentageExam =s.percentageExam;
           score.percentageHomework =s.percentageHomework;
          //Ricardo - Agregado - Fin

          for (let p of s.studentScores) {
            partials = new SchedulingPlartial();
            partials.id = p.id;
            partials.description = p.partialDecription;
            partials.activity = p.activity;
            partials.assistance = p.assistance;
            partials.comment = p.comment;
            partials.exam = p.exam;
            partials.homework = p.homework;
            partials.partialperiod = p.partialPeriod;
            partials.points = p.points;
            partials.score = p.score;
            partials.studentGroupId = p.studentGroupId;
            partials.status = p.status;
            partials.schedulingPartialId = p.schedulingPartialId;

            score.partials.push(partials);
          }
          this.finalScore.push(score);
        }
      }
      this.working = false;
    });
  }


  onChangeStudentScoreStatus(event) {

    swal({
      title: '¿Estás seguro de cerrar el grupo?',
      text: 'Ya no podrá actualizar la calificación y se cerrará el el grupo',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: '¡Sí, cerrar grupo!',
      cancelButtonText: 'Cancelar'
    }).then((result) => {
      if (result.value) {
        // this.working = true;
        // this._srvFinalScore.updateGroup(this.group).subscribe(res => {
        //   if (res.success) {
        //     swal(Appsettings.APP_NAME, 'Examen actualizado correctamente', 'success');
        //   } else {
        //     swal(Appsettings.APP_NAME, res.message, 'error');
        //   }
        //   this.working = false;
        // }, err => {
        //   this.working = false;
        // });
      } else {
        this.studentScoreStatus = false;
      }
    });
  }

//Ricardo - Agregado - Inicio
onClickDownload() {
  const DIALOG_REF = this._dialog.open( DialogDownloadFinalScoreComponent, {
    width: '500px',
    height: '600px',
    autoFocus: false,
    disableClose: true,
    data: { studenGroupId: this.studentGroupId.value.studenGroupId}
    });
  
    DIALOG_REF.afterClosed().subscribe( response => {
    });
}
//Ricardo - Agregado - Fin
//Armand - Agregado - Inicio
//Abre modal
onClickSendPdf() {
  const DIALOG_REF = this._dialog.open( DialogSendPdfComponent, {
  width: '500px',
  height: '600px',
  autoFocus: false,
  disableClose: true,
  data: { groupId: this.studentGroupId.value.studenGroupId}
  });
 
  DIALOG_REF.afterClosed().subscribe( response => {
  });
}
//Armand - Agregado - Fin
}
