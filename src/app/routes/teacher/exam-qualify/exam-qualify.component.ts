import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { Appsettings } from '../../../configuration/appsettings';

// COMPONENT SERVICES
import { AdminNavService } from '../../../components/layouts/administrator/admin-nav/admin-nav.service';
import { AdminHeaderService } from '../../../components/layouts/administrator/admin-header/admin-header.service';


// SERVICES
import { GroupService } from '../../../service/service.index';

// MODELS
import { Studen } from '../../../models/courses/student.model';

@Component({
  selector: 'app-exam-qualify',
  templateUrl: './exam-qualify.component.html',
  styleUrls: ['./exam-qualify.component.scss']
})
export class ExamQualifyComponent implements OnInit {

  students: Studen[] = [];
  rating: number;
  examScheduleId: number;
  examScheduleTypeId: number;
  typeExam: number;
  groupId: number;
  description: string;
  working: boolean;
  endDate: Date;
  addScore: boolean;

  constructor(public _srvNavService: AdminNavService,
    private _srvGroup: GroupService,
    private _route: ActivatedRoute,
    private _srvHeader: AdminHeaderService) {


  }

  ngOnInit() {
    this._route.params.subscribe(params => {

      const group = JSON.parse(localStorage.getItem('ra.group'));
      const course = JSON.parse(localStorage.getItem('ra.course'));

      const name = group.description.replace(/\//g, '%2F');
      const urlBack = `/teacher/exam-schedule/${group.id}/${course.id}`;

      this._srvHeader.setUrlBackButton([urlBack, 'Exámenes']);
      const endDate = new Date( params.endDate);
      this.endDate = new Date(  endDate.getFullYear(), endDate.getMonth(), endDate.getDate(), 0, 0, 0 );

      let date = new Date();
      const dateNow = new Date(  date.getFullYear(), date.getMonth(), date.getDate(), 0, 0, 0 );

      this.addScore = ( dateNow >= endDate ) ? true : false;

      this.examScheduleId = params.examScheduleId;
      this.typeExam = params.typeExam;
      this.groupId = params.groupId;
      this.description = params.description;
      this.examScheduleTypeId = params.statusId;

      this._srvNavService.setTitle('Calificar examen: ' + this.description);
      this._srvNavService.setUrlBackButton([urlBack, 'Exámenes']);

      this.getStudents();
    });
  }

  // Obtener los estudiantes del grupo y programación de examenes
  getStudents() {

    this.students = [];
    this.working = true;
    this._srvGroup.getStudenByCourse(this.groupId, this.examScheduleId).subscribe(result => {
      if (result.success) {
        const students = result.data.filter(item => item.checkDate !== null);
        for (const e of students) {
          const student = new Studen();
          student.id = e.userId;
          student.fullName = e.userName;
          student.rating = e.score.toString() == '' ? e.totalAnswerScore : e.score;
          student.commentary = e.comments;
          student.totalCorrect = e.totalCorrect;
          student.totalQuestions = e.totalQuestions;
          student.testId = e.testId;
          student.isAnswered = e.isAnswered;
          student.maximumExamScore = e.maximumExamScore;
          student.totalAnswerScore = e.totalAnswerScore;
          student.weightingId = e.weightingId;
          student.weightingDescription = e.weightingDescription;
          student.weightingUnit = e.weightingUnit;

          this.students.push(student);
        }
      }
      this.working = false;
    }, err => {
      this.working = false;
    });
  }

}
