import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

// Servicios
import { ActivityService } from '../../../service/service.index';
import { AdminHeaderService } from '../../../components/layouts/administrator/admin-header/admin-header.service';

// Modelos
import { StudenActivity } from '../../../models/courses/studentActivity.model';


@Component({
  selector: 'app-activity-qualify',
  templateUrl: './activity-qualify.component.html',
  styleUrls: ['./activity-qualify.component.scss']
})
export class ActivityQualifyComponent implements OnInit, OnDestroy {

  acitivityId: number;
  studenGroupId: number;
  students: StudenActivity[] = [];
  statusActivity: boolean;
  activityName: string;
  activityDescription: string;
  homeworkTypeScore: boolean;
  endDate: Date;
  addScore: boolean;
  constructor(  private _route: ActivatedRoute,
                private _srvActivities: ActivityService,
                private _srvHeader: AdminHeaderService) {
    this._route.params.subscribe( params => {

      const endDate = new Date( params.endDate);
      this.endDate = new Date(  endDate.getFullYear(), endDate.getMonth(), endDate.getDate(), 0, 0, 0 );

      let date = new Date();
      const dateNow = new Date(  date.getFullYear(), date.getMonth(), date.getDate(), 0, 0, 0 );

      this.addScore = ( dateNow >= this.endDate ) ? true : false;

      this.studenGroupId = params.groupId;
      this.acitivityId = params.examScheduleId;
      this.getAllActivities( this.studenGroupId, this.acitivityId );
      let urlBack = `/teacher/activity-schedule/${ this.studenGroupId }`;
      this._srvHeader.setUrlBackButton([urlBack, 'Actividades']);
    });
  }

  ngOnInit() {
  }


  ngOnDestroy(): void {
    this._srvHeader.setUrlBackButton([]);
  }

    // Obtiene el listado de alumnos y sus tareas
    getAllActivities(studenGroupId: number, acitivityId: number ) {
      this._srvActivities.getAllActivities(studenGroupId, acitivityId).subscribe( result => {

        if (result.success) {

          this.statusActivity = result.data.activity.qualified;
          this.activityName = result.data.activity.name;
          this.activityDescription = result.data.activity.description;
          this.homeworkTypeScore = result.data.activity.typeScore;

          for ( let s of result.data.students) {
            let status = false;
            if ( s.files ) {
              status = true;
            }
            let student = new StudenActivity();
            student.id = s.id;
            student.comment = s.comment;
            student.fullName = s.fullName;
            student.hasActivity = s.hasHomwRowk;
            student.activityId = s.activityId;
            student.score = s.score;
            student.files = s.files;
            student.userId = s.userId;
            student.statusFiles = status;
            student.delivered = s.delivered;
            student.addNewAnswers = s.addNewAnswers;
            student.typeScore = result.data.activity.typeScore;
            this.students.push( student );
          }

        }
      });
    }
}
