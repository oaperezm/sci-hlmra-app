import { Component, OnInit, Input } from '@angular/core';
import { MatDialog } from '@angular/material';

// Modelos
import { StudenActivity } from '../../../models/courses/studentActivity.model';


// Servicios
import { ActivityService } from '../../../service/service.index';

// Dialogs
import { DialogStudentActivityComponent } from '../../../dialogs/dialog-student-activity/dialog-student-activity.component';

import swal from 'sweetalert2';
import { Appsettings } from '../../../configuration/appsettings';

@Component({
  selector: 'app-activities-ratings',
  templateUrl: './activities-ratings.component.html',
  styleUrls: ['./activities-ratings.component.scss']
})
export class ActivitiesRatingsComponent implements OnInit {

  working: boolean;
  readOnly: boolean;
  examScheduleStatus: boolean;
  studentsHomework: StudenActivity[] = [];
  homeworkId: number;
  statusHomework: boolean;
  homeworkName: string;
  homeworkDescription: string;
  homeworkTypeScore: boolean;
  addScore: boolean;
  // Obtiene el listado de los alumnos
  @Input('students') set student ( value) {

    this.studentsHomework = value;
  }
  // Obtiene el listado de los alumnos
  @Input('addScore') set addScoreActivitie ( value) {

    this.addScore = value;
  }
  // Obtiene el id de la tarea
  @Input('homeworkId') set idHomework ( value ) {
    this.homeworkId = value;
  }
  // Obtiene el status de la tarea
  @Input('statusHomework') set homeworkStatus ( value ) {
    this.readOnly = value;
    this.examScheduleStatus = value;
  }
  // Obtiene el nombre de la tarea
  @Input('homeworkName') set nameHomework ( value ) {
    this.homeworkName = value;
  }
  // Obtiene la descripcion de la tarea
  @Input('homeworkDescription') set descriptionHomework ( value ) {
    this.homeworkDescription = value;
  }

   // Obtiene la descripcion de la tarea
   @Input('homeworkTypeScore') set typeScore ( value ) {
    this.homeworkTypeScore = value;
  }

  constructor( private _srvActivity: ActivityService,
    private _dialog: MatDialog ) { }

  ngOnInit() {
  }

   // Actualiza el estatus de la tarea
   onChangeExamScheduleStatus( event ) {
    this.working = true;
    this.readOnly = event;
    let status = {
      'activityId': this.homeworkId,
      'status': event
    };

    this._srvActivity.updateStatusHomwork( status ).subscribe( result => {
      if ( result.success ) {
        swal( Appsettings.APP_NAME, result.message, 'success');
        this.working = false;
      } else {
        swal( Appsettings.APP_NAME, result.message, 'error');
        this.working = false;
      }
      this.working = false;
    });
  }

  // Guarda las calificacones de un grupo
  sendData() {
    this.working = true;
    let homework = [];
    let studentsHomework = {};
      for (let h of this.studentsHomework ) {
        homework.push({
          'activityAnswerId': h.id,
          'userId': h.userId,
          'score': h.score,
          'comment': h.comment,
          'delivered': h.delivered,
          'addNewAnswers': h.addNewAnswers
        });
      }

      studentsHomework = {
        'activityId': this.homeworkId,
        'scores': homework
      };

    this._srvActivity.saveQualifyGroup(studentsHomework).subscribe( result => {
      if ( result.success ) {
        swal(Appsettings.APP_NAME, result.message, 'success');
        this.working = false;
      } else {
        swal(Appsettings.APP_NAME, result.message, 'error');
        this.working = false;
      }
    });
  }

  // Muestra modal archivos de tarea
  onClickShowFiles( item ) {
    const DIALOG_REF = this._dialog.open(DialogStudentActivityComponent, {
      width: '600px',
      height: '600px',
      autoFocus: false,
      disableClose: true,
      data: { files: item, homeworkName: this.homeworkName, homeworkDescription: this.homeworkDescription }
    });

    DIALOG_REF.afterClosed().subscribe(response => {
      // console.log( response );
    });
  }

}
