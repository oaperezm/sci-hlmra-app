import { Component, OnInit, ViewChild, Input, EventEmitter, Output } from '@angular/core';
import { FormGroup, FormControl, Validators, ValidatorFn, ValidationErrors } from '@angular/forms';
import { Router } from '@angular/router';
import { Appsettings } from '../../../configuration/appsettings';

// SERVICES
import { ExamScheduleService, TeacherExamService } from '../../../service/service.index';

// MODELS
import { ExamSchedule } from '../../../models/courses/exam-schedule.model';
import { ExamScheduleType } from '../../../models/courses/exam-schedule-type.model';
import { TeacherExam } from '../../../models/courses/teacher-exam.model';

import swal, { SweetAlertType } from 'sweetalert2';
import { Course } from '../../../models/catalogs/course.model';

@Component({
  selector: 'app-exam-schedules',
  templateUrl: './exam-schedules.component.html',
  styleUrls: ['./exam-schedules.component.scss'],
})
export class ExamSchedulesComponent implements OnInit {

  working: boolean = false;
  studentGroupId: number;
  groupName: string;
  examSchedules: ExamSchedule[];
  frmExamSchedule: FormGroup;
  showForm: boolean = false;
  teacherExams: TeacherExam[] = [];
  course: Course;
  currentSchedule: ExamSchedule;
  schedulingPartial: any[];

  dateInit: Date;
  dateEnd: Date;

  @ViewChild('formExamSchedule') formExamSchedule;
  @Input() group: any;
  @Input() showButton: boolean = false;

  // Retornamos el arreglo de id's
  @Output() closeModal: EventEmitter<any[]> = new EventEmitter<any[]>();
  constructor(private _srvExam: ExamScheduleService,
    private _router: Router,
    private _srvTeacherExam: TeacherExamService) {


    this.groupName = '..';

    this.frmExamSchedule = new FormGroup({
      id: new FormControl(0),
      description: new FormControl('', [Validators.required, Validators.maxLength(350)]),
      beginApplicationDate: new FormControl('', [Validators.required, Validators.maxLength(10)]),
      endApplicationDate: new FormControl('', [Validators.required, Validators.maxLength(10)]),
      checkedHorario: new FormControl(false),
      checkedDuration: new FormControl(false),
      beginHH: new FormControl('', [Validators.required, Validators.min(0), Validators.max(23), Validators.pattern('[0-9]*')]),
      beginMI: new FormControl('', [Validators.min(0), Validators.max(59), Validators.pattern('[0-9]*')]),
      beginSS: new FormControl('', [Validators.min(0), Validators.max(59), Validators.pattern('[0-9]*')]),
      endHH: new FormControl('', [Validators.required, Validators.min(0), Validators.max(23), Validators.pattern('[0-9]*')]),
      endMI: new FormControl('', [Validators.min(0), Validators.max(59), Validators.pattern('[0-9]*')]),
      endSS: new FormControl('', [Validators.min(0), Validators.max(59), Validators.pattern('[0-9]*')]),
      partialId: new FormControl('', [Validators.required]),
      minutesExam: new FormControl('', [Validators.required, Validators.min(1), Validators.max(999), Validators.pattern('[0-9]*')]),
      teacheExamId: new FormControl(0)
    }, { validators: [this.applicationDateValidator, this.applicationTimeValidator] });
  }

  applicationDateValidator: ValidatorFn = (control: FormGroup): ValidationErrors | null => {
    const sBeginDate = control.get('beginApplicationDate').value;
    const sEndDate = control.get('endApplicationDate').value;
    const now = new Date();
    now.setHours(0, 0, 0, 0);
    let result = null;
    try {
      let beginDate = new Date(sBeginDate);
      let endDate = new Date(sEndDate);
      if (beginDate.getTime() < now.getTime())
        result = { 'applicationDateLessToday': true }
      if (beginDate.getTime() > endDate.getTime())
        result = { 'applicationDateRange': true }
    }
    catch {
      result = { 'applicationDateRange': true }
    }
    return result;
  };

  applicationTimeValidator: ValidatorFn = (control: FormGroup): ValidationErrors | null => {
    let beginTime = new Date();
    let endTime = new Date();
    if (control.get('checkedHorario').value) {
      if (control.get('beginHH').valid
        && control.get('beginMI').valid
        && control.get('beginSS').valid
        && control.get('endHH').valid
        && control.get('endMI').valid
        && control.get('endSS').valid) {

        beginTime.setHours(control.get('beginHH').value, 0, 0, 0);
        if (control.get('beginMI').value != '')
          beginTime.setMinutes(control.get('beginMI').value);
        if (control.get('beginSS').value != '')
          beginTime.setSeconds(control.get('beginSS').value);

        endTime.setHours(control.get('endHH').value, 0, 0, 0);
        if (control.get('endMI').value != '')
          endTime.setMinutes(control.get('endMI').value);
        if (control.get('endSS').value != '')
          endTime.setSeconds(control.get('endSS').value);

        if (beginTime.getTime() >= endTime.getTime()) {
          return { 'applicationTimeRange': true }
        }
      }
    }
    return null
  };

  onClickCheckedHorario(event) {
    event.preventDefault();
    this.setActiveHoraro(!this.frmExamSchedule.controls['checkedHorario'].value);
    this.frmExamSchedule.controls['checkedHorario'].setValue(!this.frmExamSchedule.controls['checkedHorario'].value);
  }

  setActiveHoraro(enable: boolean) {
    const frm = this.frmExamSchedule.controls;
    if (enable) {
      frm['beginHH'].enable();
      frm['beginMI'].enable();
      frm['beginSS'].enable();
      frm['endHH'].enable();
      frm['endMI'].enable();
      frm['endSS'].enable();
      this.getCurrentValueTime();
    }
    else {
      if (frm['beginHH'].valid)
        this.currentSchedule.beginApplicationTime.setHours(frm['beginHH'].value);
      if (frm['beginMI'].valid && frm['beginMI'].value != '')
        this.currentSchedule.beginApplicationTime.setMinutes(frm['beginMI'].value);
      if (frm['beginSS'].valid && frm['beginSS'].value != '')
        this.currentSchedule.beginApplicationTime.setSeconds(frm['beginSS'].value);
      frm['beginHH'].setValue('');
      frm['beginMI'].setValue('');
      frm['beginSS'].setValue('');
      frm['beginHH'].disable();
      frm['beginMI'].disable();
      frm['beginSS'].disable();
      frm['beginHH'].markAsUntouched();
      frm['beginMI'].markAsUntouched();
      frm['beginSS'].markAsUntouched();
      if (frm['endHH'].valid)
        this.currentSchedule.endApplicationTime.setHours(frm['endHH'].value);
      if (frm['endMI'].valid && frm['endMI'].value != '')
        this.currentSchedule.endApplicationTime.setMinutes(frm['endMI'].value);
      if (frm['endSS'].valid && frm['endSS'].value != '')
        this.currentSchedule.endApplicationTime.setSeconds(frm['endSS'].value);
      frm['endHH'].setValue('');
      frm['endMI'].setValue('');
      frm['endSS'].setValue('');
      frm['endHH'].disable();
      frm['endMI'].disable();
      frm['endSS'].disable();
      frm['endHH'].markAsUntouched();
      frm['endMI'].markAsUntouched();
      frm['endSS'].markAsUntouched();
    }
  }

  getErrorsBeginTime() {
    const frm = this.frmExamSchedule.controls;
    if (frm['checkedHorario'].value) {
      if (frm['beginHH'].hasError('required') && frm['beginHH'].touched)
        return 'El campo horario de inicio es requerido.'
      else if ((!frm['beginHH'].valid && frm['beginHH'].touched) || !frm['beginMI'].valid || !frm['beginSS'].valid)
        return 'La hora de inicio no es valida'
    }
    return '';
  }

  getErrorsEndTime() {
    const frm = this.frmExamSchedule.controls;
    if (frm['checkedHorario'].value) {

      if (frm['endHH'].hasError('required') && frm['endHH'].touched)
        return 'El campo horario de fin es requerido.'
      else if ((!frm['endMI'].valid && frm['endMI'].touched) || !frm['endSS'].valid || !frm['endSS'].valid)
        return 'La hora de fin no es valida.'
    }
    return '';
  }

  onClickCheckedDuration(event) {
    event.preventDefault();
    this.setActiveDuration(!this.frmExamSchedule.controls['checkedDuration'].value);
    this.frmExamSchedule.controls['checkedDuration'].setValue(!this.frmExamSchedule.controls['checkedDuration'].value);

  }

  setActiveDuration(enable: boolean) {
    const frm = this.frmExamSchedule.controls;
    if (enable) {
      frm['minutesExam'].enable();
      this.setCurrentValueDuration();
    }
    else {
      if (frm['minutesExam'].valid)
        this.currentSchedule.minutesExam = frm['minutesExam'].value;
      frm['minutesExam'].setValue('');
      frm['minutesExam'].disable();
      frm['minutesExam'].markAsUntouched();
    }
  }

  getErrorsDuration() {
    if (this.frmExamSchedule.controls['checkedDuration'].value) {
      return (
        (this.frmExamSchedule.controls['minutesExam'].hasError('required')
          && this.frmExamSchedule.controls['minutesExam'].touched)
          ?
          'El campo duración es requerido.'
          :
          (!this.frmExamSchedule.controls['minutesExam'].valid
            && this.frmExamSchedule.controls['minutesExam'].touched)
            ?
            'El campo duración no es valido.'
            : ''
      )
    }
    return ''
  }

  ngOnInit() {
    this.studentGroupId = this.group.id;
    this.groupName = this.group.description;
    this.course = JSON.parse(localStorage.getItem('ra.course'));
    this.dateInit = this.course.startDate;
    this.dateEnd = this.course.endDate;
  }

  // Evento para enviar los datos al servicio y guardar o actualizar
  // los datos de la programación
  onSubmit(): void {
    const frm = this.frmExamSchedule.controls;
    const exam = new ExamSchedule();

    exam.active = true;
    exam.id = frm['id'].value;
    exam.beginApplicationDate = new Date(frm['beginApplicationDate'].value);
    exam.endApplicationDate = new Date(frm['endApplicationDate'].value);

    if (frm['checkedHorario'].value) {
      exam.beginApplicationTime = new Date(exam.beginApplicationDate);
      exam.beginApplicationTime.setHours(frm['beginHH'].value, 0, 0);
      if (frm['beginMI'].value != '')
        exam.beginApplicationTime.setMinutes(frm['beginMI'].value);
      if (frm['beginSS'].value != '')
        exam.beginApplicationTime.setSeconds(frm['beginSS'].value);
      exam.endApplicationTime = new Date(exam.endApplicationDate);
      exam.endApplicationTime.setHours(frm['endHH'].value, 0, 0);
      if (frm['endMI'].value != '')
        exam.endApplicationTime.setMinutes(frm['endMI'].value);
      if (frm['endSS'].value != '')
        exam.endApplicationTime.setSeconds(frm['endSS'].value);
    }
    else {
      exam.beginApplicationTime = null;
      exam.endApplicationTime = null;
    }

    exam.schedulingPartialId = frm['partialId'].value;
    if (exam.schedulingPartialId > 0 && this.schedulingPartial.length > 0) {
      let sp = this.schedulingPartial.filter(x => x.id == exam.schedulingPartialId)[0];
      exam.schedulingPartialDescription = sp.description;
    }
    if (frm['checkedDuration'].value)
      exam.minutesExam = frm['minutesExam'].value;
    else
      exam.minutesExam = null;
    exam.description = frm['description'].value;
    exam.studentGroupId = this.studentGroupId;
    exam.teacherExamId = frm['teacheExamId'].value === 0 ? null : frm['teacheExamId'].value;
    if (exam.id > 0) {
      this.update(exam);
    } else {
      this.save(exam);
    }

  }

  // Evento para mostrar el formulario y agregar una nueva programación o
  // actualizar los datos de una existente
  onClickAdd(): void {
    this.frmExamSchedule.reset();
    this.frmExamSchedule.controls['partialId'].setValue('');
    this.currentSchedule = new ExamSchedule();
    let dt = new Date();
    // this.frmExamSchedule.controls['beginApplicationDate'].setValue(new Date(
    //   dt.getFullYear(),
    //   dt.getMonth(),
    //   dt.getDate(),
    //   0, 0, 0, 0));
    const course = JSON.parse(localStorage.getItem('ra.course'));
    if (course) {
      //const ci = new Date(new Date(course.startDate).toDateString());
      const ced = new Date(new Date(course.endDate).toDateString());
      // this.frmExamSchedule.controls['endApplicationDate'].setValue(new Date(
      //   ced.getFullYear(),
      //   ced.getMonth(),
      //   ced.getDate(),
      //   0, 0, 0, 0));
    }
    this.currentSchedule.beginApplicationTime = new Date();
    this.currentSchedule.beginApplicationTime.setMinutes(0, 0, 0);
    this.currentSchedule.endApplicationTime = new Date();
    this.currentSchedule.endApplicationTime.setMinutes(0, 0, 0);
    this.setActiveHoraro(false);
    this.setActiveDuration(false);
    this.showForm = true;
  }

  // Eliminar una programación de exámen existente
  onClickDelete(exam: ExamSchedule) {

    swal({
      title: '¿Estás seguro?',
      text: '¡Esta operación no se podrá revertir!',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: '¡Si, eliminar ahora!',
      cancelButtonText: 'Cancelar'
    }).then((result) => {

      if (result.value) {

        this.working = true;
        this._srvExam.remove(exam.id).subscribe(res => {
          let swalType: SweetAlertType = 'error';

          if (res.success) {
            this.examSchedules = this.examSchedules.filter(x => x.id !== exam.id);
            swalType = 'success';
          }
          this.working = false;
          swal(Appsettings.APP_NAME, res.message, swalType);
        }, err => {
          this.working = false;
        });
      }
    });
  }

  // Editar los datos de una programación de exámenes
  onClickEdit(exam: ExamSchedule) {
    const frm = this.frmExamSchedule.controls;
    this.frmExamSchedule.reset();
    this.showForm = true;

    this.currentSchedule = new ExamSchedule();
    this.currentSchedule.id = exam.id;
    this.currentSchedule.description = exam.description;
    this.currentSchedule.beginApplicationDate = new Date(
      exam.beginApplicationDate.getFullYear(),
      exam.beginApplicationDate.getMonth(),
      exam.beginApplicationDate.getDate(),
      0, 0, 0);
    this.currentSchedule.endApplicationDate = new Date(
      exam.endApplicationDate.getFullYear(),
      exam.endApplicationDate.getMonth(),
      exam.endApplicationDate.getDate(),
      0, 0, 0);
    this.currentSchedule.schedulingPartialId = exam.schedulingPartialId;
    this.currentSchedule.teacherExamId = exam.teacherExamId;
    this.currentSchedule.minutesExam = exam.minutesExam;
    if (exam.beginApplicationTime != null)
      this.currentSchedule.beginApplicationTime = new Date(exam.beginApplicationTime);
    else
      this.currentSchedule.beginApplicationTime = null;
    if (exam.endApplicationTime != null)
      this.currentSchedule.endApplicationTime = new Date(exam.endApplicationTime);
    else
      this.currentSchedule.endApplicationTime = null;

    frm['checkedHorario'].setValue(this.currentSchedule.beginApplicationTime != null)
    frm['id'].setValue(this.currentSchedule.id);
    frm['description'].setValue(this.currentSchedule.description);
    frm['beginApplicationDate'].setValue(this.currentSchedule.beginApplicationDate);
    frm['beginApplicationDate'].markAsTouched();

    frm['endApplicationDate'].setValue(this.currentSchedule.endApplicationDate);
    frm['endApplicationDate'].markAsTouched();
    if (this.currentSchedule.schedulingPartialId != null && this.currentSchedule.schedulingPartialId != 0)
      frm['partialId'].setValue(this.currentSchedule.schedulingPartialId.toString());
    else
      frm['partialId'].setValue('');
    frm['partialId'].markAsTouched();
    frm['teacheExamId'].setValue(this.currentSchedule.teacherExamId);
    this.setCurrentValueDuration();
    frm['checkedDuration'].setValue(this.currentSchedule.minutesExam != null);
    this.getCurrentValueTime();
    if (this.currentSchedule.beginApplicationTime == null) {
      this.currentSchedule.beginApplicationTime = new Date()
      this.currentSchedule.beginApplicationTime.setMinutes(0, 0, 0);
    }
    if (this.currentSchedule.endApplicationTime == null) {
      this.currentSchedule.endApplicationTime = new Date()
      this.currentSchedule.endApplicationTime.setMinutes(0, 0, 0);
    }
    this.setActiveHoraro(frm['checkedHorario'].value);
    this.setActiveDuration(frm['checkedDuration'].value);
  }

  setCurrentValueDuration() {
    this.frmExamSchedule.controls['minutesExam'].setValue(this.currentSchedule.minutesExam);
  }

  getCurrentValueTime() {
    let fecha = this.currentSchedule.beginApplicationTime;
    if (fecha != null) {
      this.frmExamSchedule.controls['beginHH'].setValue(fecha.getHours());
      this.frmExamSchedule.controls['beginMI'].setValue(fecha.getMinutes());
      this.frmExamSchedule.controls['beginSS'].setValue(fecha.getSeconds());
    }
    else {
      this.frmExamSchedule.controls['beginHH'].setValue('');
      this.frmExamSchedule.controls['beginMI'].setValue('');
      this.frmExamSchedule.controls['beginSS'].setValue('');
    }
    fecha = this.currentSchedule.endApplicationTime;
    if (fecha != null) {
      this.frmExamSchedule.controls['endHH'].setValue(fecha.getHours());
      this.frmExamSchedule.controls['endMI'].setValue(fecha.getMinutes());
      this.frmExamSchedule.controls['endSS'].setValue(fecha.getSeconds());
    }
    else {
      this.frmExamSchedule.controls['endHH'].setValue('');
      this.frmExamSchedule.controls['endMI'].setValue('');
      this.frmExamSchedule.controls['endSS'].setValue('');
    }
  }

  // Evento que cancela la actualización de datos en el formulario
  onClickCancel(): void {
    this.showForm = false;
    this.frmExamSchedule.reset();
  }

  // Guardar nuevo registro de programación de exámenes
  save(exam: ExamSchedule): void {

    this.working = true;
    this._srvExam.save(exam).subscribe(res => {

      let swalType: SweetAlertType = 'error';

      if (res.success) {

        if (!this.examSchedules) {
          this.examSchedules = [];
        }

        exam.id = res.data;
        exam.status = this.getExamSCheduleStaus(exam.teacherExamId);

        this.examSchedules.push(exam);
        this.resetForm();
        swalType = 'success';
      }

      swal(Appsettings.APP_NAME, res.message, swalType);

      this.working = false;
    }, err => {
      this.working = false;
    });

  }

  // Guardar nuevo registro de programación de exámenes
  update(exam: ExamSchedule): void {

    this.working = true;
    this._srvExam.update(exam).subscribe(res => {

      let swalType: SweetAlertType = 'error';

      if (res.success) {
        let selectedExam = this.examSchedules.filter(x => x.id === exam.id)[0];

        selectedExam.beginApplicationDate = exam.beginApplicationDate;
        selectedExam.endApplicationDate = exam.endApplicationDate;
        selectedExam.beginApplicationTime = exam.beginApplicationTime;
        selectedExam.endApplicationTime = exam.endApplicationTime;
        selectedExam.schedulingPartialId = exam.schedulingPartialId;
        selectedExam.schedulingPartialDescription = exam.schedulingPartialDescription;
        selectedExam.minutesExam = exam.minutesExam;
        selectedExam.description = exam.description;
        selectedExam.status = this.getExamSCheduleStaus(exam.teacherExamId);
        selectedExam.teacherExamId = exam.teacherExamId;

        this.resetForm();
        swalType = 'success';
      }
      swal(Appsettings.APP_NAME, res.message, swalType);

      this.working = false;
    }, err => {
      this.working = false;
    });
  }

  // Reiniciar el formulario de captura de programación de exámenes
  resetForm() {
    this.frmExamSchedule.reset();
    this.formExamSchedule.resetForm();
    this.showForm = false;
  }

  // Obtener todas las programaciones de exámes del grupo
  getExamSchedules() {
    this.working = true;
    this.examSchedules = undefined;
    this.getSchedulingPartial();
    this._srvExam.getAllByGroup(this.studentGroupId).subscribe(res => {
      if (res.success) {

        if (res.data.length > 0) {
          this.examSchedules = [];

          for (let exam of res.data) {
            let prom = new ExamSchedule();
            prom.id = exam.id;
            prom.active = exam.active;
            prom.description = exam.description;
            prom.teacherExamId = exam.teacherExamId;
            prom.status = new ExamScheduleType(exam.examScheduleType.id, exam.examScheduleType.description);
            prom.typeExam = exam.teacherExam.teacherExamTypeId;
            prom.schedulingPartialDescription = exam.schedulingPartial.description;
            prom.schedulingPartialId = exam.schedulingPartialId;
            prom.beginApplicationDate = new Date(exam.beginApplicationDate);
            prom.endApplicationDate = new Date(exam.endApplicationDate);
            if (exam.beginApplicationTime != null)
              prom.beginApplicationTime = new Date(exam.beginApplicationTime);
            else
              prom.beginApplicationTime = null;
            if (exam.endApplicationTime != null)
              prom.endApplicationTime = new Date(exam.endApplicationTime);
            else
              prom.endApplicationTime = null;
            prom.minutesExam = exam.minutesExam;
            prom.ponderacion = exam.teacherExam.weighting.description || '-';
            this.examSchedules.push(prom);
          }
        }
        this.working = false;

      }
      this.getTeacherExams();

    }, err => {
      this.working = false;
    });
  }

  // Aber el modal para capturar calificaciones
  addRating(exam: ExamSchedule) {
    let name = exam.description.replace(/\//g, '%2F');
    let dateInit = new Date(exam.beginApplicationDate);
    let month = dateInit.getMonth() + 1;
    let newDate =  month  +'-'+  dateInit.getDate()  + '-'+ dateInit.getFullYear();
    this._router.navigateByUrl(`/teacher/qualify/true/${this.studentGroupId}/${exam.id}/${exam.typeExam}/${name}/${exam.status.id}/${newDate}`);
  }

  getTeacherExams(): void {
    this.working = true;
    this.teacherExams = [];
    this._srvTeacherExam.getByUserShortInfo(0).subscribe(res => {

      if (res.success) {

        for (let t of res.data) {
          const exam = new TeacherExam();
          exam.id = t.id;
          exam.active = t.active;
          exam.description = t.description;
          exam.questions = t.teacherExamQuestion;
          exam.totalQuestions = t.totalQuestions;
          exam.teacherExamType = t.teacherExamTypes;
          this.teacherExams.push(exam);
         }

      }
      this.working = false;
    }, err => {
      this.working = false;
      swal(Appsettings.APP_NAME, 'Problemas al conectarnos con el servidor', 'warning');
    });
  }

  getSchedulingPartial(): void {
    this.working = true;
    this._srvExam.getSchedulingpartial(this.studentGroupId).subscribe(res => {
      if (res.success) {
        this.schedulingPartial = [];
        for (let t of res.data) {
          this.schedulingPartial.push({
            id: t.id.toString(),
            description: t.description,
            startDate: t.startDate,
            endDate: t.endDate,
            finalPartial: t.finalPartial,
          });
        }
        this.working = false;
      }
    }, err => {
      this.working = false;
    });
  }

  getExamSCheduleStaus(teacherExamId: number): ExamScheduleType {

    let status = new ExamScheduleType(1, 'Programado');

    if (teacherExamId > 0) {
      status = new ExamScheduleType(2, 'Asociado');
    }
    return status;
  }

  // Validar rango de fechas
  rangeDateValidator(control: FormControl): { [s: string]: boolean } {
    const forma: any = this;
    const course = JSON.parse(localStorage.getItem('ra.course'));
    const scheduleDate = new Date(control.value);

    if (course) {

      const courseInit = new Date(new Date(course.startDate).toDateString());
      const courseEnd = new Date(new Date(course.endDate).toDateString());
      if (!(courseInit.getTime() <= scheduleDate.getTime() && scheduleDate.getTime() <= courseEnd.getTime())) {
        return {
          rangeDateValidator: true
        };
      }
    }
    return null;
  }

  onChangePartial( partialId ) {
    let partial = this.schedulingPartial.filter( x => x.id === partialId  )[0];

    this.dateInit = partial.startDate;
    this.dateEnd = partial.endDate;
  }
}
