import { Component, OnInit, ViewEncapsulation, AfterViewInit } from '@angular/core';

import { Router, ActivatedRoute } from '@angular/router';
import { Group } from '../../../models/courses/group.model';
import { Course } from '../../../models/catalogs/course.model';
import { MatDialog } from '@angular/material';

// Dialogs
import { DialogGroupComponent } from '../../../dialogs/dialog-group/dialog-group.component';
import { DialogCourseComponent } from '../../../dialogs/dialog-course/dialog-course.component';

import { CourseService, GroupService, StorageService } from '../../../service/service.index';
import swal from 'sweetalert2';
import { Appsettings } from '../../../configuration/appsettings';
import { Observable, Subscription, fromEvent } from 'rxjs';

declare function setFullHeightByElement(selector): any;
import { JoyrideService } from 'ngx-joyride';

@Component({
  selector: 'app-group-nav',
  templateUrl: './group-nav.component.html',
  styleUrls: ['./group-nav.component.scss']
})
export class GroupNavComponent implements OnInit, AfterViewInit {

  working: boolean = false;
  courses: Course[] = [];
  contentSection: any[] = [];

  resizeObservable$: Observable<Event>;
  resizeSubscription$: Subscription;

  showDetail: boolean;

  constructor(  private _dialog: MatDialog,
                private _router: Router,
                private _srvCourse: CourseService,
                private _srvGroup: GroupService,
                private _route: ActivatedRoute,
                private joyride: JoyrideService,
                public _srvStorage: StorageService) {

                  this.showDetail =  true;
                }

  ngOnInit() {
    this.getCourses();

    setFullHeightByElement('.lst-couses');
    this.resizeObservable$ = fromEvent(window, 'resize');

    this.resizeSubscription$ = this.resizeObservable$.subscribe( evt => {
      setFullHeightByElement('.lst-couses');
    });

    if ( this._srvStorage.showTourCourse === 0) {
      this._srvStorage.showTourCourse = 1;
      this.joyride.startTour(
        { steps: ['addCourse'],
          customTexts: {
            next: '>>',
            prev: '<<',
            done: 'Cerrar'
          }
        }
      )
    }

  }

  ngAfterViewInit(): void {
  }

  /*********** GROUP ************/

  onCreateGroup( course: Course ) {

    let group = new Group( 0, '', 0, course.id,'');

    const DIALOG_REF = this._dialog.open( DialogGroupComponent, {
      width: '500px',
      height: '350px',
      autoFocus: false,
      disableClose: true,
      data: { courseName: 'Grupo', group: group}
    });

    DIALOG_REF.afterClosed().subscribe( res => {
      if ( res ) {
        swal(Appsettings.APP_NAME,`Recuerda proporcionarle los siguientes datos a los alumnos para que puedan inscribirse al grupo. Código del grupo ${res.id} y Contraseña del grupo: ${res.code}`, 'success');
        course.groups.push(new Group( res.id ,res.description, res.status , res.courseId, res.code ));
      }
    });
  }

  onEditGroup( group: Group ) {
    const DIALOG_REF = this._dialog.open( DialogGroupComponent, {
      width: '500px',
      height: '350px',
      autoFocus: false,
      disableClose: true,
      data: { courseName: 'Grupo', group: group}
    });

    DIALOG_REF.afterClosed().subscribe( res => {
      if ( res ) {
        group.description = res.description;
        this.getCourses();
      }
    });
  }

  onDeleteGroup( course: Course, group: Group ) {

    swal({
      title: '¿Estás seguro de eliminar el grupo?',
      text: '¡Esta operación no se podrá revertir!',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: '¡Sí, eliminar ahora!',
      cancelButtonText: 'Cancelar'
    }).then((result) => {
      if (result.value) {
        this.working = true;
        this._srvGroup.delete( group.id ).subscribe( res => {
          if(res.success) {
            course.groups = course.groups.filter( x => x.id !== group.id);
            swal(Appsettings.APP_NAME, res.message, 'success');
          } else {
            swal(Appsettings.APP_NAME, res.message, 'error');
          }
          this.working = false;
        }, err => {
          this.working = false;
        });
      }
    });
  }

  /*********** COURSES ************/

  onClickNewCourse() {
    const course = new Course( [], 0, '', '', '', null, null, null, '', null, null, null, null, null, null, null, '', 0);
    const DIALOG_REF = this._dialog.open( DialogCourseComponent, {
      width: '600px',
      minHeight: '520px',
      autoFocus: false,
      disableClose: true,
      data: { nodeId: 0 , course: course, edit: false  }
    });

    DIALOG_REF.afterClosed().subscribe( res => {
      if ( res ) {
        this.courses.push( res );
      }
    });
  }

  onClickEditCourse( course: Course ) {

    const DIALOG_REF = this._dialog.open( DialogCourseComponent, {
      width: '600px',
      minHeight: '520px',
      autoFocus: false,
      disableClose: true,
      data: { nodeId: course.nodeId , course: course, edit: true  }
    });

    DIALOG_REF.afterClosed().subscribe( res => {
      if ( res != null ) {
        const updateCourse = this.courses.filter( x => x.id === res.id)[0];
        if ( updateCourse ) {
          updateCourse.description = res.description ;
          updateCourse.endDate = res.endDate ;
          updateCourse.gol = res.gol ;
          updateCourse.groupId = res.groupId ;
          updateCourse.name = res.name ;
          updateCourse.nodeId = res.nodeId ;
          updateCourse.startDate = res.startDate ;
          updateCourse.friday = res.friday ;
          updateCourse.monday = res.monday ;
          updateCourse.saturday = res.saturday ;
          updateCourse.sunday = res.sunday ;
          updateCourse.thursday = res.thursday ;
          updateCourse.tuesday = res.tuesday ;
          updateCourse.wednesday = res.wednesday ;
          updateCourse.subject = res.subject ;
        }

      }
    });
  }

  onDeleteCourse( course: Course ) {

    swal({
      title: '¿Estás seguro de eliminar el grupo?',
      text: '¡Esta operación no se podrá revertir!',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: '¡Sí, eliminar ahora!',
      cancelButtonText: 'Cancelar'
    }).then((result) => {
      if (result.value) {
        this.working = true;
        this._srvCourse.delete( course.id ).subscribe( res => {
          if ( res.success ) {
            this.courses = this.courses.filter( x => x.id !== course.id);
            swal(Appsettings.APP_NAME, res.message, 'success');
          } else {
            swal(Appsettings.APP_NAME, res.message, 'error');
          }
          this.working = false;
        }, err => {
          this.working = false;
        });
      }
    });
  }

  getCourses() {
    this.working = true;
    this.courses = [];

    this._srvCourse.getAll().subscribe( result => {
      if ( result.success) {
        let data = result.data;


       if (data) {
        for ( let c of data) {

          let course = new Course( this.contentSection,
                                   c.id, c.name,
                                   c.description,
                                   c.gol,
                                   c.startDate,
                                   c.endDate,
                                   c.nodeId,
                                   c.subject,
                                   c.monday,
                                   c.tuesday,
                                   c.wednesday,
                                   c.thursday,
                                   c.friday,
                                   c.saturday,
                                   c.sunday,
                                   c.institution,
                                   c.parentId);

          if ( c.groups.length > 0 ) {
            for ( let g of c.groups ) {

              let group = new Group(g.id, g.description, g.status, g.courseId, g.code);
              course.groups.push(group);
            }
          }

          this.courses.push(course);
        }
       }
      }
      this.setActiveCourse();
      this.working = false;
    });
  }

  setActiveCourse() {

    let course = JSON.parse(localStorage.getItem('ra.course'));
    if ( course !== undefined &&  course !== null ) {

      for ( let c of this.courses ) {
        if ( c.id === course.id ) {
          c.selected = true;
        }
      }
    }

    let group = JSON.parse(localStorage.getItem('ra.group'));
    if ( group !== undefined &&  group !== null ) {
      for ( let c of this.courses ) {
        for ( let g of c.groups ) {
          if ( g.id === group.id ) {
            g.selected = true ;
          }
        }
     }
    }
  }

  resetSelection() {
    for ( let c of this.courses ) {
      for ( let g of c.groups ) {
        g.selected = false;
      }
   }

   for ( let c of this.courses) {
     c.selected = false;
   }
  }

  /**** GO TO REDIRECT  ******/
  onClickGoToContent( course: Course) {

    this.resetSelection();

    course.selected = true;
    localStorage.setItem('ra.course', JSON.stringify(course));
    this._router.navigateByUrl(`/teacher/course-content/${course.id}`);

  }

  onClickGoToStudent( course: Course, studenGroup: Group ) {

    this.resetSelection();

    studenGroup.selected = true;
    course.selected = true;

    localStorage.setItem('ra.group', JSON.stringify(studenGroup));
    localStorage.setItem('ra.course', JSON.stringify(course));

    this._router.navigateByUrl(`/teacher/students/${studenGroup.id}/${course.id}`);
  }

  onClickGoToExamSchedule( course: Course, studenGroup: Group ) {

    this.resetSelection();

    studenGroup.selected = true;
    course.selected = true;

    localStorage.setItem('ra.group', JSON.stringify(studenGroup));
    localStorage.setItem('ra.course', JSON.stringify(course));

    this._router.navigateByUrl(`/teacher/exam-schedule/${studenGroup.id}/${course.id}`);
  }

  // Muestra la pantalla para tareas
  onClickGoToHomework( course: Course, studenGroup: Group) {
    this.resetSelection();

    studenGroup.selected = true;
    course.selected = true;

    localStorage.setItem('ra.group', JSON.stringify(studenGroup));
    localStorage.setItem('ra.course', JSON.stringify(course));

    this._router.navigateByUrl(`/teacher/task-schedule/${studenGroup.id}`);

  }
  // Muestra la pantalla para actividades
  onClickGoToActivity( course: Course, studenGroup: Group) {
    this.resetSelection();

    studenGroup.selected = true;
    course.selected = true;

    localStorage.setItem('ra.group', JSON.stringify(studenGroup));
    localStorage.setItem('ra.course', JSON.stringify(course));

    this._router.navigateByUrl(`/teacher/activity-schedule/${studenGroup.id}`);

  }
  // Muestra la pantalla para planeación de recursos
  onClickGoToPlanning( course: Course, studenGroup: Group) {

    this.resetSelection();

    studenGroup.selected = true;
    course.selected = true;

    localStorage.setItem('ra.group', JSON.stringify(studenGroup));
    localStorage.setItem('ra.course', JSON.stringify(course));

    this._router.navigateByUrl(`/teacher/resource-planning/${studenGroup.id}`);

  }
  // Muestra la pantalla para generar calificacion final
  onClickGoToFinalScore( course: Course, studenGroup: Group) {
    this.resetSelection();

    studenGroup.selected = true;
    course.selected = true;

    localStorage.setItem('ra.group', JSON.stringify(studenGroup));
    localStorage.setItem('ra.course', JSON.stringify(course));

    this._router.navigateByUrl(`/teacher/final-score/${studenGroup.id}`);

  }
  // Muestra la pantalla de sitios de interés
  onClickGoToSiteInterests( course: Course) {
    this.resetSelection();

    course.selected = true;

    localStorage.setItem('ra.course', JSON.stringify(course));

    this._router.navigateByUrl(`/teacher/interest-site/${course.id}`);
  }

  onClickGoToEvent( course: Course ) {
    this.resetSelection();
    course.selected = true;
    localStorage.setItem('ra.course', JSON.stringify(course));

    this._router.navigateByUrl(`/teacher/events/${course.id}`);
  }

  onClickGoToAssistanceList(course: Course, studenGroup: Group) {

    this.resetSelection();

    studenGroup.selected = true;
    course.selected = true;

    localStorage.setItem('ra.group', JSON.stringify(studenGroup));
    localStorage.setItem('ra.course', JSON.stringify(course));

    this._router.navigateByUrl(`/teacher/attendance/${studenGroup.id}/${course.id}`);
  }




}
