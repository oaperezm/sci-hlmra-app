import { Component, OnInit, Input } from '@angular/core';
import { MatDialog } from '@angular/material';

// MODELS
import { Studen } from '../../../models/courses/student.model';
import { Rating } from '../../../models/courses/rating.model';

// SERVICES
import { ExamRatingService, ExamScheduleService } from '../../../service/service.index';

import swal from 'sweetalert2';
import { Appsettings } from '../../../configuration/appsettings';

import { DialogStudentAnswersComponent } from '../../../dialogs/dialog-student-answers/dialog-student-answers.component';
import { pairwise } from 'rxjs/operators';
import { createReadStream } from 'fs';
@Component({
  selector: 'app-exam-ratings',
  templateUrl: './exam-ratings.component.html',
  styleUrls: ['./exam-ratings.component.scss']
})
export class ExamRatingsComponent implements OnInit {
  students: Studen[] = [];
  ratings: Rating[] = [];
  examScheduleId: number;
  tipeExam: number;
  examScheduleStatus: boolean;
  readOnly: boolean = false;
  examName: string;
  addScore: boolean;

  // Obtenemos los estudiantes
  @Input('students') set student(value) {
    this.students = value;

  }
  // Obtenemos los estudiantes
  @Input('addScore') set addScoreExam (value) {
    this.addScore = value;
  }
  // Obtenemos el examScheduleId
  @Input('examId') set examId(value: number) {
    this.examScheduleId = value;
  }

  @Input('typeExamId') set typeExamId(value: number) {
    this.tipeExam = value;
  }

  @Input('examName') set nameExamen(value: string) {
    this.examName = value;
  }

  @Input() examScheduleType: number;

  working: boolean;
  constructor(private _srvRating: ExamRatingService,
    private _srvExamSchedulte: ExamScheduleService,
    private _dialog: MatDialog) {

  }

  ngOnInit() {
    if (this.examScheduleType == 6) {
      this.readOnly = true;
      this.examScheduleStatus = true;
    }
  }

  sendData() {
     this.working = true;
     let count = 0;
    let requieredComments = false;
    for (let s of this.students) {
      // if (s.rating.toString() != s.totalAnswerScore.toString()) {
      //   requieredComments = s.commentary.toString() == '';
      //   if (requieredComments ){

      //        break;
      //   }

      // }

      let comment  = ( s.commentary.toString() === '' ) ? '' : s.commentary;
      if ( s.commentary.toString() === '' ) {
        count++;
      }
      let rating = new Rating();
      let score = Number( s.rating)

      rating.userId = s.id;
      rating.correctAnswers = s.totalCorrect;
      rating.score = score;
      rating.comments = comment;
      rating.examScheduleId = this.examScheduleId;
      rating.testId = s.testId;
      rating.maximumExamScore = s.maximumExamScore;
      rating.totalAnswerScore = s.totalAnswerScore;
      rating.weightingId = s.weightingId;
      rating.weightingDescription = s.weightingDescription;
      rating.weightingUnit = s.weightingUnit;
      this.ratings.push(rating);
    }

    if ( count === 0) {
      this._srvRating.save(this.ratings).subscribe(result => {
        if (result.success) {
          swal(Appsettings.APP_NAME, result.message, 'success');
        } else {
          swal(Appsettings.APP_NAME, result.message, 'warning');
        }
        this.working = false;
      });
    } else {
      this.working = false;
      swal(Appsettings.APP_NAME, 'Todas la calificaciones deben contener un comentario', 'warning');

    }


  }

  onChangeExamScheduleStatus(event) {
    swal({
      title: '¿Estás seguro de cerrar el examen?',
      text: 'Ya no podrá actualizar la calificación y se cerrará el examen',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: '¡Sí, cerrar examen!',
      cancelButtonText: 'Cancelar'
    }).then((result) => {
      if (result.value) {
        this.readOnly = event;

        this.working = true;
        this._srvExamSchedulte.updateStatus(this.examScheduleId, 6).subscribe(res => {
          if (res.success) {
            swal(Appsettings.APP_NAME, 'Examen actualizado correctamente', 'success');
          } else {
            swal(Appsettings.APP_NAME, res.message, 'error');
          }
          this.working = false;
        }, err => {
          this.working = false;
        });
      } else {
        this.examScheduleStatus = false;
      }
    });
  }

  // Ver las respuestas del alumno
  onShowAnswers(studen) {
    const DIALOG_REF = this._dialog.open(DialogStudentAnswersComponent, {
      width: '1000px',
      // height: '700px',
      autoFocus: false,
      disableClose: true,
      data: { teacherExamId: this.examScheduleId, nameExam: this.examName, studenId: studen.id }
    });

    DIALOG_REF.afterClosed().subscribe(response => {
    });
  }

  fnChange(studen) {
    if (studen.rating.toString() != studen.totalAnswerScore.toString()) {
    }
  }
}
