import { Component, OnInit, Input, AfterViewInit, Output, EventEmitter } from '@angular/core';

// Servicios
import { StructureService } from '../../../../service/service.index';

// Modelos
import { Content} from '../../../../models/content/content.model';

@Component({
  selector: 'app-viewer-audio',
  templateUrl: './viewer-audio.component.html',
  styleUrls: ['./viewer-audio.component.scss']
})

export class ViewerAudioComponent implements OnInit {

  showAudio: boolean;
  _song: any;
  stopSound: boolean;

  @Output() invoke: EventEmitter<boolean> = new EventEmitter();




  // Obtenemos el audio
  @Input('song') set sound ( value: Content ) {
    this.showAudio = true;
    this.changeSong( value );
  }

  @Input('stopSound') set soundStop( value ) {
    this.stopSound = value;
  }


  constructor() { }

  ngOnInit() {
  }

  // Realiza el cambio de audio cuando se selecciona uno del listado
  changeSong(song: any): void {
    this._song = song;
  }

}
