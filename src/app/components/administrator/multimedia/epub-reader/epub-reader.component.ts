import { Component, ElementRef, OnInit, ViewChild, ViewEncapsulation, Input, OnDestroy } from '@angular/core';
import { AngularEpubViewerComponent } from '../../../../core/angular-epub-viewer/src/angularEpubViewer.component';
import {
    EpubChapter,
    EpubError,
    EpubLocation,
    EpubMetadata,
    EpubPage,
    EpubSearchResult
} from '../../../../core/angular-epub-viewer/src/angularEpubViewer.models';
@Component({
    selector: 'app-epub-reader',
    templateUrl: './epub-reader.component.html',
    styleUrls: ['./epub-reader.component.scss']
})

export class EpubReaderComponent implements OnInit {

    UrlEpub: string;

    @Input('UrlEpub') set epub(value: string) {
        this.UrlEpub = value;
    }

    proxyURL = 'https://cors-anywhere.herokuapp.com/';

    @ViewChild('epubViewer')
    epubViewer: AngularEpubViewerComponent;
    @ViewChild('picker', { read: ElementRef })
    picker: ElementRef;
    @ViewChild('metadata', { read: ElementRef })
    metadata: ElementRef;

    unzippedBooks: Book[] = [].concat(UNZIPPED_BOOKS);
    zippedBooks: Book[] = [].concat(ZIPPED_BOOKS);
    // chosenDocument: Book = ZIPPED_BOOKS[0];
    chosenDocument: Book = ZIPPED_BOOKS[0];

    totalPages: number = 0;

    chapters: EpubChapter[] = [];
    chosenChapter: EpubChapter = null;

    searchText: string = null;
    matchesCount: number = 0;

    fontSizes: string[] = [].concat(FONT_SIZES);
    chosenFontSize: string = this.fontSizes[2];

    paddings: string[] = [].concat(PADDINGS);
    chosenPadding: string = this.paddings[2];

    lockDocumentChoose: boolean = true;
    lockSearch: boolean = true;
    lockPagination: boolean = true;
    lockTOC: boolean = true;

    bookDescription: any = [];
    bookName: string;
    creator: string;
    copyright: string;

    mywindow;
    url: string;
    ngOnInit() {
        // this.OnDestroy();
        // this.openLink();
        this.openEpub();
    }
    OnDestroy() {
        this.epubViewer.destroyEpub();
    }
    openEpub() {
      if(this.mywindow) {
        this.mywindow.close();
       }
      //  this.url = 'https://digital.latiendadellibrero.com/pdfreader/acrcate-la-qumica'
       this.mywindow = window.open(this.UrlEpub.toString(), "_blank", "resizable=no, toolbar=no, scrollbars=no, menubar=no, status=no, directories=no, location=no, width=1000, height=600, left=100, top=100 " );
    }

    onBookUnloaded() {
        this.lockDocumentChoose = true;
        this.lockSearch = true;
        this.lockPagination = true;
        this.lockTOC = true;
        this.totalPages = 0;
        this.chapters = [];
        this.chosenChapter = null;
        this.metadata.nativeElement.innerHTML = '';
    }
    /**
     * Metodo que selecciona el EPUB a mostrar
     */
    onSelectedBook() {
        this.onBookUnloaded();
        // removing picked file
        this.picker.nativeElement.value = null;
        // path will be translated to link
        this.epubViewer.openLink(this.chosenDocument.path);
    }

    openLink() {
        this.epubViewer.destroyEpub();
        this.onBookUnloaded();
        // removing selected book
        this.epubViewer.openLink(this.proxyURL + this.UrlEpub);

    }

    onDocumentReady() {
        this.lockDocumentChoose = false;
        this.epubViewer.setStyle('font-size', this.chosenFontSize);
    }

    onChapterUnloaded() {
        this.lockSearch = true;
        this.lockPagination = true;
    }

    onChapterDisplayed(chapter: EpubChapter) {
        this.lockSearch = false;
        this.lockPagination = this.totalPages <= 0;
        this.onSearchPrinted();
    }

    onLocationFound(location: EpubLocation) {
    }

    onPaginationComputed(pages: EpubPage[]) {
        this.lockPagination = false;
        this.totalPages = pages.length;
    }

    onTOCLoaded(chapters: EpubChapter[]) {
        this.chapters = [].concat(chapters);
        if (this.chapters.length > 0) {
            this.chosenChapter = this.chapters[0];
        }
        this.lockTOC = false;
    }

    onChapter() {
        if (this.epubViewer.documentReady) {
            this.epubViewer.goTo(this.chosenChapter.cfi);
        }
    }

    onSearchPrinted() {
        if (this.epubViewer.documentReady && this.epubViewer.isChapterDisplayed &&
            this.searchText && this.searchText.trim().length > 0) {
            this.lockSearch = true;
            this.epubViewer.searchText(this.searchText);
        } else {
            this.matchesCount = 0;
        }
    }

    onSearchFinished(results: EpubSearchResult[]) {
        this.lockSearch = false;
        this.matchesCount = results.length;
    }

    onPaddingChosen() {
        this.epubViewer.padding = this.chosenPadding;
    }

    onFontSizeChosen() {
        if (this.epubViewer.documentReady) {
            this.epubViewer.setStyle('font-size', this.chosenFontSize);
        }
    }

    onMetadataLoaded(metadata: EpubMetadata) {
        this.metadata.nativeElement.innerHTML = JSON.stringify(metadata, null, 2)
            .replace(/\n/g, '<br>')
            .replace(/ /g, '&nbsp;');
        this.bookDescription.push(metadata);
        this.bookName = this.bookDescription[0].bookTitle;
        this.creator = this.bookDescription[0].creator;
        // this.copyright = this.bookDescription[0].rights;
    }

    onErrorOccurred(error: EpubError) {
        switch (error) {
            case EpubError.OPEN_FILE:
                this.lockDocumentChoose = false;
                this.lockSearch = false;
                this.lockPagination = false;
                this.lockTOC = false;
                alert('Error while opening file');
                break;
            case EpubError.READ_FILE:
                this.lockDocumentChoose = false;
                this.lockSearch = false;
                this.lockPagination = false;
                this.lockTOC = false;
                alert('Error while reading file');
                break;
            case EpubError.NOT_LOADED_DOCUMENT:
                alert('Error while accessing unloaded document');
                break;
            case EpubError.NOT_DISPLAYED_CHAPTER:
                alert('Error while accessing not displayed chapter');
                break;
            case EpubError.SEARCH:
                this.lockSearch = false;
                alert('Error while searching text');
                break;
            case EpubError.COMPUTE_PAGINATION:
                this.lockPagination = false;
                alert('Error while calculating pagination');
                break;
            case EpubError.LOAD_METADATA:
                alert('Error while loading metadata');
                break;
            case EpubError.LOAD_TOC:
                this.lockTOC = false;
                alert('Error while loading table of contents');
                break;
        }
    }

    // getGithubIcon(): string {
    //     return `${environment.production ? '' : '../'}assets/GitHub-Mark-Light-120px-plus.png`
    // }
}

interface Book {
    path: string,
    name: string
}

const UNZIPPED_BOOKS: Book[] = [];
/**
* Aqui ponemos la url del epub ya sea en fisico o en linea
*/
const ZIPPED_BOOKS: Book[] = [];

const PADDINGS: string[] = [
    '0',
    '8px',
    '16px',
    '24px',
    '32px',
    '40px',
    '48px'
];

const FONT_SIZES: string[] = [
    '8px',
    '12px',
    '16px',
    '20px',
    '24px',
    '28px',
    '32px'
];
