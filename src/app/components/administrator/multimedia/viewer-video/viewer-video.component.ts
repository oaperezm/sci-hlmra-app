import { Component, OnInit, Input } from '@angular/core';
import { EmbedVideoService } from 'ngx-embed-video';
import {DomSanitizer, SafeHtml, SafeStyle, SafeScript, SafeUrl, SafeResourceUrl} from '@angular/platform-browser';

// Modelos
import {  Content } from '../../../../models/content/content.model';

@Component({
  selector: 'app-viewer-video',
  templateUrl: './viewer-video.component.html',
  styleUrls: ['./viewer-video.component.scss']
})
export class ViewerVideoComponent implements OnInit {

  videoEmbed: any;

  showVideo: boolean;





  // Obtenemos el id del tipo de archivo
  @Input('video') set fileVideo ( value:  Content ) {

    this.showVideo = true;
    this.viewVideo( value.urlContent);
  }

  constructor(  private _svrEmbed: EmbedVideoService,
                private sanitizer: DomSanitizer ) { }

  ngOnInit() {
  }



  // Agrega el video al componente
  viewVideo(urlVideo: string) {
    this.videoEmbed = `https://player.vimeo.com/video/${urlVideo}`
  }


}
