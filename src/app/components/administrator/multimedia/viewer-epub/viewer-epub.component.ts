import { Component, OnInit, Input } from '@angular/core';
import {DomSanitizer, SafeResourceUrl} from '@angular/platform-browser';

import { Appsettings } from '../../../../configuration/appsettings';

import { StructureService } from '../../../../service/service.index';

// Modelos
import { Content } from '../../../../models/content/content.model';
@Component({
  selector: 'app-viewer-epub',
  templateUrl: './viewer-epub.component.html',
  styleUrls: ['./viewer-epub.component.scss']
})
export class ViewerEpubComponent implements OnInit {

  showEpub: boolean;
  pageurl: SafeResourceUrl;
  _dataFile: Content[] = [];

  // Obtenemos el id del tipo de archivo
  @Input('file') set fileId ( value: Content ) {
    this.showEpub = true;    
  }

  constructor(  private _domSanitizer: DomSanitizer,
                private _srvStructure: StructureService ) {

                }

  ngOnInit() {
  }


  viewFile(idFile: number) {
     let apiUrl = `${Appsettings.API_ENDPOINT_FULL}/contents/${idFile}/download`;
     this.pageurl = this._domSanitizer.bypassSecurityTrustResourceUrl(apiUrl + '#toolbar=0&navpanes=0&scrollbar=0');
  }

}
