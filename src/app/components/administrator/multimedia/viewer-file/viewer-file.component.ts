import { Component, OnInit, Input, SecurityContext, HostListener, Renderer2 } from '@angular/core';
import {BrowserModule, DomSanitizer, SafeResourceUrl} from '@angular/platform-browser';

import { Appsettings } from '../../../../configuration/appsettings';

import { StructureService } from '../../../../service/service.index';

// Modelos
import { Content } from '../../../../models/content/content.model';
import { HotkeysService, Hotkey } from 'angular2-hotkeys';
@Component({
  selector: 'app-viewer-file',
  templateUrl: './viewer-file.component.html',
  styleUrls: ['./viewer-file.component.scss']
})
export class ViewerFileComponent implements OnInit {

  showFile: boolean;
  pageurl: SafeResourceUrl;
  _dataFile: Content[] = [];
  cargando = true;
  urlcontent:string;

  @Input()
  isViewerCustom: boolean = false;

  // Obtenemos el id del tipo de archivo
  @Input('file') set fileId ( value: Content ) {
    this.showFile = true;
    this.viewFile( value.id, value);
  }

  constructor(private _domSanitizer: DomSanitizer,
              private _srvStructure: StructureService,
              private hotkeysService: HotkeysService)
  {
    this.hotkeysService.add(new Hotkey(['ctrl+p', 'meta+p'], (event: KeyboardEvent): boolean => {
      return false;
    }));
  }

  ngOnInit() { }

 /* viewFile(idFile: number, contentTypeDesc: string ) {
    let apiUrl = `${Appsettings.API_ENDPOINT_FULL}/contents/${idFile}/download`;

    if (contentTypeDesc !== 'Recurso'){
      apiUrl += '?page=hsn#toolbar=0&navpanes=0&scrollbar=0';
    }
    this.pageurl = this._domSanitizer.bypassSecurityTrustResourceUrl(apiUrl);
    console.log(this.pageurl);
  }*/

  viewFile(idFile: number, File: Content) {
    let apiUrl = `${Appsettings.API_ENDPOINT_FULL}/contents/${idFile}/download`;
    this.pageurl = this._domSanitizer.bypassSecurityTrustResourceUrl(apiUrl);
  }
}
