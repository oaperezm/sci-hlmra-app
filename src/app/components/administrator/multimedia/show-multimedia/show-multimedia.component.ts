import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { AdminHeaderService } from '../../../layouts/administrator/admin-header/admin-header.service';

// Servicios
import {
  StorageService,
  CatalogService,
  StructureService,
  UserService,
  EventService
} from '../../../../service/service.index';

// Modelos
import { Content } from '../../../../models/content/content.model';
import { Event } from '../../../../models/content/event.model';
import { EducationLevel } from '../../../../models/catalogs/educationLevel.model';

// COMPONENT SERVICES
import { AdminNavService } from '../../../../components/layouts/administrator/admin-nav/admin-nav.service';

import { UserUtilities } from '../../../../shared/user.utilities';
@Component({
  selector: 'app-show-multimedia',
  templateUrl: './show-multimedia.component.html',
  styleUrls: ['./show-multimedia.component.scss']
})
export class ShowMultimediaComponent implements OnInit, OnDestroy {
  content: Content;
  working: boolean;
  workingSection: boolean;

  showVideo: boolean;
  showAudio: boolean;
  showFile: boolean;
  showFileCustom: boolean = false;
  showEpub: boolean;
  showDonload: boolean;
  showInteractive: boolean;
  nodeId: number;

  contentShared: Content[] = [];
  contenRoute: any[] = [];

  name: string = 'Sin especificar';
  description: String = 'Sin especificar';
  purpose: string = 'Sin especificar';
  purposes: any[] = [];
  scope: string = 'Sin especificar';

  fileType: string;
  formativeField = 'Sin especificar';
  signature = 'Sin especificar';
  grade: string = 'Sin especificar';
  block: string = 'Sin especificar';
  breadcrumb: any[] = [];

  axis: string;
  formativeFieldName: string;
  purposeName: string;

  tipeSection: string;
  userEmail: string;
  userData: any[] = [];
  educationLevel: EducationLevel[] = [];
  educationLevelSelectdId: number;
  sectionId: number;
  myCarouselOptions = {
    items: 4,
    dots: true,
    nav: true,
    margin: 10,
    responsiveClass: true,
    loop: true,
    responsive: {
      0: {
        items: 1,
        nav: true
      },
      600: {
        items: 3,
        nav: false
      },
      1000: {
        items: 6,
        nav: true,
        loop: false
      }
    }
  };
  viewevent: Event;
  timer: any;

  constructor(public _srvStorage: StorageService,
    private route: ActivatedRoute,
    public _srvHeaderAdmin: AdminHeaderService,
    private _srvCatalog: CatalogService,
    private _srvStructure: StructureService,
    public _srvAdminNav: AdminNavService,
    private _srvUser: UserService,
    private _srvEvent: EventService) {
    this.educationLevelSelectdId = 0;

  }

  ngOnInit() {



    this.initData();
    this.showVideo = false;
    this.showAudio = false;
    this.showFile = false;
    this.showEpub = false;
    this.showDonload = false;

    this._srvStorage.showBackBotton = true;
    this.content = JSON.parse(this._srvStorage.content);
    this._srvCatalog.getEducationLevelAll().subscribe(result => {
      if (result.success) {
        this.educationLevel = [];
        let datas = result.data;
        for (let e of datas) {
          let level = new EducationLevel();
          level.id = e.id;
          level.active = e.active;
          level.description = e.description;
          this.educationLevel.push(level);
        }
        this.validateLevel(this.content.content[0].node.id);
      }
    });
    this.setData(this.content);
    this.getContentShared(this.content.id);

    this.working = false;

    this.validateContentType(this.content.fileTypeId);
    this.route.params.subscribe(param => {
      this.nodeId = param.nodeId;

    });





    this.breadcrumb = this._srvHeaderAdmin.breadcrumb;
    this._srvAdminNav.setTitle(`Material: ${this.content.name}`);
    this._srvStructure.addVisitContent(this.content.id).subscribe(result => { });
    
    if (this.content.fileTypeId === 18) {
      this.tipeSection = 'Libros de texto';
      this.sectionId = 3;
      this._srvHeaderAdmin.setUrlBackButton([`/resource/libros-texto/0`, 'Libros de texto']);
    } else if (this.content.fileTypeId === 19) {
      this.tipeSection = 'Libros de texto digital';
      this.sectionId = 4;
      this._srvHeaderAdmin.setUrlBackButton([`/resource/libros-texto-digital/0`, 'Libros de texto digital']);
    } else {
      this.tipeSection = 'Material didáctico';
      this.sectionId = 1;
      this._srvHeaderAdmin.setUrlBackButton([`/resource/content/0`, 'Material didáctico']);
    }

  }

  ngOnDestroy(): void {
    clearInterval(this.timer);
    this._srvHeaderAdmin.setUrlBackButton([]);
  }

  // Valida el tipo de archivo para mostra el visor o reproductor respectivamente
  validateContentType(typeDescId: number) {
   // console.log(typeDescId);
     switch (typeDescId) {
      case 6:
        this.showVideo = false;
        this.showAudio = true;
        this.showFile = false;
        this.showInteractive = false;
        this.showEpub = false;
        this.showFileCustom = false;

        break;
      case 5:
        this.showVideo = true;
        this.showAudio = false;
        this.showFile = false;
        this.showInteractive = false;
        this.showEpub = false;
        this.showFileCustom = false;

        break;

      case 8:
        this.showVideo = false;
        this.showAudio = false;
        this.showFile = false;
        this.showInteractive = true;
        this.showEpub = false;
        this.showFileCustom = false;

        break;

        case 18:
          this.showVideo = false;
          this.showAudio = false;
          this.showFile = true;
          this.showFileCustom = true;
          this.showInteractive = false;
          this.showEpub = false;
          break;

      case 19:
        this.showVideo = false;
        this.showAudio = false;
        this.showFile = false;
        this.showInteractive = false;
        this.showEpub = true;
        this.showFileCustom = false;

        break;
      default:
        let extension = UserUtilities.getExtensionToUrl(this.content.urlContent);
        this.showVideo = false;
        this.showAudio = false;
        this.showEpub = false;
        this.showInteractive = false;
        this.showFile = extension.toLowerCase() === 'pdf' ? true : false;
        this.showDonload = extension.toLowerCase() !== 'pdf' ? true : false;
        break;
    }
  }

  getContentShared(contentId: number) {
    this.workingSection = true;
    this.contentShared = [];
    this._srvCatalog.getResourceShare(contentId).subscribe(result => {
      if (result.success) {
        for (const c of result.data) {
          const content = new Content();
          content.active = c.active;
          content.area = c.area;
          content.content = c.content;
          content.contentResourceType = c.contentResourceType;
          content.contentTypeDesc = c.contentTypeDesc;
          content.contentTypeId = c.contentTypeId;
          content.fileTypeDesc = c.fileTypeDesc;
          content.fileTypeId = c.fileTypeId;
          content.formativeField = c.formativeField.description;
          content.id = c.id;
          content.isPublic = c.isPublic;
          content.name = c.name;
          content.purpose = c.purpose;
          content.thumbnails = c.thumbnails;
          content.totalContent = c.totalContent;
          content.trainingField = c.trainingField;
          content.urlContent = c.urlContent;
          content.purpose = c.purpose.description;
          content.block = c.block.description;
          content.axis = c.axis.description;
          content.formativeFieldName = c.formativeField.description;
          content.purposeName = c.purpose.description;
          this.contentShared.push(content);
        }

      }
      this.workingSection = false;
    }, err => {
      this.workingSection = false;
    });
  }

  initData(): void {
    this._srvCatalog.getPurposesAll().subscribe(res => {
      this.purposes = [];
      if (res.data) {
        for (const f of res.data) {
          this.purposes.push(f);
        }
      }

    });
  }

  selectContent(item) {
    this._srvAdminNav.setTitle(`Material: ${item.name}`);
    this.setData(item);
    this.getContentShared(item.id);
    this.validateContentType(item.fileTypeId);
    this.content = item;
  }

  setData(item) {
    if (item.content.length > 0) {

      let cont = item.content[0].nodeRoute;
      this.contenRoute = cont.split('/');

      this.name = item.name;
      this.description = item.description;

      this.fileType = item.fileTypeDesc;

      this.block = item.block;
      this.purpose = item.purpose;
      this.formativeField = item.formativeField;


      this.axis = item.axis;
      this.formativeFieldName = item.formativeFieldName;
      this.purposeName = item.purposeName;

      if (this.contenRoute.length > 2) {
        this.grade = this.contenRoute[2];
      } else {
        this.grade = 'No aplica';
      }
      if (this.contenRoute.length > 3) {
        this.signature = this.contenRoute[3];
      } else {
        this.signature = 'No aplica';
      }


      this._srvUser.getProfileData().subscribe(res => {
        this.userData = res.data;
        this.userEmail = res.data.email;
        this.addClickEvent();
      });
    }
    else {
    }
  }



  addClickEvent() {
    this._srvUser.getaDataUserSIDE(this.userEmail).subscribe(res1 => {


      if (res1.success) {

        //let dateInit = new Date();
        let date = new Date();
        const dateInit = new Date(  date.getFullYear(), date.getMonth(), date.getDate(), 0, 0, 0 );
        let count = res1.data.length;

        let data = (count => 1) ? res1.data[count - 1] : res1.data[0];
        this.viewevent = new Event();

        // event.Id = 0;
        this.viewevent.cct = data.cct;
        this.viewevent.email = this.userEmail;
        this.viewevent.username = this.userData['fullName'];
        this.viewevent.institutionName = this.userData['institution'];
        this.viewevent.level = data.nivel;
        this.viewevent.state = data.nombreentidad;
        let school = data.clasificacion_escuela;
        this.viewevent.clasificationSchool = (school) ? school.trim() : 'N/A';
        this.viewevent.zone = (data.nombre_zona) ? data.nombre_zona : 'N/A';
        this.viewevent.control = data.nombrecontrol;
        this.viewevent.prometer = data.nombre_promotor;
        // event.sections = this.tipeSection;
        // llegan en el contenido
        this.viewevent.grade = (this.contenRoute[2]) ? this.contenRoute[2] : 'N/A';
        this.viewevent.educationField = (this.formativeFieldName) ? this.formativeFieldName : 'N/A';
        this.viewevent.subject = (this.contenRoute[3]) ? this.contenRoute[3] : 'N/A';
        // event.format = this.fileType;
        this.viewevent.ResourceName = this.name;
        this.viewevent.element = 'N/A';
        this.viewevent.comment = 'N/A';
        this.viewevent.commentDetail = 'N/A';
        this.viewevent.entryDate = dateInit;
        this.viewevent.registerDate = dateInit;
        this.viewevent.timeNavegation = '00';
        this.viewevent.eventId = 1;
        this.viewevent.fileTypeId = this.content['fileTypeId'];
        this.viewevent.contentTypeId = this.content['contentTypeId'];
        this.viewevent.educationLevelId = this.educationLevelSelectdId;
        this.viewevent.userId = 0;
        this.viewevent.sectionId = this.sectionId;
        this.saveClickEvent(this.viewevent);
      }

      this.working = false;
    }, err => {
      this.working = false;
    });
  }

  saveClickEvent(event: Event) {
    // this._srvEvent.addEventClic(event).subscribe(result => {
    //   if (result.success) {
    //     if (result.data > 0) {
    //       event.id = result.data;
    //       this.timer = setInterval(() => {
    //         this._srvEvent.addEventClic(event).subscribe(result => {
    //         });
    //       }, 3000);
    //     }
    //   }
    // });
  }


  // valida el nivel de la estrucutra con el catalogo
  validateLevel(id: number) {
    // 143 -> primaria
    // 144 -> preescolar
    // 145 -> secundaria
    let level = 0;
    switch (id) {
      case 143:
        let primaria = this.educationLevel.filter(x => x.id === 2)[0];
        this.educationLevelSelectdId = primaria.id;
        // this.onSelectEducationLevel(primaria.id);
        break;
      case 144:
        let preescolar = this.educationLevel.filter(x => x.id === 1)[0];
        this.educationLevelSelectdId = preescolar.id;
        // this.onSelectEducationLevel(preescolar.id);
        break;
      case 145:
        let secundaria = this.educationLevel.filter(x => x.id === 3)[0];
        this.educationLevelSelectdId = secundaria.id;
        // this.onSelectEducationLevel(secundaria.id);
        break;

      default:
        // console.log( this.educationLevelSelectdId);
        // this.onSelectEducationLevel( this.educationLevelSelectdId );
        break;
    }
  }
}
