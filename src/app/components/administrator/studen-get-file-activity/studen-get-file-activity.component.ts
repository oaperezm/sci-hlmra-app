import { Component, OnInit,Input } from '@angular/core';

@Component({
  selector: 'app-studen-get-file-activity',
  templateUrl: './studen-get-file-activity.component.html',
  styleUrls: ['./studen-get-file-activity.component.scss']
})
export class StudenGetFileActivityComponent implements OnInit {

  activity: any[] = [];
  files: any[] = [];

  @Input('activity') set taskData(value) {
    this.activity = value;
    this.files = value['filesId'];
  }
  constructor() { }

  ngOnInit() {
  }

}
