import { Component, OnInit, Output, Input, EventEmitter } from '@angular/core';

// Servicios
import { ExamScheduleService } from '../../../service/service.index';
import swal from 'sweetalert2';
import { Appsettings } from '../../../configuration/appsettings';

@Component({
  selector: 'app-studen-answers',
  templateUrl: './studen-answers.component.html',
  styleUrls: ['./studen-answers.component.scss']
})
export class StudenAnswersComponent implements OnInit {

  teacherExamId: number;
  questions: any[] = [];
  arrayQuestion: any[] = [];
  working: boolean;
  pageIndex: number;
  studenId: number;

  @Input('studenId') set idStuden(value) {
    this.studenId = value;
  }

  @Input('examId') set examId(value) {
    this.teacherExamId = value;
    this.getAnswers(this.teacherExamId);
  }

  @Input('indexPage') set indexPage(value) {
    this.pageIndex = value;
    this.getPaginateQuestion(this.pageIndex);
  }


  // Rertorna el index del paginador
  @Output() indexPagination: EventEmitter<number> = new EventEmitter<number>();
  @Output() testScore: EventEmitter<any> = new EventEmitter();
  @Output() close: EventEmitter<any> = new EventEmitter();

  constructor(private _srvExam: ExamScheduleService) {
    this.working = false;
  }

  ngOnInit() {
  }

  // Obtiene las preguntas del examen y
  getAnswers(examId: number): void {
    this.working = true;
    let questionTotal = [];
    let testScore = null;
    let isCorrectAnswer = null;
    this._srvExam.getAnswersStuden(examId, this.studenId).subscribe(result => {
      if (!result.success) {
        swal(Appsettings.APP_NAME, result.message, 'warning');
        this.working = false;
        this.close.emit();
        return;
      }

      let dataExam = result.data.teacherExamn.teacherExamQuestion;
      let weighting = result.data.teacherExamn.weighting;
      let questions = {};
      let i = 1;
      for (let e of dataExam) {
        isCorrectAnswer = true;
        for (let qa of e.question.questionAnswers) {
          qa.wasStudentAnswer = false;
          qa.Explanation = qa.relationDescription;
          if (e.question.questionTypeId != 3) {
            let ta = e.question.answers.filter(ta => ta.id == qa.id);
            if (ta.length > 0) {
              if (!qa.isCorrect)
                isCorrectAnswer = false;
              qa.wasStudentAnswer = true;
            }
            else if (qa.isCorrect)
              isCorrectAnswer = false;
          }
          else {
            let ta = e.question.answers.filter(ta => ta.id == qa.id);
            if (ta.length == 0)
              isCorrectAnswer = false;
            else {
              if (ta[0].relationDescription != qa.relationDescription)
                isCorrectAnswer = false;
              qa.Explanation = ta[0].relationDescription;
              qa.relationIsCorrect = ta[0].relationDescription == qa.relationDescription;
            }
          }
        }

        let answervalue = result.data.isStudent ? null : isCorrectAnswer ? e.value : 0;
        let answers = [];
        for (let a of e.question.questionAnswers) {
          let res = {
            answers: a.description,
            iscorrect: result.data.isStudent ? null : a.isCorrect,
            relation: a.Explanation,
            relationIsCorrect: result.data.isStudent ? null : a.relationIsCorrect,
            imageHeight: a.imageHeight,
            imageWidth: a.imageWidth,
            wasStudentAnswer: a.wasStudentAnswer,
          };
          answers.push(res);
        };
        testScore += answervalue;
        if (result.data.isStudent)
          isCorrectAnswer = null;
        questions = {
          index: i,
          imgQuestion: e.question.urlImage,
          content: e.question.content,
          answers: answers,
          weighting: weighting,
          studenAnswerIsCorrect: isCorrectAnswer,
          answervalue: answervalue,
          studentAnswer: e.question.answers,
          questionTypeId: e.question.questionType.id,
          imageHeight: e.question.imageHeight,
          imageWidth: e.question.imageWidth,
          value: e.value,
        };
        questionTotal.push(questions);
        i++;
      }
      if (result.data.isStudent)
        testScore = result.data.examScore;
      testScore = Math.round(testScore * 100) / 100
      this.arrayQuestion = this.chunkArray(questionTotal, 5);
      this.indexPagination.emit(this.arrayQuestion.length);

      this.testScore.emit({
        testScore: testScore,
        weighting: weighting
      });
      this.questions = this.arrayQuestion[0];
      this.working = false;
    });
  }

  getColorAnswer(val, wasStudentAnswer) {
    if (val == null)
      return wasStudentAnswer ? "blue" : "";
    return val ? "green" : "red";
  }

  getIconAnswer(val) {
    if (val == null)
      return ""
    return val ? "done" : "clear";
  }

  chunkArray(array: any[], size) {
    let index = 0;
    let arrayLength = array.length;
    let tempArray = [];

    for (index = 0; index < arrayLength; index += size) {
      let myChunk = array.slice(index, index + size);
      tempArray.push(myChunk);
    }

    return tempArray;
  }

  getPaginateQuestion(index: number) {
    this.questions = [];
    this.questions = this.arrayQuestion[index];
  }

}
