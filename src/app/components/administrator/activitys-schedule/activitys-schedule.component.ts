import { Component, OnInit, Output, EventEmitter } from '@angular/core';

import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';

// Servicios
import { ActivityService, ContentTeacherService, ExamScheduleService, CourseService } from '../../../service/service.index';

// Modelos
import { ActivitySchedule } from '../../../models/courses/activitySchedule.model';
import { TeacherContentType } from '../../../models/courses/teacherContentType.model';
import { TeacherContent } from '../../../models/courses/teacheContent.model';
import { SchedulingPlartial } from '../../../models/courses/schedulingPartial.model';

import swal from 'sweetalert2';
import { Appsettings } from '../../../configuration/appsettings';

@Component({
  selector: 'app-activitys-schedule',
  templateUrl: './activitys-schedule.component.html',
  styleUrls: ['./activitys-schedule.component.scss']
})
export class ActivitysScheduleComponent implements OnInit {
  showTotal: boolean;
  working: boolean;
  showForm: boolean;
  showQualitify: boolean;
  frmActivitySchudele: FormGroup;

  activitySchedules: ActivitySchedule[] = [];
  activitiesQualification: ActivitySchedule[] = [];
  recourseType: TeacherContentType[] = [];
  contentsTeacher: TeacherContent[] = [];
  totalContents: any[] = [];
  active: number;
  visible: boolean;
  studenGroupId: number;
  filesId: number[] = [];
  filesIds: number[] = [];
  qualification: boolean;
  qualifications: boolean;

  groupId: number;

  schedulingPlartial: SchedulingPlartial[] = [];
  activityPer: number;

  typeQualify: string;

  ponderation: number;

  partialSelect: SchedulingPlartial;
  totalQualification: number;
  msg: boolean;
  activitieId: number;
  dateInti: Date;
  dateIntiHomework: Date;
  dateEnd: Date;  // Retornamos la cantidad de actividades

  typeScoreValidate: boolean;
  typeOfQualificationValidate: boolean;

  @Output() totalActivity: EventEmitter<number> = new EventEmitter<number>();
  constructor(private _srvActivity: ActivityService,
    private _srvContent: ContentTeacherService,
    private _router: Router,
    private _srvExam: ExamScheduleService,
    private _srvCourse: CourseService, ) {
    this.working = false;
    this.showForm = false;
    this.showQualitify = true;
    // this.ponderation = 0;
    this.msg = true;
    this.totalQualification = 0;

    // Configuramos las validaciones del formulario
    this.frmActivitySchudele = new FormGroup({
      id: new FormControl(0),
      name: new FormControl('', [Validators.required, Validators.maxLength(350)]),
      scheduledDate: new FormControl('', [Validators.required]),
      openingDate: new FormControl(''),
      description: new FormControl('', [Validators.required]),
      select: new FormControl(),
      qualification: new FormControl(),
      SchedulingPartialId: new FormControl('', [Validators.required]),
      typeScore: new FormControl(),
      typeOfQualification: new FormControl(),
      assignedQualification: new FormControl()
    });

    let group = JSON.parse(localStorage.getItem('ra.group'));
    this.groupId = group.id;

    this.getPlanning(this.groupId);
  }

  ngOnInit() {
    this.getPartials(this.groupId);
  }

  // Obtiene planeacion
  getPlanning(groupId: number) {
    this._srvCourse.getPlanning(groupId).subscribe(result => {
      let planning = result.data[0];
      this.activityPer = planning.activity;
    });
  }
  // Obtiene todas la programacions de actividades del grupo
  getActivity(groupId: number) {
    this.working = true;
    this.studenGroupId = groupId;
    this.activitySchedules = [];
    this.getTypeContent();
    this._srvActivity.getAll(groupId).subscribe(result => {
      this.getTypeContent();
      this.getData();
      if (result.success) {
        this.totalQualification = 0;


        for (let h of result.data) {
          let activitie = new ActivitySchedule();
          activitie.id = h.id;
          activitie.name = h.name;
          activitie.description = h.description;
          activitie.scheduledDate = h.scheduledDate;
          activitie.filesId = h.files;
          activitie.schedulingPartialId = h.schedulingPartialId;
          activitie.openingDate = h.openingDate;
          activitie.assignedQualification = h.assignedQualification;
          activitie.typeOfQualification = h.typeOfQualification;
          activitie.typeScore = h.typeScore;
          this.activitySchedules.push(activitie);
          if (h.typeOfQualification) {
            this.totalQualification = this.totalQualification + h.assignedQualification;
          }
        }
        setTimeout(() => {
          this.totalActivity.emit(this.activitySchedules.length);
        }, 2000);
      }
      this.working = false;
    });
  }

  // Se ejecuta cuando se da guardar en el formulario de programacion de actividades
  onSubmit(): void {
    this.working = true;

    let activitieSchedule = new ActivitySchedule();



    activitieSchedule.scheduledDate = this.frmActivitySchudele.controls['scheduledDate'].value;
    activitieSchedule.name = this.frmActivitySchudele.controls['name'].value;
    activitieSchedule.description = this.frmActivitySchudele.controls['description'].value;
    activitieSchedule.filesId = this.filesId;
    activitieSchedule.openingDate = this.frmActivitySchudele.controls['openingDate'].value;
    activitieSchedule.schedulingPartialId = this.frmActivitySchudele.controls['SchedulingPartialId'].value;
    activitieSchedule.id = this.frmActivitySchudele.controls['id'].value;
    activitieSchedule.studentGroupId = this.studenGroupId;

    let typeOfQualification = (this.frmActivitySchudele.controls['typeOfQualification'].value) ? true : false;
    activitieSchedule.typeOfQualification = typeOfQualification;

    let typeScore = (this.frmActivitySchudele.controls['typeScore'].value) ? true : false;
    activitieSchedule.typeScore = typeScore;

    let assignedQualification = (this.frmActivitySchudele.controls['assignedQualification'].value) ? this.frmActivitySchudele.controls['assignedQualification'].value : 0;
    activitieSchedule.assignedQualification = assignedQualification;

    let SumOfPercentages = this.totalQualification;
    let maxSumOfPercentages = 100;


    if (this.activitySchedules.length > 0) {
      let bool = (activitieSchedule.typeOfQualification) ? true : false;
      let bool2 = (this.typeOfQualificationValidate) ? true : false;
      if (activitieSchedule.typeScore !== this.typeScoreValidate || bool !== bool2) {
        let msg = '';
        if (!this.typeScoreValidate) {
          msg = 'Conforme a la configuración de las otras activdades la calificación debe ser por "Cumplimiento".';
        } else if (!this.typeOfQualificationValidate) {
          msg = 'Conforme a la configuración de las otras actividades la calificación debe ser por "Puntuación".';
        } else {
          msg = 'Conforme a la configuración de las otras actividades la calificación debe ser por "Porcentaje".';
        }
        swal(Appsettings.APP_NAME, msg, 'error');
        this.working = false;
        return;
      }
    }

    if (SumOfPercentages >= maxSumOfPercentages) {
      let msg = '';
      msg = 'No se puede guardar otra actividad ya que el porcentaje acumulado no puede ser igual o mayor al 100%".';
      swal(Appsettings.APP_NAME, msg, 'error');
      this.working = false;
      return;
    }


    if (activitieSchedule.id > 0) {
      this.updateTaskSchedule(activitieSchedule);
    } else {
      this.saveTaskSchedule(activitieSchedule);
    }

  }

  // Guarda una nueva programacion de actividad
  saveTaskSchedule(taskSchedule) {
    this._srvActivity.save(taskSchedule).subscribe(result => {
      if (result.success) {
        swal(Appsettings.APP_NAME, result.message, 'success');
        this.showForm = false;
        this.frmActivitySchudele.reset();
        this.getActivity(this.studenGroupId);
        this.working = false;
      } else {
        swal(Appsettings.APP_NAME, result.message, 'error');
        this.working = false;
      }
    });
  }

  // Actualiza una progrmacion de examen
  updateTaskSchedule(taskSchedule) {
    this._srvActivity.update(taskSchedule).subscribe(result => {
      if (result.success) {
        swal(Appsettings.APP_NAME, result.message, 'success');
        this.showForm = false;
        this.frmActivitySchudele.reset();
        this.getActivity(this.studenGroupId);
        this.working = false;
      } else {
        swal(Appsettings.APP_NAME, result.message, 'error');
        this.working = false;
      }
    });
  }

  onClickCancel() {
    this.showQualitify = true;
    this.showForm = false;

    let totalQualification = this.totalQualification - this.ponderation;
    this.totalQualification = totalQualification;

    this.frmActivitySchudele.reset();
  }

  onClickEdit(activitie) {
    this.activitieId = activitie.id;
    this.showQualitify = false;
    this.filesIds = [];
    this.showForm = true;
    this.filesId = [];
    this.activitiesQualification = [];
    this.msg = (activitie.typeScore) ? false : true;
    this.totalQualification = 0;
    this.showTotal = (activitie.typeOfQualification) ? true : false;
    this.qualification = (activitie.typeScore) ? true : false;
    this.typeQualify = (activitie.typeOfQualification) ? '%' : 'Puntuación';

    this.activitiesQualification = this.activitySchedules.filter(x => x.schedulingPartialId === activitie.schedulingPartialId);
    if (this.activitiesQualification.length > 0) {
      for (let i of this.activitiesQualification) {
        this.totalQualification = this.totalQualification + i.assignedQualification;
      }
    }

    this.updateTypeScore();

    // Seteamos los valores en el formulario
    this.ponderation = activitie.assignedQualification;
    this.frmActivitySchudele.controls['scheduledDate'].setValue(activitie.scheduledDate);
    this.frmActivitySchudele.controls['name'].setValue(activitie.name);
    this.frmActivitySchudele.controls['description'].setValue(activitie.description);
    this.frmActivitySchudele.controls['openingDate'].setValue(activitie.openingDate);
    this.frmActivitySchudele.controls['SchedulingPartialId'].setValue(activitie.schedulingPartialId);
    this.frmActivitySchudele.controls['typeOfQualification'].setValue(activitie.typeOfQualification);
    this.frmActivitySchudele.controls['typeScore'].setValue(activitie.typeScore);
    this.frmActivitySchudele.controls['assignedQualification'].setValue(activitie.assignedQualification);
    this.frmActivitySchudele.controls['id'].setValue(activitie.id);

    for (let f of activitie.filesId) {

      this.filesIds.push(f.id);
      this.filesId.push(f.id);
    }
    this.getData();
    this.getTypeContent();
  }

  addTaskSchedule(): void {
    this.showForm = true;
    this.showQualitify = false;
    this.filesIds = [];
    this.filesId = [];
    this.activitieId = 0;

    this.qualification = false;

    this.updateTypeScore();

    this.getTypeContent();
    this.getData();

  }

  // Obtien los parciales de la planeacion
  getPartials(groupId: number) {
    this._srvExam.getSchedulingpartial(groupId).subscribe(resp => {
      this.schedulingPlartial = [];
      if (resp.success) {
        for (let p of resp.data) {
          let partial = new SchedulingPlartial();
          partial.id = p.id;
          partial.startDate = p.startDate;
          partial.endDate = p.endDate;
          partial.description = p.description;
          partial.coursePlanningId = p.coursePlanningId;

          this.schedulingPlartial.push(partial);
        }
      }
      this.working = false;
    });
  }

  // Elimina una actividad
  onClickDelete(taskScheduleId: number) {
    swal({
      title: '¿Deseas eliminar la actividad?',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: '¡Si!',
      cancelButtonText: 'Cancelar'
    }).then((res) => {
      if (res.value) {
        this.working = true;
        this._srvActivity.deletContent(taskScheduleId).subscribe(result => {
          if (result.success) {
            swal(Appsettings.APP_NAME, result.message, 'success');
            this.getActivity(this.studenGroupId);
            this.working = false;
          } else {
            swal(Appsettings.APP_NAME, result.message, 'error');
            this.working = false;
          }
        });
      }
    });
  }

  // Obtiene el listado de los tipos de contenido
  getTypeContent() {
    this._srvContent.getTypeContent().subscribe(result => {
      this.recourseType = [];
      if (result.success) {
        let data = result.data;
        for (let t of data) {
          let contentType = new TeacherContentType();
          contentType.id = t.id;
          contentType.description = t.description;
          this.recourseType.push(contentType);
        }
        this.active = this.recourseType[0].id;
      }
    });
  }

  changeFolder(folderId, contents) {
    this.contentsTeacher = [];
    this.active = folderId;
    this.visible = false;

    let contentsTeacher = this.totalContents.filter(item => item.resourceTypeId === folderId);
    if (contentsTeacher.length > 0) {
      for (let content of contentsTeacher) {
        let cont = new TeacherContent();
        cont.id = content.id;
        cont.name = content.name;
        cont.description = content.description;
        cont.registerDate = content.registerDate;
        cont.contentUrl = content.url;

        cont.selected = false;
        for (let f of this.filesId) {
          if (f === cont.id) {
            cont.selected = true;
          }
        }
        this.contentsTeacher.push(cont);
      }
      this.visible = true;
    }
  }

  // Obtiene todo el contenido del profesor
  getData() {
    this._srvContent.getAll().subscribe(result => {
      this.contentsTeacher = [];



      if (result.success) {
        // this.active = 1;
        this.totalContents = result.data;
        let contents = this.totalContents.filter(item => item.resourceTypeId === this.active);
        if (contents.length > 0) {
          for (let content of contents) {
            let cont = new TeacherContent();
            cont.id = content.id;
            cont.name = content.name;
            cont.description = content.description;
            cont.registerDate = content.registerDate;
            cont.contentUrl = content.url;

            cont.selected = false;
            for (let f of this.filesIds) {

              if (f === cont.id) {
                cont.selected = true;
              }
            }
            this.contentsTeacher.push(cont);
          }
          this.visible = true;
        }
      }
    });
  }

  onChangeAssing(id: number, event): void {

    let index = this.filesId.indexOf(id);
    if (index !== -1) {
      this.filesId.splice(index, 1);
    } else {
      this.filesId.push(id);
    }
  }

  // Abre modulo para calificar actividad
  addRating(homework) {
    // this.studenGroupId
    this._router.navigateByUrl(`/teacher/activity-qualify/${this.studenGroupId}/${homework.id}/${homework.openingDate}`);
  }

  onChagequalifications(event: boolean) {
    this.typeQualify = (event) ? '%' : 'Puntuación';
    /***
     * Var = showTotal
     * True = Porcenteje
     * False = Puntuación
     * Description: Muestra la sumatoria de todas las actividas por parcial si la calificacion es por porcentaje
     */
    this.showTotal = (event) ? true : false;

  }

  onChangeTypeScore(event) {

    this.qualification = (event) ? true : false;
    this.showTotal = false;
    if (!event) {
      this.typeQualify = 'Puntuación';
      this.frmActivitySchudele.controls['assignedQualification'].setValue('');
      this.frmActivitySchudele.controls['typeOfQualification'].setValue('');
      this.frmActivitySchudele.controls['assignedQualification'].setValidators([]);
      this.frmActivitySchudele.controls['assignedQualification'].clearValidators();
      this.frmActivitySchudele.controls['assignedQualification'].updateValueAndValidity();
      this.frmActivitySchudele.controls['assignedQualification'].markAsUntouched();
    } else {
      this.frmActivitySchudele.controls['assignedQualification'].setValidators([Validators.required]);
      this.frmActivitySchudele.controls['assignedQualification'].updateValueAndValidity();

    }
  }

  onChangePartial(partial) {
    this.partialSelect = this.schedulingPlartial.filter(x => x.id === partial.value)[0];
    this.dateInti = this.partialSelect.startDate;
    this.dateEnd = this.partialSelect.endDate;

  }

  onChangeInit(dateInitHomeWork: Date) {

    let dateInit = new Date(this.partialSelect.startDate);
    let dateInitPartial = new Date(dateInit.getFullYear(), dateInit.getMonth(), dateInit.getDate(), 0, 0, 0);

    this.dateIntiHomework = dateInitHomeWork;
    let dateEnd = new Date(this.partialSelect.endDate);
    let dateEndtPartial = new Date(dateEnd.getFullYear(), dateEnd.getMonth(), dateEnd.getDate(), 0, 0, 0);

    if (dateInitHomeWork < dateInitPartial || dateInitHomeWork > dateEndtPartial) {
      swal(Appsettings.APP_NAME, 'La fecha de inicio de la actividad no puede ser menor a la de inicio del parcial', 'warning');
      this.frmActivitySchudele.controls['scheduledDate'].setValue('');
    }
  }

  onChangeEnd(dateEndHomeWork: Date) {




    let dateInit = new Date(this.partialSelect.startDate);
    let dateInitPartial = new Date(dateInit.getFullYear(), dateInit.getMonth(), dateInit.getDate(), 0, 0, 0);

    let dateEnd = new Date(this.partialSelect.endDate);
    let dateEndtPartial = new Date(dateEnd.getFullYear(), dateEnd.getMonth(), dateEnd.getDate(), 0, 0, 0);

    if (dateEndHomeWork > dateEndtPartial || dateEndHomeWork > dateEndtPartial) {
      swal(Appsettings.APP_NAME, 'La fecha de fin de la actividad no puede ser mayor a la del parcial', 'warning');
      this.frmActivitySchudele.controls['openingDate'].setValue('');
    }

    if (this.dateIntiHomework > dateEndHomeWork) {
      swal(Appsettings.APP_NAME, 'La fecha de fin debe ser mayor a la fecha inicial', 'warning');
      this.frmActivitySchudele.controls['openingDate'].setValue('');
    }
  }

  onChangeQualification() {


    // Validamos el tipo de calificacion
    if (this.showTotal) {
      this.getQualification();
      // debugger
      // Si la ponderacion capturada es mayor a 100 se retorna error
      if (this.ponderation > 100) {
        swal(Appsettings.APP_NAME, 'El valor agregado para la calificación no puede ser mayor a 100', 'warning');
        this.ponderation = 0;
        this.getQualification();
      } else {
        // let total = Number(this.ponderation) + Number(this.totalQualification);
        if (this.totalQualification > 100) {
          swal(Appsettings.APP_NAME, 'La sumatoria de los porcentajes no puede ser mayor a 100', 'warning');
          this.ponderation = 0;
          this.getQualification();
        } else {
          // this.totalQualification = Number(this.totalQualification) + Number(this.ponderation);
          //this.getQualification();
        }
      }
    } else {
      if (this.ponderation > 100) {
        swal(Appsettings.APP_NAME, 'El valor agregado para la calificación no puede ser mayor a 100', 'warning');
        this.ponderation = 0;
      }
    }
  }

  getQualification() {
    this.activitiesQualification = this.activitySchedules.filter(x => x.schedulingPartialId === this.partialSelect.id);

    if (this.activitiesQualification.length > 0) {
      this.totalQualification = 0;

      for (let i of this.activitiesQualification) {
        let q = i.assignedQualification;
        q = (i.id === this.activitieId) ? Number(this.ponderation) : i.assignedQualification;
        this.totalQualification = this.totalQualification + q;
      }


      if (!this.activitieId) {
        this.totalQualification = this.totalQualification + Number(this.ponderation);
      }
    }
  }

  updateTypeScore() {

    if (this.activitySchedules.length > 0) {
      let task = this.activitySchedules[0];
      this.typeScoreValidate = task.typeScore;
      this.typeOfQualificationValidate = task.typeOfQualification;
    }
  }

  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;

  }
}
