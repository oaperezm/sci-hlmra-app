import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Appsettings } from '../../../configuration/appsettings';


// Componente para carga de archivos
import { FileUploader, FileItem, FileUploaderOptions, FileLikeObject } from 'ng2-file-upload';
import swal from 'sweetalert2';

// Servicios
import { StorageService } from '../../../service/shared/storage.service';
import { ContentTeacherService, ActivityService} from '../../../service/service.index';

// Modelos
import { HomeworkFile } from '../../../models/courses/homeworkFile.model';

@Component({
  selector: 'app-studen-upload-activity',
  templateUrl: './studen-upload-activity.component.html',
  styleUrls: ['./studen-upload-activity.component.scss']
})
export class StudenUploadActivityComponent implements OnInit {

  uploader;
  token: string;
  fileName: string;
  // allowedFileType =  ['image/jpeg', 'image/png'];
  maxFileSize = 1 * 1024 * 1024;
  errorMessage: string;
  frmFile: FormGroup;
  taskId: number;

  homeworkFiles: HomeworkFile[] = [];

  working: boolean;
  delivered: boolean;
  qualified: boolean;

  msg: boolean;
  comment: string;
  @Input('taskId') set homeWorkId ( value ) {
    this.taskId = value;
    this.getFilesHomeWork( this.taskId);
  }

  @Input('delivered') set homeWorkDelivered ( value ) {
    this.delivered = value;

  }

  @Input('qualified') set homeWorkQualified ( value ) {
    this.qualified = value;

  }

  // Retornamos el arreglo de id's
  @Output() closeModal: EventEmitter<boolean> = new EventEmitter<boolean>();

  // Url para carga de archivos
  url = `${Appsettings.API_ENDPOINT_FULL}/activities/uploadAnswer`;

  constructor(  private _srvStorege: StorageService,
                private _svrContent: ContentTeacherService,
                private _svrActivity: ActivityService ) {
    this.msg = true;
    this.frmFile = new FormGroup({
      'description': new FormControl('', [Validators.required]),
      'file': new FormControl('', [Validators.required])
    });

    this.uploader  = new FileUploader({
      url: this.url,
      maxFileSize: this.maxFileSize,
      allowedMimeType: [
        'application/x-zip-compressed',
        'application/zip',
        'application/msword',
        'application/msword',
        'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
        'application/vnd.openxmlformats-officedocument.wordprocessingml.template',
        'application/vnd.ms-word.document.macroEnabled.12',
        'application/vnd.ms-word.template.macroEnabled.12',
        'application/vnd.ms-excel',
        'application/vnd.ms-excel',
        'application/vnd.ms-excel',
        'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
        'application/vnd.openxmlformats-officedocument.spreadsheetml.template',
        'application/vnd.ms-excel.sheet.macroEnabled.12',
        'application/vnd.ms-excel.template.macroEnabled.12',
        'application/vnd.ms-excel.addin.macroEnabled.12',
        'application/vnd.ms-excel.sheet.binary.macroEnabled.12',
        'application/vnd.ms-powerpoint',
        'application/vnd.ms-powerpoint',
        'application/vnd.ms-powerpoint',
        'application/vnd.ms-powerpoint',
        'application/vnd.openxmlformats-officedocument.presentationml.presentation',
        'application/vnd.openxmlformats-officedocument.presentationml.template',
        'application/vnd.openxmlformats-officedocument.presentationml.slideshow',
        'application/vnd.ms-powerpoint.addin.macroEnabled.12',
        'application/vnd.ms-powerpoint.presentation.macroEnabled.12',
        'application/vnd.ms-powerpoint.presentation.macroEnabled.12',
        'application/vnd.ms-powerpoint.slideshow.macroEnabled.12',
        'image/photoshop',
        'image/x-photoshop',
        'image/psd',
        'application/photoshop',
        'application/psd',
        'zz-application/zz-winassoc-psd',
        'application/x-gtar',
        'application/x-gcompress',
        'application/compress',
        'application/x-tar',
        'application/x-rar-compressed',
        'application/octet-stream',
        'application/zip'
      ]
    });
    this.uploader.onWhenAddingFileFailed = (item, filter, options) => this.onWhenAddingFileFailed(item, filter, options);

  }

  ngOnInit() {
  }

  // Obtiene todos los archivos de la tarea
  getFilesHomeWork( homeWorkId: number ) {
    this.working = true;
    this.homeworkFiles = [];
    this._svrActivity.getFilesStudent( homeWorkId ).subscribe( result => {
      if ( result.success ) {
        for ( let f of result.data ) {
          let file = new HomeworkFile();
          file.id = f.id;
          file.url = f.url;
          file.comment = f.comment;
          file.registerDate = f.registerDate;

          this.homeworkFiles.push( file );
        }
      }
      this.working = false;
    });
  }

  // Metodo envia el archivo al storage
  onSaveResoruce() {
    this.working = true;
    this._svrContent.validateToken().subscribe( resutl => {
      if ( resutl.status === 200) {
        this.token = this._srvStorege.token;
        this.uploader.setOptions({
          authTokenHeader: 'Authorization',
          authToken: 'Bearer ' + this.token
        });

        // Metodo para el envio de archivos
        this.uploader.onBeforeUploadItem = (item) => {
            item.withCredentials = false;
            this.uploader.options.additionalParameter = {
              activityId: this.taskId,
              comment: this.frmFile.controls['description'].value,
            // Description: this.frmContent.controls['resourceDescription'].value,
            // TeacherResourceId: this.frmContent.controls['typeFile'].value
          };
        };

        this.uploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
          if ( response) {
            let response_json = JSON.parse(response);
            if ( response_json['success'] ) {
              this.onCloseConfirm( true );
              swal( Appsettings.APP_NAME, response_json['message'], 'success');
            } else {
              this.onCloseConfirm( false );
              swal( Appsettings.APP_NAME, response_json['message'], 'warning');
            }
          } else {
            swal( Appsettings.APP_NAME,'La carga de archivo fue cancelada', 'warning');
          }

          // this.resetClassForm();
        };
        this.uploader.uploadAll();
      } else {
        console.log( 'El token no esta activo');
      }
      this.working = false;
    });
  }

  onChangeFile( e) {
    this.fileName = e;
  }

  onWhenAddingFileFailed(item: FileLikeObject, filter: any, options: any) {
    (<HTMLInputElement>document.getElementById('file')).value = '';
    this.fileName = '';
    this.uploader.queue.splice(0, 1);
    this.frmFile.controls['file'].setValue('');
    switch (filter.name) {
        case 'fileSize':
            this.errorMessage = `El tamaño del archivo excede el máximo permitido de 10mb.`;
            swal( Appsettings.APP_NAME, this.errorMessage , 'warning');
            break;
        case 'mimeType':
            this.errorMessage = `El tipo de archivo seleccionado no es permitido, solo se permiten archivos .zip`;
            swal( Appsettings.APP_NAME, this.errorMessage , 'warning');
            break;
        default:
            this.errorMessage = `Unknown error (filter is ${filter.name})`;
    }
  }

  onCloseConfirm( event: boolean) {
    setTimeout(() => {
      this.closeModal.emit( event  );
    }, 2000);
  }

  onDeleteContent( homeworkFileId: number) {
    this._svrActivity.deleteFilesHomerwork( this.taskId, homeworkFileId ).subscribe( result => {
      if ( result.success ) {

      this.getFilesHomeWork( this.taskId);
      }
    });
  }

  onChangeDes( ){
    this.msg = (this.comment !== '' ) ? false : true;
    }
}
