import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';

// Servicios
import {  HomeworksService,
          ContentTeacherService,
          PlanningService,
          CourseService,
          ExamScheduleService } from '../../../service/service.index';

// Modelos
import { TaskSchedule} from '../../../models/courses/taskSchedule.model';
import { TeacherContentType } from '../../../models/courses/teacherContentType.model';
import { TeacherContent } from '../../../models/courses/teacheContent.model';
import { SchedulingPlartial } from '../../../models/courses/schedulingPartial.model';
import swal from 'sweetalert2';
import { Appsettings } from '../../../configuration/appsettings';

@Component({
  selector: 'app-tasks-schedule',
  templateUrl: './tasks-schedule.component.html',
  styleUrls: ['./tasks-schedule.component.scss']
})
export class TasksScheduleComponent implements OnInit {

  working: boolean;
  showForm: boolean;
  showQualitify: boolean;
  frmTaskSchudele: FormGroup;

  taskQualification: TaskSchedule[] = [];
  taskSchedules: TaskSchedule[] = [];
  recourseType: TeacherContentType[] = [];
  contentsTeacher: TeacherContent[] = [];
  totalContents: any[] = [];
  active: number;
  visible: boolean;
  studenGroupId: number;
  filesId: number[] = [];
  filesIds: number[] = [];
  qualification: boolean;
  qualifications: boolean;

  homeworkPer: number;

  schedulingPlartial: SchedulingPlartial[] = [];
  partialSelect: SchedulingPlartial;
  ponderation: number;

  typeQualify: string;
  msg: boolean;
  totalQualification: number;
  showTotal: boolean;
  homeworkId: number;

  dateInti: Date;
  dateIntiHomework: Date;
  dateEnd: Date;

  typeScoreValidate: boolean;
  typeOfQualificationValidate: number;
  newTask: boolean;
  // Retornamos la cantidad de tareas
  @Output() totalHomework: EventEmitter<number> = new EventEmitter<number>();

  constructor(  private _srvHomework: HomeworksService,
                private _srvContent: ContentTeacherService,
                private _router: Router,
                private _srvPlanning: PlanningService,
                private _srvCourse: CourseService,
                private _srvExam: ExamScheduleService ) {

    this.working = false;
    this.showForm = false;
    this.showQualitify = true;
    this.msg = true;
    this.totalQualification = 0;

    // this.ponderation = 0;
    let group = JSON.parse(localStorage.getItem('ra.group'));
    let course = JSON.parse(localStorage.getItem('ra.course'));


    this.getPlanning( group.id );
    this.getPartials( group.id );
    this.frmTaskSchudele = new FormGroup({
      id: new FormControl(0),
      scheduledDate: new FormControl('', [Validators.required]),
      name: new FormControl('', [Validators.required, Validators.maxLength(350)]),
      description: new FormControl('', [Validators.required]),
      openingDate: new FormControl(''),
      assignedQualification: new FormControl(''),
      SchedulingPartialId: new FormControl('', [Validators.required]),
      typeOfQualification: new FormControl(),
      typeScore: new FormControl()
    });
  }

  ngOnInit() {
  }

  // Obtien los parciales de la planeacion
  getPartials( groupId: number) {
    this._srvExam.getSchedulingpartial( groupId ).subscribe( resp => {
      this.schedulingPlartial = [];
      if ( resp.success) {
        for ( let p of resp.data){
          let partial = new SchedulingPlartial();

          partial.id = p.id;
          partial.startDate = p.startDate;
          partial.endDate = p.endDate;
          partial.description = p.description;
          partial.coursePlanningId = p.coursePlanningId;

          this.schedulingPlartial.push( partial  );
        }
      }
    });
  }

  // Obtiene todas la programacions de tarea del grupo
  getTask( groupId: number) {
    this.working = true;
    this.studenGroupId = groupId;
    this.taskSchedules = [];

    this._srvHomework.getAll( groupId ).subscribe( result => {
      this.getTypeContent();
      this.getData();
      if ( result.success ) {
        this.totalQualification = 0;
        for (let h of result.data ) {
          let homework = new TaskSchedule();
          homework.id = h.id;
          homework.name = h.name;
          homework.description = h.description;
          homework.scheduledDate = h.scheduledDate;
          homework.filesId = h.files;
          homework.openingDate = h.openingDate;
          homework.assignedQualification = h.assignedQualification;
          homework.SchedulingPartialId = h.schedulingPartialId;
          homework.typeOfQualification = h.typeOfQualification;
          homework.typeScore = h.typeScore;
          this.taskSchedules.push( homework );
          if ( h.typeOfQualification ){
            this.totalQualification = this.totalQualification + h.assignedQualification;

          }
        }
        setTimeout(() => {
          this.totalHomework.emit( this.taskSchedules.length  );
        }, 2000);
      }
      this.working = false;
    });
  }

  // Se ejecuta cuando se da guardar en el formulario de programacion de taras
  onSubmit(): void {
    this.working = true;

    let taskSchedule = new TaskSchedule();

    taskSchedule.scheduledDate = this.frmTaskSchudele.controls['scheduledDate'].value;
    taskSchedule.name = this.frmTaskSchudele.controls['name'].value;
    taskSchedule.description = this.frmTaskSchudele.controls['description'].value;
    taskSchedule.studentGroupId = this.studenGroupId;
    taskSchedule.filesId = this.filesId;
    taskSchedule.openingDate = this.frmTaskSchudele.controls['openingDate'].value;

    let typeOfQualification = ( this.frmTaskSchudele.controls['typeOfQualification'].value) ? 1 : 0;
    taskSchedule.typeOfQualification = typeOfQualification;
    // El valor de la calificación
    // tslint:disable-next-line:max-line-length
    let assignedQualification = (this.frmTaskSchudele.controls['assignedQualification'].value) ? this.frmTaskSchudele.controls['assignedQualification'].value : 0;
    taskSchedule.assignedQualification = assignedQualification;
    taskSchedule.SchedulingPartialId = this.frmTaskSchudele.controls['SchedulingPartialId'].value;

    taskSchedule.id = this.frmTaskSchudele.controls['id'].value;
    let typeScore = (this.frmTaskSchudele.controls['typeScore'].value) ? true : false;
    taskSchedule.typeScore = typeScore;

    let SumOfPercentages = this.totalQualification;
    let maxSumOfPercentages = 100;


    if ( this.taskSchedules.length > 0 ) {
      let bool = ( taskSchedule.typeOfQualification === 0 ) ? false : true;
      let bool2 = ( this.typeOfQualificationValidate ) ? true : false;
      if ( taskSchedule.typeScore !== this.typeScoreValidate || bool !== bool2 ){
        let msg = '';
        if ( ! this.typeScoreValidate ) {
          msg = 'Conforme a la configuración de las otras tareas la calificación debe ser por "Cumplimiento".';
        } else if( !this.typeOfQualificationValidate ) {
          msg = 'Conforme a la configuración de las otras tareas la calificación debe ser por "Puntuación".';
        } else {
          msg = 'Conforme a la configuración de las otras tareas la calificación debe ser por "Porcentaje".';
        }
        swal( Appsettings.APP_NAME , msg, 'error');
        this.working = false;
        return;
      }
    }

    if (SumOfPercentages > maxSumOfPercentages) {
      let msg = '';
      msg = 'No se puede guardar otra tarea ya que el porcentaje acumulado no puede ser mayor o igual al 100%".';
      swal(Appsettings.APP_NAME, msg, 'error');
      this.working = false;
      return;
    }


    if ( taskSchedule.id  > 0 ) {
      this.updateTaskSchedule( taskSchedule );
    } else {
      this.saveTaskSchedule( taskSchedule );
    }

  }

  // Guarda una nueva programacion de examen
  saveTaskSchedule( taskSchedule ) {
    this._srvHomework.save( taskSchedule ).subscribe( result => {
      if ( result.success ) {
        swal( Appsettings.APP_NAME, result.message, 'success');
        this.showForm = false;
        this.frmTaskSchudele.reset();
        this.getTask( this.studenGroupId );
        this.working = false;
      } else {
        swal( Appsettings.APP_NAME , result.message, 'error');
        this.working = false;
      }
    });
  }

  // Actualiza una progrmacion de examen
  updateTaskSchedule( taskSchedule ) {
    this._srvHomework.update( taskSchedule, taskSchedule.id ).subscribe( result => {
      if ( result.success ) {
        swal( Appsettings.APP_NAME, result.message, 'success');
        this.showForm = false;
        this.frmTaskSchudele.reset();
        this.getTask( this.studenGroupId );
        this.working = false;
      } else {
        swal( Appsettings.APP_NAME, result.message, 'error');
        this.working = false;
      }
    });
  }
  onClickCancel() {
    this.showQualitify = true;
    this.showForm = false;

    let totalQualification = this.totalQualification - this.ponderation;
    this.totalQualification = totalQualification;
    this.frmTaskSchudele.reset();


  }

  onClickEdit( task ) {

    this.homeworkId = task.id;

    this.taskQualification = [];
    this.showQualitify = false;
    this.filesIds = [];
    this.showForm = true;
    this.filesId = [];
    this.msg = (task.typeScore) ? false : true;
    this.qualification = ( task.typeScore ) ? true : false;
    this.typeQualify = ( task.typeOfQualification ) ? '%' : 'Puntuación';

    this.showTotal = ( task.typeOfQualification) ? true : false;

    this.totalQualification = 0;

    this.qualification = ( task.typeScore ) ? true : false;
    this.taskQualification = this.taskSchedules.filter( x => x.SchedulingPartialId === task.SchedulingPartialId);
    if( this.taskQualification.length > 0 ) {
      for ( let i of this.taskQualification  ) {
        this.totalQualification = this.totalQualification + i.assignedQualification;
      }
    }

    this.updateTypeScore();


    this.ponderation = task.assignedQualification;
    this.frmTaskSchudele.controls['id'].setValue( task.id );
    this.frmTaskSchudele.controls['name'].setValue( task.name );
    this.frmTaskSchudele.controls['description'].setValue( task.description );
    this.frmTaskSchudele.controls['scheduledDate'].setValue( task.scheduledDate );
    this.frmTaskSchudele.controls['openingDate'].setValue( task.openingDate );
    this.frmTaskSchudele.controls['assignedQualification'].setValue( task.assignedQualification );
    this.frmTaskSchudele.controls['SchedulingPartialId'].setValue( task.SchedulingPartialId );
    this.frmTaskSchudele.controls['typeOfQualification'].setValue( task.typeOfQualification );
    this.frmTaskSchudele.controls['typeScore'].setValue( task.typeScore );


    for ( let f of task.filesId) {

      this.filesIds.push( f.id );
      this.filesId.push( f.id );
    }
    this.getData();
  }

  addTaskSchedule(): void {
    this.showForm = true;
    this.showQualitify = false;
    this.filesIds = [];
    this.filesId = [];
    this.updateTypeScore();
    this.qualification = false;
  }

  onClickDelete( taskScheduleId: number ) {
    swal({
      title: '¿Deseas eliminar la tarea?',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: '¡Si!',
      cancelButtonText: 'Cancelar'
    }).then((res) => {
      if ( res.value) {
        this.working = true;
        this._srvHomework.deletContent( taskScheduleId ).subscribe( result => {
          if ( result.success ) {
            swal( Appsettings.APP_NAME , result.message, 'success');
            this.getTask( this.studenGroupId );
            this.working = false;
          } else {
            swal( Appsettings.APP_NAME, result.message, 'error');
            this.working = false;
          }
        });
      }
    });
  }

  // Obtiene el listado de los tipos de contenido
  getTypeContent() {
    this._srvContent.getTypeContent().subscribe( result => {
      this.recourseType = [];
      if ( result.success ) {
        let data = result.data;
        for ( let t of data ) {
          let contentType = new TeacherContentType();
          contentType.id = t.id;
          contentType.description = t.description;
          this.recourseType.push( contentType );
        }
        this.active = this.recourseType[0].id;
      }
    });
  }

  changeFolder( folderId, contents ) {
    this.contentsTeacher = [];
    this.active = folderId;
    this.visible = false;

    let contentsTeacher = this.totalContents.filter( item => item.resourceTypeId === folderId );
    if ( contentsTeacher.length > 0) {
      for ( let content of contentsTeacher) {
        let cont = new TeacherContent();
        cont.id = content.id;
        cont.name = content.name;
        cont.description = content.description;
        cont.registerDate = content.registerDate;
        cont.contentUrl = content.url;

        cont.selected = false;
        for ( let f of this.filesId ) {
          if ( f === cont.id) {
            cont.selected = true;
          }
        }
        this.contentsTeacher.push( cont );
      }
      this.visible = true;
    }
  }

  // Obtiene todo el contenido del profesor
  getData() {
    this._srvContent.getAll().subscribe( result => {
      this.contentsTeacher = [];
      if ( result.success ) {
        // this.active = 1;
        this.totalContents = result.data;
        let contents = this.totalContents.filter( item => item.resourceTypeId === this.active);
        if ( contents.length > 0 ) {
          for ( let content of contents) {
            let cont = new TeacherContent();
            cont.id = content.id;
            cont.name = content.name;
            cont.description = content.description;
            cont.registerDate = content.registerDate;
            cont.contentUrl = content.url;

            cont.selected = false;
            for ( let f of this.filesIds ) {
              if ( f === cont.id) {
                cont.selected = true;
              }
            }
            this.contentsTeacher.push( cont );
          }
          this.visible = true;
        }
      }
    });
  }

  onChangeAssing( id: number, event): void {

    let index = this.filesId.indexOf(id);
    if ( index !== -1 )  {
      this.filesId.splice(index, 1);
    } else {
      this.filesId.push( id );
    }
  }

  // Abre modulo para calificar tareas
  addRating( homework) {

    // this.studenGroupId
    this._router.navigateByUrl(`/teacher/homework-qualify/${this.studenGroupId}/${homework.id}/${homework.openingDate}`);

  }


  // Obtiene planeacion
  getPlanning( groupId: number ) {
    this._srvCourse.getPlanning( groupId).subscribe( result => {
      let planning = result.data[0];
      this.homeworkPer = planning.homework;
    });
  }

  onChangePartial( partial ) {

    this.partialSelect = this.schedulingPlartial.filter( x => x.id === partial.value)[0];
    this.dateInti = this.partialSelect.startDate;
    this.dateEnd = this.partialSelect.endDate;
  }

  onChangeInit( dateInitHomeWork: Date) {

    let dateInit = new Date(this.partialSelect.startDate);
    let dateInitPartial = new Date(  dateInit.getFullYear(), dateInit.getMonth(), dateInit.getDate(), 0, 0, 0 );

    this.dateIntiHomework = dateInitHomeWork;
    let dateEnd = new Date(this.partialSelect.endDate);
    let dateEndtPartial = new Date(  dateEnd.getFullYear(), dateEnd.getMonth(), dateEnd.getDate(), 0, 0, 0 );


    if ( dateInitHomeWork < dateInitPartial || dateInitHomeWork > dateEndtPartial ) {
      swal( Appsettings.APP_NAME, 'La fecha de inicio debe estar en el rango de fechas del parcial', 'warning');
      this.frmTaskSchudele.controls['scheduledDate'].setValue('');
    }
  }

  onChangeEnd( dateEndHomeWork: Date) {

    let dateInit = new Date(this.partialSelect.startDate);
    let dateInitPartial = new Date(  dateInit.getFullYear(), dateInit.getMonth(), dateInit.getDate(), 0, 0, 0 );

    let dateEnd = new Date(this.partialSelect.endDate);
    let dateEndtPartial = new Date(  dateEnd.getFullYear(), dateEnd.getMonth(), dateEnd.getDate(), 0, 0, 0 );

    if ( dateEndHomeWork < dateInitPartial || dateEndHomeWork > dateEndtPartial ) {
      swal( Appsettings.APP_NAME, 'La fecha de fin debe estar en el rango de fechas del parcial', 'warning');
      this.frmTaskSchudele.controls['openingDate'].setValue('');
    }

    if ( this.dateIntiHomework > dateEndHomeWork ){
      swal( Appsettings.APP_NAME, 'La fecha de fin debe ser mayor a la fecha inicial', 'warning');
      this.frmTaskSchudele.controls['openingDate'].setValue('');
    }
  }

  onChagequalifications( event: boolean ) {
    this.typeQualify = ( event ) ? '%' : 'Puntuación';
    this.showTotal = ( event ) ? true : false;

  }

  onChangeTypeScore ( event ) {

    this.qualification = ( event ) ? true : false;
    this.showTotal =  false;

    if ( !event ) {
      this.typeQualify = 'Puntuación';

      this.frmTaskSchudele.controls['assignedQualification'].setValue('');
      this.frmTaskSchudele.controls['typeOfQualification'].setValue('');
      this.frmTaskSchudele.controls['assignedQualification'].setValidators([]);
      this.frmTaskSchudele.controls['assignedQualification'].clearValidators();
      this.frmTaskSchudele.controls['assignedQualification'].updateValueAndValidity();
      this.frmTaskSchudele.controls['assignedQualification'].markAsUntouched();
    }else {
      this.frmTaskSchudele.controls['assignedQualification'].setValidators([Validators.required]);
      this.frmTaskSchudele.controls['assignedQualification'].updateValueAndValidity();

    }





  }

  onChangeQualification( ) {

    // Validamos el tipo de calificacion
    if ( this.showTotal) {
      this.getQualification();
      // debugger
      // Si la ponderacion capturada es mayor a 100 se retorna error
      if ( this.ponderation > 100) {
        swal( Appsettings.APP_NAME, 'El valor agregado para la calificación no puede ser mayor a 100', 'warning');
        this.ponderation = 0;
        this.getQualification();
      } else {
        // let total = Number(this.ponderation) + Number(this.totalQualification);
        if ( this.totalQualification >  100 ) {
          swal( Appsettings.APP_NAME, 'La sumatoria de los porcentajes no puede ser mayor a 100', 'warning');
          this.ponderation = 0;
          this.getQualification();
        } else {
          // this.totalQualification = Number(this.totalQualification) + Number(this.ponderation);
          //this.getQualification();
        }
      }
    } else {
      if ( this.ponderation > 100) {
        swal( Appsettings.APP_NAME, 'El valor agregado para la calificación no puede ser mayor a 100', 'warning');
        this.ponderation = 0;
      }
    }
  }

  getQualification() {


    // Obtenemos todas las tareas del parcial
    this.taskQualification = this.taskSchedules.filter( x => x.SchedulingPartialId === this.partialSelect.id);

    // Si tiene tareas hacemos la sumatoria de todas las tareas
    if( this.taskQualification.length > 0 ) {
      this.totalQualification = 0;

      for ( let i of this.taskQualification  ) {
        let q = i.assignedQualification;
        q = ( i.id === this.homeworkId ) ? Number(this.ponderation) : i.assignedQualification;
        this.totalQualification = this.totalQualification + q;
      }

      if ( !this.homeworkId )
      {
        this.totalQualification = this.totalQualification + Number( this.ponderation);
      }
    }
  }

  updateTypeScore() {
    if ( this.taskSchedules.length > 0){
      let task = this.taskSchedules[0];
      this.typeScoreValidate = task.typeScore;
      this.typeOfQualificationValidate = task.typeOfQualification;
    }
  }

  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;

  }
}
