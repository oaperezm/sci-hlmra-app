
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule} from '@angular/forms';
import { CommonModule } from '@angular/common';
import { CdkTreeModule } from '@angular/cdk/tree';
import { CdkTableModule } from '@angular/cdk/table';

// MODULES
import { CoreModule } from '../../core/core.module';
import { MatTreeModule } from '@angular/material/tree';
import { HttpClientModule} from '@angular/common/http';
import { OwlModule } from 'ngx-owl-carousel';
import { PdfJsViewerModule } from 'ng2-pdfjs-viewer'; // <-- Import PdfJsViewerModule module


import {
  MatInputModule,
  MatDatepickerModule,
  MatRadioModule,
  MatCardModule,
  MatSelectModule,
  MatDialogModule,
  MatIconModule,
  MatButtonModule,
  MatCheckboxModule,
  MatTableModule,
  MatMenuModule,
  MatPaginatorModule,
  MatNativeDateModule,
  MatSlideToggleModule,
  MatToolbarModule,
  MatDividerModule,
  MatListModule,
  MatProgressBarModule,
  MatTooltipModule,
  MatPaginatorIntl,
  MatBadgeModule,
  MatTabsModule,
  MatExpansionModule,
  MatSidenavModule,
  MatProgressSpinnerModule,
  MatSliderModule
} from '@angular/material';

import { ViewerAudioComponent } from './multimedia/viewer-audio/viewer-audio.component';
import { AudioPlayerComponent } from './multimedia/audio-player/audio-player.component';
import { ViewerFileComponent } from './multimedia/viewer-file/viewer-file.component';
import { ViewerEpubComponent } from './multimedia/viewer-epub/viewer-epub.component';
import { ViewerVideoComponent } from './multimedia/viewer-video/viewer-video.component';
import { ShowMultimediaComponent } from './multimedia/show-multimedia/show-multimedia.component';
import { AttendancesListComponent } from './attendances-list/attendances-list.component';
import { GroupNavComponent } from './group-nav/group-nav.component';
import { ExamRatingsComponent } from './exam-ratings/exam-ratings.component';
import { StudenAnswersComponent } from './studen-answers/studen-answers.component';

// COMPONENTS
import { ExamSchedulesComponent } from './exam-schedules/exam-schedules.component';
import { ExamSelectQuestionsComponent } from './exam-select-questions/exam-select-questions.component';
import { DialogGroupComponent } from '../../dialogs/dialog-group/dialog-group.component';
import { DialogCourseComponent } from '../../dialogs/dialog-course/dialog-course.component';
import { AddThreadComponent } from './add-thread/add-thread.component';

// DIALOGS
import { DialogExamScheduleComponent } from '../../dialogs/dialog-exam-schedule/dialog-exam-schedule.component';
import { InterestSitesComponent } from './interest-sites/interest-sites.component';
import { ShowSitesInterestComponent } from './show-sites-interest/show-sites-interest.component';
import { TasksScheduleComponent } from './tasks-schedule/tasks-schedule.component';
import { ActivitysScheduleComponent } from './activitys-schedule/activitys-schedule.component';
import { ResourcesPlanningComponent } from './resources-planning/resources-planning.component';
import { StudenUploadHomeworkComponent } from './studen-upload-homework/studen-upload-homework.component';
import { StudenGetFileComponent } from './studen-get-file/studen-get-file.component';

import { FileUploadModule } from 'ng2-file-upload';
import { HomeworkRatingsComponent } from './homework-ratings/homework-ratings.component';
import { StudenGetFileActivityComponent } from './studen-get-file-activity/studen-get-file-activity.component';
import { StudenUploadActivityComponent } from './studen-upload-activity/studen-upload-activity.component';
import { ActivitiesRatingsComponent } from './activities-ratings/activities-ratings.component';
import { TeacherRepositoryComponent } from './teacher-repository/teacher-repository.component';
import { EpubReaderComponent } from './multimedia/epub-reader/epub-reader.component';
import { AngularEpubViewerModule } from 'src/app/core/angular-epub-viewer/src/public_api';
// import { AngularEpubViewerComponent } from '../../core/angular-epub-viewer/src/angularEpubViewer.component';
import { NgxAudioPlayerModule } from 'ngx-audio-player';
import { JoyrideModule } from 'ngx-joyride';

@NgModule({
  declarations: [
    ViewerAudioComponent,
    AudioPlayerComponent,
    ViewerFileComponent,
    ViewerVideoComponent,
    ViewerEpubComponent,
    ShowMultimediaComponent,
    AttendancesListComponent,
    GroupNavComponent,
    DialogGroupComponent,
    DialogCourseComponent,
    AddThreadComponent,
    ExamSelectQuestionsComponent,
    ExamSchedulesComponent,
    DialogExamScheduleComponent,
    ExamRatingsComponent,
    StudenAnswersComponent,
    InterestSitesComponent,
    ShowSitesInterestComponent,
    TasksScheduleComponent,
    ActivitysScheduleComponent,
    ResourcesPlanningComponent,
    StudenUploadHomeworkComponent,
    StudenGetFileComponent,
    HomeworkRatingsComponent,
    StudenGetFileActivityComponent,
    StudenUploadActivityComponent,
    ActivitiesRatingsComponent,
    TeacherRepositoryComponent,
    EpubReaderComponent,

    // AngularEpubViewerComponent
  ],
  exports: [
    ViewerAudioComponent,
    AudioPlayerComponent,
    ViewerFileComponent,
    ViewerVideoComponent,
    ViewerEpubComponent,
    HttpClientModule,
    ShowMultimediaComponent,
    GroupNavComponent,
    AttendancesListComponent,
    AddThreadComponent,
    ExamSelectQuestionsComponent,
    ExamSchedulesComponent,
    ExamRatingsComponent,
    StudenAnswersComponent,
    InterestSitesComponent,
    ShowSitesInterestComponent,
    TasksScheduleComponent,
    ActivitysScheduleComponent,
    ResourcesPlanningComponent,
    StudenUploadHomeworkComponent,
    StudenGetFileComponent,
    HomeworkRatingsComponent,
    StudenGetFileActivityComponent,
    StudenUploadActivityComponent,
    ActivitiesRatingsComponent,
    TeacherRepositoryComponent,
    EpubReaderComponent,
    // AngularEpubViewerComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    OwlModule,
    ReactiveFormsModule,
    CdkTableModule,
    MatInputModule,
    MatCardModule,
    MatDatepickerModule,
    MatRadioModule,
    MatSelectModule,
    MatCheckboxModule,
    MatDialogModule,
    MatIconModule,
    MatButtonModule,
    MatTableModule,
    MatMenuModule,
    MatPaginatorModule,
    MatTreeModule,
    MatNativeDateModule,
    MatSlideToggleModule,
    MatToolbarModule,
    MatDividerModule,
    MatListModule,
    MatProgressBarModule,
    MatTooltipModule,
    CdkTreeModule,
    MatBadgeModule,
    MatExpansionModule,
    MatSidenavModule,
    MatSliderModule,
    CoreModule,
    HttpClientModule,
    MatProgressSpinnerModule,
    FileUploadModule,
    PdfJsViewerModule, //libreria pdf
    AngularEpubViewerModule,
    JoyrideModule.forChild(),
    NgxAudioPlayerModule
    ],
  entryComponents: [
    DialogGroupComponent,
    DialogCourseComponent,
    DialogExamScheduleComponent
  ]
})
export class AdmonComponentModule {}
