import { Component, OnInit } from '@angular/core';

// Servicios
import { HeaderService } from './header.service';
import { Router } from '@angular/router';

// Models
import { NewHome } from 'src/app/models/home/new.model';
import { GeneralEvent } from 'src/app/models/home/general-event.model';
import { TipHome } from 'src/app/models/home/tip.model';

// Services
import { NewsHomeService, TipsHomeService, GeneralEventService } from '../../../../service/service.index';

declare function scrollingToElement(element): any;

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  constructor(public _headerSrv: HeaderService,
    private _router: Router,
    private _srvNews: NewsHomeService,
    private _srvTip: TipsHomeService,
    private _srvEvent: GeneralEventService) {

  }

  ngOnInit() {
     this.getNews();
     this.getTips();
     this.getEvent();
  }

  onClickNav(id: string, url: string) {

    if (this._router.url.indexOf('inicio') > - 1) {
      scrollingToElement(id);
    } else {
      this._router.navigateByUrl(url);
    }
  }

  getNews() {
    this._headerSrv.news = [];
    this._srvNews.getAll(1, 3, '').subscribe(result => {

      if (result.success) {

        for (const n of result.data) {
          const newHome = new NewHome();

          newHome.id = n.id;
          newHome.description = n.description;
          newHome.published = n.published;
          newHome.registerDate = new Date(n.registerDate);
          newHome.title = n.title;
          newHome.urlImage = n.urlImage;

          this._headerSrv.news.push(newHome);
        }
      }

    });
  }

  getTips() {

    this._srvTip.getAll(1, 3, '').subscribe(result => {

      this._headerSrv.tips = [];
      if (result.success) {
        for (const t of result.data) {
          const tip = new TipHome();

          tip.id = t.id;
          tip.description = t.description;
          tip.published = t.published;
          tip.registerDate = new Date(t.registerDate);
          tip.name = t.name;
          tip.urlImage = t.urlImage;
          this._headerSrv.tips.push(tip);
        }
      }
    }, result => {
    });


  }

  // Cambiar de página
  getEvent() {

    this._srvEvent.getAll(1, 5, '').subscribe(result => {

      this._headerSrv.generalEvents = [];
      let index = 1;

      if (result.success) {

        for (const n of result.data) {
          const event = new GeneralEvent();

          event.id = n.id;
          event.description = n.description;
          event.published = n.published;
          event.eventDate = new Date(n.eventDate);
          event.name = n.name;
          event.hours = n.hours;
          event.place = n.place;
          event.urlImage = n.urlImage;
          this._headerSrv.generalEvents.push(event);
          index++;
        }
      }
    });
  }
}
