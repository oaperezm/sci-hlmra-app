import { Injectable, Output, EventEmitter } from '@angular/core';
import { NewHome } from 'src/app/models/home/new.model';
import { TipHome } from 'src/app/models/home/tip.model';
import { GeneralEvent } from 'src/app/models/home/general-event.model';

@Injectable({
  providedIn: 'root'
})

export class HeaderService {

  showButton = true;
  news: NewHome[] = [];
  tips: TipHome[] = [];
  generalEvents: GeneralEvent[] = [];

  @Output() change: EventEmitter<boolean> = new EventEmitter();

  showReturnButton( visible: boolean)  {
    this.showButton = visible;
  }


}
