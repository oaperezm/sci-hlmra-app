import { NgModule} from '@angular/core';
import { CommonModule} from '@angular/common';

import { RouterModule } from '@angular/router';
import { FooterComponent } from './footer/footer.component';
import { HeaderComponent } from './header/header.component';

// Servicios
import { HeaderService } from './header/header.service';
import { InformationComponent } from './information/information.component';
@NgModule({
  declarations: [
    FooterComponent,
    HeaderComponent,
    InformationComponent
  ],
  exports: [
    FooterComponent,
    HeaderComponent,
    InformationComponent
  ],
  imports: [
    RouterModule,
    CommonModule
  ],
  providers: [
    HeaderService
  ]
})

export class AnonymousModule {}
