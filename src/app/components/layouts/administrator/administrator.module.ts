import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

// COMPONENTS
import { AdminFooterComponent } from './admin-footer/admin-footer.component';
import { AdminHeaderComponent } from './admin-header/admin-header.component';
import { AdminNavComponent } from './admin-nav/admin-nav.component';

// SERVICES
import { AdminHeaderService  } from './admin-header/admin-header.service';
import { AdminNavService  } from './admin-nav/admin-nav.service';

//  Modulos
import { AdmonComponentModule  } from '../../administrator/admon-component.module';
import { CoreModule } from '../../../core/core.module';

// MATERIALIZE
import { MatTabsModule,
  MatInputModule,
  MatDatepickerModule,
  MatRadioModule,
  MatCardModule,
  MatSelectModule,
  MatDialogModule,
  MatIconModule,
  MatButtonModule,
  MatCheckboxModule,
  MatTableModule,
  MatMenuModule,
  MatPaginatorModule,
  MatNativeDateModule,
  MatSlideToggleModule,
  MatToolbarModule } from '@angular/material';
import { MenuListItemComponent } from './menu-list-item/menu-list-item.component';
import { TopNavComponent } from './top-nav/top-nav.component';
import { JoyrideModule } from 'ngx-joyride';

@NgModule({
  declarations: [
    AdminHeaderComponent,
    AdminFooterComponent,
    AdminNavComponent,
    MenuListItemComponent,
    TopNavComponent,
  ],
  exports: [
    AdminFooterComponent,
    AdminHeaderComponent,
    AdminNavComponent,
    MenuListItemComponent,
    TopNavComponent,
  ],
  imports: [
    RouterModule,
    CommonModule,
    MatTabsModule,
    MatInputModule,
    MatDatepickerModule,
    MatRadioModule,
    MatCardModule,
    MatSelectModule,
    MatDialogModule,
    MatIconModule,
    MatButtonModule,
    MatCheckboxModule,
    MatTableModule,
    MatMenuModule,
    MatPaginatorModule,
    MatNativeDateModule,
    MatSlideToggleModule,
    MatToolbarModule,
    AdmonComponentModule,
    CoreModule,
    JoyrideModule.forChild(),
  ],
  providers: [
    AdminHeaderService,
    AdminNavService
  ]
})

export class AdministratorModule {}
