import { Injectable, ElementRef } from '@angular/core';
import { StorageService } from '../../../../service/service.index';
import { MatSidenav } from '@angular/material';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AdminNavService {

  openedMenu = true;
  title: string;
  urlReturn: string[];
  notifications: any[];
  private sidenav: MatSidenav;

  constructor( private _storageSrv: StorageService,
               private _router: Router ) {
    this.title = '';
    this.notifications = [];
  }

  setTitle( title: string ): void {
    this.title = title;
  }

  public setSidenav(sidenav: MatSidenav) {
    this.sidenav = sidenav;
  }

  setUrlBackButton( url: string[] ): void {
    this.urlReturn = url;
  }

  onClickBackButton() {
    this._router.navigateByUrl( this.urlReturn[0] );
    this.urlReturn = [];
  }

  onOpenMenu ( ) {
    this.sidenav.toggle();
  }

  setNotifications() {
    this.notifications = JSON.parse(this._storageSrv.notifications);
  }

}
