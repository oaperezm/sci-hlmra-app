import { Component, OnInit, AfterViewInit } from '@angular/core';
import { MatIconRegistry } from '@angular/material/icon';
import { DomSanitizer } from '@angular/platform-browser';
import { MenuService, UserService, SettingsService } from '../../../../service/service.index';

// COMPONENT SERVICES
import { AdminHeaderService } from '../admin-header/admin-header.service';

// SERVICES
import { StorageService } from '../../../../service/service.index';
import { AdminNavService } from './admin-nav.service';
import swal from 'sweetalert2';
import { AngularFireMessaging } from '@angular/fire/messaging';
import { JoyrideService } from 'ngx-joyride';


@Component({
  selector: 'app-admin-nav',
  templateUrl: './admin-nav.component.html',
  styleUrls: ['./admin-nav.component.scss']
})
export class AdminNavComponent implements OnInit, AfterViewInit {

  menu: any = [];
  profileImage: string;
  working = false;


  constructor(
    public _srvMenu: MenuService,
    public _srvHeader: AdminHeaderService,
    public _srvStorage: StorageService,
    public _srvAdminNav: AdminNavService,
    private afMessaging: AngularFireMessaging,
    public _srvUser: UserService,
    public _srveSettings: SettingsService,
    private joyride: JoyrideService,
    private matIconRegistry: MatIconRegistry,
    private domSanitizer: DomSanitizer) {

      const profileImg = _srvStorage.profileImage !== null
                          ? _srvStorage.profileImage
                          : 'assets/img/profileimg/profile-placeholder.png';
      this.profileImage = profileImg;
      this.menu = _srvMenu.getMenu();
      this._srveSettings.aplicarTema(JSON.parse(this._srvStorage.nodes));


      this.matIconRegistry.addSvgIcon(
        'whatsapp',
        this.domSanitizer.bypassSecurityTrustResourceUrl('../../../../../assets/fonts/whatsapp.svg')
      );

    }

    ngOnInit() {
      this._srvAdminNav.setUrlBackButton([]);
      if ( this._srvStorage.showTour === 0){
        this._srvStorage.showTour = 1;
        this.joyride.startTour(
            { steps: ['firstSteps1'],
              customTexts: {
                next: '>>',
                prev: '<<',
                done: 'Cerrar'
              }
            }
          )
      }
      // this.joyride.startTour(
      //   { steps: ['firstSteps1']}
      // )
      //   console.log('hola menu');

    }

    ngAfterViewInit() {
      setTimeout(() => {
        this._srvHeader.setNotifications();
      });

    }

    getNotifications() {
      this._srvHeader.setNotifications();
    }

    public onCloseMenu() {
      this._srvAdminNav.openedMenu = !this._srvAdminNav.openedMenu;
    }


    sharedReferredCode(social: string) {
      const codigo = 'adjakdi83848190';
      const endMessage = ' lo puedes utilizar para registrarte en la plataforma https://hlmra.com/  y obtener $50 de bonificación.';
      const bodyMessage = encodeURI('Mi código referido es: ' + codigo + endMessage);
      let url = '';

      if (social === 'whatsapp') {
        url = 'https://api.whatsapp.com/send?text=' + bodyMessage;
        
      } else if (social === 'mail') {
        const subject = encodeURI('Codigo referido de la plataforma https://hlmra.com/');
        url = `mailto:?subject=${subject}&body=${bodyMessage}`;
      }

      window.open(url, '_blank');

    }
}
