import { Component, OnInit } from '@angular/core';

// Servicios
import { NavService } from '../../../../service/service.index';
@Component({
  selector: 'app-top-nav',
  templateUrl: './top-nav.component.html',
  styleUrls: ['./top-nav.component.scss']
})
export class TopNavComponent implements OnInit {

  constructor(  public navService: NavService ) { }

  ngOnInit() {
  }

}
