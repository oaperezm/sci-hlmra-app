import { Component, OnInit } from '@angular/core';
import { Appsettings } from '../../../../configuration/appsettings';

// SERVICES
import { StorageService, UserService, SettingsService } from '../../../../service/service.index';

// COMPONENT SERVICES
import { AdminHeaderService } from './admin-header.service';
import { AdminNavService } from '../admin-nav/admin-nav.service';

import swal from 'sweetalert2';
import { AngularFireMessaging } from '@angular/fire/messaging';

@Component({
  selector: 'app-admin-header',
  templateUrl: './admin-header.component.html',
  styleUrls: ['./admin-header.component.scss']
})
export class AdminHeaderComponent implements OnInit {

  nameUser: string;
  profileImage: string;
  email: string;
  working: boolean;

  constructor(
    public _srvStorage: StorageService,
    private _srvUser: UserService,
    public _srvAdminNav: AdminNavService,
    private afMessaging: AngularFireMessaging,
    public _srvHeaderNav: AdminHeaderService,
    public _srveSettings: SettingsService,
  ) {
    const profileImg =
    _srvStorage.profileImage != null
      ? _srvStorage.profileImage
      : 'assets/img/profileimg/profile-placeholder.png';

    this.profileImage = profileImg;
    this.email = _srvStorage.email;
    this._srvAdminNav.notifications = [];

    if (this._srvStorage.loggedIn) {
      this.afMessaging.requestToken.subscribe(
        token => {
          if ( this._srvStorage.tokenMessage !== token ) {
            this._srvUser.updateDevice(token).subscribe( res => {
              this._srvStorage.tokenMessage = token;
            });
          }
        },
        error => {
        }
      );

    this.afMessaging.messages.subscribe(message => {


      const messageData = message['data'];
        let notification;
        const optionsNotification = {
          body: `${message['notification'].title} ${messageData['gcm.notification.text']}`,
        };

      if (!('Notification' in window)) {
          console.log( 'This browser does not support desktop notification' );
        } else if ( Notification.prototype.permission === 'granted' ) {
          notification = new Notification( Appsettings.APP_NAME, optionsNotification);
          notification.onclick = function() {
            this.close();
            };
        } else if ( Notification.prototype.permission !== 'denied') {
          Notification.requestPermission(function (permission) {
            if (permission === 'granted') {
                notification = new Notification( Appsettings.APP_NAME , optionsNotification);
              notification.onclick = function() {
                this.close();
              };
            }
          });
        }
      });
    }
}

updateImageProfile() {
  this.profileImage = this._srvStorage.profileImage;
}

ngOnInit() {
    this.nameUser = this._srvStorage.name;
    this._srvAdminNav.setNotifications();
}

// Permite cerrar sesión del sistema y eliminar los daltos locales
signOut() {

  swal({
    title: 'Cerrar sesión',
    text: '¿Realmente deseas salir de la aplicación?',
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Cerrar ahora',
    cancelButtonText: 'Cancelar'
  }).then(result => {
    if (result.value) {
      this.working = true;
      this._srvUser.logOut().subscribe(
        res => {
          if ( this._srvStorage.tokenMessage) {
              this.afMessaging.deleteToken(this._srvStorage.tokenMessage);
          }
          this._srvStorage.logoutRedirect();
          this._srveSettings.applyDefaultTheme();
        },
        error => {
          this._srvStorage.logoutRedirect();
          this._srveSettings.applyDefaultTheme();
        }
      );
    }
  });
}

onReadMessages( id: number ) {
    this._srvUser.readNotification( id ).subscribe( res => {
      if ( res.success ) {
        this._srvStorage.notifications = JSON.stringify( this._srvAdminNav.notifications.filter( x => x.id !== id) );
        this.getNotifications();
      }
     });
}

  getNotifications() {
    this._srvAdminNav.setNotifications();
  }

}
