export class GeneralUtilies {

  /// Obtener una imagen en un arreglo de bytes
  public static toDataURL(url, callback) {
    const xhr = new XMLHttpRequest();
    xhr.onload = function() {

      const reader = new FileReader();
      reader.onloadend = function() {
        callback(reader.result);
      };

      reader.readAsDataURL(xhr.response);
    };
    xhr.open('GET', url);
    xhr.responseType = 'blob';
    xhr.send();
  }

  public static getExtensionToUrl( url: string ): string {

    const filenameString = url.substring(url.lastIndexOf('/') + 1 );
    const fileName = filenameString.split('?')[0];
    const extension = fileName.split('.')[1].replace('%22', '');

    return extension;
  }

}
