import { Directive, HostListener } from '@angular/core';

@Directive({
    selector: '[appInputNumber]'
})
export class InputNumberDirective {
    numbers: string[] = [ '1', '2', '3', '4', '5', '6', '7', '8', '9', '0'];

    constructor() { }
  
    @HostListener('keypress', ['$event']) validateNumberOfkeyPress(event: KeyboardEvent) {
  
      if(!!this.numbers.find((number) => number == event.key) )
        return event;           
      else
        event.preventDefault();
    }
}