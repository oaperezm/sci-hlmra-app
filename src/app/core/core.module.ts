import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule} from '@angular/forms';

import { TreeviewModule } from 'ngx-treeview';

import { CommonModule } from '@angular/common';
import { AngularEditorModule } from '@kolkov/angular-editor';
import { HttpClientModule} from '@angular/common/http';
import { InputNumberDirective } from '../core/directives/input-number-directive';

// MATERIALIZE
import { 
  MatTabsModule,
  MatInputModule,
  MatDatepickerModule,
  MatRadioModule,
  MatCardModule,
  MatSelectModule,
  MatDialogModule,
  MatIconModule,
  MatButtonModule,
  MatCheckboxModule,
  MatTableModule,
  MatMenuModule,
  MatPaginatorModule,
  MatNativeDateModule,
  MatSlideToggleModule,
  MatToolbarModule } from '@angular/material';

// PIPES
import { GenerateIdElementPipe } from './pipes/generate-id-element.pipe';
import { ActivoBooleanPipe } from './pipes/activo-boolean.pipe';
import { StatusInvitationPipe } from './pipes/status-invitation.pipe';
import { TruncatePipe } from './pipes/truncate.pipe';
import { IconFilePipe } from './pipes/icon-file.pipe';
import { FileTypePipe } from './pipes/file-type.pipe';
import { GetTimeSongPipe } from './pipes/get-time-song.pipe';
import { ShortNamePipe } from './pipes/short-name.pipe';
import { TypeFilePipe } from './pipes/type-file.pipe';
import { FileIconShowPipe } from './pipes/file-icon-show.pipe';
import { CalificacionFinalPipe } from './pipes/finalScore-text.pipe';

// Componentes
import { LoadingComponent } from './loading/loading.component';
import { DropdownTreeviewSelectComponent } from './ngx-dropdown-treeview-select/dropdown-treeview-select.component';
import { PaginationComponent } from './pagination/pagination.component';
import { SafePipe } from './pipes/safe.pipe';



@NgModule({
  declarations: [
    LoadingComponent,
    LoadingComponent,
    TypeFilePipe,
    FileIconShowPipe,
    TruncatePipe,
    GenerateIdElementPipe,
    ActivoBooleanPipe,
    StatusInvitationPipe,
    IconFilePipe,
    FileTypePipe,
    GetTimeSongPipe,
    ShortNamePipe,
    DropdownTreeviewSelectComponent,
    PaginationComponent,
    SafePipe,
    CalificacionFinalPipe,
    InputNumberDirective
  ],
  exports: [
    LoadingComponent,
    HttpClientModule,
    TypeFilePipe,
    FileIconShowPipe,
    TruncatePipe,
    GenerateIdElementPipe,
    ActivoBooleanPipe,
    StatusInvitationPipe,
    IconFilePipe,
    FileTypePipe,
    GetTimeSongPipe,
    ShortNamePipe,
    DropdownTreeviewSelectComponent,
    AngularEditorModule,
    PaginationComponent,
    SafePipe,
    CalificacionFinalPipe,
    InputNumberDirective
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    TreeviewModule.forRoot(),
    HttpClientModule,
    MatSelectModule,
    MatTabsModule,
    MatInputModule,
    MatDatepickerModule,
    MatRadioModule,
    MatCardModule,
    MatSelectModule,
    MatDialogModule,
    MatIconModule,
    MatButtonModule,
    MatCheckboxModule,
    MatTableModule,
    MatMenuModule,
    MatPaginatorModule,
    MatNativeDateModule,
    MatSlideToggleModule,
    AngularEditorModule
  ]
})
export class CoreModule {}
