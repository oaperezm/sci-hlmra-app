import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'typeFile'
})
export class TypeFilePipe implements PipeTransform {

  transform(value: any, args?: any): any {
    let icon: string;
    switch (value) {
      case 5:
          icon = 'fa fa-file-video';
        break;

      case 6:
          icon = 'fa fa-file-audio';
        break;
      case 7:
          icon = 'fa fa-file-pdf';
        break;

      case 11:
          icon = 'fa fa-file-pdf';
        break;
      case 16:
          icon = 'fas fa-book';
        break;
      case 8:
          icon = 'fa fa-cube';
        break;

      case 9:
          icon = 'fas fa-book-reader';
        break;
      default:
      icon = 'fa fa-file';
        break;
    }
    return icon;
  }

}
