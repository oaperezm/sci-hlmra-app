import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'CalificacionFinalPipe'
})

export class CalificacionFinalPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    let status: string;
    switch (value) {
      case '100':
        status = 'Calificación final';
        break;
      case 'Final':
        status = 'Extra';
        break;
      default:
        status = value;
        break;
    }
    return status;
  }


}
