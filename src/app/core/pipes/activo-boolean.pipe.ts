import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'activoBoolean' })
export class ActivoBooleanPipe implements PipeTransform {
    transform(value: boolean): string {
        return  (value) ? 'Activo' : 'Inactivo';
    }
}
