import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'fileIconShow'
})
export class FileIconShowPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    let icon: string;
    switch (value) {
      case 5:
          icon = 'play_arrow';
        break;
      case 6:
          icon = 'play_arrow';
        break;
      case 7:
          icon = 'pageview';
        break;
      default:
        icon = 'visibility';
        break;
    }
    return icon;
  }

}
