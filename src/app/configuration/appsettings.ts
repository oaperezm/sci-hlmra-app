import { environment } from '../../environments/environment.release';

export class Appsettings {
  public static VERSION = 'v0';
  public static SYSTEM_NAME = environment.nameApp;
  public static APP_NAME = environment.nameApp;
  public static API_ENDPOINT = environment.apiUrl;
  public static API_ENDPOINT_FULL = environment.apiUrl + '/api';
  public static PRIVACY_ANNOUNCEMENT = '/assets/docs/POLÍTICA_DE_PRIVACIDADLAROUSSEYPATRIA2019.pdf';
  public static VIMEO_TOKEN = '9564efdf10605005a6876dc2612f1088';
  public static SALI_STRUCTURE_ID = 2;
  public static DEFAULT_SIZE_PAGE = '5';
  public static PLAN_LECTOR_NODE_ID = 500;
  public static PLAN_LECTOR_NODE_ID_STRUCUTURE = 975;
  public static SYSTEM_ID = 2;
  public static ROL_ID = 73;
  public static FIREBASE_CONFIG = environment.firebase;
  public static NODE_ID = 145;
}
